<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarCategory extends Model
{
   	protected $table ="car_categories";
    protected $guarded = [];
    public function car_category_name()
    {
        return $this->hasOne('App\Category','id','category_id');
    }
}
