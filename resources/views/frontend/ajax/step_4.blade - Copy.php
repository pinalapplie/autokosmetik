@php $temp_array = [];@endphp
@foreach ($service_category as $key => $value) 
    @if(!in_array( $value->service_category_name->id, $temp_array))
        <div class="h3">{{ucfirst($value->service_category_name->name)}}</div>
        @php $temp_array[] = $value->service_category_name->id; @endphp
    @endif    
    <div class="form-group">
        <input type="checkbox" name="step_5" class="form-control checkbox" value="{{$value->id}}" id="step_5_{{$value->id}}" s-id="{{$value->parent_id}}" v-id="{{ucfirst($value->name)}}">
        <label role="button" for="step_5_{{$value->id}}">{{ucfirst($value->name)}}</label>
    </div>
@endforeach    
