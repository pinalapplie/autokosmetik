@php $id = encrypt($id); @endphp 
<div class="custom-control custom-switch">
    <input type="checkbox" class="custom-control-input switch_checkbox_btn" id="customSwitch{{ $id }}"
         entry-id="{{ $id }}" {{ ($is_active) ? 'checked': '' }}>
    <label class="custom-control-label" for="customSwitch{{ $id }}"></label>
</div>
