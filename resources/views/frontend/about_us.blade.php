@section('title', 'ÜBER UNS')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<!-- Sidebar  -->
		<div id="main-content" class="active">
			<!-- Section Breadcrumb 1 -->
		    <div id="section-breadcrumb1" class="bg1 bg-dark-autokosmetic">
		    	<div class="container">
		    		<div class="row">
		    			<div class="content col-12">
		    				<div class="h1">ÜBER UNS</div>
		    				<ul>
		    					<li><a href="{{ route('index')}}">ZUHAUSE</a></li>
		    					<li class="current text-light">ÜBER UNS</li>
		    				</ul>
		    			</div>
		    		</div>
		    	</div>
		    </div>
			<div id="section-allstarted">
				<div class="container">   
					<div class="row">
						<div class="left col-sm-12 col-md-6" data-aos="fade-up">
							<h1>Autokosmetik.li - mehr als Autoaufbereitung</h1>
							<p>Die Leidenschaft für schöne Fahrzeuge steht im Zentrum unserer Arbeit. Und zwar schon seit 2008 als Pascal Nitzlnader die Firma gründete und damit begann Kunden in Liechtenstein und der angrenzenden Schweiz Dienstleistungen im Bereich der Autoaufbereitung anzubieten. Mittlerweile befindet sich der Standort in den Geschäftsräumlichkeiten der Max Heidegger AG in Triesen.</p>
							<p>Damals wie heute bürgen wir für höchste Qualität, dies spiegelt sich auch in unserem Firmen-Leitspruch:</p>
							<blockquote class="font-weight-bold">„Kleinigkeiten sind es, die Perfektion ausmachen, aber Perfektion ist alles andere als eine Kleinigkeit“ - <cite>Sir Frederick Henry Royce </cite></blockquote>
							<p>Während der Fokus zu Beginn darauf lag, Fahrzeuge optisch und hygienisch in den Ursprungszustand zu versetzen, steht heute das erweiterte Konzept «Pflege-Schutz-Design» im Scheinwerferlicht.</p>
							<p>Zu den bereits bestehenden Dienstleistungen der Fahrzeugaufbereitung und Autoveredelung wird verstärkt der Bereich des nachhaltigen Lackschutzes im eigenen Lackschutzzentrum angeboten. Unsere langjährigen Erfahrungen mit Keramikbeschichtungen und selbstheilenden Lackschutzfolien haben gezeigt, dass diese einen ausgezeichneten Schutz gegen Steinschlag, Gebrauchsspuren, Kratzer, Vogelkot, Waschanlagen- und Umwelteinflüsse wie UV-Strahlung bieten.</p>
							<p>Nebst unserem neuen Konzept bieten wir bei der Autokosmetik.li auch Foliendesign für die individuelle Optik, Beschriftungen sowie Scheibentönungen zum Schutz vor schädlicher UV-Strahlung an.</p>
							<p>Sie sehen: Wir bieten umfangreiche Dienstleistungen rund um Ihr Fahrzeug.</p>
							<p>Autokosmetik.li – mehr als Autoaufbereitung.</p>
						</div>
						<div class="right col-sm-12 col-md-6">
							<img class="img-fluid w-100 my-3 my-lg-0" src="{{ asset('assets/frontend/images/about-1.png') }}" alt="autokosmetik">
						</div>
						
						<div class="strategic-left col-sm-12 col-md-12 col-lg-7" data-aos="fade-up" data-aos-delay="200">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/about-us-1.jpg') }}" alt="autokosmetik">
						</div>
						<div class="strategic-right col-sm-12 col-md-12 col-lg-5" >
							<div class="item style1" data-aos="fade-up" data-aos-delay="400">
								<i class="flaticon-network"></i>
								<div class="h3">Strategische Vorstellung</div>
								<p>Kleinigkeiten sind es, die Perfektion ausmachen, aber Perfektion ist alles andere als eine Kleinigkeit</p>
							</div>
							<div class="item style2" data-aos="fade-up" data-aos-delay="800">
								<i class="flaticon-diamond"></i>
								<div class="h3">Mission</div>
								<p>Die Liebe zu perfekten, schönen Autos, ein sehr gutes und geübtes Auge, eine gute körperliche Verfassung und volle Konzentration bei der Arbeit runden das Bild der Mitarbeiter ab.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="section-testimonial1" class="inner-banner-wrap">				
				<img src="{{ asset('assets/frontend/images/index-bg.jpg') }}" alt="" class="inner-page-banner">
								
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="title1 ez-animate col-12" data-animation="fadeInUp">
								<div class="h6">Was Kunden über uns sagen</div>
								<div class="h2">Kundenfeedback</div>
								<i class="flaticon-download"></i>
							</div>
							<div class="owl-carousel owl-theme">
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial1.png') }}"alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum</h6>
										<p>Lorem ipsum</p>
									</div>
								</div>
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial2.png') }}" alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum</h6>
										<p>Lorem ipsum</p>
									</div>
								</div>
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial3.png') }}" alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum</h6>
										<p>Lorem ipsum</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="section-ourteam1">
				<div class="container">
					<div class="row ez-animate" data-animation="fadeInUp">
						<div class="title1 col-12">
							<div class="h2 text-dark"><span>Treffen Sie den</span> Eigentümer</div>
							<i class="flaticon-download"></i>
						</div>
						<div class="item ol-sm-12 col-md-6 col-lg-6" data-aos="fade-up" data-aos-delay="200">
							<a href="#">
								<div class="content">
									<h3>Rudi Nitzlnader</h3>
									<p>Geschäftsführender Inhaber und administrativer Leiter</p>
								</div>
								<div class="image">
									<img class="img-fluid" src="{{ asset('assets/frontend/images/img3.png') }}" alt="autokosmetik">
								</div>
							</a>
						</div>
						<div class="item ol-sm-12 col-md-6 col-lg-6" data-aos="fade-up" data-aos-delay="400">
							<a href="#">
								<div class="content">
									<h3>Pascal Nitzlnader</h3>
									<p>Geschäftsführender Inhaber und technischer Leiter</p>
								</div>
								<div class="image">
									<img class="img-fluid" src="{{ asset('assets/frontend/images/img4.png') }}" alt="autokosmetik">
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div id="section-miscellaneous1">
				<div class="container">
					<div class="row">
						<div class="item col-6 col-sm-6 col-md-3">
							<i class="flaticon-heart"></i>
							<h3 class="counter-value count">288.702</h3>
							<p>Kunden</p>
						</div>
						<div class="item col-6 col-sm-6 col-md-3">
							<i class="flaticon-stopwatch"></i>
							<h3 class="counter-value count">140.545</h3>
							<p>Arbeiten abgeschlossen</p>
						</div>
						<div class="item col-6 col-sm-6 col-md-3">
							<i class="flaticon-internet"></i>
							<h3 class="counter-value count">100.204</h3>
							<p>Projekte des Designs</p>
						</div>
						<div class="item col-6 col-sm-6 col-md-3">
							<i class="flaticon-id-card"></i>
							<h3 class="counter-value count">22.368</h3>
							<p>Unternehmen</p>
						</div>
					</div>
				</div>
			</div>
			<div id="section-cta3">
				<div class="container">
					<div class="row ez-animate">
						<div class="title1 col-12" data-aos="fade-up" data-aos-delay="200">
							<h2><span>Immer auf der Suche nach mehr</span> Einzigartige Talente</h2>
							<i class="flaticon-download"></i>
						</div>
						<div class="cta-content col-12" data-aos="fade-up" data-aos-delay="400">
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores quod cumque veniam rerum neque nulla aspernatur esse minus facere, culpa excepturi cum pariatur ducimus quae cupiditate nostrum. Nesciunt, non excepturi. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquam sunt provident</p>
							<a href="#" class="btn-4">Siehe Stellenangebote</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection    
	