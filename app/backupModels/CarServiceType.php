<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarServiceType extends Model
{
    protected $table ="car_service_types";
    protected $guarded = [];
    public function car_service_type_name()
    {
        return $this->hasOne('App\Category','id','category_id');
    }
}
