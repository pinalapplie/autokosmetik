@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>Car Service Type Mgmt</h4>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('carservicetype.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add car service type</a>
                </div>
            </div>
            <br>
            <div class="col-md-12  mt-3">
                @if (session('message'))
                <div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('message') }}
                </div>
                @endif
               <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($car_service_types as $key => $car_service_type)
                                    <tr data-entry-id="{{ $car_service_type->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $car_service_type->title ?? ''}}</td>
                                        <td>{{ ($car_service_type->car_service_type_name) ? $car_service_type->car_service_type_name->title: '-'}}</td>
                                        <td>
                                            @include('partials.switch', ['id'=>
                                            $car_service_type->id,'is_active'=>$car_service_type->is_active])
                                        </td>
                                        <td>@include('partials.actions', ['id' => $car_service_type->id])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
