<?php

namespace App\Http\Controllers;

use App\Service;
use App\ServicePortfolio;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;
use App\CarType;
use App\CarServiceType;
use App\CarConditionType;
use App\ServicePrice;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::with(['service_category_name','service_category_name.category_name','service_category_name.category_name.category_name'])->latest()->get();
        //dd($services);
        return view('service.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $parent_categorys = Category::get();
         return view('service.create',compact('parent_categorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = new Service;
        $service->name = $request->title;
        $service->short_description = $request->short_description;
        $service->description = $request->description;
        $service->category_id = $request->category;
        $service->type = $request->type;
        
        if ($request->file('image')) {
            $storage_path = 'public/images';
            $service->image = Storage::disk('local')->put($storage_path,$request->file('image'));
        }

        $service->save();

        if ($request->file('portfolio')) {
            $service_portfolio = [];
            $storage_path = 'public/portfolio';
            foreach ($request->file('portfolio') as $image) {
                //$file_path = commonUploadImage($storage_path, $image);
                $file_path = Storage::disk('local')->put($storage_path,$image);
                $service_portfolio =  new ServicePortfolio();
                $service_portfolio->image = $file_path;
                $service_portfolio->car_service_types_id = $service->id;
                $service_portfolio->save();

                //$service_portfolio[] = new ServicePortfolio(['image' => $file_path]);
            }
            //$service->portfolioImage()->saveMany($service_portfolio);
        }

        return redirect()->route('services.index')->with('message', "Service added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $parent_categorys = Category::get();
        $cartypes = CarType::where('is_active',1)->get();
        $carservicetypes = CarServiceType::where('category_id',$service->category_id)->where('is_active',1)->get();
        $carconditiontype = CarConditionType::where('is_active',1)->get();
        
        $service = Service::with(['portfolioImage','service_price' =>function($query){ $query->orderBy('car_service_type_id');}])->where('id',$service->id)->first();
        //dd($service);
        //$query = DB::table('services as s')->JOIN('service_price as sp','sp.service_id','=','s.id')->get();
        //echo "<pre>"; print_r($service->service_price); exit();
        return view('service.edit', compact('service','parent_categorys','cartypes','carservicetypes','carconditiontype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {   
        //ServicePrice::where('service_id',$service->id)->delete();
        $input = $request->all();
        $cartypes = CarType::where('is_active',1)->get();
        foreach ($input['service_type'] as $key => $value) {
            $radio_value = $input['condition_type_'.$value];
            foreach($cartypes as $value1){
                $input_value = $input['car_type_'.$value.'_'.$value1->id];
                
                $car_service_type_id = $value;
                $car_condition_type_id = $input['condition_type_'.$value];
                $price=$input['car_type_'.$value.'_'.$value1->id];
                $car_type_id = $value1->id;


                $where_arr = ['service_id' => $service->id,'car_service_type_id'=> $car_service_type_id,'car_type_id' => $car_type_id];
                $insert_arr = ['price' => $price,'car_condition_type_id' => $car_condition_type_id];
                ServicePrice::updateOrCreate($where_arr, $insert_arr);

            }
        }
        $service->name = $request->title;
        $service->description = $request->description;
        $service->short_description = $request->short_description;
        if ($request->file('image')) {
            $storage_path = 'public/images';
            $service->image = Storage::disk('local')->put($storage_path,$request->file('image'));
        }

        $service->save();

        if ($request->file('portfolio')) {
            $service_portfolio = [];
            $storage_path = 'public/portfolio';
            foreach ($request->file('portfolio') as $image) {
                $file_path = Storage::disk('local')->put($storage_path,$image);
                $service_portfolio =  new ServicePortfolio();
                $service_portfolio->image = $file_path;
                $service_portfolio->car_service_types_id = $service->id;
                $service_portfolio->save();
            }
        }
        return redirect()->route('services.index')->with('message', "Service update successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $service = Service::find(request('ids'));
        $service->delete();
        return response()->noContent();
    }
    public function switchUpdate(Request $request)
    {
        $service = Service::find($request->ids);
        if (empty($service->is_active)) {
            $service->is_active = 1;
        } else {
            $service->is_active = 0;
        }
        $service->save();
        return response()->noContent();
    }
}
