@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.create') }} {{ trans('cruds.servicemangement.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('servicemangement.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('servicemangement.store') }}" method="POST" enctype="multipart/form-data" id="add_form">
                    @csrf
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.servicemangement.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($servicemanagement->name) ? $servicemanagement->name : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">{{ trans('cruds.servicemangement.fields.description') }}</label>
                        <textarea name="description" class="form-control" id="description" rows="5">
                            {!! old('description', isset($servicemanagement->description) ? $servicemanagement->description : '') !!}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="category">{{ trans('cruds.servicemangement.fields.subcategory') }}<span class="required_class">*</span></label>
                        <select class="form-control" name="service_category_id" id="category">
                            <option value="">{{ trans('cruds.servicemangement.fields.select_subcategory') }}</option>  
                            @foreach($service_category as $value)
                                <?php 
                                    $final_category_name='';
                                    if($value->parent_id == 0){
                                        $final_category_name = $value->name;
                                    }else {
                                        if($value->service_category_name->parent_id == 0){
                                            $category_name1=$value->service_category_name->name;
                                            $final_category_name = $value->name.'->'.$category_name1;    
                                        }else{
                                            $category_name1=$value->service_category_name->name;
                                            $category_name2=$value->service_category_name->service_category_name->name;
                                            $final_category_name = $value->name.'->'.$category_name1.'->'.$category_name2;
                                        }
                                    }
                                ?>
                                <option value="{{$value->id}}">{{ $final_category_name }}</option>  
                            @endforeach
                        </select>
                    </div>
                    <!--<div class="form-group">
                        <label for="category">{{ trans('cruds.servicemangement.fields.type') }}<span class="required_class">*</span></label>
                        <select class="form-control" name="type" id="select_type">
                            <option value="">{{ trans('cruds.servicemangement.fields.select_type') }}</option>  
                            <option value="specification">{{ trans('cruds.servicemangement.fields.specification') }}</option>  
                            <option value="pricechart" >{{ trans('cruds.servicemangement.fields.pricechart') }}</option>  
                        </select>
                    </div>
                    <div class="form-group price_text_box" style="display: none;">
                        <label for="">{{ trans('cruds.servicemangement.fields.price') }}</label>
                        <input type="number" class="form-control" id="name" name="price" value="{{ old('price', isset($servicemanagement->price) ? $servicemanagement->price : '') }}">
                    </div>-->
                    <div class="form-group">
                        @foreach($vehicle_category as $key => $vehicleCategory)
                            <label>{{ $vehicleCategory->name }} : </label>
                            <div class="row mb-3">                                
                                @foreach($vehicle_conditions as $key => $value)
                                <div class="col-6">
                                <label>{{ $value->name }}</label>
                                <input class="form-control" type="number" name="{{ strtolower($value->name) }}_{{ $vehicleCategory->id }}_{{ $value->id }}" value="" placeholder="{{ $value->name }}" required="" />
                                </div>    
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                    <button type="submit" class="btn btn-primary">{{ trans('global.submit') }}</button>
                </form>
            </div>
        </div>  
    </div>
</div>  
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        CKEDITOR.replace('description');
        //CKEDITOR.replace('short_description');
       $('#select_type').on('change', function(e) {
            e.preventDefault();
            var selectedType= $(this).children("option:selected").val();
            var selectedattrValue= $(this).children("option:selected").attr('value');
            if(selectedType == "" || selectedattrValue == "" || selectedattrValue == "pricechart"){
                $('.price_text_box').hide();
            }else{
                $('.price_text_box').show();
            }
            
        });
        $("#add_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
                // description: {
                //     required: function(textarea) {
                //         CKEDITOR.instances[textarea.id].updateElement();
                //         var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                //         return editorcontent.length === 0;
                //     }
                // },
                service_category_id: {
                    required: true
                },
                type: {
                    required: true
                }

            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                },
                description: {
                    required: "@lang('validation.required',['attribute'=>'Description'])",
                },
                service_category_id: {
                    required: "@lang('validation.required',['attribute'=>'Category'])",
                },
                type: {
                    required: "@lang('validation.required',['attribute'=>'Type'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection