<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table ="services";
    protected $guarded = [];

    public function portfolioImage()
    {
        return $this->hasMany('App\ServicePortfolio','car_service_types_id','id');
    }
    public function service_category_name()
    {
        return $this->hasOne('App\Category','id','category_id');
    }
    public function service_price()
    {
        return $this->hasMany('App\ServicePrice','service_id','id');
    }

}
