<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicePortfolio extends Model
{
    protected $table ="service_portfolios";
    protected $guarded = [];
}
