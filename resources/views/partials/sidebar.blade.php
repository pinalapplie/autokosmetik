    <aside class="vertical-menu">

        <div data-simplebar class="h-100">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <!-- Admin show menu start -->


                <ul class="metismenu list-unstyled" id="side-menu">
                    <li class="menu-title">{{ trans('cruds.menu') }}</li>
                    <li  class="{{ request()->is('admin/vehicletypes') || request()->is('admin/vehicletypes/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('vehicletypes.index') }}" class="waves-effect {{ request()->is('vehicletypes') || request()->is('admin/vehicletypes/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>{{ trans('cruds.vehicletypes.title') }}</span>
                        </a>
                    </li>
                    <li  class="{{ request()->is('admin/vehiclecategories') || request()->is('admin/vehiclecategories/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('vehiclecategories.index') }}" class="waves-effect {{ request()->is('vehiclecategories') || request()->is('admin/vehiclecategories/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>{{ trans('cruds.vehiclecategories.title') }}</span>
                        </a>
                    </li>
                    <li  class="{{ request()->is('admin/vehicleconditions') || request()->is('admin/vehicleconditions/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('vehicleconditions.index') }}" class="waves-effect {{ request()->is('vehicleconditions') || request()->is('admin/vehicleconditions/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>{{ trans('cruds.vehicleconditions.title') }}</span>
                        </a>
                    </li>
                    <li  class="{{ request()->is('admin/vehiclebrands') || request()->is('admin/vehiclebrands/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('vehiclebrands.index') }}" class="waves-effect {{ request()->is('vehiclebrands') || request()->is('admin/vehiclebrands/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>{{ trans('cruds.vehiclebrands.title') }}</span>
                        </a>
                    </li>
                    <li  class="{{ request()->is('admin/drivevehicles') || request()->is('admin/drivevehicles/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('drivevehicles.index') }}" class="waves-effect {{ request()->is('drivevehicles') || request()->is('admin/drivevehicles/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>{{ trans('cruds.drivevehicles.title') }}</span>
                        </a>
                    </li>
                    <li  class="{{ request()->is('admin/servicecategories') || request()->is('admin/servicecategories/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('servicecategories.index') }}" class="waves-effect {{ request()->is('servicecategories') || request()->is('admin/servicecategories/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>{{ trans('cruds.servicecategories.title') }}</span>
                        </a>
                    </li>
                    <li  class="{{ request()->is('admin/servicesubcategories') || request()->is('admin/servicesubcategories/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('servicesubcategories.index') }}" class="waves-effect {{ request()->is('servicesubcategories') || request()->is('admin/servicesubcategories/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>{{ trans('cruds.servicesubcategories.title') }}</span>
                        </a>
                    </li>
                    <!--<li  class="{{ request()->is('admin/vehicleservices') || request()->is('admin/vehicleservices/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('vehicleservices.index') }}" class="waves-effect {{ request()->is('vehicleservices') || request()->is('admin/vehicleservices/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>{{ trans('cruds.vehicleservices.title') }}</span>
                        </a>
                    </li>-->
                    <li  class="{{ request()->is('admin/servicemangement') || request()->is('admin/servicemangement/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('servicemangement.index') }}" class="waves-effect {{ request()->is('servicemangement') || request()->is('admin/servicemangement/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>{{ trans('cruds.servicemangement.title') }}</span>
                        </a>
                    </li>
                    <li  class="{{ request()->is('admin/news') || request()->is('admin/news/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('news.index') }}" class="waves-effect {{ request()->is('news') || request()->is('admin/news/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>{{ trans('cruds.news.title') }}</span>
                        </a>
                    </li>
                    <li  class="{{ request()->is('admin/portfolio') || request()->is('admin/portfolio/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('portfolio.index') }}" class="waves-effect {{ request()->is('portfolio') || request()->is('admin/portfolio/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>{{ trans('cruds.portfolio.title') }}</span>
                        </a>
                    </li>       
                    <li  class="{{ request()->is('admin/addonservice') || request()->is('admin/addonservice/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('addonservice.index') }}" class="waves-effect {{ request()->is('addonservice') || request()->is('admin/addonservice/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>{{ trans('cruds.addonservice.title') }}</span>
                        </a>
                    </li>
                    <li  class="{{ request()->is('admin/feedback') || request()->is('admin/feedback/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('feedback.index') }}" class="waves-effect {{ request()->is('feedback') || request()->is('admin/feedback/*') ? 'active' : '' }}">
                            <i class="bx bx-news"></i>
                            <span>{{ trans('cruds.feedback.title') }}</span>
                        </a>
                    </li> 
                </ul>
                <!-- Admin show menu end -->
            </div>
            <!-- Sidebar -->
        </div>
    </aside>
    <!-- Left Sidebar End -->
