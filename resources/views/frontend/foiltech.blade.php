@section('title', 'Services')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<div id="main-content" class="active">
			<div id="section-breadcrumb1" class="inner-banner-wrap">
				<img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
				<div class="container">
					<div class="row">
						<div class="content col-12">
							<h1>FOLIENDESIGN</h1>
							<ul>
								<li><a href="{{ route('index')}}">Zuhause</a></li>
								<li><a href="{{ route('services')}}">Dienstleistungen</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="section-services2" class="section-car-ceramic">
				<div class="container">
					<div class="row">
						<div class="left col-sm-12 col-md-8">
							<p>Besonders Individualisten wissen die vielfältigen Möglichkeiten von Autofolien zu schätzen. Dem Design und der individuellen Gestaltung sind fast keine Grenzen gesetzt. Gerne beraten wir diesbezüglich unsere anspruchsvollen Kunden. Sowohl betreffend die verwendeten Folien als auch in Bezug auf unsere Arbeit können Sie höchste Qualität erwarten. Bei allen Folien gilt eine Garantie von 7 Jahren.</p>
							<ul>
								<li class="circle">Individuelle Optik</li>
								<li class="circle">Designberatung</li>
								<li class="circle">Waschstrassenfest</li>
							</ul>
						</div>
						<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-1.jpg') }}" alt="autokosmetik">
						</div>
					</div>
				</div>
			</div>
			<div class="section-services2 services2-car-bg-2">
				<div class="container">
					<div class="row align-items-center">
						<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-2.jpg') }}" alt="autokosmetik">
						</div>
						<div class="col-sm-12 col-md-7">
							<div class="ceramic-coating-pading">
								<h1>Besonders geeignet für:</h1>
								<p>begrenzte Zeit eine andere Farbe brauchen</p>
								<ul>
									<li class="circle">Individualisten</li>
									<li class="circle">Showfahrzeuge, die besonders auffallen wollen</li>
									<li class="circle">Fahrzeuge, welche für eine </li>
								</ul>
							</div>
						</div>
						<div class="col-md-12 pt-5">
							<p>Für das Foliendesign gibt es keine generelle Preisliste. Rechnen Sie bei einer Vollfolierung für einen Golf mit Minimum CHF 3.000,- bis CHF 3.600,-,  ein SUV wird Minimum CHF 4.000.- bis CHF 5.000 kosten. Je nach speziellen Anforderungen und Features können die Preise auch noch darüber liegen.</p>
							<p>Teilfolierungen kosten entsprechend weniger und beginnen bereits bei wenigen hundert Franken.</p>
							<p>Achtung: Es gibt immer mehr „Möchte-Gern-Folierer“, welche neben der schlechten Verklebung auch dem Lack sehr viel Schaden zufügen können. Auch werden teilweise sehr billige, schlechte Folien verwendet, bei denen keinerlei Garantie gewährt wird.</p>
						</div>
					</div>
				</div>
			</div>
			<div id="section-portfoliodetails1" class="ceramic-coting-images">
				<div class="container">
					<div class="row">
						<div class="related-projects col-12">
							<div class="row">
								<div class="item col-sm-12 col-md-4 my-3">
									<a href="#">
										<div class="img-container w-100">
											<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/foil-3.png') }}" alt="autokosmetik">
										</div>
									</a>
								</div>
								<div class="item col-sm-12 col-md-4 my-3">
									<a href="#">
										<div class="img-container w-100">
											<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/foil-4.png') }}" alt="autokosmetik">
										</div>
									</a>
								</div>
								<div class="item col-sm-12 col-md-4 my-3">
									<a href="#">
										<div class="img-container w-100">
											<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/foil-5.jpg') }}" alt="autokosmetik">
										</div>
									</a>
								</div>
								<div class="item col-sm-12 col-md-4 my-3">
									<a href="#">
										<div class="img-container w-100">
											<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/foil-6.png') }}" alt="autokosmetik">
										</div>
									</a>
								</div>
								<div class="item col-sm-12 col-md-4 my-3">
									<a href="#">
										<div class="img-container w-100">
											<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/foil-7.jpg') }}" alt="autokosmetik">
										</div>
									</a>
								</div>
								<div class="item col-sm-12 col-md-4 my-3">
									<a href="#">
										<div class="img-container w-100">
											<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/foil-8.jpg') }}" alt="autokosmetik">
										</div>
									</a>
								</div>
								<div class="item col-sm-12 col-md-4 my-3">
									<a href="#">
										<div class="img-container w-100">
											<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/foil-9.png') }}" alt="autokosmetik">
										</div>
									</a>
								</div>
								<div class="item col-sm-12 col-md-4 my-3">
									<a href="#">
										<div class="img-container w-100">
											<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/foil-10.jpg') }}" alt="autokosmetik">
										</div>
									</a>
								</div>
								<div class="item col-sm-12 col-md-4 my-3">
									<a href="#">
										<div class="img-container w-100">
											<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/foil-11.jpg') }}" alt="autokosmetik">
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="col-12 text-center pb-5">
							<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection    
	