@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>{{ trans('cruds.vehicleservices.title_singular') }} {{ trans('global.list') }}</h4>
            </div>

            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('vehicleservices.create') }}" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        {{ trans('global.add') }} {{ trans('cruds.vehicleservices.title_singular') }}
                    </a>
                </div>
            </div>
           
            <div class="col-md-12 mt-3">
               <div class="card">
                    <div class="card-body">
                        @include("partials.alert")
                        <div class="table-responsive">
                            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                    <tr>
                                        <th>{{ trans('cruds.vehicleservices.fields.id') }}</th>
                                        <th>{{ trans('cruds.vehicleservices.fields.name') }}</th>
                                        <th>{{ trans('cruds.vehicleservices.fields.service') }}</th>
                                        <th>{{ trans('global.status') }}</th>
                                        <th>{{ trans('global.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($vehicle_service as $key => $value)
                                    <tr data-entry-id="{{ $value->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->name ?? ''}}</td>
                                        <td>{{ ($value->service_management_name)  ? $value->service_management_name->name : '-'}}</td>
                                        <td>
                                            @include('partials.switch', ['id'=>
                                            $value->id,'is_active'=>$value->is_active])
                                        </td>
                                        <td>@include('partials.actions', ['id' => $value->id])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
