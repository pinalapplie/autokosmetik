<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use App\Http\Model\VehicleService;
use App\Http\Model\VehicleConditions;
use Illuminate\Support\Facades\Storage;
use File;

use App\Http\Model\News;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::latest()->get();
        return view('backend.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                $news = News::where('id',$id)->first();
                if (empty($news->is_active)) {
                    $news->is_active = 1;
                } else {
                    $news->is_active = 0;
                }
                    $news->save();
                return response()->json(['success' => 1]);
            }else{
                if(!empty($request->id)){
                    $this->validate($request, [
                        'name' => 'required',
                        'date' => 'required',
                        'description' => 'required'
                    ]);
                
                    $id = Crypt::decrypt($request->id);
                    $news = News::where('id',$id)->first();
                    
                    $path = '';
                    if ($request->hasFile('image')) {                        
                        $image = $request->file('image');
                        //echo "<pre>"; print_r($image); exit();
                        $filename  = time() . '.' . $image->getClientOriginalExtension();
                        Storage::disk('public')->put('news/'.$filename,  File::get($image));
                        $path = 'news/'.$filename; 
                        $news->image = $path;
                    }
                    

                    $news->name = $request->name;
                    $news->description = $request->description;
                    $news->date = date('Y-m-d', strtotime($request->date));
                    
                    $news->save();
   
                    $message = trans('message.update', ['entity' => trans('cruds.news.title_singular')]);
                }else{
                    $this->validate($request, [
                        'name' => 'required',
                        'date' => 'required',
                        'image' => 'required',
                        'description' => 'required'
                    ]);
                    
                    $path = '';
                    if ($request->hasFile('image')) {                        
                        $image = $request->file('image');
                        //echo "<pre>"; print_r($image); exit();
                        $filename  = time() . '.' . $image->getClientOriginalExtension();
                        Storage::disk('public')->put('news/'.$filename,  File::get($image));
                        $path = 'news/'.$filename; 
                    }
                    
                    $news = new News;
                    $news->name = $request->name;
                    $news->description = $request->description;
                    $news->date = date('Y-m-d', strtotime($request->date));
                    $news->image = $path;
                    $news->save();
                    $message = trans('message.create', ['entity' => trans('cruds.news.title_singular')]);
                }
                   return redirect()->route('news.index')->with('success',$message);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
            return redirect()->back()->with('danger', $e->getMessage());    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceManagement  $serviceManagement
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceManagement $servicemanagement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceManagement  $serviceManagement
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $news = News::where('id',$id)->first();
        return view('backend.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceManagement  $serviceManagement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceManagement $serviceManagement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceManagement  $serviceManagement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                News::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }
}
