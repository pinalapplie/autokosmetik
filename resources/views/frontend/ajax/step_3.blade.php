@foreach ($service_category as $key => $scvalue)
<div id="mainService{{$scvalue->id}}" class="all_service" style="display: none;">
    <?php $service_category1 = getSubCategory($scvalue->id); ?>
    @foreach ($service_category1 as $key => $value) 
    <div id="collapse{{ $value->id }}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
    	<div class="">
    		<div class="h5">{{ $value->name }}</div>
            <?php $services = getServices($value->id); ?>
            @foreach($services as $service)
    		<?php $servicePrice = getServicesWithPrice($service->id,$vehicle_condition_id,$vehicle_type_id);  ?>
            <div class="form-group">
    			<input type="checkbox" name="step_4" class="form-control checkbox" value="{{ $service->id }}" id="step_{{ $service->id }}" c-name="{{ $service->name }}" p-id="{{ $value->parent_id }}" price="{{ $servicePrice->price }}" s-id="{{ $value->id }}" s-name="{{ $value->name }}">
    			<label role="button" for="step_{{ $service->id }}" class="align-items-center d-flex ">
    				<span>{{ $service->name }}</span><span class="ml-auto">{{ $servicePrice->price }} CHF</span>
    				<div class="infoicon-i ml-2"  role="button" data-toggle="modal" data-target="#modal_bookappointment_{{ $service->id }}" title="Read more">
    					<img src="{{ asset('assets/frontend/images/info-icon.svg') }}" class="d-block">
    				</div>
    			</label>
    		</div>	
            @endforeach
        </div>
    </div>   
    @endforeach
</div>
@endforeach
