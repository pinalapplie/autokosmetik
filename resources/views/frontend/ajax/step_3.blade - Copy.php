@foreach ($drive_vehicle as $key => $value) 
    <div class="form-group">
        <input type="radio" name="step_4" class="form-control radio" id="step_4_{{$value->id}}" v-id="{{$value->name}}">
        <label role="button" for="step_4_{{$value->id}}">{{ucfirst($value->name)}}</label>
    </div>
@endforeach        