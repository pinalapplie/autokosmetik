@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.create') }} {{ trans('cruds.vehiclebrands.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('vehiclebrands.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('vehiclebrands.store') }}" method="POST" enctype="multipart/form-data" id="add_form">
                    @csrf
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.vehiclebrands.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($vehiclebrands->name) ? $vehiclebrands->name : '') }}">
                    </div>
                    <!-- <div class="form-group">
                        <label for="category">{{ trans('cruds.vehiclebrands.fields.vehicle_category') }}<span class="required_class">*</span></label>
                        <select class="form-control" name="vehicle_categorie_id" id="category">
                            <option value="">{{ trans('cruds.vehiclebrands.fields.select_vehicle_category') }}</option>  
                            @foreach($vehicle_category as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>  
                            @endforeach
                        </select>
                    </div> -->
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.news.fields.image') }}<span class="required_class">*</span></label>
                        <input type="file" class="form-control" id="image" name="image" value="{{ old('image', isset($vehiclebrands->image) ? $vehiclebrands->image : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="vehicle_type">{{ trans('cruds.servicecategories.fields.vehicle_type') }}</label>
                        <!-- <select name="vehicle_type_ids[]"  class="select2 form-control select2-multiple" multiple="multiple" >
                            @foreach($vehicle_type as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>  
                            @endforeach
                        </select> -->
                        <select name="vehicle_type_ids[]"  class="select2 form-control select2-multiple" multiple="multiple" data-placeholder="select vehicle types">
                            <optgroup label="select vehicle types">
                                @foreach($vehicle_type as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>  
                                @endforeach
                            </optgroup>
                        </select>
                    </div>


                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        $(".select2").select2();
        $("#add_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
                "vehicle_type_ids[]": {
                         required: true
                },
                image: {
                    required: true
                },
            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                },
                "vehicle_type_ids[]": {
                    required: "@lang('validation.required',['attribute'=> 'Vehicle Type'])"
                },
                image: {
                    required: "@lang('validation.required',['attribute'=>'Image'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection