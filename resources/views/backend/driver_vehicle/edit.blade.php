@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.update') }} {{ trans('cruds.drivevehicles.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('drivevehicles.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('drivevehicles.store') }}" method="POST" enctype="multipart/form-data" id="add_form">
                    @csrf
                    <input type="hidden" name="id" value="{{encrypt($drivevehicles->id)}}">
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.drivevehicles.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($drivevehicles->name) ? $drivevehicles->name : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="model">{{ trans('cruds.drivevehicles.fields.modal') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="model" name="model" value="{{ old('model', isset($drivevehicles->model) ? $drivevehicles->model : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="category">{{ trans('cruds.drivevehicles.fields.vehicle_category') }}<span class="required_class">*</span></label>
                        <select class="form-control" name="vehicle_categorie_id" >
                            <option value="">{{ trans('cruds.drivevehicles.fields.select_vehicle_category') }}</option>  
                            @foreach($vehicle_category as $value)
                                @if($value->id == $drivevehicles->vehicle_categorie_id)
                                    <option value="{{$value->id}}" selected="">{{$value->name}}</option>  
                                @else
                                    <option value="{{$value->id}}">{{$value->name}}</option>  
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="category">{{ trans('cruds.drivevehicles.fields.vehicle_brand') }}<span class="required_class">*</span></label>
                        <select class="form-control" name="vehicle_brand_id" id="vehicle_brand_id">
                            <option value="">{{ trans('cruds.drivevehicles.fields.select_vehicle_brand') }}</option>  
                            @foreach($vehicle_brands as $value)
                                @if($value->id == $drivevehicles->vehicle_brand_id)
                                    <option value="{{$value->id}}" data-value-temp="{{$value->vehicle_type_ids}}" selected='' >{{$value->name}}</option>  
                                @else
                                    <option value="{{$value->id}}" data-value-temp="{{$value->vehicle_type_ids}}">{{$value->name}}</option>  
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group car_window_selection" style="display: none">
                        <label for="model">{{ trans('cruds.drivevehicles.fields.number_of_window') }}</label><br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="number_of_window" id="bike_3" value="3" checked="">
                            <label class="form-check-label" for="bike_3">3</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="number_of_window" id="bike_5" value="5">
                            <label class="form-check-label" for="bike_5">5</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="number_of_window" id="bike_7" value="7">
                            <label class="form-check-label" for="bike_7">7</label>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="price">{{ trans('cruds.drivevehicles.fields.price') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="price" name="price" value="{{ old('price', isset($drivevehicles->price) ? $drivevehicles->price : '') }}">
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
       
        $("#add_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },

                vehicle_brand_id: {
                    required: true
                },
                vehicle_categorie_id: {
                    required: true
                },
                model:{
                     required: true
                },
                price:{
                     required: true
                }
            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                },
                vehicle_brand_id: {
                    required: "@lang('validation.required',['attribute'=>'Vehicle Brand'])",
                },
                vehicle_categorie_id: {
                    required: "@lang('validation.required',['attribute'=>'Vehicle Category'])",
                },
                model: {
                    required: "@lang('validation.required',['attribute'=>'Model'])",
                },
                price: {
                    required: "@lang('validation.required',['attribute'=>'Price'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
        $('#vehicle_brand_id').on('change', function(e) {
            e.preventDefault();
            

            var selectedType= $(this).children("option:selected").val();
            var selectedattrValue= $(this).children("option:selected").attr('data-value-temp');
            
            var str = "1"; 
            var check1 = str.includes(selectedattrValue);


            if(selectedType == "" || selectedattrValue == "" || check1 == false){
                $('.car_window_selection').hide();
            }else{
                $('.car_window_selection').show();
            }
            
        });
    });
</script>
@endsection