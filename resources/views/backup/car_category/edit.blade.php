@extends('layouts.admin')
@section('content')
<style type="text/css">
    .required_class{
        color: red;
    }
</style>
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>Edit Car Category</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('carcategory.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                <form action="{{ route('carcategory.update',[$carcategory->id]) }}" method="POST" enctype="multipart/form-data" id="edit_car_category_form">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">Title<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ old('title', isset($carcategory->title) ? $carcategory->title : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="category">Parent Category</label>
                        <select class="form-control" name="category_id" id="category">
                            <option value="">Select Category</option>  
                            @foreach($parent_categorys as $value)
                                @if($value->id == $carcategory->category_id)
                                    <option value="{{$value->id}}" selected="">{{$value->title}}</option>  
                                @else
                                    <option value="{{$value->id}}">{{$value->title}}</option>  
                                @endif
                            @endforeach     
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        $(function() {
        $("#edit_car_category_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                title: {
                    required: true
                },
                category_id: {
                    required: true
                }
            },
            // Specify validation error messages
            messages: {
                title: {
                    required: "@lang('validation.required',['attribute'=>'Title'])",
                },
                category_id: {
                    required: "@lang('validation.required',['attribute'=>'Category'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
    });
</script>
@endsection