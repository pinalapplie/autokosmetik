<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>Autokosmetik</title>
    <style>       
        * { box-sizing: border-box; -moz-box-sizing: border-box; }        
    </style>
</head>
<body style="background: url('cid:background_2u') no-repeat; margin: 0; padding: 0;font-family: 'Arial', cursive;font-size: 16px;color: #111111;">
    <table align="center" bgcolor="#fff" cellspacing="0" cellpadding="0" border="0" width="100%" style="max-width: 650px; margin: 30px auto; box-shadow: 1px 1px 20px 2px #00000014; ">
        <tr>
           <td style="background: #7f141b;">
                 <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top: 25px; background: #fff; border: 1px solid #eee;">
                    <tr>
                        <td align="center" valign="top" style="padding: 25px 0;">
                            <a href="#" target="_blank" style="color: #111111;text-decoration: none;"><img src="cid:logo_2u" alt="autokosmetik" style="max-height: 70px;"></a>
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="center" colspan="2" class="text-center" style="padding: 70px; background: #F9F9F9;">
                            <h1 style="font-size: 20px; font-weight: 900;text-transform: capitalize; margin: 0; color: #7F141B;"> Herzlichen Glückwunsch </h1>
                            <h1 style="font-size: 18px; font-weight: normal; margin-bottom: 0; margin: 5px 0;">Sie haben eine neue Anfrage von {{ $name }}</h1>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding: 70px;">
                            <h1 style="font-size: 18px; font-weight: bold;margin: 0;margin-bottom: 0.3rem;">E-Mail</h1>
                            <p style="margin:0;">{{ $email }}</p>
                            <hr style="border-color: #ffffff4d; margin: 20px 0;">
                            <h1 style="font-size: 18px; font-weight: bold;margin: 0;margin-bottom: 0.3rem;">Kontakt Nummer</h1>
                            <p style="margin:0;">{{ $number }}</p>
                            <hr style="border-color: #ffffff4d; margin: 20px 0;">
                            <h1 style="font-size: 18px; font-weight: bold;margin: 0;margin-bottom: 0.3rem;">Betreff</h1>
                            <p style="margin:0;">{{ $subject }}</p>
                            <hr style="border-color: #ffffff4d; margin: 20px 0;">
                            <h1 style="font-size: 18px; font-weight: bold;margin: 0;margin-bottom: 0.3rem;">Nachricht</h1>
                            <p style="margin:0;">{{ $message }}</p>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>    
    </table>
</body>
</html>