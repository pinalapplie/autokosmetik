<?php

namespace App\Http\Controllers;

use App\CarCategory;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCarCategoryRequest;
use App\Http\Requests\UpdateCarCategoryRequest;
use Illuminate\Support\Facades\Storage;     

class CarCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $car_categorys = CarCategory::with(['car_category_name','car_category_name.category_name','car_category_name.category_name.category_name'])->latest()->get();
        //dd($car_categorys);
        return view('car_category.index', compact('car_categorys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent_categorys = Category::get();
        return view('car_category.create',compact('parent_categorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $carcategory = new CarCategory;
        $carcategory->title = $request->title;
        $carcategory->category_id = $request->category_id;
        $carcategory->save();
        return redirect()->route('carcategory.index')->with('message', "Car Category added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarCategory  $carCategory
     * @return \Illuminate\Http\Response
     */
    public function show(CarCategory $carCategory)
    {
        $car_category= CarCategory::with(['category_name'])->where('id',$carcategory->id)->first();
        return view('car_category.view',compact('car_category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarCategory  $carcategory
     * @return \Illuminate\Http\Response
     */
    public function edit(CarCategory $carcategory)
    {
        $parent_categorys = Category::get();
        return view('car_category.edit', compact('carcategory','parent_categorys'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarCategory  $carcategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CarCategory $carcategory)
    {
        $carcategory->title = $request->title;
        $carcategory->category_id = $request->category_id;
        $carcategory->save();
        return redirect()->route('carcategory.index')->with('message', "Car Category updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarCategory  $carCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $categorys = CarCategory::find(request('ids'));
        $categorys->delete();
        return response()->noContent();
    }
    public function switchUpdate(Request $request)
    {
        $categorys = CarCategory::find($request->ids);
        if (empty($categorys->is_active)) {
            $categorys->is_active = 1;
        } else {
            $categorys->is_active = 0;
        }
        $categorys->save();
        return response()->noContent();
    }
}