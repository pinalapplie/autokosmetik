@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.update') }} {{ trans('cruds.servicemangement.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('servicemangement.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('servicemangement.store') }}" method="POST" enctype="multipart/form-data" id="add_form">
                    @csrf
                     <input type="hidden" name="id" value="{{encrypt($servicemanagement->id)}}">
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.servicemangement.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($servicemanagement->name) ? $servicemanagement->name : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">{{ trans('cruds.servicemangement.fields.description') }}</label>
                        <textarea name="description" class="form-control" id="description" rows="5">
                            {!! old('description', isset($servicemanagement->description) ? $servicemanagement->description : '') !!}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="category">{{ trans('cruds.servicemangement.fields.subcategory') }}<span class="required_class">*</span></label>
                        <select class="form-control" name="service_category_id" id="category" disabled="">
                            <option value="">{{ trans('cruds.servicemangement.fields.select_subcategory') }}</option>  
                            @foreach($service_category as $value)
                                <?php 
                                    $final_category_name='';
                                    if($value->parent_id == 0){
                                        $final_category_name = $value->name;
                                    }else {
                                        if($value->service_category_name->parent_id == 0){
                                            $category_name1=$value->service_category_name->name;
                                            $final_category_name = $value->name.'->'.$category_name1;    
                                        }else{
                                            $category_name1=$value->service_category_name->name;
                                            $category_name2=$value->service_category_name->service_category_name->name;
                                            $final_category_name = $value->name.'->'.$category_name1.'->'.$category_name2;
                                        }
                                    }
                                ?>
                                <option value="{{$value->id}}" @if($servicemanagement->service_category_id==$value->id) selected="" @endif>{{$final_category_name}}</option>  
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="form-group">
                        @foreach($vehicle_category as $key => $vehicleCategory)
                            <label>{{ $vehicleCategory->name }} : </label>
                            <div class="row mb-3">                                
                                @foreach($vehicle_conditions as $key => $value)
                                <?php
                                    $sprice = '';
                                    $servicePriceId = '';
                                    foreach($service_price as $servicePrice){
                                        if($vehicleCategory->id == $servicePrice->vehicle_categorie_id && $value->id == $servicePrice->vehicle_condition_id){
                                            $sprice = $servicePrice->price;
                                            $servicePriceId = $servicePrice->id;
                                        }
                                    }
                                ?>
                                <div class="col-6">
                                    <label>{{ $value->name }}</label>
                                    <input class="form-control" type="number" name="{{ strtolower($value->name) }}_{{ $vehicleCategory->id }}_{{ $value->id }}" value="{{ $sprice }}" placeholder="{{ $value->name }}" required=""  />
                                    <input class="form-control" type="hidden" name="{{ strtolower($value->name) }}1_{{ $vehicleCategory->id }}_{{ $value->id }}" value="{{ $servicePriceId }}"  />
                                </div>    
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                    
                    <button type="submit" class="btn btn-primary">{{ trans('global.submit') }}</button>
                </form>
            </div>
        </div>  
    </div>
</div>  
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        CKEDITOR.replace('description');
        //CKEDITOR.replace('short_description');
        $("#add_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
                // description: {
                //     required: function(textarea) {
                //         CKEDITOR.instances[textarea.id].updateElement();
                //         var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                //         return editorcontent.length === 0;
                //     }
                // },
                // service_category_id: {
                //     required: true
                // },
                // type: {
                //     required: true
                // }

            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                },
                // description: {
                //     required: "@lang('validation.required',['attribute'=>'Description'])",
                // },
                // service_category_id: {
                //     required: "@lang('validation.required',['attribute'=>'Category'])",
                // },
                // type: {
                //     required: "@lang('validation.required',['attribute'=>'Type'])",
                // }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection