<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleBrands extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table ="vehicle_brands";
     protected $dates = ['deleted_at'];

    protected $with = ['vehicle_category_name'];

    public function vehicle_category_name()
    {
        return $this->hasOne('App\Http\Model\VehicleCategory','id','vehicle_categorie_id');
    }
     public function vehicle_type_name()
    {
        return $this->hasOne('App\Http\Model\VehicleType','id','vehicle_type_id');
    }
}
