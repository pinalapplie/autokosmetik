<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_brands', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('vehicle_type_ids')->nullable()->comment('comma seperater types');
            $table->string('image')->nullable();
            //$table->bigInteger('vehicle_categorie_id')->unsigned();
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
             $table->softDeletes();
            //$table->foreign('vehicle_categorie_id')->references('id')->on('vehicle_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_brands');
    }
}
