<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceManagement extends Model
{
    use SoftDeletes; 
    protected $guarded = [];
    protected $dates = ['deleted_at'];
    protected $table ="service_management";

    protected $with = ['service_category_name','service_price'];

    public function service_category_name()
    {
        return $this->hasOne('App\Http\Model\ServiceCategory','id','service_category_id');
    }
	public function service_price()
    {
        return $this->hasMany('App\Http\Model\ServicePrice','service_management_id','id')->orderBy('vehicle_service_id');
    }
}
