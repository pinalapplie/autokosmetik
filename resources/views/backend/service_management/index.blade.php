@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>{{ trans('cruds.servicemangement.title_singular') }} {{ trans('global.list') }}</h4>
            </div>

            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('servicemangement.create') }}" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        {{ trans('global.add') }} {{ trans('cruds.servicemangement.title_singular') }}
                    </a>
                </div>
            </div>
            <div class="col-md-12  mt-3">
                <div class="card">
                    <div class="card-body">
                        @include("partials.alert")
                        <div class="table-responsive">
                            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                    <tr>
                                        <th>{{ trans('cruds.servicemangement.fields.id') }}</th>
                                        <th>{{ trans('cruds.servicemangement.fields.name') }}</th>
                                        <th>{{ trans('cruds.servicemangement.fields.service_category') }}</th>
                                        <!--<th>{{ trans('cruds.servicemangement.fields.type') }}</th>
                                        <th>{{ trans('cruds.servicemangement.fields.price') }}</th>-->
                                        <th>{{ trans('cruds.servicecategories.fields.vehicle_type') }}</th>
                                        <!-- <th>{{ trans('cruds.servicemangement.fields.description') }}</th> -->
                                        <th>{{ trans('global.status') }}</th>
                                        <th>{{ trans('global.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($service_management as $key => $value)
                                    <tr data-entry-id="{{ $value->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->name ?? ''}}</td>
                                        <?php 
                                            $category_name1="";
                                            $category_name2="";
                                            $category_name3="";
                                            if($value->service_category_name && $value->service_category_name->name !=""){
                                                $category_name1 = $value->service_category_name->name;
                                                if($value->service_category_name->service_category_name && $value->service_category_name->service_category_name->name !=""){
                                                    $category_name2 = ' -> '.$value->service_category_name->service_category_name->name;
                                                    if($value->service_category_name->service_category_name->service_category_name && $value->service_category_name->service_category_name->service_category_name->name !=""){
                                                        $category_name3 = ' -> '.$value->service_category_name->service_category_name->service_category_name->name;
                                                    }
                                                }
                                            }
                                        ?>
                                        <td>{{ $category_name1.$category_name2.$category_name3}}</td>
                                        <!--<td>{{ $value->type ?? ''}}</td>
                                        <td>{{ $value->price ?? '-'}}</td>-->
                                        <td>{{ $value->service_category_name ? $value->service_category_name->vehicle_type_name->name : '-' }}</td>
                                        <!-- <td>{!! $value->description ?? '-' !!}</td> -->
                                        <td>
                                            @include('partials.switch', ['id'=>
                                            $value->id,'is_active'=>$value->is_active])
                                        </td>
                                        <td>@include('partials.actions', ['id' => $value->id])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
