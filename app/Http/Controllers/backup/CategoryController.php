<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Support\Facades\Storage;     

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorys = Category::with(['category_name'])->latest()->get();
        return view('category.index', compact('categorys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent_categorys = Category::get();
        return view('category.create',compact('parent_categorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $category = new Category;
            $category->title = $request->title;
            $category->short_description = $request->short_description;
            $category->description = $request->description;
            $category->parent_id = $request->parent_id;
            $category->vehicle_category = $request->vehicle_category;

            if ($request->file('image')) {
                $storage_path = 'public/images';
                $category->image = Storage::disk('local')->put($storage_path,$request->file('image'));
            }

            $category->save();
            return redirect()->route('category.index')->with('success', "Category added successfully");
        }catch(\Exception $e){
            return redirect()->back()->with('danger', trans('global.something_wrong'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $category= Category::with(['category_name'])->where('id',$category->id)->first();
        return view('category.view',compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $parent_categorys = Category::get();
        return view('category.edit', compact('category','parent_categorys'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        try{
            $category->title = $request->title;
            $category->short_description = $request->short_description;
            $category->description = $request->description;
            $category->parent_id = $request->parent_id;
            $category->vehicle_category = $request->vehicle_category;
            
            if ($request->file('image')) {
                Storage::disk('local')->delete($category->image);

                $storage_path = 'public/images';
                $category->image = Storage::disk('local')->put($storage_path,$request->file('image'));
            }

            $category->save();
            return redirect()->route('category.index')->with('message', "Category updated successfully");
        }catch(\Exception $e){
            return redirect()->back()->with('danger', trans('global.something_wrong'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $categorys = Category::find(request('ids'));
        Storage::disk('local')->delete($categorys->image);
        $categorys->delete();
        return response()->noContent();
    }
    public function switchUpdate(Request $request)
    {
        $categorys = Category::find($request->ids);
        if (empty($categorys->is_active)) {
            $categorys->is_active = 1;
        } else {
            $categorys->is_active = 0;
        }
        $categorys->save();
        return response()->noContent();
    }
}
