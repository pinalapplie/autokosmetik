<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Mail;
use Illuminate\Support\Facades\Storage;
use App\Http\Model\ServiceCategory;
use App\Http\Model\VehicleType;
use App\Http\Model\VehicleBrands;
use App\Http\Model\VehicleConditions;
use App\Http\Model\DriveVehicle;
use App\Http\Model\VehicleService;
use App\Http\Model\ServiceManagement;
use App\Http\Model\BookingAppointment;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Http\Model\VehicleCategory;
use App\Http\Model\Addonservice;

use App\Http\Model\News;
use App\Http\Model\Portfolio;
use App\Http\Model\Feedback;

class WebsiteController extends Controller
{
    public function comingsoon(){
        return view('frontend.comingsoon');
    }
     public function second_skin(){
        return view('frontend.secondskin');
    }
    public function index()
    {
        
        $portfolio = Portfolio::where('is_active', 1)->orderBy('id', 'desc')->get();
        $portfolio->map(function($item){
            $tempArray = explode(",",$item->service_category_id);
            $item->services_category = ServiceCategory::whereIn('id',$tempArray)->get();
            return $item;
        });
        $feedback = Feedback::where('is_active', 1)->latest()->get();
        return view('frontend.index', compact('portfolio','feedback'));
    }
    public function aboutUs()
    {
        return view('frontend.about_us');
    }
    
    public function news()
    {
        $news = News::where('is_active',1)->orderBy('id', 'desc')->paginate(5);
        //$randomNews = News::where('is_active',1)->random(4);

        $models = News::where('is_active', 1)->get();
        $randomNews = $models->shuffle()->slice(0,4);
        return view('frontend.news',compact('news','randomNews'));
    }
    
    public function newsDetails($id){
        $news = News::find($id);
        
        $models = News::where('is_active', 1)->get();
        $randomNews = $models->shuffle()->slice(0,4);
        return view('frontend.newsDetails',compact('news','randomNews'));
    }
    
    public function services()
    {
        return view('frontend.services');
    }
    public function carCosmetics()
    {
        return view('frontend.car_cosmetics');
    }
    public function foilTechnology()
    {
        return view('frontend.foil_technology');
    }
    public function rimTire()
    {
        return view('frontend.rim_tire');
    }
    public function voucher()
    {
        return view('frontend.voucher');
    }
    public function contact()
    {
        return view('frontend.contact');
    }
    public function portfolio()
    {
        $serviceCategory = ServiceCategory::where('parent_id', 0)->where('is_glass', 0)->get();
        //$portfolio = Portfolio::where('is_active', 1)->orderBy('id', 'desc')->get();
        $portfolio = Portfolio::where('is_active', 1)->orderBy('id', 'desc')->paginate(3);
        $portfolio->map(function($item){
            $tempArray = explode(",",$item->service_category_id);
            $item->services_category = ServiceCategory::whereIn('id',$tempArray)->get();
            return $item;
        });
        return view('frontend.portfolio',compact('portfolio','serviceCategory'));
    }
    public function ceramic_coating()
    {
        return view('frontend.ceramic_coating');
    }
    public function bookappointment()
    {
        $vehicle_type = VehicleType::where('is_active',1)->get();
        //$service_category = ServiceCategory::where('parent_id',0)->where('is_active',1)->get();
        //return view('frontend.bookappointment',compact('service_category','vehicle_type'));
        return view('frontend.bookappointment',compact('vehicle_type'));
    }
    public function sealing()
    {
        return view('frontend.sealing');
    }
    public function swissvax()
    {
        return view('frontend.swissvax');
    }
    public function conventinal()
    {
        return view('frontend.conventinal');
    }
    public function specialcase()
    {
        return view('frontend.specialcase');
    }
    public function car_window_tinting()
    {
        return view('frontend.car_window_tinting');
    }
    public function foiltech()
    {
        return view('frontend.foiltech');
    }
    public function paint()
    {
        return view('frontend.paint');
    }

    public function mono()
    {
        return view('frontend.mono');
    }

    public function interior_design()
    {
        return view('frontend.interior_design');
    }

    public function ajaxStep0(Request $request)
    {
        $step_1 = explode(",",$request->id);
        $vehicle_conditions = VehicleConditions::where('is_active',1)->get();
        
        $result = 0;
        if (!empty($vehicle_conditions) && $vehicle_conditions->count()) {
            $result = 1;
        }
        $html = view('frontend.ajax.step_1',['vehicle_conditions' =>$vehicle_conditions]);
        $html = $html->render();
        return response()->json(['result' => $result,'html' => $html]);
    }
    public function ajaxStep1(Request $request)
    {
        $step_20 = explode(",",$request->id);
        $vehicle_categorys = VehicleCategory::where('is_active',1)->get();
        
        $result = 0;
        if (!empty($vehicle_categorys) && $vehicle_categorys->count()) {
            $result = 1;
        }
        $html = view('frontend.ajax.step_20',['vehicle_categorys' =>$vehicle_categorys]);
        $html = $html->render();
        return response()->json(['result' => $result,'html' => $html]);
    }
    public function ajaxStep2(Request $request)
    {
        $step_20 = $request->id;
        $vehicle_categorys = VehicleCategory::where('vehicle_type_id',$step_20)->where('is_active',1)->get();
        
        $result = 0;
        if (!empty($vehicle_categorys) && $vehicle_categorys->count()) {
            $result = 1;
        }
        $html = view('frontend.ajax.step_2',['vehicle_categorys' =>$vehicle_categorys]);
        $html = $html->render();
        return response()->json(['result' => $result,'html' => $html]);
    }
    public function ajaxStep20(Request $request)
    {
        $step_0 = explode(",",$request->id);
        //$service_category = ServiceCategory::where('parent_id',0)->where('is_active',1)->whereIn('vehicle_type_id',$step_0)->get();
        $service_category = ServiceCategory::where('parent_id',0)->where('is_active',1)->get();
        $result = 0;
        if (!empty($service_category) && $service_category->count()) {
            $result = 1;
        }
        $html = view('frontend.ajax.step_20',['service_category' =>$service_category]);
        $html = $html->render();
        return response()->json(['result' => $result,'html' => $html]);
    }
    public function ajaxStep3(Request $request) 
    {
        //$step_3 = explode(",",$request->id);
        $step_3 = $request->id;
        $vehicle_id = $request->vehicle;
        $vehicle_condition_id = $request->vehicle_condition;
        $vehicle_type_id = $request->vehicle_type;
        
        $service_category = ServiceCategory::where('parent_id',0)->where('is_glass',0)->where('is_active',1)->get();
        
        $result = 0;
        if (!empty($service_category) && $service_category->count()) {
            $result = 1;
        }
        $html = view('frontend.ajax.step_3',['service_category' =>$service_category,'vehicle_id' =>$vehicle_id,'vehicle_condition_id' =>$vehicle_condition_id,'vehicle_type_id' =>$vehicle_type_id,'cat_id'=>$step_3]);
        $html = $html->render();
        return response()->json(['result' => $result,'html' => $html]);
    }
    public function ajaxStep4(Request $request)
    {
        $step_4 = explode(",",$request->id);
        $addonservice = Addonservice::where('is_active',1)->get();
        $result = 0;
        if (!empty($addonservice) && $addonservice->count()) {
            $result = 1;
        }
        $html = view('frontend.ajax.step_4',['addonservice' =>$addonservice]);
        $html = $html->render();
        return response()->json(['result' => $result,'html' => $html]);
    }
    public function ajaxStep5(Request $request)
    {
        //dd($request->id);
        $step_5 = explode(",",$request->id);
        $service_with_sub_category = [];
        foreach ($step_5 as $key => $value) {
           $service_category_temp = ServiceCategory::where('parent_id',$value)->where('is_active',1)->get();
           if (!empty($service_category_temp) && $service_category_temp->count()) {
           }else{
                $service_with_sub_category[] = ServiceCategory::where('id',$value)->where('is_active',1)->first();
           }

        }
        //dd($service_temp);
        $service_category = ServiceCategory::whereIn('parent_id',$step_5)->where('is_active',1)->get();
        $result = 0;
        if (!empty($service_category) && $service_category->count()) {
            $result = 1;
        }
        $html = view('frontend.ajax.step_5',['service_category' =>$service_category,'service_with_sub_category' => $service_with_sub_category]);
        $html = $html->render();
        return response()->json(['result' => $result,'html' => $html]);
    }
    public function ajaxStep6(Request $request)
    {
        $step_6 = $request->id;
        $vehicle_type= $request->vehicle_type;
        $vehicle_brands = VehicleBrands::where('is_active',1)->where('vehicle_type_ids',$step_6)->get();
        
        $result = 0;
        if (!empty($vehicle_brands) && $vehicle_brands->count()) {
            $result = 1;
        }
        $html = view('frontend.ajax.step_6',['vehicle_brands' =>$vehicle_brands,'vehicle_type' => $vehicle_type]);
        $html = $html->render();
        return response()->json(['result' => $result,'html' => $html]);

    }
    public function ajaxStep7(Request $request)
    {
        $step_7 = explode(",",$request->id);
        $drive_vehicle = DriveVehicle::whereIn('vehicle_brand_id',$step_7)->get();
        $result = 0;
        if (!empty($drive_vehicle) && $drive_vehicle->count()) {
            $result = 1;
        }
        $html = view('frontend.ajax.step_7',['drive_vehicle' =>$drive_vehicle]);
        $html = $html->render();
        return response()->json(['result' => $result,'html' => $html]);

        // $step_7 = explode(",",$request->id);
        // //dd($step_7);
        // $service_category_temp = ServiceManagement::whereIn('service_category_id',$step_7)->where('is_active',1)->pluck('id','id');
        // $service_category = VehicleService::whereIn('service_management_id',$service_category_temp)->where('is_active',1)->get();
        // $html='';
        // $temp_array = [];
        // foreach ($service_category as $key => $value) {
           
                                                    
        //     if(!in_array( $value->service_management_name->id, $temp_array)){
        //         $html .='<h3 class="pb-3">'.$value->service_management_name->name.'</h3>';
        //         $temp_array[] = $value->service_management_name->id;
        //     }
        //     $html .='<div class="form-group">';
        //     $html .='<input type="checkbox" name="step_8" class="form-control checkbox" id='.$value->name.' value='.$value->id.'>';
        //     $html .='<label role="button" for='.$value->name.'>'.$value->name.'</label>';
        //     //$html .='<div class="infoicon-i" role="button" data-toggle="modal" data-target=#modal_bookappointment_'.$value->id.'>';
        //     //$html .='<img src='.asset('assets/frontend/images/info-icon.svg').'></div>';
        //     $html .='</div>';

        // }
        // echo $html;
    }
    public function ajaxStep8(Request $request)
    {
        $step_8 = explode(",",$request->id);
        $step_5 = explode(",",$request->step_5_checkbox);
        $step_21 = $request->step_21;
        $vehicle_condition_id = $request->vehicle_condition;
        $vehicle_type_id = $request->vehicle_type;

        $service_management = ServiceManagement::whereIn('id',$step_8)->where('is_active',1)->get();
        $addonservice = Addonservice::whereIn('id',$step_5)->where('is_active',1)->get();
        $drive_vehicle = DriveVehicle::find($step_21);
        
        $service_management_array = array();
        $service_category_array = array();
        if(!empty($request->id)){
            $service_management_comma = ServiceManagement::whereIn('id', $step_8)->groupBy('service_category_id')->pluck('service_category_id')->implode(',');
            $service_management_array = explode(",",$service_management_comma);
            
            $service_category_comma = ServiceCategory::whereIn('id', $service_management_array)->groupBy('parent_id')->pluck('parent_id')->implode(',');
            $service_category_array = explode(",",$service_category_comma);
        }
        
        $result = 0;
        if (!empty($service_management) && $service_management->count()) {
            $today = date("F j, Y"); 

            $bookingObj = new BookingAppointment;
            $bookingObj->name = $request->contact_name;
            $bookingObj->number = $request->contact_number;
            $bookingObj->email = $request->contact_email;
            $bookingObj->address = $request->contact_address;
            $bookingObj->zip = $request->contact_zip;
            $bookingObj->city = $request->contact_city;
            $bookingObj->state = $request->contact_state;
            $bookingObj->service_ids = $request->id;
            $bookingObj->save();

            $result = 1;
            
            $html = view('frontend.email_template',["service_management" =>$service_management,"insert_id" =>$bookingObj->id,"today"=> $today,"name" =>$request->contact_name,"number" =>$request->contact_number,"email" =>$request->contact_email,"address" =>$request->contact_address,"zip" =>$request->contact_zip,"city" =>$request->contact_city,"state" =>$request->contact_state,'addonservice'=>$addonservice,'drive_vehicle'=>$drive_vehicle,'vehicle_condition_id'=>$vehicle_condition_id,'vehicle_type_id'=>$vehicle_type_id,'service_category_array'=>$service_category_array,'service_management_array'=>$service_management_array,'step_8'=>$step_8]);
            $html = $html->render();
            
            require_once public_path().'/phpmailer/autoload.php';
            $attachPath = public_path().'/assets/frontend/images/logo-bkp.png';
            //$customer_email = "multipz.sharvang@gmail.com";
            $customer_email = $request->contact_email;

            try{
                $mail = new PHPMailer(true); 
               
                //$html = view('email_template',["insert_id" => $insert_id])->render();
                $email = $customer_email;
                
                //$mail->SMTPDebug = 2;                                        
                $mail->isSMTP();                                             
                $mail->Host       = 'smtp.gmail.com';                     
                $mail->SMTPAuth   = true;                              
                $mail->Username   = 'multipz.sharvang@gmail.com';                  
                $mail->Password   = 'nmnslinysusratyv';                         
                $mail->SMTPSecure = 'tls';                               
                $mail->Port       = 587;   
                
                $mail->setfrom('multipz.sharvang@gmail.com', 'Autokosmetik');            
                $mail->addAddress($email); 
                //$mail->addaddress('receiver2@gfg.com', 'Name'); 
                //$mail->addAttachment($attachPath, 'logo.png');
                $mail->AddEmbeddedImage($attachPath, 'logo_2u');
                   
                $mail->isHTML(true);                                   
                $mail->Subject = 'Autokosmetik Invoice'; 
                $mail->Body    = $html;
                //$mail->AltBody = 'Body in plain text for non-HTML mail clients'; 
                $mail->send(); 
                //echo "Mail has been sent successfully!"; 
                $result = 1;

                
                
            }catch(\Exception $e){
                $result = 0;
            }
        }


        return response()->json(['result' => $result]);
    }
    
    public function contactform1(Request $request)
    {  
        $html = view('frontend.email_contact',["name" =>$request->name,"email" =>$request->email,"number" =>$request->phoneNumber,"subject" =>$request->subject,"message" =>$request->message]);
        $html = $html->render();
        
        require_once public_path().'/phpmailer/autoload.php';
        $attachPath = public_path().'/assets/frontend/images/logo-bkp.png';
        $attachPath_background = public_path().'/assets/frontend/images/inquiry-bg.png';
        //$customer_email = "multipz.sharvang@gmail.com";
        $email = 'multipz.sharvang@gmail.com';
        
        try{
            $mail = new PHPMailer(true); 
           
            //$html = view('email_template',["insert_id" => $insert_id])->render();
            //$email = $customer_email;
            
            //$mail->SMTPDebug = 2;                                        
            $mail->isSMTP();                                             
            $mail->Host       = 'smtp.gmail.com';                     
            $mail->SMTPAuth   = true;                              
            $mail->Username   = 'multipz.sharvang@gmail.com';                  
            $mail->Password   = 'nmnslinysusratyv';                         
            $mail->SMTPSecure = 'tls';                               
            $mail->Port       = 587;   
            
            $mail->setfrom('multipz.sharvang@gmail.com', 'Autokosmetik');            
            $mail->addAddress($email); 
            //$mail->addaddress('receiver2@gfg.com', 'Name'); 
            //$mail->addAttachment($attachPath, 'logo.png');
            //$mail->AddEmbeddedImage($attachPath, 'logo_2u');
            $mail->AddEmbeddedImage($attachPath, 'logo_2u');
            $mail->AddEmbeddedImage($attachPath_background, 'background_2u');


            $mail->isHTML(true);                                   
            $mail->Subject = 'KONTAKT FORMULAR'; 
            $mail->Body    = $html;
            //$mail->AltBody = 'Body in plain text for non-HTML mail clients'; 
            $mail->send(); 
            //echo "Mail has been sent successfully!"; 
            $result = 1;
        
        }catch(\Exception $e){
            $result = 0;
        }
        return response()->json(['result' => $result]);
    }
    
    public function email(){
        $vehicle_condition_id = 1;
        $vehicle_type_id = 1;
        $step_8 = [26,28,31,38,32];
        $step_5 = [1,2];
        
        $service_management_comma = ServiceManagement::whereIn('id', $step_8)->groupBy('service_category_id')->pluck('service_category_id')->implode(',');
        $service_management_array = explode(",",$service_management_comma);
        
        $service_category_comma = ServiceCategory::whereIn('id', $service_management_array)->groupBy('parent_id')->pluck('parent_id')->implode(',');
        $service_category_array = explode(",",$service_category_comma);
        
        $service_management = ServiceManagement::whereIn('id', $step_8)->where('is_active',1)->get();
        
        /*foreach($service_management as $sm){
            echo $sm->name."<br />";
            echo $sm->service_category_parent->name."<br />";
            echo $sm->service_category_name->name."<br />";
        }
        exit();*/
        $drive_vehicle = '';
        $addonservice = Addonservice::whereIn('id', $step_5)->where('is_active',1)->get();
        $logo = public_path().'/assets/frontend/images/logo.png';
        return view('frontend.email_template',['name'=>'Wolfinger Fabio','insert_id'=>'9','today' =>'11. April 2021','email'=>'kathiriyagaurang@gmail.com','number'=>'1234567890','logo' => $logo, 'service_management' => $service_management, 'addonservice' => $addonservice, 'vehicle_condition_id'=>$vehicle_condition_id, 'vehicle_type_id'=>$vehicle_type_id, 'service_category_array'=>$service_category_array, 'service_management_array'=>$service_management_array, 'step_8'=>$step_8, 'drive_vehicle'=>$drive_vehicle]);     
    }
    public function carcare_swissvax(){
        return view('frontend.carcare_swissvax');
    }
    public function car_interior_cleaning(){
        return view('frontend.car_interior_cleaning');
    }
    public function car_interior_processing_special(){
        return view('frontend.car_interior_processing_special');
    }
    public function stone_impact_protection_film(){
        return view('frontend.stone_impact_protection_film');
    }
    public function leather_care_restoration(){
        return view('frontend.leather_care_restoration');
    }
    public function car_wrapping(){
        return view('frontend.car_wrapping');
    }
    public function vehicle_lettering(){
        return view('frontend.vehicle_lettering');
    }
    public function water_transfer_printing(){
        return view('frontend.water_transfer_printing');
    }
    public function chrome_plating(){
        return view('frontend.chrome_plating');
    }

}    