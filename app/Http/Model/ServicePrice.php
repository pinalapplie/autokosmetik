<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServicePrice extends Model
{
	use SoftDeletes; 
    protected $guarded = [];
    protected $table ="service_prices";
    protected $dates = ['deleted_at'];

    
}
