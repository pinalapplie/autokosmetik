<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceCategory extends Model
{
    use SoftDeletes;  
    protected $guarded = [];

    protected $table ="service_categories";
    protected $dates = ['deleted_at'];
    protected $with = ['service_category_name','vehicle_type_name'];

    public function service_category_name()
    {
        return $this->hasOne('App\Http\Model\ServiceCategory','id','parent_id');
    }
    public function vehicle_type_name()
    {
        return $this->hasOne('App\Http\Model\VehicleType','id','vehicle_type_id');
    }
}
