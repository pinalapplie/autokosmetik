@foreach ($vehicle_conditions as $key => $value) 
    <div class="form-group">
    	<input type="radio" name="step_2" class="form-control radio" id="step_2_{{$value->id}}" v-id="{{$value->name}}">
    	<label role="button" for="step_2_{{$value->id}}">{{$value->name}}</label>
    </div>
@endforeach