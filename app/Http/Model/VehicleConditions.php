<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleConditions extends Model
{
	use SoftDeletes;  
   	protected $guarded = [];
    protected $table ="vehicle_conditions";
    protected $dates = ['deleted_at'];
}
