@extends('layouts.admin')
@section('content')
<style type="text/css">
    .required_class{
        color: red;
    }
</style>
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>Edit Service</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('services.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                <form action="{{ route('services.update',[$service->id]) }}" method="POST" enctype="multipart/form-data" id="edit_service_form">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">Title<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ old('title', isset($service->name) ? $service->name : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">Image<span class="required_class">*</span></label>
                        <input type="file" name="image" class="form-control" id="image" accept="image/x-png,image/gif,image/jpeg">
                        <br>
                        <a href="{{ asset(Storage::url($service->image)) }}" target="_blank">
                            <img src="{{ asset(Storage::url($service->image)) }}" alt="" height=100 width=100>
                        </a>
                    </div>
                    <div class="form-group">
                        <label for="short_description">Short Description<span class="required_class">*</span></label>
                        <textarea name="short_description" class="form-control" id="short_description" rows="2">
                               {!! old('short_description', isset($service->short_description) ? $service->short_description : '') !!}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="description">Description<span class="required_class">*</span></label>
                        <textarea name="description" class="form-control" id="description" rows="5">
                            {!! old('description', isset($service->description) ? $service->description : '') !!}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="description">Portfolio Image</label>
                        <input type="file" name="portfolio[]" class="form-control" multiple accept="image/x-png,image/gif,image/jpeg">
                        <br>
                        @foreach($service->portfolioImage as $key=> $value)    
                            
                                <a href="{{ asset(Storage::url($value->image)) }}" target="_blank" >
                                    <img src="{{ asset(Storage::url($value->image)) }}" alt="" height=100 width=100 style="margin-right:10px;">
                                </a>
                            
                        @endforeach    
                    </div>
                    <div class="form-group">
                        <label for="category">Type<span class="required_class">*</span></label>
                        <select class="form-control" name="type" disabled="">
                            <option value="">Select Type</option>  
                            <option value="specification" @if($service->type=="specification") selected="" @endif>Specification</option>  
                            <option value="pricechart"  @if($service->type=="pricechart") selected="" @endif>Pricechart</option>  
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="category">Category<span class="required_class">*</span></label>
                        <select class="form-control" name="category" id="category" disabled="">
                            <option value="">Select Category</option>  
                            @foreach($parent_categorys as $value)
                                <option value="{{$value->id}}" @if($service->category_id==$value->id) selected="" @endif>{{$value->title}}</option>  
                            @endforeach
                        </select>
                    </div>
                    @if($service->type=="pricechart")
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                                        <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col">Service Name</th>
                                            <th scope="col">Car Condition Type</th>
                                            @foreach($cartypes as $value)
                                                <th scope="col">{{$value->name}}</th>
                                            @endforeach    
                                        </tr>
                                        </thead>
                                        <tbody>
                                             
                                            @foreach($carservicetypes as $key => $value)
                                                <tr>
                                                    <input type="hidden" name="service_type[]" value="{{$value->id}}">
                                                    <th scope="row">{{$key+1}}</th>
                                                    <td>{{$value->title}}</td>
                                                    <td>
                                                    @foreach($carconditiontype as $key1 => $value1)
                                                            @php $uncheck = 0; @endphp
                                                            @if($service->service_price && count($service->service_price))
                                                               @foreach($service->service_price as $service_price)
                                                                    @if($service_price->car_service_type_id == $value->id && $service_price->car_condition_type_id  == $value1->id)
                                                                         @php $uncheck = 1; @endphp
                                                                    @endif
                                                                @endforeach
                                                                <input type="radio" id="condition_type_{{$key.$key1}}" name="condition_type_{{$value->id}}" value="{{$value1->id}}" placeholder="" @if($uncheck == 1) checked="" @endif>
                                                            @else
                                                                 <input type="radio" id="condition_type_{{$key.$key1}}" name="condition_type_{{$value->id}}" value="{{$value1->id}}" placeholder="" @if(empty($key1)) checked="" @endif>
                                                            @endif    
                                                            <label for="condition_type_{{$key.$key1}}">{{$value1->name}}</label>
                                                    @endforeach
                                                    </td>
                                                    @php $temp = 0;@endphp
                                                    @foreach($cartypes as $value2)
                                                        <td>

                                                           @if($service->service_price && count($service->service_price) && $service->service_price[$temp]->car_type_id  == $value2->id)
                                                                @foreach($service->service_price as $service_price)
                                                                    @if($service_price->car_service_type_id == $value->id && $service_price->car_type_id  == $value2->id)
                                                                        <input type="number" name="car_type_{{$value->id}}_{{$value2->id}}" placeholder="" class="form-control" required="" value="{{$service_price->price}}">
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                <input type="number" name="car_type_{{$value->id}}_{{$value2->id}}" placeholder="" class="form-control" required="" value="">
                                                            @endif
                                                        </td>
                                                        @php $temp++;@endphp
                                                    @endforeach  
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endif
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    

@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        // CKEDITOR.replace('description');
        CKEDITOR.replace('description', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace('short_description', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        $("#edit_service_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                title: {
                    required: true
                },
                short_description: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement();
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                        return editorcontent.length === 0;
                    }
                },
                description: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement();
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                        return editorcontent.length === 0;
                    }
                },
                category: {
                    required: true
                },
                type: {
                    required: true
                }

            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Title'])",
                },
                description: {
                    required: "@lang('validation.required',['attribute'=>'Description'])",
                },
                short_description: {
                    required: "@lang('validation.required',['attribute'=>'Short Description'])",
                },
                image: {
                    required: "@lang('validation.required',['attribute'=>'Image'])",
                },
                category: {
                    required: "@lang('validation.required',['attribute'=>'Category'])",
                },
                type: {
                    required: "@lang('validation.required',['attribute'=>'Type'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection