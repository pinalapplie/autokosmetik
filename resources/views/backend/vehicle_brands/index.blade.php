@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>{{ trans('cruds.vehiclebrands.title_singular') }} {{ trans('global.list') }}</h4>
            </div>

            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('vehiclebrands.create') }}" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        {{ trans('global.add') }} {{ trans('cruds.vehiclebrands.title_singular') }}
                    </a>
                </div>
            </div>
           
            <div class="col-md-12 mt-3">
               <div class="card">
                    <div class="card-body">
                        @include("partials.alert")
                        <div class="table-responsive">
                            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                    <tr>
                                        <th>{{ trans('cruds.vehiclebrands.fields.id') }}</th>
                                        <th>{{ trans('cruds.vehiclebrands.fields.name') }}</th>
                                        <th>{{ trans('cruds.servicecategories.fields.vehicle_type') }}</th>
                                         <th>{{ trans('cruds.news.fields.image') }}</th>
                                        <th>{{ trans('global.status') }}</th>
                                        <th>{{ trans('global.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($vehicle_brands as $key => $value)
                                    <tr data-entry-id="{{ $value->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->name ?? ''}}</td>
                                        <td>
                                           <?php
                                            $temp_array = explode(",",$value->vehicle_type_ids); 
                                            $name = '';
                                            $temp_count = 0;
                                            foreach($vehicle_type as $key1 =>$value1){
                                                if(in_array($value1->id,$temp_array)){
                                                    if(empty($temp_count)){
                                                        $name.=$value1->name;
                                                        $temp_count++;
                                                    }else{
                                                        $name.=','.$value1->name;
                                                    }    
                                                }
                                            }
                                            ?>
                                            {{ $name}}
                                        </td>
                                         <td><img src="{{asset(Storage::url($value->image)) ?? ''}}" alt="" height=50 width=auto></td>
                                        <td>
                                            @include('partials.switch', ['id'=>
                                            $value->id,'is_active'=>$value->is_active])
                                        </td>
                                        <td>@include('partials.actions', ['id' => $value->id])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
