<?php

namespace App\Http\Controllers;

use App\CarConditionType;
use Illuminate\Http\Request;

class CarConditionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $car_condition_types = CarConditionType::latest()->get();
        return view('car_condition_type.index', compact('car_condition_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('car_condition_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $car_condition_type = new CarConditionType;
            $car_condition_type->name = $request->name;
            $car_condition_type->save();
            return redirect()->route('carconditiontype.index')->with('success',trans('message.car_condition_type.create'));
        }catch(\Exception $e){
            return redirect()->back()->with('danger', trans('global.something_wrong'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarConditionType  $carconditiontype
     * @return \Illuminate\Http\Response
     */
    public function show(CarConditionType $carconditiontype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarConditionType  $carconditiontype
     * @return \Illuminate\Http\Response
     */
    public function edit(CarConditionType $carconditiontype)
    {
        return view('car_condition_type.edit', compact('carconditiontype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarConditionType  $carconditiontype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CarConditionType $carconditiontype)
    {
        try{
            $carconditiontype->name = $request->name;
            $carconditiontype->save();
            return redirect()->route('carconditiontype.index')->with('success',trans('message.car_condition_type.update'));
        }catch(\Exception $e){
            return redirect()->back()->with('danger', trans('global.something_wrong'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarConditionType  $carconditiontype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $car_condition_type = CarConditionType::find(request('ids'));
        $car_condition_type->delete();
        return response()->noContent();
    }
    public function switchUpdate(Request $request)
    {
        $car_condition_type = CarConditionType::find($request->ids);
        if (empty($car_condition_type->is_active)) {
            $car_condition_type->is_active = 1;
        } else {
            $car_condition_type->is_active = 0;
        }
        $car_condition_type->save();
        return response()->noContent();
    }
}
