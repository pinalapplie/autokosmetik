@section('title', 'Dienstleistungen')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<div id="main-content" class="active">
			<div id="section-breadcrumb1" class="inner-banner-wrap">
				<img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
								
				<div class="container">
					<div class="row">
						<div class="content col-12">
							<h1>Dienstleistungen</h1>
							<ul>
								<li><a href="{{ route('index')}}">ZUHAUSE</a></li>
								<li class="current text-light">Dienstleistungen</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="section-services1" class="bg-light service-accordian-main accordion">
				<div class="container feature-car-area">
					<div class="accordion-wrap" id="accordionExample">
						<div class="row">
							<div class="col-md-4 mb-3">
								<div class="service-level-1 bg-white shadow-sm collapsed Pflege" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" style="background: url({{ asset('assets/frontend/images/car-care-bg.png')}}) no-repeat center / cover;" role="button">
									<div class="service-title-wrap">
										<div class="avatar-sm mb-3">
											<img src="{{ asset('assets/frontend/images/car-care.png')}}" alt="" class="img-fluid">
										</div>
										<h3 class="h3 font-weight-bold mb-0">Pflege</h3>
										<p>Perfekte Pflege für Ihr Fahrzeug: Auto-Innenreinigung, Lederpflege und –reparatur sowie Aussenaufbereitung.</p>
										<span class="text-primary btn-link">Mehr erfahren</span>
									</div>
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<div class="service-level-1 bg-white shadow-sm collapsed Schutz" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2" style="background: url({{ asset('assets/frontend/images/car-protect-bg.jpg')}}) no-repeat center / cover;" role="button">
									<div class="service-title-wrap">
										<div class="avatar-sm mb-3">
											<img src="{{ asset('assets/frontend/images/car-protect.png')}}" alt="" class="img-fluid">
										</div>
										<h3 class="h3 font-weight-bold mb-0">Schutz</h3>
										<p>Steinschlagschutzfolien, eine Keramikbeschichtung und eine Scheibentönung schützen Ihr Auto oder Ihr Motorrad.</p>
										<span class="text-primary btn-link">Mehr erfahren</span>
									</div>
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<div class="service-level-1 bg-white shadow-sm collapsed design" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3" style="background: url({{ asset('assets/frontend/images/paint-1.jpg')}}) no-repeat center / cover;" role="button">
									<div class="service-title-wrap">
										<div class="avatar-sm mb-3">
											<img src="{{ asset('assets/frontend/images/car-foil.png')}}" alt="" class="img-fluid">
										</div>
										<h3 class="h3 font-weight-bold mb-0">Design</h3>
										<p>Interieur-Design, Verchromungen, Wassertransferdruck und eine Autofolie machen Ihr Fahrzeug zum Blickfang.</p>
										<span class="text-primary btn-link">Mehr erfahren</span>
									</div>
								</div>
							</div>
						</div>
						
						<div class="clearfix"></div>
						<div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
							<div class="container" id="service-category">
								<div class="row">
									<div class="col-md-6">
										<div class="service-level-1 bg-white shadow-sm collapsed aussenaufbereitung" data-toggle="collapse" data-target="#collapse1_a" aria-expanded="false" aria-controls="collapse1_a" style="background: url({{ asset('assets/frontend/images/car-cosmetic.jpg')}}) no-repeat center / cover;" role="button">
											<div class="service-title-wrap">
												<div class="media">
													<div class="avatar-md mb-3 mr-3">
														<img src="{{ asset('assets/frontend/images/car-exterior-icon.png')}}" alt="" class="img-fluid">
													</div>
													<div class="media-body">
														<h4 class="h3 font-weight-bold mb-0">Aussenaufbereitung</h4>
														<p>3 Items</p>
													</div>
												</div>
												<div class="text-primary text-right btn-link">Weiterlesen</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="service-level-1 bg-white shadow-sm collapsed Innenaufbereitung" data-toggle="collapse" data-target="#collapse1_b" aria-expanded="false" aria-controls="collapse1_b" style="background: url({{ asset('assets/frontend/images/img-car-3.jpg')}}) no-repeat center / cover;" role="button">
											<div class="service-title-wrap">
												<div class="media">
													<div class="avatar-md mb-3 mr-3">
														<img src="{{ asset('assets/frontend/images/car-interior-icon.png')}}" alt="" class="img-fluid">
													</div>
													<div class="media-body">
														<h4 class="h3 font-weight-bold mb-0">Innenaufbereitung</h4>
														<p>3 Items</p>
													</div>
												</div>
												<div class="text-primary text-right btn-link">Weiterlesen</div>
											</div>
										</div>
									</div>
								</div>
								
								<div id="collapse1_a_parent" class="mt-3">
									<div id="collapse1_a" class="collapse" data-parent="#service-category">
										<div id="service-category-2" class="care-for-carousel owl-carousel owl-theme">
											<div class="item pb-4">
												<div class="bg-white shadow-sm collapsed aussenaufbereitung_a" data-toggle="collapse" data-target="#collapse1_a_1" aria-expanded="false" aria-controls="collapse1_a_1" role="button">
													<div class="ratio ratio-16X9">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
													</div>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<h5 class="font-weight-bold h4 pt-3 m-0">Aussenaufbereitung</h5>
														<p class="h6 py-2">Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und...
															<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
														</p>
													</div>
												</div>
											</div>
											<div class="item pb-4">
												<div class="bg-white shadow-sm collapsed swissvaxFahrzeugaufbereitung" data-toggle="collapse" data-target="#collapse1_a_2" aria-expanded="false" aria-controls="collapse1_a_2" role="button">
													<div class="ratio ratio-16X9">
														<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
													</div>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<h5 class="font-weight-bold h4 pt-3 m-0">swissvax fahrzeugaufbereitung</h5>
														<p class="h6 py-2">Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem...
															<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
														</p>
													</div>
												</div>
											</div>
											<div class="item pb-4">
												<div class="bg-white shadow-sm collapsed lackpflege" data-toggle="collapse" data-target="#collapse1_a_3" aria-expanded="false" aria-controls="collapse1_a_3" role="button">
													<div class="ratio ratio-16X9">	
														<img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="" class="img-fluid">
													</div>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<h5 class="font-weight-bold h4 pt-3 m-0">lackpflege</h5>
														<p class="h6 py-2">In unserem Swissvax Car Care Center Liechtenstein/Rheintal ..
															<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
														</p>
													</div>
												</div>
											</div>
											<div class="item pb-4">
												<div class="bg-white shadow-sm collapsed metallpflege" data-toggle="collapse" data-target="#collapse1_a_4" aria-expanded="false" aria-controls="collapse1_a_4" role="button">
													<div class="ratio ratio-16X9">	
														<img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="" class="img-fluid">
													</div>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<h5 class="font-weight-bold h4 pt-3 m-0">metallpflege</h5>
														<p class="h6 py-2">In unserem Swissvax Car Care Center Liechtenstein/Rheintal ..
															<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
														</p>
													</div>
												</div>
											</div>
										</div>

										<div id="collapse1_a_1" class="collapse" data-parent="#collapse1_a_parent">
											<div class="shadow-sm bg-white p-3">
												<div class="section-car-ceramic">
													<div class="container">
														<div class="row">
															<div class="left col-sm-12 col-md-8">
																<p>
																Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und ist absolut waschstrassenfest. Besonders geeignet ist unsere Keramikbeschichtung für dunkle, grosse Autos, welche im Alltagseinsatz stehen. Jede Keramikbeschichtung erhält ein Zertifikat, welches die Haltbarkeit von Minimum 3 Jahren garantiert.
																</p>
																<ul>
																	<li class="circle">Der panzerglasartige Lackschutz</li>
																	<li class="circle">Haltbarkeit 36+ Monate</li>
																	<li class="circle">Waschstrassenfest</li>
																</ul>
															</div>
															<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="autokosmetik"> 
															</div>
														</div>
													</div>
												</div>
												<div class="services2-car-bg-2">
													<div class="container">
														<div class="row align-items-center">
															<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
															</div>
															<div class="col-sm-12 col-md-7">
																<div class="ceramic-coating-pading">
																	<h1>Unser neues Keramik-Material ServFaces</h1>
																	<p>In den ersten 3 Jahren unserer Arbeit mit sogenannten Keramikbeschichtungen, haben wir mit einem fernöstlichen Produkt gearbeitet. Bis wir feststellen mussten, dass sich das Material auf einmal anders verhält  und auch anders zu verarbeiten ist. Für unsere Firma ein absolutes "NO GO". <br><br> Wir konnten jedoch in kürzester Zeit eine Ersatz-Produktlinie aus deutscher Produktion finden und haben seit nunmehr 2 Jahren nur die besten Erfahrungen gemacht. Und so konnten wir unsere Garantie von 2 auf 3 Jahre erhöhen. Ab Anfang 2016 haben wir deshalb auch die Vertretung von ServFaces für die Schweiz und Deutschland übernommen.</p>
																	<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="ceramic-coting-images">
													<div class="container">
														<div class="related-projects-carousel owl-carousel owl-theme mt-4">
															<div class="item">
																<a href="{{asset('assets/frontend/images/auto-1.jpg')}}" data-fancybox="gallery">
																	<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-1.jpg') }}" alt="autokosmetik">																
																</a>
															</div>
															<div class="item">
																<a href="{{asset('assets/frontend/images/auto-2.jpg')}}" data-fancybox="gallery">
																	<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-2.jpg') }}" alt="autokosmetik">																
																</a>
															</div>
															<div class="item">
																<a href="{{asset('assets/frontend/images/auto-3.jpg')}}" data-fancybox="gallery">
																	<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-3.jpg') }}" alt="autokosmetik">																
																</a>
															</div>
															<div class="item">
																<a href="{{asset('assets/frontend/images/auto-4.jpg')}}" data-fancybox="gallery">
																	<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-4.jpg') }}" alt="autokosmetik">																
																</a>
															</div>
															<div class="item">
																<a href="{{asset('assets/frontend/images/auto-5.jpg')}}" data-fancybox="gallery">
																	<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-5.jpg') }}" alt="autokosmetik">																
																</a>
															</div>
															<div class="item">
																<a href="{{asset('assets/frontend/images/auto-6.jpg')}}" data-fancybox="gallery">
																	<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-6.jpg') }}" alt="autokosmetik">																
																</a>
															</div>
															<div class="item">
																<a href="{{asset('assets/frontend/images/auto-7.jpg')}}" data-fancybox="gallery">
																	<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-7.jpg') }}" alt="autokosmetik">																
																</a>
															</div>
															<div class="item">
																<a href="{{asset('assets/frontend/images/auto-8.jpg')}}" data-fancybox="gallery">
																	<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-8.jpg') }}" alt="autokosmetik">																
																</a>
															</div>
															<div class="item">
																<a href="{{asset('assets/frontend/images/auto-9.jpg')}}" data-fancybox="gallery">
																	<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-9.jpg') }}" alt="autokosmetik">																
																</a>
															</div>
															
														</div>
														<div class="loadmore-btn col-12 text-center mt-4">
															<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
														</div>
														
													</div>
												</div>
											</div>
										</div>
										<div id="collapse1_a_2" class="collapse" data-parent="#collapse1_a_parent">
											<div class="shadow-sm bg-white p-3">
												<div class="section-car-ceramic">
													<div class="container">
														<div class="row">
															<div class="left col-sm-12 col-md-8">
																<p>
																Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und genügt für die meisten Autos. Ausserdem ist sie sehr preiswert. Wir empfehlen jedoch bei der Teflon-Versiegelung Hand- oder Lanzenwäsche, da die Waschstrasse die Versiegelung zu schnell abreibt.
																</p>
																<ul>
																	<li class="circle">Der Standard-Lackschutz</li>
																	<li class="circle">Haltbarkeit 6+ Monate</li>
																	<li class="circle">Preiswert</li>
																</ul>
															</div>
															<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="autokosmetik">
															</div>
														</div>
													</div>
												</div>
												<div class="services2-car-bg-2">
													<div class="container">
														<div class="row align-items-center">
															<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																<img class="w-100" src="{{ asset('assets/frontend/images/seal-2.jpg') }}" alt="autokosmetik">
															</div>
															<div class="col-sm-12 col-md-7">
																<div class="ceramic-coating-pading">
																	<h1>Besonders geeignet für:</h1>
																	<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen. <br> </p>
																	<ul>
																		<li class="circle_2 m-0 h6">alle Autos, Motorräder, Wohnmobile, LKW's usw.</li>
																		<li class="circle_2 m-0 h6">Gebrauchtwagen</li>
																		<li class="circle_2 m-0 h6">Leasingrückgaben</li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="ceramic-coting-images">
													<div class="container">
														<div class="row">
															<!-- Related Project -->
															<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																<div class="item">
																	<a href="{{asset('assets/frontend/images/seal-3.jpg')}}" data-fancybox="gallery1_a_2">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-3.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/seal-4.jpg')}}" data-fancybox="gallery1_a_2">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-4.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/seal-5.jpg')}}" data-fancybox="gallery1_a_2">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-5.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/seal-6.jpg')}}" data-fancybox="gallery1_a_2">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-6.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/seal-7.jpg')}}" data-fancybox="gallery1_a_2">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-7.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/seal-8.png')}}" data-fancybox="gallery1_a_2">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-8.png') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/seal-9.png')}}" data-fancybox="gallery1_a_2">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-9.png') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/seal-10.png')}}" data-fancybox="gallery1_a_2">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-10.png') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/seal-11.png')}}" data-fancybox="gallery1_a_2">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-11.png') }}" alt="autokosmetik">
																	</a>
																</div>
																
															</div>
															<div class="loadmore-btn col-12 text-center mt-3">
																<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="collapse1_a_3" class="collapse" data-parent="#collapse1_a_parent">
											<div class="shadow-sm bg-white p-3">
												<div class="section-car-ceramic">
													
													<div class="row">
														<div class="left col-sm-12 col-md-8">
															<p> In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. </p>
															<ul>
																<li class="circle">hochwertigste Reinigungsmittel</li>
																<li class="circle">optimale Langzeitpflege</li>
																<li class="circle">ausschliesslich Swissvax-Produkte</li>
															</ul>
															
														</div>
														<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
															<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
														</div>
														
													</div>
													<p class="pt-5">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
													
												</div>
												<div class="services2-car-bg-2">
													<div class="container">
														<div class="row align-items-center">
															<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
															</div>
															<div class="col-sm-12 col-md-7">
																<div class="ceramic-coating-pading">
																	<h1>Swissvax-Saisonalitäten</h1>
																	<ul>
																		<li class="circle">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
																		<li class="circle">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
																		<li class="circle">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
																	</ul>
																	<p>
																		Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
																		Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
																		Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
																	</p>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="ceramic-coting-images">
													<div class="container">
														<div class="row">
															<!-- Related Project -->
															<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-12.jpg')}}" data-fancybox="gallery1_a_3">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-13.jpg')}}" data-fancybox="gallery1_a_3">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-14.jpg')}}" data-fancybox="gallery1_a_3">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
															</div>
															<div class="loadmore-btn col-12 text-center mt-3">
																<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="collapse1_a_4" class="collapse" data-parent="#collapse1_a_parent">
											<div class="shadow-sm bg-white p-3">
												<div class="section-car-ceramic">
													
													<div class="row">
														<div class="left col-sm-12 col-md-8">
															<p> In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. </p>
															<ul>
																<li class="circle">hochwertigste Reinigungsmittel</li>
																<li class="circle">optimale Langzeitpflege</li>
																<li class="circle">ausschliesslich Swissvax-Produkte</li>
															</ul>
															
														</div>
														<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
															<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
														</div>
														
													</div>
													<p class="pt-5">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
													
												</div>
												<div class="services2-car-bg-2">
													<div class="container">
														<div class="row align-items-center">
															<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
															</div>
															<div class="col-sm-12 col-md-7">
																<div class="ceramic-coating-pading">
																	<h1>Swissvax-Saisonalitäten</h1>
																	<ul>
																		<li class="circle">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
																		<li class="circle">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
																		<li class="circle">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
																	</ul>
																	<p>
																		Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
																		Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
																		Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
																	</p>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="ceramic-coting-images">
													<div class="container">
														<div class="row">
															<!-- Related Project -->
															<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-12.jpg')}}" data-fancybox="gallery1_a_3">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-13.jpg')}}" data-fancybox="gallery1_a_3">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-14.jpg')}}" data-fancybox="gallery1_a_3">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
															</div>
															<div class="loadmore-btn col-12 text-center mt-3">
																<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="collapse1_b" class="collapse" data-parent="#service-category">
										<div id="service-category-2" class="care-for-carousel-Innenaufbereitung owl-carousel owl-theme">
											<div class="item pb-4">
												<div class="bg-white shadow-sm collapsed lederpflegeRestauration" data-toggle="collapse" data-target="#collapse1_b_1" aria-expanded="false" aria-controls="collapse1_b_1" role="button">
													<div class="ratio ratio-16X9">
														<img src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="" class="img-fluid">
													</div>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<h5 class="font-weight-bold h4 pt-3 m-0">Lederpflege & Restauration</h5>
														<p class="h6 py-2">Bei einer herkömmlichen Innenaufbereitung arbeiten wir mit einem...
															<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
														</p>
													</div>
												</div>
											</div>
											<div class="item pb-4">
												<div class="bg-white shadow-sm collapsed innenreinigung" data-toggle="collapse" data-target="#collapse1_b_2" aria-expanded="false" aria-controls="collapse1_b_2" role="button">
													<div class="ratio ratio-16X9">
														<img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="" class="img-fluid">
													</div>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<h5 class="font-weight-bold h4 pt-3 m-0">Innenreinigung</h5>
														<p class="h6 py-2">In unserem Swissvax Car Care Center Liechtenstein/Rheintal...
															<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
														</p>
													</div>
												</div>
											</div>
											<div class="item pb-4">
												<div class="bg-white shadow-sm collapsed lederpflege" data-toggle="collapse" data-target="#collapse1_b_3" aria-expanded="false" aria-controls="collapse1_b_3" role="button">
													<div class="ratio ratio-16X9">	
														<img src="{{ asset('assets/frontend/images/special-1.jpg') }}" alt="" class="img-fluid">
													</div>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<h5 class="font-weight-bold h4 pt-3 m-0">lederpflege</h5>
														<p class="h6 py-2">Bei diesen "speziellen Fällen" können wir keine Fixpreise machen, denn...
															<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
														</p>
													</div>
												</div>
											</div>
										</div>
										<div id="collapse1_b_1" class="collapse" data-parent="#collapse1_a_parent">
											<div class="shadow-sm bg-white p-3">
												<div class="section-car-ceramic">
													<div class="container">
														<div class="row">
															<div class="left col-sm-6 col-md-8">
																<p>
																Bei einer herkömmlichen Innenaufbereitung arbeiten wir mit einem neutralen, ökologischen Universalreiniger. Der ganze Innenraum inkl. Kofferraum wird gesaugt, Teppiche und Sitze werden shampooniert bzw. das Leder wird gereinigt und mit einer professionellen Ledermilch eingepflegt.
																</p>
																<ul>
																	<li class="circle">inkl. shampoonieren</li>
																	<li class="circle">Lederreinigung und -pflege</li>
																	<li class="circle">Reinigung mit Tornador</li>
																</ul>
															</div>
															<div class="right ez-animate col-sm-6 col-md-4" data-animation="fadeInRight">
																<img class="img-fluid" src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="autokosmetik">
															</div>
															<div class="col-md-12">
																<p>Bei der sogenannten "Tornadorreinigung" wird mit sehr hohem Luftdruck der Innenraum (ohne Himmel) inkl. Armaturen, allen Verkleidungen, Einstiegen, Fälzen und Schlitzen optimal gereinigt. Zum Abschluss werden die Innenscheiben professionell nur mit Wasser gereinigt. <br>Sitze und Teppiche mit problematischen Flecken werden nach dem Shampoonieren getrockenet und gegebenenfalls nochmals shampooniert oder händisch nachbearbeitet. <br>Anbei finden Sie die komplette Preisliste für die Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
															</div>
														</div>
													</div>
												</div>
												<div class="ceramic-coting-images">
													<div class="container">
														<div class="related-projects-carousel owl-carousel owl-theme mt-4">
															<div class="item">
																<a href="{{ asset('assets/frontend/images/auto-10.jpg') }}" data-fancybox="gallery1_b_1">
																	<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-10.jpg') }}" alt="autokosmetik">
																</a>
															</div>
															<div class="item">
																<a href="{{ asset('assets/frontend/images/auto-11.jpg') }}" data-fancybox="gallery1_b_1">
																	<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-11.jpg') }}" alt="autokosmetik">
																</a>
															</div>															
														</div>
														<div class="loadmore-btn col-12 text-center mt-4">
															<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="collapse1_b_2" class="collapse" data-parent="#collapse1_a_parent">
											<div class="shadow-sm bg-white p-3">
												<div class="section-car-ceramic">
													<div class="container">
														<div class="row">
															<div class="left col-sm-6 col-md-8">
																<p> In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. </p>
																<ul>
																	<li class="circle">hochwertigste Reinigungsmittel</li>
																	<li class="circle">optimale Langzeitpflege</li>
																	<li class="circle">ausschliesslich Swissvax-Produkte</li>
																</ul>
															</div>
															<div class="right ez-animate col-sm-6 col-md-4" data-animation="fadeInRight">
																<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
															</div>
															<div class="clearfix"></div>
															<div class="col-md-12">
																<p class="pt-5">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
															</div>
														</div>
													</div>
												</div>
												<div class="services2-car-bg-2">
													<div class="container">
														<div class="row align-items-center">
															<div class="ez-animate col-sm-6 col-md-4 text-center" data-animation="fadeInLeft">
																<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
															</div>
															<div class="col-sm-6 col-md-8">
																<div class="ceramic-coating-pading">
																	<h1>Swissvax-Saisonalitäten</h1>
																	<ul>
																		<li class="circle">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
																		<li class="circle">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
																		<li class="circle">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
																	</ul>
																	<p>
																		Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
																		Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
																		Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
																	</p>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="ceramic-coting-images">
													<div class="container">
														<div class="row">
															<!-- Related Project -->
															<div class="related-projects-carousel owl-carousel owl-theme mt-4">

																<div class="item" data-fancybox="gallery1_b_2">
																	<a href="{{ asset('assets/frontend/images/auto-12.jpg') }}" data-fancybox="gallery1_b_2">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item" data-fancybox="gallery1_b_2">
																	<a href="{{ asset('assets/frontend/images/auto-13.jpg') }}">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{ asset('assets/frontend/images/auto-14.jpg') }}">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
																	</a>
																</div>																
															</div>
															<div class="loadmore-btn col-12 text-center mt-3">
																<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="collapse1_b_3" class="collapse" data-parent="#collapse1_a_parent">
											<div class="shadow-sm bg-white p-3">
												<div class="section-car-ceramic">
													<div class="container">
														<div class="row">
															<div class="left col-sm-12 col-md-8">
																<p>Bei diesen "speziellen Fällen" können wir keine Fixpreise machen, denn der jeweilige Reinigungsaufwand ist von Fall zu Fall sehr unterschiedlich. Entsprechend arbeiten wir nach dem Standardstundensatz von CHF 97.10 inkl. 10% für Material und inkl. Mwst. </p>
																<ul>
																	<li class="circle">Hundehaare</li>
																	<li class="circle">Geruchsbehandlungen</li>
																	<li class="circle">Schimmel, usw.</li>
																</ul>
															</div>
															<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																<img class="img-fluid" src="{{ asset('assets/frontend/images/special-1.jpg') }}" alt="autokosmetik"> 
															</div>
															<div class="col-md-12 pt-5">
																<p>Ozon-Geruchsbehandlung: Voraussetzung für eine Ozon-Geruchsbehandlung ist die optimale Reinigung mit anschliessender Trocknung. Erst bei einem komplett trockenen Innenraum kann eine wirksame Ozonbehandlung durchgeführt werden. <br>Die Ozonbehandlung verrechnen wir mit CHF 30.-. Teilweise sind aber mehrere Ozonbehandlungen nötig.</p>
															</div>
														</div>
													</div>													
												</div>
												<div class="ceramic-coting-images">
													<div class="container">
														<div class="row">
															<!-- Related Project -->
															<h2 class="h5">Beispiele von extremen Verschmutzungen die unserer Spezialbehandlung bedürfen:</h2>
															<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																<div class="item">
																	<a href="{{ asset('assets/frontend/images/special-2.jpg') }}" data-fancybox="gallery1_b_3">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/special-2.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{ asset('assets/frontend/images/special-3.jpg') }}" data-fancybox="gallery1_b_3">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/special-3.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
															</div>
															<h2 class="h5">Vorher-Nachher Gegenüberstellungen bei Extremfällen:</h2>
															<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																
																<div class="item">
																	<a href="{{ asset('assets/frontend/images/special-4.jpg') }}" data-fancybox="gallery1_b_3">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/special-4.jpg') }}" alt="autokosmetik">																		
																	</a>
																</div>
																<div class="item">
																	<a href="{{ asset('assets/frontend/images/special-5.png') }}" data-fancybox="gallery1_b_3">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/special-5.png') }}" alt="autokosmetik">
																	</a>
																</div>
																
															</div>
															<div class="loadmore-btn col-12 text-center mt-3">
																<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="collapse2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
							<div class="container" id="service-category-collapse-2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="service-level-1 bg-white shadow-sm collapsed Folien" data-toggle="collapse" data-target="#collapse2_a" aria-expanded="false" aria-controls="collapse2_a" style="background: url({{ asset('assets/frontend/images/foil-technology.jpg')}}) no-repeat center / cover;" role="button">
                                            <div class="service-title-wrap">
                                                <div class="media">
                                                    <div class="avatar-md mb-3 mr-3">
                                                        <img src="{{ asset('assets/frontend/images/car-foil.png')}}" alt="" class="img-fluid">
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="h3 font-weight-bold mb-0">Folien</h4>
                                                        <p>2 Items</p>
                                                    </div>
                                                </div>
                                                <div class="text-primary text-right btn-link">Weiterlesen</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="service-level-1 bg-white shadow-sm collapsed beschichtung" data-toggle="collapse" data-target="#collapse2_b" aria-expanded="false" aria-controls="collapse2_b" style="background: url({{ asset('assets/frontend/images/window-tinting.jpg')}}) no-repeat center / cover;" role="button">
                                            <div class="service-title-wrap">
                                                <div class="media">
                                                    <div class="avatar-md mb-3 mr-3">
                                                        <img src="{{ asset('assets/frontend/images/car-coating.png')}}" alt="" class="img-fluid">
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="h3 font-weight-bold mb-0">Beschichtung</h4>
                                                        <p>2 Items</p>
                                                    </div>
                                                </div>
                                                <div class="text-primary text-right btn-link">Weiterlesen</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="folianMainParent" class="col-md-12 mt-3">
                                        <div id="collapse2_a" class="collapse" data-parent="#service-category-collapse-2">
                                            <div id="service-category-2" class="care-for-carousel-folien owl-carousel owl-theme">
												<div class="item pb-4">
                                                    <div class="bg-white shadow-sm scheibentönung" data-toggle="collapse" data-target="#collapse2_a_2" aria-expanded="false" aria-controls="collapse2_a_2" role="button">
                                                        <div class="ratio ratio-16X9">
                                                            <img src="{{ asset('assets/frontend/images/tint-10.jpg') }}" alt="" class="img-fluid">
                                                        </div>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <h5 class="font-weight-bold h4 pt-3 m-0">Scheibentönung</h5>
                                                            <p class="h6 py-2">Eine Scheibentönung sieht nicht nur gut aus, sondern schützt in mehrfacher...
                                                                <a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm steinschlagschutzfolien" data-toggle="collapse" data-target="#collapse2_a_1" aria-expanded="false" aria-controls="collapse2_a_1" role="button">
                                                        <div class="ratio ratio-16X9">
                                                            <img src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="" class="img-fluid">
                                                        </div>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <h5 class="font-weight-bold h4 pt-3 m-0">Steinschlagschutzfolien</h5>
                                                            <p class="h6 py-2">Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschläge...
                                                                <a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="item pb-4">
                                                    <div class="bg-white shadow-sm lackschutz" data-toggle="collapse" data-target="#collapse2_a_3" aria-expanded="false" aria-controls="collapse2_a_3" role="button">
                                                        <div class="ratio ratio-16X9">
                                                            <img src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="" class="img-fluid">
                                                        </div>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <h5 class="font-weight-bold h4 pt-3 m-0">lackschutz</h5>
                                                            <p class="h6 py-2">Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschläge...
                                                                <a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapse2_a_1" class="collapse" data-parent="#folianMainParent">
												<div class="shadow-sm bg-white p-3">
													<div class="section-car-ceramic">
														<div class="container">
															<div class="row">
																<div class="left col-sm-6 col-md-8">
																	<p>Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschläge. Unsere verwendete Profifolie ist sehr dick und dadurch auch ganz besonders widerstandsfähig. Allerdings ist Sie sehr schwer zu verlegen und auch relativ kostspielig.</p>
																	<ul>
																		<li class="circle">Hochtransparent/unsichtbar</li>
																		<li class="circle">Der optimale Schutz gegen Steinschlag</li>
																		<li class="circle">Waschstrassenfest</li>
																	</ul>
																</div>
																<div class="right ez-animate col-sm-6 col-md-4" data-animation="fadeInRight">
																	<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="autokosmetik"> 
																</div>
															</div>
														</div>
													</div>
													<div class="services2-car-bg-2">
														<div class="container">
															<div class="row align-items-center">
																<div class="ez-animate col-sm-6 col-md-4 text-center" data-animation="fadeInLeft">
																	<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-2.jpg') }}" alt="autokosmetik">
																</div>
																<div class="col-sm-6 col-md-8">
																	<div class="ceramic-coating-pading">
																		<h1>Besonders geeignet für:</h1>
																		<p>Die Preise für eine Lackschutzfolie variieren sehr stark und sind abhängig von der Beschaffenheit des Autos und Anzahl jener Stellen, welche mit einer Lackschutzfolie versehen werden sollen.</p>
																		<p>Kommen Sie vorbei, wir unterbreiten Ihnen gerne ein unverbindliches Angebot.</p>
																		<ul>
																			<li class="circle">Ladekante und alle Türkanten</li>
																			<li class="circle">Stossstange und Motorhaube</li>
																			<li class="circle">Generell alle exponierten Stellen</li>
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="ceramic-coting-images">
														<div class="container">
															<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																<div class="item">
																	<a href="{{ asset('assets/frontend/images/paint-3.jpg') }}" data-fancybox="gallery1_b_4">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-3.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{ asset('assets/frontend/images/paint-4.jpg') }}" data-fancybox="gallery1_b_4">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-4.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{ asset('assets/frontend/images/paint-5.jpg') }}" data-fancybox="gallery1_b_4">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-5.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{ asset('assets/frontend/images/paint-6.png') }}" data-fancybox="gallery1_b_4">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-6.png') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{ asset('assets/frontend/images/paint-7.png') }}" data-fancybox="gallery1_b_4">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-7.png') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{ asset('assets/frontend/images/paint-8.png') }}" data-fancybox="gallery1_b_4">
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-8.png') }}" alt="autokosmetik">
																	</a>
																</div>
															</div>
															<div class="loadmore-btn col-12 text-center mt-4">
																<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
															</div>
															
														</div>
													</div>
												</div>
											</div>
											<div id="collapse2_a_2" class="collapse" data-parent="#folianMainParent">
												<div class="shadow-sm bg-white p-3">
													<div class="section-car-ceramic">
														<div class="container">
															<div class="row">
																<div class="left col-sm-12 col-md-8">
																	<p>Eine Scheibentönung sieht nicht nur gut aus, sondern schützt in mehrfacher Hinsicht. Der Innenraum heizt weniger auf, 99% der schädlichen UV-Strahlung werden abgehalten und sie schützt vor neugierigen Blicken.<br> Wir arbeiten ausschliesslich mit Markenfolien der Firma Johnson Window Films, Folie Marathon, und gewähren 7 Jahre Garantie. Jede Scheibentönung erhält ein Zertifikat.</p>
																	<ul>
																		<li class="circle">99%-iger UV-Schutz</li>
																		<li class="circle">Hitzereduktion, Blickschutz</li>
																		<li class="circle">Edle Optik</li>
																	</ul>
																</div>
																<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																	<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-10.jpg') }}" alt="autokosmetik"> 
																</div>
															</div>
														</div>
													</div>
													<div class="services2-car-bg-2">
														<div class="container">
															<div class="row align-items-center">
																<div class="ez-animate col-sm-6 col-md-4 text-center" data-animation="fadeInLeft">
																	<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-11.jpg') }}" alt="autokosmetik">
																</div>
																<div class="col-sm-6 col-md-8">
																	<div class="ceramic-coating-pading">
																		<h1>Besonders geeignet für:</h1>
																		<p>Kofferraum oder auf der Rückbank</p>
																		<p class="pt-3">Anbei finden Sie die komplette Preisliste für fast alle Automarken - und Modelle. Sollte Ihr Auto nicht gelistet sein, bitten wir um ein kurzes Telefonat oder Mail. Wir werden Ihnen umgehend ein Angebot unterbreiten.</p>
																		<ul>
																			<li class="circle">Familienautos</li>
																			<li class="circle">Personen im Fond</li>
																			<li class="circle">Hundeautos</li>
																			<li class="circle">Wertgegenstände im</li>
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="ceramic-coting-images">
														<div class="container">
															<div class="row">
																<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-1.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-1.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-2.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-2.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-3.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-3.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-4.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-4.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-5.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-5.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-6.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-6.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-7.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-7.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-8.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-8.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-9.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-9.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																</div>
																<div class="loadmore-btn col-12 text-center mt-3">
																	<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div id="collapse2_a_3" class="collapse" data-parent="#folianMainParent">
												<div class="shadow-sm bg-white p-3">
													<div class="section-car-ceramic">
														<div class="container">
															<div class="row">
																<div class="left col-sm-12 col-md-8">
																	<p>Eine Scheibentönung sieht nicht nur gut aus, sondern schützt in mehrfacher Hinsicht. Der Innenraum heizt weniger auf, 99% der schädlichen UV-Strahlung werden abgehalten und sie schützt vor neugierigen Blicken.<br> Wir arbeiten ausschliesslich mit Markenfolien der Firma Johnson Window Films, Folie Marathon, und gewähren 7 Jahre Garantie. Jede Scheibentönung erhält ein Zertifikat.</p>
																	<ul>
																		<li class="circle">99%-iger UV-Schutz</li>
																		<li class="circle">Hitzereduktion, Blickschutz</li>
																		<li class="circle">Edle Optik</li>
																	</ul>
																</div>
																<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																	<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-10.jpg') }}" alt="autokosmetik"> 
																</div>
															</div>
														</div>
													</div>
													<div class="services2-car-bg-2">
														<div class="container">
															<div class="row align-items-center">
																<div class="ez-animate col-sm-6 col-md-4 text-center" data-animation="fadeInLeft">
																	<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-11.jpg') }}" alt="autokosmetik">
																</div>
																<div class="col-sm-6 col-md-8">
																	<div class="ceramic-coating-pading">
																		<h1>Besonders geeignet für:</h1>
																		<p>Kofferraum oder auf der Rückbank</p>
																		<p class="pt-3">Anbei finden Sie die komplette Preisliste für fast alle Automarken - und Modelle. Sollte Ihr Auto nicht gelistet sein, bitten wir um ein kurzes Telefonat oder Mail. Wir werden Ihnen umgehend ein Angebot unterbreiten.</p>
																		<ul>
																			<li class="circle">Familienautos</li>
																			<li class="circle">Personen im Fond</li>
																			<li class="circle">Hundeautos</li>
																			<li class="circle">Wertgegenstände im</li>
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="ceramic-coting-images">
														<div class="container">
															<div class="row">
																<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-1.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-1.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-2.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-2.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-3.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-3.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-4.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-4.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-5.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-5.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-6.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-6.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-7.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-7.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-8.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-8.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{ asset('assets/frontend/images/tint-9.jpg') }}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-9.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																</div>
																<div class="loadmore-btn col-12 text-center mt-3">
																	<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
                                        </div>
										<div id="collapse2_b" class="collapse" data-parent="#service-category-collapse-2">
											<div id="service-category-2" class="care-for-carousel-Beschichtung owl-carousel owl-theme">
												<div class="item pb-4">
													<div class="bg-white shadow-sm keramikbeschichtung" data-toggle="collapse" data-target="#collapse2_b_1" aria-expanded="false" aria-controls="collapse2_b_1" role="button">
														<div class="ratio ratio-16X9">
															<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
														</div>
														<div class="px-2 position-relative">
															<a href="{{ route('bookappointment')}}" role="button">
																<div class="align-items-center bg-white d-flex m-0 btn-service">
																	<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																	<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																</div>
															</a>
															<h5 class="font-weight-bold h4 pt-3 m-0">Keramikbeschichtung</h5>
															<p class="h6 py-2">Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und...
																<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
															</p>
														</div>
													</div>
												</div>
												<!-- <div class="item pb-4">
													<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_b_2" aria-expanded="false" aria-controls="collapse2_b_2" role="button">
														<div class="ratio ratio-16X9">
															<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
														</div>
														<div class="px-2 position-relative">
															<a href="{{ route('bookappointment')}}" role="button">
																<div class="align-items-center bg-white d-flex m-0 btn-service">
																	<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																	<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																</div>
															</a>
															<h5 class="font-weight-bold h4 pt-3 m-0">Teflonversiegelung</h5>
															<p class="h6 py-2">Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und ...
																<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
															</p>
														</div>
													</div>
												</div> -->
											</div>
											<div id="collapse2_b_1" class="collapse" data-parent="#folianMainParent">
												<div class="shadow-sm bg-white p-3">
													<div class="section-car-ceramic">
														<div class="container">
															<div class="row">
																<div class="left col-sm-12 col-md-8">
																	<p>
																	Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und ist absolut waschstrassenfest. Besonders geeignet ist unsere Keramikbeschichtung für dunkle, grosse Autos, welche im Alltagseinsatz stehen. Jede Keramikbeschichtung erhält ein Zertifikat, welches die Haltbarkeit von Minimum 3 Jahren garantiert.
																	</p>
																	<ul>
																		<li class="circle">Der panzerglasartige Lackschutz</li>
																		<li class="circle">Haltbarkeit 36+ Monate</li>
																		<li class="circle">Waschstrassenfest</li>
																	</ul>
																</div>
																<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																	<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="autokosmetik"> 
																</div>
															</div>
														</div>
													</div>
													<div class="services2-car-bg-2">
														<div class="container">
															<div class="row align-items-center">
																<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																	<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
																</div>
																<div class="col-sm-12 col-md-7">
																	<div class="ceramic-coating-pading">
																		<h1>Unser neues Keramik-Material ServFaces</h1>
																		<p>In den ersten 3 Jahren unserer Arbeit mit sogenannten Keramikbeschichtungen, haben wir mit einem fernöstlichen Produkt gearbeitet. Bis wir feststellen mussten, dass sich das Material auf einmal anders verhält  und auch anders zu verarbeiten ist. Für unsere Firma ein absolutes "NO GO". <br><br> Wir konnten jedoch in kürzester Zeit eine Ersatz-Produktlinie aus deutscher Produktion finden und haben seit nunmehr 2 Jahren nur die besten Erfahrungen gemacht. Und so konnten wir unsere Garantie von 2 auf 3 Jahre erhöhen. Ab Anfang 2016 haben wir deshalb auch die Vertretung von ServFaces für die Schweiz und Deutschland übernommen.</p>
																		<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="ceramic-coting-images">
														<div class="container">
															<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-1.jpg')}}"  data-fancybox="gallery" >
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-1.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-2.jpg')}}"  data-fancybox="gallery" >
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-2.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-3.jpg')}}"  data-fancybox="gallery" >
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-3.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a rel="lack" href="{{ asset('assets/frontend/images/auto-4.jpg') }}"  data-fancybox="gallery" >
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-4.jpg') }}" alt="autokosmetik">
																		
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-5.jpg')}}"  data-fancybox="gallery" >
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-5.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-6.jpg')}}"  data-fancybox="gallery" >
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-6.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-7.jpg')}}"  data-fancybox="gallery" >
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-7.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-8.jpg')}}"  data-fancybox="gallery" >
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-8.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																<div class="item">
																	<a href="{{asset('assets/frontend/images/auto-9.jpg')}}"  data-fancybox="gallery" >
																		<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-9.jpg') }}" alt="autokosmetik">
																	</a>
																</div>
																
															</div>
															<div class="loadmore-btn col-12 text-center mt-4">
																<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
															</div>
															
														</div>
													</div>
												</div>
											</div>
											<!-- <div id="collapse2_b_2" class="collapse" data-parent="#folianMainParent">
												<div class="shadow-sm bg-white p-3">
													<div class="section-car-ceramic">
														<div class="container">
															<div class="row">
																<div class="left col-sm-12 col-md-8">
																	<p>
																	Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und genügt für die meisten Autos. Ausserdem ist sie sehr preiswert. Wir empfehlen jedoch bei der Teflon-Versiegelung Hand- oder Lanzenwäsche, da die Waschstrasse die Versiegelung zu schnell abreibt.
																	</p>
																	<ul>
																		<li class="circle">Der Standard-Lackschutz</li>
																		<li class="circle">Haltbarkeit 6+ Monate</li>
																		<li class="circle">Preiswert</li>
																	</ul>
																</div>
																<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																	<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="autokosmetik">
																</div>
															</div>
														</div>
													</div>
													<div class="services2-car-bg-2">
														<div class="container">
															<div class="row align-items-center">
																<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																	<img class="w-100" src="{{ asset('assets/frontend/images/seal-2.jpg') }}" alt="autokosmetik">
																</div>
																<div class="col-sm-12 col-md-7">
																	<div class="ceramic-coating-pading">
																		<h1>Besonders geeignet für:</h1>
																		<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen. <br> </p>
																		<ul>
																			<li class="circle_2 m-0 h6">alle Autos, Motorräder, Wohnmobile, LKW's usw.</li>
																			<li class="circle_2 m-0 h6">Gebrauchtwagen</li>
																			<li class="circle_2 m-0 h6">Leasingrückgaben</li>
																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="ceramic-coting-images">
														<div class="container">
															<div class="row">
																<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																	<div class="item">
																		<a  data-fancybox="gallery1_b_5" href="{{ asset('assets/frontend/images/seal-3.jpg') }}">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-3.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a  data-fancybox="gallery1_b_5" href="{{asset('assets/frontend/images/seal-4.jpg')}}">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-4.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a  data-fancybox="gallery1_b_5" href="{{asset('assets/frontend/images/seal-5.jpg')}}">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-5.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a  data-fancybox="gallery1_b_5" href="{{asset('assets/frontend/images/seal-6.jpg')}}">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-6.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a  data-fancybox="gallery1_b_5" href="{{asset('assets/frontend/images/seal-7.jpg')}}">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-7.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a  data-fancybox="gallery1_b_5" href="{{asset('assets/frontend/images/seal-8.png')}}">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-8.png') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a  data-fancybox="gallery1_b_5" href="{{asset('assets/frontend/images/seal-9.png')}}">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-9.png') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a  data-fancybox="gallery1_b_5" href="{{asset('assets/frontend/images/seal-10.png')}}">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-10.png') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a  data-fancybox="gallery1_b_5" href="{{asset('assets/frontend/images/seal-11.png')}}">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-11.png') }}" alt="autokosmetik">
																		</a>
																	</div>
																</div>
																<div class="loadmore-btn col-12 text-center mt-3">
																	<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div> -->
										</div>
									</div>
                                </div>
                            </div>
						</div>
						<div id="collapse3" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
							<div id="service-category-collapse-3" class="container">
								<div class="Design-carousel owl-carousel owl-theme">
									<div class="item">
										<div class="service-level-1 bg-white shadow-sm collapsed komplettfolierung" data-toggle="collapse" data-target="#collapse3_a" aria-expanded="false" aria-controls="collapse3_a" style="background: url({{ asset('assets/frontend/images/car-cosmetic.jpg')}}) no-repeat center / cover;" role="button">
											<div class="service-title-wrap">
												<div class="media">
													<div class="avatar-md mb-3 mr-3">
														<img src="{{ asset('assets/frontend/images/complete-foil.png')}}" alt="" class="img-fluid">
													</div>
													<div class="media-body">
														<h4 class="h3 font-weight-bold mb-0">Komplettfolierung</h4>
														<p>2 Items</p>
													</div>
												</div>
												<div class="text-primary text-right btn-link">Weiterlesen</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="service-level-1 bg-white shadow-sm collapsed" data-toggle="collapse" data-target="#collapse3_b" aria-expanded="false" aria-controls="collapse3_b" style="background: url({{ asset('assets/frontend/images/car-cosmetic.jpg')}}) no-repeat center / cover;" role="button">
											<div class="service-title-wrap">
												<div class="media">
													<div class="avatar-md mb-3 mr-3">
														<img src="{{ asset('assets/frontend/images/partial-foil.png')}}" alt="" class="img-fluid">
													</div>
													<div class="media-body">
														<h4 class="h3 font-weight-bold mb-0">Teilfolierungen</h4>
														<p>2 Items</p>
													</div>
												</div>
												<div class="text-primary text-right btn-link">Weiterlesen</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="service-level-1 bg-white shadow-sm collapsed" style="background: url({{ asset('assets/frontend/images/window-filming.png')}}) no-repeat center / cover;" role="button">
											<div class="service-title-wrap">
												<div class="media">
													<div class="avatar-md mb-3 mr-3">
														<img src="{{ asset('assets/frontend/images/car-door.png')}}" alt="" class="img-fluid">
													</div>
													<div class="media-body">
														<h4 class="h3 font-weight-bold mb-0">Fensterfolierung</h4>
														<p>0 Items</p>
													</div>
												</div>
												<div class="text-primary text-right btn-link">Weiterlesen</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="service-level-1 bg-white shadow-sm collapsed" style="background: url({{ asset('assets/frontend/images/car-care-bg.png')}}) no-repeat center / cover;" role="button">
											<div class="service-title-wrap">
												<div class="media">
													<div class="avatar-md mb-3 mr-3">
														<img src="{{ asset('assets/frontend/images/car-interior-icon.png')}}" alt="" class="img-fluid">
													</div>
													<div class="media-body">
														<h4 class="h3 font-weight-bold mb-0">Interieur</h4>
														<p>0 Items</p>
													</div>
												</div>
												<div class="text-primary text-right btn-link">Weiterlesen</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="service-level-1 bg-white shadow-sm collapsed" style="background: url({{ asset('assets/frontend/images/car-labeling.jpg')}}) no-repeat center / cover;" role="button">
											<div class="service-title-wrap">
												<div class="media">
													<div class="avatar-md mb-3 mr-3">
														<img src="{{ asset('assets/frontend/images/label.png')}}" alt="" class="img-fluid">
													</div>
													<div class="media-body">
														<h4 class="h3 font-weight-bold mb-0">Beschriftung</h4>
														<p>0 Items</p>
													</div>
												</div>
												<div class="text-primary text-right btn-link">Weiterlesen</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
                                    <div id="DesignMainParent" class="col-md-12 mt-3">
                                        <div id="collapse3_a" class="collapse" data-parent="#service-category-collapse-3">
                                            <div id="service-category-2" class="care-for-carousel-Komplettfolierung owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm collapsed" data-toggle="collapse" data-target="#collapse3_a_1" aria-expanded="false" aria-controls="collapse3_a_1" role="button">
                                                        <div class="ratio ratio-16X9">
                                                            <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
                                                        </div>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <h5 class="font-weight-bold h4 pt-3 m-0">Mono</h5>
                                                            <p class="h6 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda ...
                                                                <a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm collapsed" data-toggle="collapse" data-target="#collapse3_a_2" aria-expanded="false" aria-controls="collapse3_a_2" role="button">
                                                        <div class="ratio ratio-16X9">
                                                            <img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
                                                        </div>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <h5 class="font-weight-bold h4 pt-3 m-0">Design</h5>
                                                            <p class="h6 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda ...
                                                                <a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapse3_a_1" class="collapse" data-parent="#DesignMainParent">
												<div class="shadow-sm bg-white p-3">
													<div class="section-car-ceramic">
														<div class="container">
															<div class="row">
																<div class="left col-sm-12 col-md-12">
																	<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur fugit iste, fuga ea, quae dolore hic facere consequatur vitae repudiandae laboriosam? Laudantium est aut expedita cumque placeat. Neque, dicta doloribus</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div id="collapse3_a_2" class="collapse" data-parent="#DesignMainParent">
												<div class="shadow-sm bg-white p-3">
													<div class="section-car-ceramic">
														<div class="container">
															<div class="row">
																<div class="left col-sm-12 col-md-12">
																	<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. In odio quibusdam, numquam consequuntur eum quam, fugit quis ut eveniet id corporis dolores voluptates aspernatur quos quo necessitatibus, pariatur assumenda dolorem.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
                                        </div>
                                        
							
										<div id="collapse3_b" class="collapse" data-parent="#service-category-collapse-3">
                                            <div id="service-category-2" class="care-for-carousel-Teilfolierungen owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_b_1" aria-expanded="false" aria-controls="collapse3_b_1" role="button">
                                                        <div class="ratio ratio-16X9">
                                                            <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
                                                        </div>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <h5 class="font-weight-bold h4 pt-3 m-0">Mono</h5>
                                                            <p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...
                                                                <a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_b_2" aria-expanded="false" aria-controls="collapse3_b_2" role="button">
                                                        <div class="ratio ratio-16X9">
                                                            <img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
                                                        </div>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <h5 class="font-weight-bold h4 pt-3 m-0">Design</h5>
                                                            <p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...
                                                                <a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div id="collapse3_b_1" class="collapse" data-parent="#DesignMainParent">
												<div class="shadow-sm bg-white p-3">
													<div class="section-car-ceramic">
														<div class="container">
															<div class="row">
																<div class="left col-sm-12 col-md-12">
																	<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nesciunt delectus maiores inventore quidem repellat. Natus consequatur illo blanditiis ducimus, impedit eius. Molestiae, dolorem. Quibusdam aperiam eos sapiente harum distinctio adipisci.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div id="collapse3_b_2" class="collapse" data-parent="#DesignMainParent">
												<div class="shadow-sm bg-white p-3">
													<div class="section-car-ceramic">
														<div class="container">
															<div class="row">
																<div class="left col-sm-12 col-md-12">
																	<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis quam accusantium itaque nobis reprehenderit minus laborum molestiae velit esse cupiditate provident, unde excepturi voluptatum consequuntur, autem optio quos eveniet vitae.</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
                                        </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="services-mobile">
				<section class="mt-lg-5">
					<h3 class="h2 text-center mb-3 mb-lg-5">Unser Serviceangebot</h3>
					<div class="tabpanel-service main-tabpanel-service">
						<ul class="nav justify-content-start nav-pills mb-3" id="pills-tab" role="tablist">
							<li class="nav-item" role="presentation">
							<button class="nav-link active bg-white" data-toggle="pill" data-target="#pills-care" type="button" role="tab" aria-controls="pills-care" aria-selected="false">
										<div class="service-level-1 car-care-icon bg-white p-3" role="button" data-class1="service-first">
											<div class="service-title-wrap">
												<div class="avatar-sm mb-3">
													<img src="{{ asset('assets/frontend/images/car-care.png')}}" alt="" class="img-fluid">
												</div>
												<h4 class="h6 text-center font-weight-bold mb-0 text-body">Pflege</h4>
											</div>
										</div>
									</button>
							</li>
							<li class="nav-item" role="presentation">
							<button class="nav-link bg-white" data-toggle="pill" data-target="#pills-protect" type="button" role="tab" aria-controls="pills-protect" aria-selected="false">
										<div class="service-level-1 car-care-icon bg-white p-3" role="button" data-class1="service-first">
											<div class="service-title-wrap">
												<div class="avatar-sm mb-3">
													<img src="{{ asset('assets/frontend/images/car-protect.png')}}" alt="" class="img-fluid">
												</div>
												<h4 class="h6 text-center font-weight-bold mb-0 text-body">Schutz</h4>
											</div>
										</div>
									</button>
							</li>
							<li class="nav-item" role="presentation">
							<button class="nav-link bg-white" data-toggle="pill" data-target="#pills-enhance" type="button" role="tab" aria-controls="pills-enhance" aria-selected="false">
										<div class="service-level-1 car-care-icon bg-white p-3" role="button" data-class1="service-first">
											<div class="service-title-wrap">
												<div class="avatar-sm mb-3">
													<img src="{{ asset('assets/frontend/images/car-foil.png')}}" alt="" class="img-fluid">
												</div>
												<h4 class="h6 text-center font-weight-bold mb-0 text-body">Design</h4>
											</div>
										</div>
									</button>
							</li>
						</ul>
					</div>
					<div class="tabpanel-service">
						<div class="tab-content" id="pills-tabContent">
							<div class="tab-pane fade show active" id="pills-care" role="tabpanel">
								<ul class="nav justify-content-start nav-tabs mb-3" id="pills-tab" role="tablist">
									<li class="nav-item" role="presentation">
										<button class="nav-link active show" data-toggle="pill" data-target="#Aussenaufbereitung" type="button" role="tab" aria-controls="Aussenaufbereitung" aria-selected="false">Aussenaufbereitung</button>
									</li>
									<li class="nav-item" role="presentation">
										<button class="nav-link " data-toggle="pill" data-target="#Innenaufbereitung" type="button" role="tab" aria-controls="Innenaufbereitung" aria-selected="false">Innenaufbereitung</button>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane fade show active" id="Aussenaufbereitung" role="tabpanel">
										<div id="collapse1_a_parent" class="mt-3 accordion px-2">
											<div class="care-for-carousel owl-carousel owl-theme">
												<div class="item pb-4">
													<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_a_1" aria-expanded="false" aria-controls="collapse1_a_1" role="button">
														<div class="ratio ratio-16X9">
															<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
														</div>
														<div class="px-2 position-relative">
															<a href="{{ route('bookappointment')}}" role="button">
																<div class="align-items-center bg-white d-flex m-0 btn-service">
																	<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																	<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																</div>
															</a>
															<h5 class="font-weight-bold h4 pt-3 m-0">Keramikbeschichtung</h5>
															<p class="h6 py-2">Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und...
																<a href="{{ route('ceramic_coating')}}" class="font-weight-bolder">Weiterlesen</a>
															</p>
														</div>
													</div>
												</div>
												<div class="item pb-4">
													<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_a_2" aria-expanded="false" aria-controls="collapse1_a_2" role="button">
														<div class="ratio ratio-16X9">
															<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
														</div>
														<div class="px-2 position-relative">
															<a href="{{ route('bookappointment')}}" role="button">
																<div class="align-items-center bg-white d-flex m-0 btn-service">
																	<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																	<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																</div>
															</a>
															<h5 class="font-weight-bold h4 pt-3 m-0">Teflonversiegelung</h5>
															<p class="h6 py-2">Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem...
																<a href="{{ route('sealing')}}" class="font-weight-bolder">Weiterlesen</a>
															</p>
														</div>
													</div>
												</div>
												<div class="item pb-4">
													<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_a_3" aria-expanded="false" aria-controls="collapse1_a_3" role="button">
														<div class="ratio ratio-16X9">	
															<img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="" class="img-fluid">
														</div>
														<div class="px-2 position-relative">
															<a href="{{ route('bookappointment')}}" role="button">
																<div class="align-items-center bg-white d-flex m-0 btn-service">
																	<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																	<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																</div>
															</a>
															<h5 class="font-weight-bold h4 pt-3 m-0">Swissvax</h5>
															<p class="h6 py-2">In unserem Swissvax Car Care Center Liechtenstein/Rheintal ..
																<a href="{{ route('swissvax')}}" class="font-weight-bolder">Weiterlesen</a>
															</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane fade" id="Innenaufbereitung" role="tabpanel">
										<div id="collapse1_a_parent" class="mt-3">
											<div class=" px-2">
												<div class="care-for-carousel owl-carousel owl-theme">
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_b_1" aria-expanded="false" aria-controls="collapse1_b_1" role="button">
															<div class="ratio ratio-16X9">
																<img src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Herkömmlich</h5>
																<p class="h6 py-2">Bei einer herkömmlichen Innenaufbereitung arbeiten wir mit einem...
																	<a href="{{ route('conventinal') }}" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_b_2" aria-expanded="false" aria-controls="collapse1_b_2" role="button">
															<div class="ratio ratio-16X9">
																<img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Swissvax</h5>
																<p class="h6 py-2">In unserem Swissvax Car Care Center Liechtenstein/Rheintal...
																	<a href="{{ route('swissvax') }}" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_b_3" aria-expanded="false" aria-controls="collapse1_b_3" role="button">
															<div class="ratio ratio-16X9">	
																<img src="{{ asset('assets/frontend/images/special-1.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">SPEZIALFÄLLE</h5>
																<p class="h6 py-2">Bei diesen "speziellen Fällen" können wir keine Fixpreise machen, denn...
																	<a href="{{ route('specialcase') }}" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>	
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="pills-protect" role="tabpanel">
								<ul class="nav justify-content-start nav-tabs mb-3" id="pills-tab" role="tablist">
									<li class="nav-item" role="presentation">
										<button class="nav-link active show" data-toggle="pill" data-target="#Folien" type="button" role="tab" aria-controls="Folien" aria-selected="false">Folien</button>
									</li>
									<li class="nav-item" role="presentation">
										<button class="nav-link " data-toggle="pill" data-target="#Beschichtung" type="button" role="tab" aria-controls="Beschichtung" aria-selected="false">Beschichtung</button>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane fade show active" id="Folien" role="tabpanel">
										<div id="folianMainParent" class="mt-3">
											<div class="px-2">
												<div class="care-for-carousel owl-carousel owl-theme">
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_a_1" aria-expanded="false" aria-controls="collapse2_a_1" role="button">
															<div class="ratio ratio-16X9">
																<img src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Lack</h5>
																<p class="h6 py-2">Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschläge...
																	<a href="{{ route('paint') }}" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_a_2" aria-expanded="false" aria-controls="collapse2_a_2" role="button">
															<div class="ratio ratio-16X9">
																<img src="{{ asset('assets/frontend/images/tint-10.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Scheiben</h5>
																<p class="h6 py-2">Eine Scheibentönung sieht nicht nur gut aus, sondern schützt in mehrfacher...
																	<a href="{{ route('car_window_tinting') }}" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane fade" id="Beschichtung" role="tabpanel">
										<div id="folianMainParent" class="mt-3">
											<div class="care-for-carousel owl-carousel owl-theme">
												<div class="item pb-4">
													<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_b_1" aria-expanded="false" aria-controls="collapse2_b_1" role="button">
														<div class="ratio ratio-16X9">
															<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
														</div>
														<div class="px-2 position-relative">
															<a href="{{ route('bookappointment')}}" role="button">
																<div class="align-items-center bg-white d-flex m-0 btn-service">
																	<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																	<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																</div>
															</a>
															<h5 class="font-weight-bold h4 pt-3 m-0">Keramikbeschichtung</h5>
															<p class="h6 py-2">Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und...
																<a href="{{ route('ceramic_coating') }}" class="font-weight-bolder">Weiterlesen</a>
															</p>
														</div>
													</div>
												</div>
												<div class="item pb-4">
													<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_b_2" aria-expanded="false" aria-controls="collapse2_b_2" role="button">
														<div class="ratio ratio-16X9">
															<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
														</div>
														<div class="px-2 position-relative">
															<a href="{{ route('bookappointment')}}" role="button">
																<div class="align-items-center bg-white d-flex m-0 btn-service">
																	<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																	<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																</div>
															</a>
															<h5 class="font-weight-bold h4 pt-3 m-0">Teflonversiegelung</h5>
															<p class="h6 py-2">Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und ...
																<a href="{{ route('sealing') }}" class="font-weight-bolder">Weiterlesen</a>
															</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="pills-enhance" role="tabpanel">
								<ul class="nav justify-content-start nav-tabs mb-3" id="pills-tab" role="tablist">
									<li class="nav-item" role="presentation">
										<button class="nav-link active show" data-toggle="pill" data-target="#Komplettfolierung" type="button" role="tab" aria-controls="Komplettfolierung" aria-selected="false">Komplettfolierung</button>
									</li>
									<li class="nav-item" role="presentation">
										<button class="nav-link " data-toggle="pill" data-target="#Teilfolierungen" type="button" role="tab" aria-controls="Teilfolierungen" aria-selected="false">Teilfolierungen</button>
									</li>
									<li class="nav-item" role="presentation">
										<button class="nav-link" data-toggle="pill" data-target="#Fensterfolierung" type="button" role="tab" aria-controls="Fensterfolierung" aria-selected="false">Fensterfolierung</button>
									</li>
									<li class="nav-item" role="presentation">
										<button class="nav-link " data-toggle="pill" data-target="#Interieur" type="button" role="tab" aria-controls="Interieur" aria-selected="false">Interieur</button>
									</li>
									<li class="nav-item" role="presentation">
										<button class="nav-link " data-toggle="pill" data-target="#Beschriftung" type="button" role="tab" aria-controls="Teilfolierungen" aria-selected="false">Beschriftung</button>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane fade show active" id="Komplettfolierung" role="tabpanel">
										<div id="service-category-collapse-3">
											<div id="DesignMainParent" class="mt-3">
												<div id="collapse3_a" data-parent="#service-category-collapse-3">
													<div class="care-for-carousel owl-carousel owl-theme">
														<div class="item pb-4">
															<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_a_1" aria-expanded="false" aria-controls="collapse3_a_1" role="button">
																<div class="ratio ratio-16X9">
																	<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
																</div>
																<div class="px-2 position-relative">
																	<a href="{{ route('bookappointment')}}" role="button">
																		<div class="align-items-center bg-white d-flex m-0 btn-service">
																			<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																			<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																		</div>
																	</a>
																	<h5 class="font-weight-bold h4 pt-3 m-0">Mono</h5>
																	<p class="h6 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda ...
																		<a href="{{ route('mono') }}" class="font-weight-bolder">Weiterlesen</a>
																	</p>
																</div>
															</div>
														</div>
														<div class="item pb-4">
															<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_a_2" aria-expanded="false" aria-controls="collapse3_a_2" role="button">
																<div class="ratio ratio-16X9">
																	<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
																</div>
																<div class="px-2 position-relative">
																	<a href="{{ route('bookappointment')}}" role="button">
																		<div class="align-items-center bg-white d-flex m-0 btn-service">
																			<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																			<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																		</div>
																	</a>
																	<h5 class="font-weight-bold h4 pt-3 m-0">Design</h5>
																	<p class="h6 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda ...
																		<a href="{{ route('interior_design') }}" class="font-weight-bolder">Weiterlesen</a>
																	</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="tab-pane fade" id="Teilfolierungen">
										<div id="service-category-2" class="care-for-carousel-Teilfolierungen owl-carousel owl-theme">
											<div class="item pb-4">
												<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_b_1" aria-expanded="false" aria-controls="collapse3_b_1" role="button">
													<div class="ratio ratio-16X9">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
													</div>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<h5 class="font-weight-bold h4 pt-3 m-0">Mono</h5>
														<p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...
															<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
														</p>
													</div>
												</div>
											</div>
											<div class="item pb-4">
												<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_b_2" aria-expanded="false" aria-controls="collapse3_b_2" role="button">
													<div class="ratio ratio-16X9">
														<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
													</div>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<h5 class="font-weight-bold h4 pt-3 m-0">Design</h5>
														<p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...
															<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
														</p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane fade" id="Fensterfolierung">
										<p class="text-center">No Details Found..</p>
									</div>
									<div class="tab-pane fade" id="Interieur">
										<p class="text-center">No Details Found..</p>
									</div>
									<div class="tab-pane fade" id="Beschriftung">
										<p class="text-center">No Details Found..</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</section>
			</div>
			<div id="section-services2" class="section-service-text-color">
				<div class="container">
					<div class="row align-items-center">
						<div class="left col-sm-12 col-md-6">
							<h2 class="h1 text-uppercase">Pflege</h2>
							<p>Autos, Motorräder und LKWs benötigen professionelle Pflege. Als spezialisiertes Unternehmen in diesem Bereich sorgen wir für die perfekte Pflege für Ihr Fahrzeug. Dabei kümmern wir uns sowohl um die Innen- als auch die Aussenaufbereitung.</p>
							<p>Für den Innenraum bieten wir die herkömmliche Innenreinigung inklusive Shampoonieren, Lederpflege und Reinigung mit Tornador, sowie die Pflege mit der Swissvax-Methode.</p>
							<p>Für die Aussenhülle empfehlen wir für die Pflege eine Teflonversiegelung.</p>
						</div>
						<div class="right ez-animate col-sm-12 col-md-6" data-animation="fadeInRight">
							<img class="img-fluid w-75" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
						</div>
					</div>
				</div>
			</div>
			<div class="section-services2 bg-2 bg-dark-autokosmetic">
				<div class="container">
					<div class="row align-items-center">
						<div class="right ez-animate col-sm-12 col-md-6 text-center" data-animation="fadeInLeft">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/window-filming.png') }}" alt="autokosmetik">
						</div>
						<div class="left col-sm-12 col-md-6">
							<h2 class="h1 text-uppercase">Schutz</h2>
							<p class="text-light">Besonderer Schutz für Ihr Verkehrsmittel. Eine volltransparente Lackschutzfolie schützt den darunter liegenden Lack vor Steinschlag und Kratzern. Doch auch eine Keramikbeschichtung verspricht einen hohen Schutz für den Lack. Für die Scheiben empfehlen wir eine Scheibentönung. Die entsprechenden Folien schützen ihre Kinder und Haustiere vor UV-Strahlung und sind ein Blickfang.</p>
							<!-- <p class="m-0 mb-3 text-light">INNENAUFBEREITUNG HERKÖMMLICH</p>
							<ul class="text-light">
								<li>inkl. shampoonieren</li>
								<li>Lederreinigung und -pflege</li>
								<li>Reinigung mit Tornador</li>
							</ul> -->
						</div>
					</div>
				</div>
			</div>
			<div class="section-services2 section-service-text-color">
				<div class="container">
					<div class="row align-items-center">
						<div class="left col-sm-12 col-md-6">
							<h2 class="h1 text-uppercase">Design</h2>
							<p>Eine Teil- oder Vollfolierung sorgt für ein komplett verändertes Äusseres – ganz ohne eine teure Neulackierung. Der Fantasie sind dabei nur wenige Grenzen gesetzt. Neben Matt- und Hochglanzfolien sowie Strukturfolien ist auch eine Carbon-Optik in zahlreichen aufregenden Farben möglich. Häufig nachgefragt werden auch Werbefolien. Neben dem einzigartigen Look für Ihr Fahrzeug, schützen die Folien auch den darunter liegenden Lack.</p>
							<p>Verchromungen sind ein ganz besonderer Hingucker und machen Ihr Fahrzeug einzigartig.</p>
							<p>Apropos einzigartig: Wie wäre es mit einem Interieur-Design ganz nach Ihrem Geschmack? </p>
							<!-- <p class="mb-3">Besonders geeignet für:</p>
							<ul>
								<li>Individualisten</li>
								<li>Showfahrzeuge, die besonders auffallen wollen</li>
								<li>Fahrzeuge, welche für eine</li>
							</ul> -->
						</div>
						<div class="right ez-animate col-sm-12 col-md-6" data-animation="fadeInRight">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/img-services3.png') }}" alt="autokosmetik">
						</div>
					</div>
				</div>
			</div>
			<div id="section-howwework">
				<div class="container">
					<div class="row">
						<div class="col-12 text-center">
							<i class="flaticon-multimedia"></i>
							<h1>How we <strong>Work</strong></h1>
						</div>
					</div>
				</div>
			</div>
			<!-- <div id="section-cta3">
				<div class="container">
					<div class="row ez-animate">
						<div class="title1 col-12" data-aos="fade-up" data-aos-delay="200">
							<h2><span>Immer auf der Suche nach mehr</span> Einzigartige Talente</h2>
							<i class="flaticon-download"></i>
						</div>
						<div class="cta-content col-12" data-aos="fade-up" data-aos-delay="400">
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi sequi tempora totam voluptatem natus maiores consectetur illo nobis quas fugiat commodi, aut ratione quasi blanditiis labore in doloremque quam? Adipisci! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Id ex ratione nesciunt velit! Unde repellendus, ipsam aliquid beatae nihil, aut tempora excepturi blanditiis corrupti libero odio quaerat dolor, optio itaque?</p>
							<a href="{{ route('contact')}}" class="btn-4">In Kontakt kommen</a>
						</div>
					</div>
				</div>
			</div> -->
		</div>
	</div>

@endsection

@section('script')
<script>
	$(document).ready(function () {
		if (localStorage.getItem("targetClass") === "lederpflegeRestauration" || localStorage.getItem("targetClass") === "aussenaufbereitung_a" || localStorage.getItem("targetClass") === "innenreinigung" || localStorage.getItem("targetClass") === "swissvaxFahrzeugaufbereitung" || localStorage.getItem("targetClass") === "lederpflege" || localStorage.getItem("targetClass") === "lackpflege" || localStorage.getItem("targetClass") === "metallpflege") {

			$('.Pflege').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			$('#collapse1').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			
			if(localStorage.getItem("targetClass") === "lederpflegeRestauration"){
				$('.Innenaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_b').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_b_1').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			}
			else if (localStorage.getItem("targetClass") === "aussenaufbereitung_a") { 
				$('.aussenaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a_1').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			}
			else if (localStorage.getItem("targetClass") === "innenreinigung") { 
				$('.innenreinigung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_b').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_b_2').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel-Innenaufbereitung').trigger('to.owl.carousel', 1)
			}
			else if (localStorage.getItem("targetClass") === "swissvaxFahrzeugaufbereitung") { 
				$('.aussenaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#swissvaxFahrzeugaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a_2').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel').trigger('to.owl.carousel', 1)
			}
			else if (localStorage.getItem("targetClass") === "lederpflege") { 
				$('.Innenaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_b').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.lederpflege').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_b_3').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel-Innenaufbereitung').trigger('to.owl.carousel', 2)
			}
			else if (localStorage.getItem("targetClass") === "lackpflege") { 
				$('.aussenaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.lackpflege').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a_3').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel').trigger('to.owl.carousel', 2)
			}
			else if (localStorage.getItem("targetClass") === "metallpflege") { 
				$('.aussenaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.metallpflege').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a_4').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel').trigger('to.owl.carousel', 3)
			}
			localStorage.clear();
		}
		else if (localStorage.getItem("targetClass") === "scheibentönung" || localStorage.getItem("targetClass") === "keramikbeschichtung" || localStorage.getItem("targetClass") === "steinschlagschutzfolien" || localStorage.getItem("targetClass") === "lackschutz") {
			
			$('.Schutz').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			$('#collapse2').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			
			if(localStorage.getItem("targetClass") === "scheibentönung"){
				$('.Folien').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_a_2').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			}
			else if(localStorage.getItem("targetClass") === "keramikbeschichtung"){
				$('.beschichtung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_b').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_b_1').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			}
			else if(localStorage.getItem("targetClass") === "steinschlagschutzfolien"){
				$('.Folien').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_a_1').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel-folien').trigger('to.owl.carousel', 1)
			}
			else if(localStorage.getItem("targetClass") === "lackschutz"){
				$('.Folien').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_a_1').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel-folien').trigger('to.owl.carousel', 2)
			}
			localStorage.clear();
		}

		else if (localStorage.getItem("targetClass") === "fahrzeugbeschriftung") {
			
			$('.design').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			$('#collapse3').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			
			if(localStorage.getItem("targetClass") === "fahrzeugbeschriftung"){
				$('.komplettfolierung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse3_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse3_a_2').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			}
			localStorage.clear();
		}
		
	});
	
</script>

@endsection