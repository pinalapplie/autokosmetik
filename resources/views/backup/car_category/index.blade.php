@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>Car Category Mgmt</h4>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('carcategory.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add new car category</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                @if (session('message'))
                <div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('message') }}
                </div>
                @endif
               <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($car_categorys as $key => $car_category)
                                    <tr data-entry-id="{{ $car_category->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $car_category->title ?? ''}}</td>
                                        <td>
                                            <?php 
                                            $category_name1="";
                                            $category_name2="";
                                            $category_name3="";
                                            if($car_category->car_category_name && $car_category->car_category_name->title !=""){
                                                $category_name1 = $car_category->car_category_name->title;
                                                if($car_category->car_category_name->category_name && $car_category->car_category_name->category_name->title !=""){
                                                    $category_name2 = ' -> '.$car_category->car_category_name->category_name->title;
                                                    if($car_category->car_category_name->category_name->category_name && $car_category->car_category_name->category_name->category_name->title !=""){
                                                        $category_name3 = ' -> '.$car_category->car_category_name->category_name->category_name->title;
                                                    }
                                                }
                                            }
                                            ?>
                                            {{ $category_name1.$category_name2.$category_name3}}
                                        </td>
                                        <td>
                                            @include('partials.switch', ['id'=>
                                            $car_category->id,'is_active'=>$car_category->is_active])
                                        </td>
                                        <td>@include('partials.actions', ['id' => $car_category->id])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
