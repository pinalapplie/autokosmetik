@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>Edit Product</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('products.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                <form action="{{ route('products.update',[$product->id]) }}" method="POST" enctype="multipart/form-data" id="edit_product_form">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ old('title', isset($product->title) ? $product->title : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">Image</label>
                        <input type="file" name="image" class="form-control" id="image" accept="image/x-png,image/gif,image/jpeg">
                        <br>
                        <a href="{{ asset(Storage::url($product->image)) }}" target="_blank">
                            <img src="{{ asset(Storage::url($product->image)) }}" alt="" height=100 width=100>
                        </a>
                    </div>
                    <div class="form-group">
                        <label for="short_description">Short Description</label>
                        <textarea name="short_description" class="form-control" id="short_description" rows="2">
                             {!! $product->short_description !!}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" class="form-control" id="description" rows="5">
                            {!! $product->description !!}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="category">Category</label>
                        <select class="form-control" name="category" id="category">
                              <option value="">Select Category</option>  
                              <option @if($product->category == "APP / MUSIC") selected @endif value="APP / MUSIC">APP / MUSIC</option>  
                              <option @if($product->category == "VIDEO GAME" ) selected @endif  value="VIDEO GAME" >VIDEO GAME</option>  
                              <option @if($product->category == "APP / MOBILE" ) selected @endif value="APP / MOBILE">APP / MOBILE</option>  
                              <option @if($product->category == "WEBSITE" ) selected @endif value="WEBSITE">WEBSITE</option>  
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        CKEDITOR.replace('description');
        CKEDITOR.replace('short_description');
        $("#edit_product_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                title: {
                    required: true
                },
                 description: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement();
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                        return editorcontent.length === 0;
                    }
                },
                short_description: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement();
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                        return editorcontent.length === 0;
                    }
                },
                category: {
                    required: true
                }

            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Title'])",
                },
                description: {
                    required: "@lang('validation.required',['attribute'=>'Description'])",
                },
                short_description: {
                    required: "@lang('validation.required',['attribute'=>'Short Description'])",
                },
                image: {
                    required: "@lang('validation.required',['attribute'=>'Image'])",
                },
                category: {
                    required: "@lang('validation.required',['attribute'=>'Category'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection