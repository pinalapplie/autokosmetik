@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.update') }} {{ trans('cruds.car_condition_type.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('carconditiontype.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('carconditiontype.update',[$carconditiontype->id]) }}" method="POST" enctype="multipart/form-data" id="add_car_type_form">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.car_condition_type.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($carconditiontype->name) ? $carconditiontype->name : '') }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        $("#add_car_type_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection