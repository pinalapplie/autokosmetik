<div class="row align-items-center justify-content-center mx-0">
@foreach ($service_category as $key => $value) 
    <div class="col-auto text-center mt-2">
    	<div class="card select-category collapsed" data-toggle="collapse" data-target="#collapse1" aria-expanded="false">
    		<input role="button" type="radio" role="button" name="step_3" class="form-control checkbox" id="{{$value->name}}" value="{{$value->id}}" g-id="{{$value->is_glass}}">
    		<div class="card-body px-2 py-2">
    			<div class="avatar-lg mb-1">
                    <img src="{{asset(Storage::url($value->image)) ?? ''}}" alt="" class="img-fluid">
    			</div>
                <p class="font-weight-bold h5 text-dark mt-2">{{$value->name}}</p>
    		</div>
    	</div>
    </div>
@endforeach      
</div>     