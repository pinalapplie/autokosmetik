@section('title', 'Zuhause')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">        
        <!-- Page Content  -->
        <div id="main-content" class="active">
            <!-- Revolution Slider 1 -->
            <div id="rev_slider_14_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="autokosmetik" data-source="gallery" style="background:#ffffff;padding:0px;">
				<!-- START REVOLUTION SLIDER 5.4.8.1 fullscreen mode -->
				<div id="rev_slider_14_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8.1">
					<ul>	<!-- SLIDE  -->
						<li data-index="rs-49" data-transition="slidingoverlayright" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="{{ asset('assets/frontend/revslider1/assets/100x50_9e6da-slider1.jpg') }}"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
							<!-- MAIN IMAGE -->
							<img src="{{ asset('assets/frontend/revslider1/assets/9e6da-slider1.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
							<!-- LAYERS -->
							<!-- LAYER NR. 1 -->
							<div class="tp-caption"
								id="slide-49-layer-3"
								data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
								data-y="['middle','middle','middle','middle']" data-voffset="['-82','-82','-72','-47']"
								data-fontsize="['60','60','50','30']"
								data-lineheight="['80','80','70','40']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;"><strong>Autokosmetik</strong>  ist das perfekte </div>
							<!-- LAYER NR. 2 -->
							<div class="tp-caption  "
								id="slide-49-layer-4"
								data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
								data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
								data-fontsize="['60','60','50','30']"
								data-lineheight="['80','80','70','40']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 6; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;">für alle <strong>Typen</strong> </div>
							<!-- LAYER NR. 3 -->
							<div class="tp-caption"
								id="slide-49-layer-5"
								data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
								data-y="['middle','middle','middle','middle']" data-voffset="['81','83','70','45']"
								data-fontsize="['60','60','50','30']"
								data-lineheight="['80','80','70','40']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;">von <strong>Lösungen</strong> </div>
							<!-- LAYER NR. 4 -->
							<div class="tp-caption  "
								id="slide-49-layer-7"
								data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
								data-y="['middle','middle','middle','middle']" data-voffset="['-143','-147','-138','-91']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":10,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 8; white-space: nowrap; font-size: 48px; line-height: 48px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;"><i class="pe-7s-glasses"></i> </div>
							<!-- LAYER NR. 5 -->
							<a class="tp-caption scroll-down"
								href="#section-aboutus1" target="_self"	id="slide-49-layer-9"
								data-x="['right','right','right','right']" data-hoffset="['-38','-38','-38','-38']"
								data-y="['middle','middle','middle','middle']" data-voffset="['188','196','173','136']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-actions=''
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;rZ:-90;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 9; white-space: nowrap; font-size: 14px; line-height: 100px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;text-decoration: none;"> Runterscrollen  </a>
						</li>
						<!-- SLIDE  -->
						<li data-index="rs-68" data-transition="slidingoverlayright" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="assets/revslider1/assets/100x50_93b58-slider2.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
							<!-- MAIN IMAGE -->
							<img src="{{ asset('assets/frontend/revslider1/assets/93b58-slider2.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
							<!-- LAYERS -->
							<!-- LAYER NR. 6 -->
							<div class="tp-caption"
								id="slide-68-layer-3"
								data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
								data-y="['middle','middle','middle','middle']" data-voffset="['-82','-82','-72','-47']"
								data-fontsize="['60','60','50','30']"
								data-lineheight="['80','80','70','40']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;"><strong>Autokosmetik</strong> die Agentur</div>
							<!-- LAYER NR. 7 -->
							<div class="tp-caption"
								id="slide-68-layer-4"
								data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
								data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
								data-fontsize="['60','60','50','30']"
								data-lineheight="['80','80','70','40']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 6; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;">kreativ <strong>und innovativ</strong> </div>
							<!-- LAYER NR. 8 -->
							<div class="tp-caption"
								id="slide-68-layer-5"
								data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
								data-y="['middle','middle','middle','middle']" data-voffset="['81','83','70','45']"
								data-fontsize="['60','60','50','30']"
								data-lineheight="['80','80','70','40']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;">des gesamten <strong>Netzwerk</strong> </div>
							<!-- LAYER NR. 9 -->
							<div class="tp-caption"
								id="slide-68-layer-7"
								data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
								data-y="['middle','middle','middle','middle']" data-voffset="['-143','-147','-138','-91']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":10,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 8; white-space: nowrap; font-size: 48px; line-height: 48px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;"><i class="pe-7s-glasses"></i> </div>
							<!-- LAYER NR. 10 -->
							<a class="tp-caption scroll-down"
								href="#section-aboutus1" target="_self"	id="slide-68-layer-9"
								data-x="['right','right','right','right']" data-hoffset="['-38','-38','-38','-38']"
								data-y="['middle','middle','middle','middle']" data-voffset="['188','196','173','136']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-actions=''
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;rZ:-90;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 9; white-space: nowrap; font-size: 14px; line-height: 100px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;text-decoration: none;"> Runterscrollen  </a>
						</li>
						<!-- SLIDE  -->
						<li data-index="rs-69" data-transition="slidingoverlayright" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="{{ asset('assets/frontend/revslider1/assets/100x50_30d37-slider5.jpg') }}"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
							<!-- MAIN IMAGE -->
							<img src="{{ asset('assets/frontend/revslider1/assets/30d37-slider5.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
							<!-- LAYERS -->
							<!-- LAYER NR. 11 -->
							<div class="tp-caption"
								id="slide-69-layer-3"
								data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
								data-y="['middle','middle','middle','middle']" data-voffset="['-82','-82','-72','-47']"
								data-fontsize="['60','60','50','30']"
								data-lineheight="['80','80','70','40']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;"><strong>Autokosmetik</strong>  ist das perfekte </div>
							<!-- LAYER NR. 12 -->
							<div class="tp-caption"
								id="slide-69-layer-4"
								data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
								data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
								data-fontsize="['60','60','50','30']"
								data-lineheight="['80','80','70','40']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 6; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;">für alle <strong>Typen</strong> </div>
							<!-- LAYER NR. 13 -->
							<div class="tp-caption"
								id="slide-69-layer-5"
								data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
								data-y="['middle','middle','middle','middle']" data-voffset="['81','83','70','45']"
								data-fontsize="['60','60','50','30']"
								data-lineheight="['80','80','70','40']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 80px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;">von <strong>Lösungen</strong> </div>
							<!-- LAYER NR. 14 -->
							<div class="tp-caption"
								id="slide-69-layer-7"
								data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']"
								data-y="['middle','middle','middle','middle']" data-voffset="['-143','-147','-138','-91']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":10,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 8; white-space: nowrap; font-size: 48px; line-height: 48px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;"><i class="pe-7s-glasses"></i> </div>
							<!-- LAYER NR. 15 -->
							<a class="tp-caption scroll-down"
								href="#section-aboutus1" target="_self"			 id="slide-69-layer-9"
								data-x="['right','right','right','right']" data-hoffset="['-38','-38','-38','-38']"
								data-y="['middle','middle','middle','middle']" data-voffset="['188','196','173','136']"
								data-width="none"
								data-height="none"
								data-whitespace="nowrap"
								data-type="text"
								data-actions=''
								data-responsive_offset="on"
								data-responsive="off"
								data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;rZ:-90;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
								data-textAlign="['inherit','inherit','inherit','inherit']"
								data-paddingtop="[0,0,0,0]"
								data-paddingright="[0,0,0,0]"
								data-paddingbottom="[0,0,0,0]"
								data-paddingleft="[0,0,0,0]"
							style="z-index: 9; white-space: nowrap; font-size: 14px; line-height: 100px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Quicksand;text-decoration: none;"> Runterscrollen  </a>
						</li>
					</ul>
				<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
			</div>
			<!-- /.Revolution Slider 1 -->
			<!-- Setion About us -->
			<div class="bg-dark-autokosmetic">
				<div class="container overflow-hidden">
					<div class="row align-items-center">
						<div class="left col-sm-12 col-md-6 mt-4 mt-md-0" data-aos="fade-up" data-aos-anchor-placement="top-center">
							<h6 class="text-white">ÜBER UNS</h6>
							<h1 class="text-white">Willkommen zu <span>Autokosmetik</span> ist ein <span>Spezialisiert</span> Designagentur.</h1>
							<p class="text-light">beim führenden Automobilhersteller im Fürstentum Liechtenstein / Schweiz Rheintal. Unabhängig davon, welches Produkt oder welche Dienstleistung Sie bei uns kaufen, können Sie davon ausgehen, dass Sie eine optimale Qualität erwarten können. Zu einem fairen Preis.
							<br>
							Zögern Sie nicht, uns bei Fragen anzurufen oder einfach bei Gamprin auf einen Kaffee vorbeizuschauen.<span class="text-right"> - Pascal & Rudi Nitzlnader</span>
								<br>
							</p>
							<a href="{{ route('about.us')}}" class="btn-3">Mehr <i class="fa fa-chevron-right"></i></a>
							<br><br>
						</div>
						<div class="right ez-animate col-sm-12 col-md-6" data-aos="fade-left">
							<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/FotoPascal.png') }}" alt="FotoPascal">
						</div>
						<!-- <div class="bottom ez-animate col-12" data-aos="fade-right">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/img-aboutus-2.png') }}" alt="autokosmetik">
							<label>Gründer Autokosmetik </label>
						</div> -->
					</div>
				</div>
			</div>
			<!-- /.Setion About us -->
			<!-- Section miscellaneous 1 -->
			<div class="car-bike-1">
				<div id="section-services1" class="feature-car-area">
					<div class="container">
						<div class="row modal-main service-accordian-main">
							<div class="col-md-12">
								<div class="home-service-title pb-3 text-center">
									<h2 class="h1 pt-4">Unser Serviceangebot</h2>
									<p class="h6"> Wir bieten einen kompletten Service für Autoreparatur und Wartung </p>
								</div>
							</div>
							
							<div class="col-md-4 mb-3">
								<div class="service-level-1 car-care-icon bg-white shadow-sm" style="background: url({{ asset('assets/frontend/images/car-care-bg.png')}}) no-repeat center / cover;" role="button" data-class1="service-first">
									<div class="service-title-wrap">
										<div class="avatar-sm mb-3">
											<img src="{{ asset('assets/frontend/images/car-care.png')}}" alt="" class="img-fluid">
										</div>
										<h4 class="h3 font-weight-bold mb-0">Pflege</h4>
										<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Magni, quidem voluptatibus.</p>
										<!-- <span class="text-primary btn-link">Read more</span> -->
									</div>
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<div class="service-level-1 car-care-icon bg-white shadow-sm" style="background: url({{ asset('assets/frontend/images/car-protect-bg.jpg')}}) no-repeat center / cover;" role="button" data-class1="service-second">
									<div class="service-title-wrap">
										<div class="avatar-sm mb-3">
											<img src="{{ asset('assets/frontend/images/car-protect.png')}}" alt="" class="img-fluid">
										</div>
										<h4 class="h3 font-weight-bold mb-0">Schutz</h4>
										<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Magni, quidem voluptatibus.</p>
										<!-- <span class="text-primary btn-link">Read more</span> -->
									</div>
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<div class="service-level-1 car-care-icon bg-white shadow-sm" style="background: url({{ asset('assets/frontend/images/paint-1.jpg')}}) no-repeat center / cover;" role="button" data-class1="service-third">
									<div class="service-title-wrap">
										<div class="avatar-sm mb-3">
											<img src="{{ asset('assets/frontend/images/car-foil.png')}}" alt="" class="img-fluid">
										</div>
										<h4 class="h3 font-weight-bold mb-0">Design</h4>
										<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Magni, quidem voluptatibus.</p>
										<!-- <span class="text-primary btn-link">Read more</span> -->
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div id="section-miscellaneous1-1" class="bg-white pt-5 page-section-pt feature-car-area">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="home-service-image">
									<img src="{{ asset('assets/frontend/images/car-service.png') }}" class="w-100 img-fluid" alt="autokosmetik">
								</div>
								<div class="tooltip-main service-first second">
									<div class="tooltip">
										<div class="info-main info-wash">
											<div class="info modal_animation_main custom_modal_right_info">
												<h5 class="text-uppercase">KERAMIKBESCHICHTUNG</h5>
												<ul class="m-0 pl-4">
													<li>Der panzerglasartige Lackschutz</li>
													<li>Haltbarkeit 36+ Monate</li>
													<li>Waschstrassenfest</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom custom_modal_right">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-6">
																			<p class="h6">Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und ist absolut waschstrassenfest. Besonders geeignet ist unsere Keramikbeschichtung für dunkle, grosse Autos, welche im Alltagseinsatz stehen. Jede Keramikbeschichtung erhält ein Zertifikat, welches die Haltbarkeit von Minimum 3 Jahren garantiert.</p>
																			<ul class="m-0">
																				<li class="circle m-0 h6">Der panzerglasartige Lackschutz</li>
																				<li class="circle m-0 h6">Haltbarkeit 36+ Monate</li>
																				<li class="circle m-0 h6">Waschstrassenfest</li>
																			</ul>
																		</div>
																		<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2 py-5">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">Unser neues Keramik-Material ServFaces</h1>
																				<p class="h6">In den ersten 3 Jahren unserer Arbeit mit sogenannten Keramikbeschichtungen, haben wir mit einem fernöstlichen Produkt gearbeitet. Bis wir feststellen mussten, dass sich das Material auf einmal anders verhält  und auch anders zu verarbeiten ist. Für unsere Firma ein absolutes "NO GO". Wir konnten jedoch in kürzester Zeit eine Ersatz-Produktlinie aus deutscher Produktion finden und haben seit nunmehr 2 Jahren nur die besten Erfahrungen gemacht. Und so konnten wir unsere Garantie von 2 auf 3 Jahre erhöhen. Ab Anfang 2016 haben wir deshalb auch die Vertretung von ServFaces für die Schweiz und Deutschland übernommen.</p>
																				<p class="pt-3 h6">Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-1.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-2.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-3.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-4.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-5.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-6.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-7.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-8.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-9.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="info modal_animation_main custom_modal_right_info">
												<h5 class="text-uppercase">NACH SWISSVAX</h5>
												<!-- <ul class="m-0 pl-4">
													<li>Preiswert</li>
													<li>Haltbarkeit 36+ Monate</li>
													<li>Autowaschfest</li>
												</ul> -->
												<ul class="m-0 pl-4">
													<li>hochwertigste Reinigungsmittel</li>
													<li>optimale Langzeitpflege</li>
													<li>ausschliesslich Swissvax-Produkte</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom custom_modal_right custom_modal_right_info">
													<div class="modal-content">
														<div class="modal-body">
															<div class="services2-car-bg-2 pt-2 pb-3">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">INNENAUFBEREITUNG NACH SWISSVAX</h1>
																				<p class="h6">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte.<br>
																				</p>
																				<ul class="m-0">
																					<li class="circle_2 m-0 h6">hochwertigste Reinigungsmittel</li>
																					<li class="circle_2 m-0 h6">optimale Langzeitpflege</li>
																					<li class="circle_2 m-0 h6">ausschliesslich Swissvax-Produkte</li>
																				</ul>
																			</div>
																		</div>
																		<div class="pt-3 col-md-12">
																			<p class="h6">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
																		</div>
																	</div>
																</div>
															</div>
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-12">
																			<h1 class="h4">Swissvax-Saisonalitäten</h1>
																			<ul class="m-0">
																				<li class="circle m-0 h6">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
																				<li class="circle m-0 h6">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
																				<li class="circle m-0 h6">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
																			</ul>
																			<p class="h6 py-3">
																				Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
																				Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
																				Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-12.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-13.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-14.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="info modal_animation_main custom_modal_right_info">
												<h5 class="text-uppercase">TEFLONVERSIEGELUNG</h5>
												<ul class="m-0 pl-4">
													<li>Der Standard-Lackschutz</li>
													<li>Haltbarkeit 6+ Monate</li>
													<li>Preiswert</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom custom_modal_right">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-6">
																			<p class="h6">Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und genügt für die meisten Autos. Ausserdem ist sie sehr preiswert. Wir empfehlen jedoch bei der Teflon-Versiegelung Hand- oder Lanzenwäsche, da die Waschstrasse die Versiegelung zu schnell abreibt.</p>
																			<ul class="m-0">
																				<li class="circle m-0 h6">Der Standard-Lackschutz</li>
																				<li class="circle m-0 h6">Haltbarkeit 6+ Monate</li>
																				<li class="circle m-0 h6">Preiswert</li>
																			</ul>
																		</div>
																		<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2 py-5">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-2.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">Besonders geeignet für:</h1>
																				<p class="h6">Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.
																				</p>
																				<ul>
																					<li class="circle_2 m-0 h6">alle Autos, Motorräder, Wohnmobile, LKW's usw.</li>
																					<li class="circle_2 m-0 h6">Gebrauchtwagen</li>
																					<li class="circle_2 m-0 h6">Leasingrückgaben</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-3.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-3.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-4.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-4.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-5.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-5.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-6.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-6.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-7.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-7.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-8.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-8.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-9.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-9.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-10.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-10.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tooltip-main service-first first">
									<div class="tooltip">
										<div class="info-main info-care">
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">HERKÖMMLICH</h5>
												<ul class="m-0 pl-4">
													<li>inkl. shampoonieren</li>
													<li>Lederreinigung und -pflege</li>
													<li>Reinigung mit Tornador</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div class="services2-car-bg-2 py-3">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">INNENAUFBEREITUNG HERKÖMMLICH</h1>
																				<p class="h6"> Bei einer herkömmlichen Innenaufbereitung arbeiten wir mit einem neutralen, ökologischen Universalreiniger. Der ganze Innenraum inkl. Kofferraum wird gesaugt, Teppiche und Sitze werden shampooniert bzw. das Leder wird gereinigt und mit einer professionellen Ledermilch eingepflegt.<br>
																				</p>
																				<ul>
																					<li class="circle_2 m-0 h6">inkl. shampoonieren</li>
																					<li class="circle_2 m-0 h6">Lederreinigung und -pflege</li>
																					<li class="circle_2 m-0 h6">Reinigung mit Tornador</li>
																				</ul>
																			</div>
																		</div>
																		<div class="pt-3 col-md-12">
																			<p class="h6">Bei der sogenannten "Tornadorreinigung" wird mit sehr hohem Luftdruck der Innenraum (ohne Himmel) inkl. Armaturen, allen Verkleidungen, Einstiegen, Fälzen und Schlitzen optimal gereinigt. Zum Abschluss werden die Innenscheiben professionell nur mit Wasser gereinigt. <br>Sitze und Teppiche mit problematischen Flecken werden nach dem Shampoonieren getrockenet und gegebenenfalls nochmals shampooniert oder händisch nachbearbeitet. <br>Anbei finden Sie die komplette Preisliste für die Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<h2 class="h5">Reinigung mit Tornador</h2>
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a href="#">
																						<div class="img-container">
																							<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-10.jpg') }} ">
																								<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-10.jpg') }} " alt="autokosmetik">
																							</a>
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a href="#">
																						<div class="img-container">
																							<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-11.jpg') }}">
																								<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-11.jpg') }}" alt="autokosmetik">
																							</a>
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">NACH SWISSVAX</h5>
												<ul class="m-0 pl-4">
													<li>hochwertigste Reinigungsmittel</li>
													<li>optimale Langzeitpflege</li>
													<li>ausschliesslich Swissvax-Produkte</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div class="services2-car-bg-2 pt-2 pb-3">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">INNENAUFBEREITUNG NACH SWISSVAX</h1>
																				<p class="h6">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte.<br></p>
																				<ul class="m-0">
																					<li class="circle_2 m-0 h6">hochwertigste Reinigungsmittel</li>
																					<li class="circle_2 m-0 h6">optimale Langzeitpflege</li>
																					<li class="circle_2 m-0 h6">ausschliesslich Swissvax-Produkte</li>
																				</ul>
																			</div>
																		</div>
																		<div class="pt-3 col-md-12">
																			<p class="h6">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
																		</div>
																	</div>
																</div>
															</div>
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-12">
																			<h1 class="h4">Swissvax-Saisonalitäten</h1>
																			<ul class="m-0">
																				<li class="circle m-0 h6">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
																				<li class="circle m-0 h6">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
																				<li class="circle m-0 h6">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
																			</ul>
																			<p class="h6 py-3">
																				Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
																				Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
																				Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a href="#">
																						<div class="img-container">
																							<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-12.jpg') }}">
																								<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																							</a>
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a href="#">
																						<div class="img-container">
																							<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-13.jpg') }}">
																								<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
																							</a>
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a href="#">
																						<div class="img-container">
																							<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-14.jpg') }}">
																								<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
																							</a>
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">SPEZIALFÄLLE</h5>
												<ul class="m-0 pl-4">
													<li>Hundehaare</li>
													<li>Geruchsbehandlungen</li>
													<li>Schimmel, usw.</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-6">
																			<p class="h6">Bei diesen "speziellen Fällen" können wir keine Fixpreise machen, denn der jeweilige Reinigungsaufwand ist von Fall zu Fall sehr unterschiedlich. Entsprechend arbeiten wir nach dem Standardstundensatz von CHF 97.10 inkl. 10% für Material und inkl. Mwst. </p>
																			<ul class="m-0">
																				<li class="circle m-0 h6">Hundehaare</li>
																				<li class="circle m-0 h6">Geruchsbehandlungen</li>
																				<li class="circle m-0 h6">Schimmel, usw.</li>
																			</ul>
																		</div>
																		<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/special-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																		<div class="col-md-12">
																			<p class="py-3 h6">Ozon-Geruchsbehandlung: Voraussetzung für eine Ozon-Geruchsbehandlung ist die optimale Reinigung mit anschliessender Trocknung. Erst bei einem komplett trockenen Innenraum kann eine wirksame Ozonbehandlung durchgeführt werden. <br>Die Ozonbehandlung verrechnen wir mit CHF 30.-. Teilweise sind aber mehrere Ozonbehandlungen nötig.</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<h2 class="h5">Beispiele von extremen Verschmutzungen die unserer Spezialbehandlung bedürfen:</h2>
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/special-2.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/special-2.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/special-3.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/special-3.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="related-projects col-12">
																			<h2 class="h5">Vorher-Nachher Gegenüberstellungen bei Extremfällen:</h2>
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/special-4.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/special-4.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/special-5.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/special-5.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tooltip-main service-second third">
									<div class="tooltip">
										<div class="info-main info-repair">
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">LACKSCHUTZFOLIE</h5>
												<ul class="m-0 pl-4">
													<li>Hochtransparent/unsichtbar</li>
													<li>Waschstrassenfest</li>
													<li>Der optimale Schutz <br> gegen Steinschlag</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-6">
																			<p class="h6">Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschläge. Unsere verwendete Profifolie ist sehr dick und dadurch auch ganz besonders widerstandsfähig. Allerdings ist Sie sehr schwer zu verlegen und auch relativ kostspielig.</p>
																			<ul class="m-0">
																				<li class="circle m-0 h6">Hochtransparent/unsichtbar</li>
																				<li class="circle m-0 h6">Der optimale Schutz gegen Steinschlag</li>
																				<li class="circle m-0 h6">Waschstrassenfest</li>
																			</ul>
																		</div>
																		<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2 py-5">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-2.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">Besonders geeignet für:</h1>
																				<p class="h6">Die Preise für eine Lackschutzfolie variieren sehr stark und sind abhängig von der Beschaffenheit des Autos und Anzahl jener Stellen, welche mit einer Lackschutzfolie versehen werden sollen. </p>
																				<p class="h6">Kommen Sie vorbei, wir unterbreiten Ihnen gerne ein unverbindliches Angebot.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">Ladekante und alle Türkanten</li>
																					<li class="circle m-0 h6">Stossstange und Motorhaube</li>
																					<li class="circle m-0 h6">Generell alle exponierten Stellen</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/paint-3.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-3.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/paint-4.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-4.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/paint-5.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-5.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/paint-6.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-6.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/paint-7.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-7.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/paint-8.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-8.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">SCHEIBENTÖNUNG</h5>
												<ul class="m-0 pl-4">
													<li>99%-iger UV-Schutz</li>
													<li>Hitzereduktion, Blickschutz</li>
													<li>Edle Optik</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-6">
																			<p class="h6">Eine Scheibentönung sieht nicht nur gut aus, sondern schützt in mehrfacher Hinsicht. Der Innenraum heizt weniger auf, 99% der schädlichen UV-Strahlung werden abgehalten und sie schützt vor neugierigen Blicken.</p>
																			<p class="h6 pt-2">Wir arbeiten ausschliesslich mit Markenfolien der Firma Johnson Window Films, Folie Marathon, und gewähren 7 Jahre Garantie. Jede Scheibentönung erhält ein Zertifikat.</p>
																			<ul class="m-0">
																				<li class="circle m-0 h6">99%-iger UV-Schutz</li>
																				<li class="circle m-0 h6">Hitzereduktion, Blickschutz</li>
																				<li class="circle m-0 h6">Edle Optik</li>
																			</ul>
																		</div>
																		<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-10.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2 py-5">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-11.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">Besonders geeignet für:</h1>
																				<p class="h6">Kofferraum oder auf der Rückbank</p>
																				<p class="pt-3">Anbei finden Sie die komplette Preisliste für fast alle Automarken - und Modelle. Sollte Ihr Auto nicht gelistet sein, bitten wir um ein kurzes Telefonat oder Mail. Wir werden Ihnen umgehend ein Angebot unterbreiten.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">Familienautos</li>
																					<li class="circle m-0 h6">Personen im Fond</li>
																					<li class="circle m-0 h6">Hundeautos</li>
																					<li class="circle m-0 h6">Wertgegenstände im</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-1.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-1.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-2.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-2.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-3.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-3.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-4.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-4.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-5.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-5.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-6.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-6.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-7.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-7.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-8.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-8.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-9.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-9.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tooltip-main service-second four">
									<div class="tooltip">
										<div class="info-main info-repair">
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">KERAMIKBESCHICHTUNG</h5>
												<ul class="m-0 pl-4">
													<li>Der panzerglasartige Lackschutz</li>
													<li>Haltbarkeit 36+ Monate</li>
													<li>Waschstrassenfest</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-6">
																			<p class="h6">Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und ist absolut waschstrassenfest. Besonders geeignet ist unsere Keramikbeschichtung für dunkle, grosse Autos, welche im Alltagseinsatz stehen. Jede Keramikbeschichtung erhält ein Zertifikat, welches die Haltbarkeit von Minimum 3 Jahren garantiert.</p>
																			<ul class="m-0">
																				<li class="circle m-0 h6">Der panzerglasartige Lackschutz</li>
																				<li class="circle m-0 h6">Haltbarkeit 36+ Monate</li>
																				<li class="circle m-0 h6">Waschstrassenfest</li>
																			</ul>
																		</div>
																		<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2 py-5">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">Unser neues Keramik-Material ServFaces</h1>
																				<p class="h6">In den ersten 3 Jahren unserer Arbeit mit sogenannten Keramikbeschichtungen, haben wir mit einem fernöstlichen Produkt gearbeitet. Bis wir feststellen mussten, dass sich das Material auf einmal anders verhält  und auch anders zu verarbeiten ist. Für unsere Firma ein absolutes "NO GO". Wir konnten jedoch in kürzester Zeit eine Ersatz-Produktlinie aus deutscher Produktion finden und haben seit nunmehr 2 Jahren nur die besten Erfahrungen gemacht. Und so konnten wir unsere Garantie von 2 auf 3 Jahre erhöhen. Ab Anfang 2016 haben wir deshalb auch die Vertretung von ServFaces für die Schweiz und Deutschland übernommen.</p>
																				<p class="pt-3 h6">Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-1.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-2.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-3.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-4.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-5.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-6.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-7.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-8.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-9.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">TEFLONVERSIEGELUNG</h5>
												<ul class="m-0 pl-4">
													<li>Der Standard-Lackschutz</li>
													<li>Haltbarkeit 6+ Monate</li>
													<li>Preiswert</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-6">
																			<p class="h6">Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und genügt für die meisten Autos. Ausserdem ist sie sehr preiswert. Wir empfehlen jedoch bei der Teflon-Versiegelung Hand- oder Lanzenwäsche, da die Waschstrasse die Versiegelung zu schnell abreibt.</p>
																			<ul class="m-0">
																				<li class="circle m-0 h6">Der Standard-Lackschutz</li>
																				<li class="circle m-0 h6">Haltbarkeit 6+ Monate</li>
																				<li class="circle m-0 h6">Preiswert</li>
																			</ul>
																		</div>
																		<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2 py-5">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-2.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">Besonders geeignet für:</h1>
																				<p class="h6">Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.
																				</p>
																				<ul>
																					<li class="circle_2 m-0 h6">alle Autos, Motorräder, Wohnmobile, LKW's usw.</li>
																					<li class="circle_2 m-0 h6">Gebrauchtwagen</li>
																					<li class="circle_2 m-0 h6">Leasingrückgaben</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-3.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-3.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-4.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-4.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-5.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-5.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-6.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-6.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-7.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-7.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-8.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-8.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-9.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-9.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-10.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-10.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tooltip-main service-third five">
									<div class="tooltip">
										<div class="info-main info-repair">
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">Mono</h5>
												<ul class="m-0 pl-4">
													<li>Lorem ipsum dolor</li>
													<li>Lorem ipsum dolor</li>
													<li>Lorem ipsum dolor</li>
												</ul>
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-12">
																			<p class="h6">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis molestias deleniti neque voluptatum sapiente quia harum nulla inventore, accusamus saepe voluptas quae corrupti repudiandae unde? Est quas provident aperiam modi.</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">Design</h5>
												<ul class="m-0 pl-4">
													<li>Lorem ipsum dolor</li>
													<li>Lorem ipsum dolor</li>
													<li>Lorem ipsum dolor</li>
												</ul>
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-12">
																			<p class="h6">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi nesciunt corporis fugiat, dignissimos cupiditate aspernatur delectus ipsam sit architecto eius numquam pariatur! Nostrum placeat quisquam dicta fugiat voluptate quas cumque!</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="tooltip-main service-third six">
									<div class="tooltip">
										<div class="info-main info-repair">
											<div class="info modal_animation_main custom_modal_right_info">
												<h5 class="text-uppercase">Fensterfolierung</h5>
												<ul class="m-0 pl-4">
													<li>Lorem ipsum dolor sit</li>
													<li>Lorem ipsum dolor sit</li>
													<li>Lorem ipsum dolor sit</li>
												</ul>
												<div id="myModalCustom" class="modal_custom custom_modal_right">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-12">
																			<p class="h6">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis molestias deleniti neque voluptatum sapiente quia harum nulla inventore, accusamus saepe voluptas quae corrupti repudiandae unde? Est quas provident aperiam modi.</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tooltip-main service-third seven">
									<div class="tooltip">
										<div class="info-main info-repair">
											<div class="info modal_animation_main custom_modal_right_info">
												<h5 class="text-uppercase">Beschriftung</h5>
												<ul class="m-0 pl-4">
													<li>Lorem ipsum dolor sit</li>
													<li>Lorem ipsum dolor sit</li>
													<li>Lorem ipsum dolor sit</li>
												</ul>
												<div id="myModalCustom" class="modal_custom custom_modal_right">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-12">
																			<p class="h6">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis molestias deleniti neque voluptatum sapiente quia harum nulla inventore, accusamus saepe voluptas quae corrupti repudiandae unde? Est quas provident aperiam modi.</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="car-bike-2">
				<div id="section-services1" class="feature-car-area">
					<div class="container">
						<div class="row modal-main service-accordian-main">
							<div class="col-md-12">
								<div class="home-service-title pb-3 text-center">
									<h2 class="h1 pt-4">Unser Serviceangebot</h2>
									<p class="h6"> Wir bieten einen kompletten Service für Autoreparatur und Wartung </p>
								</div>
							</div>
							
							<div class="col-md-4 mb-3">
								<div class="service-level-1 car-care-icon  bg-white shadow-sm active" style="background: url({{ asset('assets/frontend/images/car-care-bg.png')}}) no-repeat center / cover;" role="button" data-class1="service-four">
									<div class="service-title-wrap">
										<div class="avatar-sm mb-3">
											<img src="{{ asset('assets/frontend/images/car-care.png')}}" alt="" class="img-fluid">
										</div>
										<h4 class="h3 font-weight-bold mb-0">Pflege</h4>
										<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Magni, quidem voluptatibus.</p>
										<!-- <span class="text-primary btn-link">Read more</span> -->
									</div>
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<div class="service-level-1 car-care-icon  bg-white shadow-sm" style="background: url({{ asset('assets/frontend/images/car-protect-bg.jpg')}}) no-repeat center / cover;" role="button" data-class1="service-five">
									<div class="service-title-wrap">
										<div class="avatar-sm mb-3">
											<img src="{{ asset('assets/frontend/images/car-protect.png')}}" alt="" class="img-fluid">
										</div>
										<h4 class="h3 font-weight-bold mb-0">Schutz</h4>
										<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Magni, quidem voluptatibus.</p>
										<!-- <span class="text-primary btn-link">Read more</span> -->
									</div>
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<div class="service-level-1 car-care-icon  bg-white shadow-sm" style="background: url({{ asset('assets/frontend/images/car-enhance-bg.png')}}) no-repeat center / cover;" role="button" data-class1="service-six">
									<div class="service-title-wrap">
										<div class="avatar-sm mb-3">
											<img src="{{ asset('assets/frontend/images/car-enhance.png')}}" alt="" class="img-fluid">
										</div>
										<h4 class="h3 font-weight-bold mb-0">Design</h4>
										<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Magni, quidem voluptatibus.</p>
										<!-- <span class="text-primary btn-link">Read more</span> -->
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div id="section-miscellaneous1-1" class="bg-white pt-5 page-section-pt">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-md-10">
								<div class="home-service-image">
									<img src="{{ asset('assets/frontend/images/bike-2.png') }}" class="w-100 img-fluid" alt="autokosmetik">
								</div>
								<div class="tooltip-main service-four first first-bike" id="first">
									<div class="tooltip">
										<div class="info-main info-care" data-link="service-care">
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">HERKÖMMLICH</h5>
												<ul class="m-0 pl-4">
													<li>inkl. shampoonieren</li>
													<li>Lederreinigung und -pflege</li>
													<li>Reinigung mit Tornador</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div class="services2-car-bg-2 py-3">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">INNENAUFBEREITUNG HERKÖMMLICH</h1>
																				<p class="h6"> Bei einer herkömmlichen Innenaufbereitung arbeiten wir mit einem neutralen, ökologischen Universalreiniger. Der ganze Innenraum inkl. Kofferraum wird gesaugt, Teppiche und Sitze werden shampooniert bzw. das Leder wird gereinigt und mit einer professionellen Ledermilch eingepflegt.<br>
																				</p>
																				<ul>
																					<li class="circle_2 m-0 h6">inkl. shampoonieren</li>
																					<li class="circle_2 m-0 h6">Lederreinigung und -pflege</li>
																					<li class="circle_2 m-0 h6">Reinigung mit Tornador</li>
																				</ul>
																			</div>
																		</div>
																		<div class="pt-3 col-md-12">
																			<p class="h6">Bei der sogenannten "Tornadorreinigung" wird mit sehr hohem Luftdruck der Innenraum (ohne Himmel) inkl. Armaturen, allen Verkleidungen, Einstiegen, Fälzen und Schlitzen optimal gereinigt. Zum Abschluss werden die Innenscheiben professionell nur mit Wasser gereinigt. <br>Sitze und Teppiche mit problematischen Flecken werden nach dem Shampoonieren getrockenet und gegebenenfalls nochmals shampooniert oder händisch nachbearbeitet. <br>Anbei finden Sie die komplette Preisliste für die Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<h2 class="h5">Reinigung mit Tornador</h2>
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a href="#">
																						<div class="img-container">
																							<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-10.jpg') }} ">
																								<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-10.jpg') }} " alt="autokosmetik">
																							</a>
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a href="#">
																						<div class="img-container">
																							<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-11.jpg') }}">
																								<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-11.jpg') }}" alt="autokosmetik">
																							</a>
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">NACH SWISSVAX</h5>
												<ul class="m-0 pl-4">
													<li>hochwertigste Reinigungsmittel</li>
													<li>optimale Langzeitpflege</li>
													<li>ausschliesslich Swissvax-Produkte</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div class="services2-car-bg-2 pt-2 pb-3">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">INNENAUFBEREITUNG NACH SWISSVAX</h1>
																				<p class="h6">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte.<br></p>
																				<ul class="m-0">
																					<li class="circle_2 m-0 h6">hochwertigste Reinigungsmittel</li>
																					<li class="circle_2 m-0 h6">optimale Langzeitpflege</li>
																					<li class="circle_2 m-0 h6">ausschliesslich Swissvax-Produkte</li>
																				</ul>
																			</div>
																		</div>
																		<div class="pt-3 col-md-12">
																			<p class="h6">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
																		</div>
																	</div>
																</div>
															</div>
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-12">
																			<h1 class="h4">Swissvax-Saisonalitäten</h1>
																			<ul class="m-0">
																				<li class="circle m-0 h6">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
																				<li class="circle m-0 h6">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
																				<li class="circle m-0 h6">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
																			</ul>
																			<p class="h6 py-3">
																				Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
																				Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
																				Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a href="#">
																						<div class="img-container">
																							<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-12.jpg') }}">
																								<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																							</a>
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a href="#">
																						<div class="img-container">
																							<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-13.jpg') }}">
																								<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
																							</a>
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a href="#">
																						<div class="img-container">
																							<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-14.jpg') }}">
																								<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
																							</a>
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">SPEZIALFÄLLE</h5>
												<ul class="m-0 pl-4">
													<li>Hundehaare</li>
													<li>Geruchsbehandlungen</li>
													<li>Schimmel, usw.</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-6">
																			<p class="h6">Bei diesen "speziellen Fällen" können wir keine Fixpreise machen, denn der jeweilige Reinigungsaufwand ist von Fall zu Fall sehr unterschiedlich. Entsprechend arbeiten wir nach dem Standardstundensatz von CHF 97.10 inkl. 10% für Material und inkl. Mwst. </p>
																			<ul class="m-0">
																				<li class="circle m-0 h6">Hundehaare</li>
																				<li class="circle m-0 h6">Geruchsbehandlungen</li>
																				<li class="circle m-0 h6">Schimmel, usw.</li>
																			</ul>
																		</div>
																		<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/special-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																		<div class="col-md-12">
																			<p class="py-3 h6">Ozon-Geruchsbehandlung: Voraussetzung für eine Ozon-Geruchsbehandlung ist die optimale Reinigung mit anschliessender Trocknung. Erst bei einem komplett trockenen Innenraum kann eine wirksame Ozonbehandlung durchgeführt werden. <br>Die Ozonbehandlung verrechnen wir mit CHF 30.-. Teilweise sind aber mehrere Ozonbehandlungen nötig.</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<h2 class="h5">Beispiele von extremen Verschmutzungen die unserer Spezialbehandlung bedürfen:</h2>
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/special-2.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/special-2.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/special-3.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/special-3.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="related-projects col-12">
																			<h2 class="h5">Vorher-Nachher Gegenüberstellungen bei Extremfällen:</h2>
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/special-4.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/special-4.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/special-5.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/special-5.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tooltip-main service-five second second-bike" id="second" >
									<div class="tooltip">
										<div class="info-main info-wash" data-link="servicewash">
											<div class="info modal_animation_main custom_modal_right_info">
												<h5 class="text-uppercase">KERAMIKBESCHICHTUNG</h5>
												<ul class="m-0 pl-4">
													<li>Der panzerglasartige Lackschutz</li>
													<li>Haltbarkeit 36+ Monate</li>
													<li>Waschstrassenfest</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom custom_modal_right">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-6">
																			<p class="h6">Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und ist absolut waschstrassenfest. Besonders geeignet ist unsere Keramikbeschichtung für dunkle, grosse Autos, welche im Alltagseinsatz stehen. Jede Keramikbeschichtung erhält ein Zertifikat, welches die Haltbarkeit von Minimum 3 Jahren garantiert.</p>
																			<ul class="m-0">
																				<li class="circle m-0 h6">Der panzerglasartige Lackschutz</li>
																				<li class="circle m-0 h6">Haltbarkeit 36+ Monate</li>
																				<li class="circle m-0 h6">Waschstrassenfest</li>
																			</ul>
																		</div>
																		<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2 py-5">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">Unser neues Keramik-Material ServFaces</h1>
																				<p class="h6">In den ersten 3 Jahren unserer Arbeit mit sogenannten Keramikbeschichtungen, haben wir mit einem fernöstlichen Produkt gearbeitet. Bis wir feststellen mussten, dass sich das Material auf einmal anders verhält  und auch anders zu verarbeiten ist. Für unsere Firma ein absolutes "NO GO". Wir konnten jedoch in kürzester Zeit eine Ersatz-Produktlinie aus deutscher Produktion finden und haben seit nunmehr 2 Jahren nur die besten Erfahrungen gemacht. Und so konnten wir unsere Garantie von 2 auf 3 Jahre erhöhen. Ab Anfang 2016 haben wir deshalb auch die Vertretung von ServFaces für die Schweiz und Deutschland übernommen.</p>
																				<p class="pt-3 h6">Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-1.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-2.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-3.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-4.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-5.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-6.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-7.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-8.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-9.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="info modal_animation_main custom_modal_right_info">
												<h5 class="text-uppercase">NACH SWISSVAX</h5>
												<!-- <ul class="m-0 pl-4">
													<li>Preiswert</li>
													<li>Haltbarkeit 36+ Monate</li>
													<li>Autowaschfest</li>
												</ul> -->
												<ul class="m-0 pl-4">
													<li>hochwertigste Reinigungsmittel</li>
													<li>optimale Langzeitpflege</li>
													<li>ausschliesslich Swissvax-Produkte</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom custom_modal_right custom_modal_right_info">
													<div class="modal-content">
														<div class="modal-body">
															<div class="services2-car-bg-2 pt-2 pb-3">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">INNENAUFBEREITUNG NACH SWISSVAX</h1>
																				<p class="h6">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte.<br>
																				</p>
																				<ul class="m-0">
																					<li class="circle_2 m-0 h6">hochwertigste Reinigungsmittel</li>
																					<li class="circle_2 m-0 h6">optimale Langzeitpflege</li>
																					<li class="circle_2 m-0 h6">ausschliesslich Swissvax-Produkte</li>
																				</ul>
																			</div>
																		</div>
																		<div class="pt-3 col-md-12">
																			<p class="h6">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
																		</div>
																	</div>
																</div>
															</div>
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-12">
																			<h1 class="h4">Swissvax-Saisonalitäten</h1>
																			<ul class="m-0">
																				<li class="circle m-0 h6">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
																				<li class="circle m-0 h6">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
																				<li class="circle m-0 h6">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
																			</ul>
																			<p class="h6 py-3">
																				Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
																				Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
																				Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-12.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-13.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-14.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="info modal_animation_main custom_modal_right_info">
												<h5 class="text-uppercase">TEFLONVERSIEGELUNG</h5>
												<ul class="m-0 pl-4">
													<li>Der Standard-Lackschutz</li>
													<li>Haltbarkeit 6+ Monate</li>
													<li>Preiswert</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom custom_modal_right">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-6">
																			<p class="h6">Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und genügt für die meisten Autos. Ausserdem ist sie sehr preiswert. Wir empfehlen jedoch bei der Teflon-Versiegelung Hand- oder Lanzenwäsche, da die Waschstrasse die Versiegelung zu schnell abreibt.</p>
																			<ul class="m-0">
																				<li class="circle m-0 h6">Der Standard-Lackschutz</li>
																				<li class="circle m-0 h6">Haltbarkeit 6+ Monate</li>
																				<li class="circle m-0 h6">Preiswert</li>
																			</ul>
																		</div>
																		<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2 py-5">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-2.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">Besonders geeignet für:</h1>
																				<p class="h6">Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.
																				</p>
																				<ul>
																					<li class="circle_2 m-0 h6">alle Autos, Motorräder, Wohnmobile, LKW's usw.</li>
																					<li class="circle_2 m-0 h6">Gebrauchtwagen</li>
																					<li class="circle_2 m-0 h6">Leasingrückgaben</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-3.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-3.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-4.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-4.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-5.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-5.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-6.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-6.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-7.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-7.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-8.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-8.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-9.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-9.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/seal-10.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-10.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tooltip-main service-six third third-bike" id="third" >
									<div class="tooltip">
										<div class="info-main info-repair" data-link="servicerepair">
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">FOLIENDESIGN</h5>
												<ul class="m-0 pl-4">
													<li>Individuelle Optik</li>
													<li>Designberatung</li>
													<li>Waschstrassenfest</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-6">
																			<p class="h6">Besonders Individualisten wissen die vielfältigen Möglichkeiten von Autofolien zu schätzen. Dem Design und der individuellen Gestaltung sind fast keine Grenzen gesetzt. Gerne beraten wir diesbezüglich unsere anspruchsvollen Kunden. Sowohl betreffend die verwendeten Folien als auch in Bezug auf unsere Arbeit können Sie höchste Qualität erwarten. Bei allen Folien gilt eine Garantie von 7 Jahren.</p>
																			<ul class="m-0">
																				<li class="circle m-0 h6">Individuelle Optik</li>
																				<li class="circle m-0 h6">Designberatung</li>
																				<li class="circle m-0 h6">Waschstrassenfest</li>
																			</ul>
																		</div>
																		<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2 py-5">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-2.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">Besonders geeignet für:</h1>
																				<p class="h6">begrenzte Zeit eine andere Farbe brauchen</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">Individualisten</li>
																					<li class="circle m-0 h6">Showfahrzeuge, die besonders auffallen wollen</li>
																					<li class="circle m-0 h6">Fahrzeuge, welche für eine </li>
																				</ul>
																			</div>
																		</div>
																		<div class="col-md-12 pt-3">
																			<p class="h6">Für das Foliendesign gibt es keine generelle Preisliste. Rechnen Sie bei einer Vollfolierung für einen Golf mit Minimum CHF 3.000,- bis CHF 3.600,-,  ein SUV wird Minimum CHF 4.000.- bis CHF 5.000 kosten. Je nach speziellen Anforderungen und Features können die Preise auch noch darüber liegen.</p>
																			<p class="pt-3 h6">Teilfolierungen kosten entsprechend weniger und beginnen bereits bei wenigen hundert Franken.</p>
																			<p class="pt-3 h6">Achtung: Es gibt immer mehr „Möchte-Gern-Folierer“, welche neben der schlechten Verklebung auch dem Lack sehr viel Schaden zufügen können. Auch werden teilweise sehr billige, schlechte Folien verwendet, bei denen keinerlei Garantie gewährt wird.</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/foil-3.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-3.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/foil-4.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-4.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/foil-5.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-5.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/foil-6.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-6.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/foil-7.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-7.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/foil-8.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-8.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/foil-9.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-9.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/foil-10.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-10.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/foil-11.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-11.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">LACKSCHUTZFOLIE</h5>
												<ul class="m-0 pl-4">
													<li>Hochtransparent/unsichtbar</li>
													<li>Waschstrassenfest</li>
													<li>Der optimale Schutz <br> gegen Steinschlag</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-6">
																			<p class="h6">Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschläge. Unsere verwendete Profifolie ist sehr dick und dadurch auch ganz besonders widerstandsfähig. Allerdings ist Sie sehr schwer zu verlegen und auch relativ kostspielig.</p>
																			<ul class="m-0">
																				<li class="circle m-0 h6">Hochtransparent/unsichtbar</li>
																				<li class="circle m-0 h6">Der optimale Schutz gegen Steinschlag</li>
																				<li class="circle m-0 h6">Waschstrassenfest</li>
																			</ul>
																		</div>
																		<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2 py-5">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-2.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">Besonders geeignet für:</h1>
																				<p class="h6">Die Preise für eine Lackschutzfolie variieren sehr stark und sind abhängig von der Beschaffenheit des Autos und Anzahl jener Stellen, welche mit einer Lackschutzfolie versehen werden sollen. </p>
																				<p class="h6">Kommen Sie vorbei, wir unterbreiten Ihnen gerne ein unverbindliches Angebot.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">Ladekante und alle Türkanten</li>
																					<li class="circle m-0 h6">Stossstange und Motorhaube</li>
																					<li class="circle m-0 h6">Generell alle exponierten Stellen</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/paint-3.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-3.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/paint-4.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-4.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/paint-5.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-5.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/paint-6.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-6.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/paint-7.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-7.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/paint-8.png') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-8.png') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="info modal_animation_main">
												<h5 class="text-uppercase">SCHEIBENTÖNUNG</h5>
												<ul class="m-0 pl-4">
													<li>99%-iger UV-Schutz</li>
													<li>Hitzereduktion, Blickschutz</li>
													<li>Edle Optik</li>
												</ul>
												
												<div id="myModalCustom" class="modal_custom">
													<div class="modal-content">
														<div class="modal-body">
															<div id="" class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-6">
																			<p class="h6">Eine Scheibentönung sieht nicht nur gut aus, sondern schützt in mehrfacher Hinsicht. Der Innenraum heizt weniger auf, 99% der schädlichen UV-Strahlung werden abgehalten und sie schützt vor neugierigen Blicken.</p>
																			<p class="h6 pt-2">Wir arbeiten ausschliesslich mit Markenfolien der Firma Johnson Window Films, Folie Marathon, und gewähren 7 Jahre Garantie. Jede Scheibentönung erhält ein Zertifikat.</p>
																			<ul class="m-0">
																				<li class="circle m-0 h6">99%-iger UV-Schutz</li>
																				<li class="circle m-0 h6">Hitzereduktion, Blickschutz</li>
																				<li class="circle m-0 h6">Edle Optik</li>
																			</ul>
																		</div>
																		<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-10.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2 py-5">
																<div class="container">
																	<div class="row">
																		<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-11.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-12 col-md-7">
																			<div class="ceramic-coating-pading">
																				<h1 class="h4">Besonders geeignet für:</h1>
																				<p class="h6">Kofferraum oder auf der Rückbank</p>
																				<p class="pt-3">Anbei finden Sie die komplette Preisliste für fast alle Automarken - und Modelle. Sollte Ihr Auto nicht gelistet sein, bitten wir um ein kurzes Telefonat oder Mail. Wir werden Ihnen umgehend ein Angebot unterbreiten.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">Familienautos</li>
																					<li class="circle m-0 h6">Personen im Fond</li>
																					<li class="circle m-0 h6">Hundeautos</li>
																					<li class="circle m-0 h6">Wertgegenstände im</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects col-12">
																			<div class="row">
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-1.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-1.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-2.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-2.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-3.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-3.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-4.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-4.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-5.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-5.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-6.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-6.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-7.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-7.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-8.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-8.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																				<div class="item col-sm-12 col-md-3 my-3">
																					<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/tint-9.jpg') }}">
																						<div class="img-container">
																							<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-9.jpg') }}" alt="autokosmetik">
																						</div>
																					</a>
																				</div>
																			</div>
																		</div>
																		<div class="col-12 text-center pb-5">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			

			<div id="section-miscellaneous1-1" class="bg-white pt-5 page-section-pt car-bike-small-1" role="button">
				<div class="container-fluid feature-car-area">
					<div class="row">
						<div class="col-md-3">
							<div class="home-service-image">
								<img src="{{ asset('assets/frontend/images/car-service.png') }}" class="w-100 img-fluid" alt="autokosmetik">
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="section-miscellaneous1-1" class="bg-white page-section-pt pb-5 white-bg car-bike-small-2" role="button">
				<div class="container-fluid feature-car-area">
					<div class="row">
						<div class="col-md-2">
							<div class="home-service-image">
								<img src="{{ asset('assets/frontend/images/bike-2.png') }}" class="w-100 img-fluid" alt="autokosmetik">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="services-mobile">
				<section class="mt-lg-5">
					<h3 class="h2 text-center mb-3 mb-lg-5">Unser Serviceangebot</h3>
					<div class="tabpanel-service main-tabpanel-service">
						<ul class="nav justify-content-start nav-pills mb-3" id="pills-tab" role="tablist">
							<li class="nav-item" role="presentation">
								<button class="nav-link active p-0" data-toggle="pill" data-target="#pills-car" type="button" role="tab" aria-controls="pills-car" aria-selected="false">
									<div class="card select-category">
										<input role="button" type="radio" name="step_0" class="form-control checkbox">
										<div class="card-body px-2 py-0">
											<div class="avatar-md">
												<img src="{{ asset('assets/frontend/images/form-car.png') }}" alt="" class="img-fluid">
											</div>
											<p class="font-weight-bold h5 text-dark m-0 mb-2">Auto</p>
										</div>
									</div>
								</button>
							</li>
							<li class="nav-item" role="presentation">
								<button class="nav-link p-0" data-toggle="pill" data-target="#pills-bike" type="button" role="tab" aria-controls="pills-bike" aria-selected="false">
									<div class="card select-category">
										<input role="button" type="radio" name="step_0" class="form-control checkbox">
										<div class="card-body px-2 py-0">
											<div class="avatar-md">
												<img src="{{ asset('assets/frontend/images/form-bike.png') }}" alt="" class="img-fluid">
											</div>
											<p class="font-weight-bold h5 text-dark m-0 mb-2">Motorrad</p>
										</div>
									</div>
								</button>
							</li>
						</ul>
					</div>
					<div class="tabpanel-service">
						<div class="tab-content" id="pills-tabContent">
							<div class="tab-pane fade show active" id="pills-car" role="tabpanel">
								<div class="tabpanel-service main-tabpanel-service">
									<ul class="nav justify-content-start nav-pills mb-3" id="pills-tab" role="tablist">
										<li class="nav-item" role="presentation">
											<button class="nav-link active" data-toggle="pill" data-target="#pills-care" type="button" role="tab" aria-controls="pills-care" aria-selected="false">
												Pflege
											</button>
										</li>
										<li class="nav-item" role="presentation">
											<button class="nav-link " data-toggle="pill" data-target="#pills-protect" type="button" role="tab" aria-controls="pills-protect" aria-selected="false">
												Schutz
											</button>
										</li>
										<li class="nav-item" role="presentation">
											<button class="nav-link " data-toggle="pill" data-target="#pills-enhance" type="button" role="tab" aria-controls="pills-enhance" aria-selected="false">
												Design
											</button>
										</li>
									</ul>
								</div>
								<div class="tabpanel-service">
									<div class="tab-content" id="pills-tabContent">
										<div class="tab-pane fade show active" id="pills-care" role="tabpanel">
											<ul class="nav justify-content-start nav-tabs mb-3" id="pills-tab" role="tablist">
												<li class="nav-item" role="presentation">
													<button class="nav-link active" data-toggle="pill" data-target="#Aussenaufbereitung" type="button" role="tab" aria-controls="Aussenaufbereitung" aria-selected="false">
														Aussenaufbereitung
													</button>
												</li>
												<li class="nav-item" role="presentation">
													<button class="nav-link " data-toggle="pill" data-target="#Innenaufbereitung" type="button" role="tab" aria-controls="Innenaufbereitung" aria-selected="false">
														Innenaufbereitung
													</button>
												</li>
											</ul>
										</div>
										<div class="tab-pane fade" id="pills-protect" role="tabpanel">
											<ul class="nav justify-content-start nav-tabs mb-3" id="pills-tab" role="tablist">
												<li class="nav-item" role="presentation">
													<button class="nav-link active" data-toggle="pill" data-target="#Folien" type="button" role="tab" aria-controls="Folien" aria-selected="false">
														Folien
													</button>
												</li>
												<li class="nav-item" role="presentation">
													<button class="nav-link " data-toggle="pill" data-target="#Beschichtung" type="button" role="tab" aria-controls="Beschichtung" aria-selected="false">
														Beschichtung
													</button>
												</li>
											</ul>
										</div>
										<div class="tab-pane fade" id="pills-enhance" role="tabpanel">
											<ul class="nav justify-content-start nav-tabs mb-3" id="pills-tab" role="tablist">
												<li class="nav-item" role="presentation">
													<button class="nav-link active" data-toggle="pill" data-target="#Komplettfolierung" type="button" role="tab" aria-controls="Komplettfolierung" aria-selected="false">
														Komplettfolierung
													</button>
												</li>
												<li class="nav-item" role="presentation">
													<button class="nav-link " data-toggle="pill" data-target="#Teilfolierungen" type="button" role="tab" aria-controls="Teilfolierungen" aria-selected="false">
														Teilfolierungen
													</button>
												</li>
												<li class="nav-item" role="presentation">
													<button class="nav-link" data-toggle="pill" data-target="#Fensterfolierung" type="button" role="tab" aria-controls="Fensterfolierung" aria-selected="false">
														Fensterfolierung
													</button>
												</li>
												<li class="nav-item" role="presentation">
													<button class="nav-link " data-toggle="pill" data-target="#Interieur" type="button" role="tab" aria-controls="Interieur" aria-selected="false">
														Interieur
													</button>
												</li>
												<li class="nav-item" role="presentation">
													<button class="nav-link " data-toggle="pill" data-target="#Teilfolierungen" type="button" role="tab" aria-controls="Teilfolierungen" aria-selected="false">
														Teilfolierungen
													</button>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="tabpanel-service">
									<div class="tab-content" id="pills-tabContent">
										<div class="tab-pane fade show active" id="Aussenaufbereitung" role="tabpanel">
											<div id="collapse1_a_parent" class="mt-3 accordion px-2">
												<div class="care-for-carousel owl-carousel owl-theme">
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_a_1" aria-expanded="false" aria-controls="collapse1_a_1" role="button">
															<div class="ratio ratio-16X9">
																<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Keramikbeschichtung</h5>
																<p class="h6 py-2">Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und...
																	<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_a_2" aria-expanded="false" aria-controls="collapse1_a_2" role="button">
															<div class="ratio ratio-16X9">
																<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Teflonversiegelung</h5>
																<p class="h6 py-2">Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem...
																	<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_a_3" aria-expanded="false" aria-controls="collapse1_a_3" role="button">
															<div class="ratio ratio-16X9">	
																<img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Swissvax</h5>
																<p class="h6 py-2">In unserem Swissvax Car Care Center Liechtenstein/Rheintal ..
																	<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
												</div>

												<div id="collapse1_a_1" class="collapse" data-parent="#collapse1_a_parent">
													<div class="shadow-sm bg-white p-3">
														<div class="section-car-ceramic">
															<div class="container">
																<div class="row">
																	<div class="left col-sm-12 col-md-8">
																		<p>
																		Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und ist absolut waschstrassenfest. Besonders geeignet ist unsere Keramikbeschichtung für dunkle, grosse Autos, welche im Alltagseinsatz stehen. Jede Keramikbeschichtung erhält ein Zertifikat, welches die Haltbarkeit von Minimum 3 Jahren garantiert.
																		</p>
																		<ul>
																			<li class="circle">Der panzerglasartige Lackschutz</li>
																			<li class="circle">Haltbarkeit 36+ Monate</li>
																			<li class="circle">Waschstrassenfest</li>
																		</ul>
																	</div>
																	<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="autokosmetik"> 
																	</div>
																</div>
															</div>
														</div>
														<div class="services2-car-bg-2">
															<div class="container">
																<div class="row align-items-center">
																	<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
																	</div>
																	<div class="col-sm-12 col-md-7">
																		<div class="ceramic-coating-pading">
																			<h1>Unser neues Keramik-Material ServFaces</h1>
																			<p>In den ersten 3 Jahren unserer Arbeit mit sogenannten Keramikbeschichtungen, haben wir mit einem fernöstlichen Produkt gearbeitet. Bis wir feststellen mussten, dass sich das Material auf einmal anders verhält  und auch anders zu verarbeiten ist. Für unsere Firma ein absolutes "NO GO". <br><br> Wir konnten jedoch in kürzester Zeit eine Ersatz-Produktlinie aus deutscher Produktion finden und haben seit nunmehr 2 Jahren nur die besten Erfahrungen gemacht. Und so konnten wir unsere Garantie von 2 auf 3 Jahre erhöhen. Ab Anfang 2016 haben wir deshalb auch die Vertretung von ServFaces für die Schweiz und Deutschland übernommen.</p>
																			<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="ceramic-coting-images">
															<div class="container">
																<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-1.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-1.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-2.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-2.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-3.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-3.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-4.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-4.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-5.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-5.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-6.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-6.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-7.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-7.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-8.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-8.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-9.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-9.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																</div>
																<div class="loadmore-btn col-12 text-center mt-4">
																	<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div id="collapse1_a_2" class="collapse" data-parent="#collapse1_a_parent">
													<div class="shadow-sm bg-white p-3">
														<div class="section-car-ceramic">
															<div class="container">
																<div class="row">
																	<div class="left col-sm-12 col-md-8">
																		<p>
																		Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und genügt für die meisten Autos. Ausserdem ist sie sehr preiswert. Wir empfehlen jedoch bei der Teflon-Versiegelung Hand- oder Lanzenwäsche, da die Waschstrasse die Versiegelung zu schnell abreibt.
																		</p>
																		<ul>
																			<li class="circle">Der Standard-Lackschutz</li>
																			<li class="circle">Haltbarkeit 6+ Monate</li>
																			<li class="circle">Preiswert</li>
																		</ul>
																	</div>
																	<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="autokosmetik">
																	</div>
																</div>
															</div>
														</div>
														<div class="services2-car-bg-2">
															<div class="container">
																<div class="row align-items-center">
																	<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																		<img class="w-100" src="{{ asset('assets/frontend/images/seal-2.jpg') }}" alt="autokosmetik">
																	</div>
																	<div class="col-sm-12 col-md-7">
																		<div class="ceramic-coating-pading">
																			<h1>Besonders geeignet für:</h1>
																			<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen. <br> </p>
																			<ul>
																				<li class="circle_2 m-0 h6">alle Autos, Motorräder, Wohnmobile, LKW's usw.</li>
																				<li class="circle_2 m-0 h6">Gebrauchtwagen</li>
																				<li class="circle_2 m-0 h6">Leasingrückgaben</li>
																			</ul>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="ceramic-coting-images">
															<div class="container">
																<div class="row">
																	<!-- Related Project -->
																	<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-3.jpg')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-3.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-4.jpg')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-4.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-5.jpg')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-5.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-6.jpg')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-6.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-7.jpg')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-7.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-8.png')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-8.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-9.png')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-9.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-10.png')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-10.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-11.png')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-11.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		
																	</div>
																	<div class="loadmore-btn col-12 text-center mt-3">
																		<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div id="collapse1_a_3" class="collapse" data-parent="#collapse1_a_parent">
													<div class="shadow-sm bg-white p-3">
														<div class="section-car-ceramic">
															
															<div class="row">
																<div class="left col-sm-12 col-md-8">
																	<p> In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. </p>
																	<ul>
																		<li class="circle">hochwertigste Reinigungsmittel</li>
																		<li class="circle">optimale Langzeitpflege</li>
																		<li class="circle">ausschliesslich Swissvax-Produkte</li>
																	</ul>
																	
																</div>
																<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																	<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
																</div>
																
															</div>
															<p class="pt-5">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
															
														</div>
														<div class="services2-car-bg-2">
															<div class="container">
																<div class="row align-items-center">
																	<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																	</div>
																	<div class="col-sm-12 col-md-7">
																		<div class="ceramic-coating-pading">
																			<h1>Swissvax-Saisonalitäten</h1>
																			<ul>
																				<li class="circle">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
																				<li class="circle">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
																				<li class="circle">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
																			</ul>
																			<p>
																				Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
																				Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
																				Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="ceramic-coting-images">
															<div class="container">
																<div class="row">
																	<!-- Related Project -->
																	<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/auto-12.jpg')}}" data-fancybox="gallery1_a_3">
																				<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/auto-13.jpg')}}" data-fancybox="gallery1_a_3">
																				<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/auto-14.jpg')}}" data-fancybox="gallery1_a_3">
																				<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																	</div>
																	<div class="loadmore-btn col-12 text-center mt-3">
																		<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="Innenaufbereitung" role="tabpanel">
											<div id="collapse1_a_parent" class="mt-3">
												<div class=" px-2">
													<div class="care-for-carousel owl-carousel owl-theme">
														<div class="item pb-4">
															<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_b_1" aria-expanded="false" aria-controls="collapse1_b_1" role="button">
																<div class="ratio ratio-16X9">
																	<img src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="" class="img-fluid">
																</div>
																<div class="px-2 position-relative">
																	<a href="{{ route('bookappointment')}}" role="button">
																		<div class="align-items-center bg-white d-flex m-0 btn-service">
																			<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																			<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																		</div>
																	</a>
																	<h5 class="font-weight-bold h4 pt-3 m-0">Herkömmlich</h5>
																	<p class="h6 py-2">Bei einer herkömmlichen Innenaufbereitung arbeiten wir mit einem...
																		<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																	</p>
																</div>
															</div>
														</div>
														<div class="item pb-4">
															<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_b_2" aria-expanded="false" aria-controls="collapse1_b_2" role="button">
																<div class="ratio ratio-16X9">
																	<img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="" class="img-fluid">
																</div>
																<div class="px-2 position-relative">
																	<a href="{{ route('bookappointment')}}" role="button">
																		<div class="align-items-center bg-white d-flex m-0 btn-service">
																			<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																			<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																		</div>
																	</a>
																	<h5 class="font-weight-bold h4 pt-3 m-0">Swissvax</h5>
																	<p class="h6 py-2">In unserem Swissvax Car Care Center Liechtenstein/Rheintal...
																		<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																	</p>
																</div>
															</div>
														</div>
														<div class="item pb-4">
															<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_b_3" aria-expanded="false" aria-controls="collapse1_b_3" role="button">
																<div class="ratio ratio-16X9">	
																	<img src="{{ asset('assets/frontend/images/special-1.jpg') }}" alt="" class="img-fluid">
																</div>
																<div class="px-2 position-relative">
																	<a href="{{ route('bookappointment')}}" role="button">
																		<div class="align-items-center bg-white d-flex m-0 btn-service">
																			<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																			<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																		</div>
																	</a>
																	<h5 class="font-weight-bold h4 pt-3 m-0">SPEZIALFÄLLE</h5>
																	<p class="h6 py-2">Bei diesen "speziellen Fällen" können wir keine Fixpreise machen, denn...
																		<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																	</p>
																</div>
															</div>
														</div>
													</div>
													<div id="collapse1_b_1" class="collapse" data-parent="#collapse1_a_parent">
														<div class="shadow-sm bg-white p-3">
															<div class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-6 col-md-8">
																			<p>
																			Bei einer herkömmlichen Innenaufbereitung arbeiten wir mit einem neutralen, ökologischen Universalreiniger. Der ganze Innenraum inkl. Kofferraum wird gesaugt, Teppiche und Sitze werden shampooniert bzw. das Leder wird gereinigt und mit einer professionellen Ledermilch eingepflegt.
																			</p>
																			<ul>
																				<li class="circle">inkl. shampoonieren</li>
																				<li class="circle">Lederreinigung und -pflege</li>
																				<li class="circle">Reinigung mit Tornador</li>
																			</ul>
																		</div>
																		<div class="right ez-animate col-sm-6 col-md-4" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-md-12">
																			<p>Bei der sogenannten "Tornadorreinigung" wird mit sehr hohem Luftdruck der Innenraum (ohne Himmel) inkl. Armaturen, allen Verkleidungen, Einstiegen, Fälzen und Schlitzen optimal gereinigt. Zum Abschluss werden die Innenscheiben professionell nur mit Wasser gereinigt. <br>Sitze und Teppiche mit problematischen Flecken werden nach dem Shampoonieren getrockenet und gegebenenfalls nochmals shampooniert oder händisch nachbearbeitet. <br>Anbei finden Sie die komplette Preisliste für die Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/auto-10.jpg') }}" data-fancybox="gallery1_b_1">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-10.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/auto-11.jpg') }}" data-fancybox="gallery1_b_1">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-11.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>															
																	</div>
																	<div class="loadmore-btn col-12 text-center mt-4">
																		<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div id="collapse1_b_2" class="collapse" data-parent="#collapse1_a_parent">
														<div class="shadow-sm bg-white p-3">
															<div class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-6 col-md-8">
																			<p> In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. </p>
																			<ul>
																				<li class="circle">hochwertigste Reinigungsmittel</li>
																				<li class="circle">optimale Langzeitpflege</li>
																				<li class="circle">ausschliesslich Swissvax-Produkte</li>
																			</ul>
																		</div>
																		<div class="right ez-animate col-sm-6 col-md-4" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="clearfix"></div>
																		<div class="col-md-12">
																			<p class="pt-5">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2">
																<div class="container">
																	<div class="row align-items-center">
																		<div class="ez-animate col-sm-6 col-md-4 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-6 col-md-8">
																			<div class="ceramic-coating-pading">
																				<h1>Swissvax-Saisonalitäten</h1>
																				<ul>
																					<li class="circle">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
																					<li class="circle">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
																					<li class="circle">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
																				</ul>
																				<p>
																					Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
																					Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
																					Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
																				</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<!-- Related Project -->
																		<div class="related-projects-carousel owl-carousel owl-theme mt-4">

																			<div class="item" data-fancybox="gallery1_b_2">
																				<a href="{{ asset('assets/frontend/images/auto-12.jpg') }}" data-fancybox="gallery1_b_2">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item" data-fancybox="gallery1_b_2">
																				<a href="{{ asset('assets/frontend/images/auto-13.jpg') }}">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/auto-14.jpg') }}">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>																
																		</div>
																		<div class="loadmore-btn col-12 text-center mt-3">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div id="collapse1_b_3" class="collapse" data-parent="#collapse1_a_parent">
														<div class="shadow-sm bg-white p-3">
															<div class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-8">
																			<p>Bei diesen "speziellen Fällen" können wir keine Fixpreise machen, denn der jeweilige Reinigungsaufwand ist von Fall zu Fall sehr unterschiedlich. Entsprechend arbeiten wir nach dem Standardstundensatz von CHF 97.10 inkl. 10% für Material und inkl. Mwst. </p>
																			<ul>
																				<li class="circle">Hundehaare</li>
																				<li class="circle">Geruchsbehandlungen</li>
																				<li class="circle">Schimmel, usw.</li>
																			</ul>
																		</div>
																		<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/special-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																		<div class="col-md-12 pt-5">
																			<p>Ozon-Geruchsbehandlung: Voraussetzung für eine Ozon-Geruchsbehandlung ist die optimale Reinigung mit anschliessender Trocknung. Erst bei einem komplett trockenen Innenraum kann eine wirksame Ozonbehandlung durchgeführt werden. <br>Die Ozonbehandlung verrechnen wir mit CHF 30.-. Teilweise sind aber mehrere Ozonbehandlungen nötig.</p>
																		</div>
																	</div>
																</div>													
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<!-- Related Project -->
																		<h2 class="h5">Beispiele von extremen Verschmutzungen die unserer Spezialbehandlung bedürfen:</h2>
																		<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/special-2.jpg') }}" data-fancybox="gallery1_b_3">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/special-2.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/special-3.jpg') }}" data-fancybox="gallery1_b_3">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/special-3.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																		</div>
																		<h2 class="h5">Vorher-Nachher Gegenüberstellungen bei Extremfällen:</h2>
																		<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																			
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/special-4.jpg') }}" data-fancybox="gallery1_b_3">
																					<img class="img-fluid" src="{{ asset('assets/frontend/images/special-4.jpg') }}" alt="autokosmetik">																		
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/special-5.png') }}" data-fancybox="gallery1_b_3">
																					<img class="img-fluid" src="{{ asset('assets/frontend/images/special-5.png') }}" alt="autokosmetik">
																				</a>
																			</div>
																			
																		</div>
																		<div class="loadmore-btn col-12 text-center mt-3">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>	
										</div>
										<div class="tab-pane fade" id="Folien" role="tabpanel">
											<div id="folianMainParent" class="mt-3">
												<div class="px-2">
													<div class="care-for-carousel owl-carousel owl-theme">
														<div class="item pb-4">
															<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_a_1" aria-expanded="false" aria-controls="collapse2_a_1" role="button">
																<div class="ratio ratio-16X9">
																	<img src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="" class="img-fluid">
																</div>
																<div class="px-2 position-relative">
																	<a href="{{ route('bookappointment')}}" role="button">
																		<div class="align-items-center bg-white d-flex m-0 btn-service">
																			<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																			<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																		</div>
																	</a>
																	<h5 class="font-weight-bold h4 pt-3 m-0">Lack</h5>
																	<p class="h6 py-2">Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschläge...
																		<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																	</p>
																</div>
															</div>
														</div>
														<div class="item pb-4">
															<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_a_2" aria-expanded="false" aria-controls="collapse2_a_2" role="button">
																<div class="ratio ratio-16X9">
																	<img src="{{ asset('assets/frontend/images/tint-10.jpg') }}" alt="" class="img-fluid">
																</div>
																<div class="px-2 position-relative">
																	<a href="{{ route('bookappointment')}}" role="button">
																		<div class="align-items-center bg-white d-flex m-0 btn-service">
																			<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																			<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																		</div>
																	</a>
																	<h5 class="font-weight-bold h4 pt-3 m-0">Scheiben</h5>
																	<p class="h6 py-2">Eine Scheibentönung sieht nicht nur gut aus, sondern schützt in mehrfacher...
																		<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																	</p>
																</div>
															</div>
														</div>
													</div>
													<div id="collapse2_a_1" class="collapse" data-parent="#folianMainParent">
														<div class="shadow-sm bg-white p-3">
															<div class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-6 col-md-8">
																			<p>Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschläge. Unsere verwendete Profifolie ist sehr dick und dadurch auch ganz besonders widerstandsfähig. Allerdings ist Sie sehr schwer zu verlegen und auch relativ kostspielig.</p>
																			<ul>
																				<li class="circle">Hochtransparent/unsichtbar</li>
																				<li class="circle">Der optimale Schutz gegen Steinschlag</li>
																				<li class="circle">Waschstrassenfest</li>
																			</ul>
																		</div>
																		<div class="right ez-animate col-sm-6 col-md-4" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2">
																<div class="container">
																	<div class="row align-items-center">
																		<div class="ez-animate col-sm-6 col-md-4 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-2.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-6 col-md-8">
																			<div class="ceramic-coating-pading">
																				<h1>Besonders geeignet für:</h1>
																				<p>Die Preise für eine Lackschutzfolie variieren sehr stark und sind abhängig von der Beschaffenheit des Autos und Anzahl jener Stellen, welche mit einer Lackschutzfolie versehen werden sollen.</p>
																				<p>Kommen Sie vorbei, wir unterbreiten Ihnen gerne ein unverbindliches Angebot.</p>
																				<ul>
																					<li class="circle">Ladekante und alle Türkanten</li>
																					<li class="circle">Stossstange und Motorhaube</li>
																					<li class="circle">Generell alle exponierten Stellen</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/paint-3.jpg') }}" data-fancybox="gallery1_b_4">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-3.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/paint-4.jpg') }}" data-fancybox="gallery1_b_4">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-4.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/paint-5.jpg') }}" data-fancybox="gallery1_b_4">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-5.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/paint-6.png') }}" data-fancybox="gallery1_b_4">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-6.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/paint-7.png') }}" data-fancybox="gallery1_b_4">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-7.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/paint-8.png') }}" data-fancybox="gallery1_b_4">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-8.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																	</div>
																	<div class="loadmore-btn col-12 text-center mt-4">
																		<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																	</div>
																	
																</div>
															</div>
														</div>
													</div>
													<div id="collapse2_a_2" class="collapse" data-parent="#folianMainParent">
														<div class="shadow-sm bg-white p-3">
															<div class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-8">
																			<p>Eine Scheibentönung sieht nicht nur gut aus, sondern schützt in mehrfacher Hinsicht. Der Innenraum heizt weniger auf, 99% der schädlichen UV-Strahlung werden abgehalten und sie schützt vor neugierigen Blicken.<br> Wir arbeiten ausschliesslich mit Markenfolien der Firma Johnson Window Films, Folie Marathon, und gewähren 7 Jahre Garantie. Jede Scheibentönung erhält ein Zertifikat.</p>
																			<ul>
																				<li class="circle">99%-iger UV-Schutz</li>
																				<li class="circle">Hitzereduktion, Blickschutz</li>
																				<li class="circle">Edle Optik</li>
																			</ul>
																		</div>
																		<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-10.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2">
																<div class="container">
																	<div class="row align-items-center">
																		<div class="ez-animate col-sm-6 col-md-4 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-11.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-6 col-md-8">
																			<div class="ceramic-coating-pading">
																				<h1>Besonders geeignet für:</h1>
																				<p>Kofferraum oder auf der Rückbank</p>
																				<p class="pt-3">Anbei finden Sie die komplette Preisliste für fast alle Automarken - und Modelle. Sollte Ihr Auto nicht gelistet sein, bitten wir um ein kurzes Telefonat oder Mail. Wir werden Ihnen umgehend ein Angebot unterbreiten.</p>
																				<ul>
																					<li class="circle">Familienautos</li>
																					<li class="circle">Personen im Fond</li>
																					<li class="circle">Hundeautos</li>
																					<li class="circle">Wertgegenstände im</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-1.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-1.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-2.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-2.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-3.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-3.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-4.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-4.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-5.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-5.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-6.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-6.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-7.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-7.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-8.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-8.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-9.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-9.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																		</div>
																		<div class="loadmore-btn col-12 text-center mt-3">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="Beschichtung" role="tabpanel">
											<div id="folianMainParent" class="mt-3">
												<div class="care-for-carousel owl-carousel owl-theme">
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_b_1" aria-expanded="false" aria-controls="collapse2_b_1" role="button">
															<div class="ratio ratio-16X9">
																<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Keramikbeschichtung</h5>
																<p class="h6 py-2">Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und...
																	<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_b_2" aria-expanded="false" aria-controls="collapse2_b_2" role="button">
															<div class="ratio ratio-16X9">
																<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Teflonversiegelung</h5>
																<p class="h6 py-2">Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und ...
																	<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
												</div>
												<div id="collapse2_b_1" class="collapse" data-parent="#folianMainParent">
													<div class="shadow-sm bg-white p-3">
														<div class="section-car-ceramic">
															<div class="container">
																<div class="row">
																	<div class="left col-sm-12 col-md-8">
																		<p>
																		Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und ist absolut waschstrassenfest. Besonders geeignet ist unsere Keramikbeschichtung für dunkle, grosse Autos, welche im Alltagseinsatz stehen. Jede Keramikbeschichtung erhält ein Zertifikat, welches die Haltbarkeit von Minimum 3 Jahren garantiert.
																		</p>
																		<ul>
																			<li class="circle">Der panzerglasartige Lackschutz</li>
																			<li class="circle">Haltbarkeit 36+ Monate</li>
																			<li class="circle">Waschstrassenfest</li>
																		</ul>
																	</div>
																	<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="autokosmetik"> 
																	</div>
																</div>
															</div>
														</div>
														<div class="services2-car-bg-2">
															<div class="container">
																<div class="row align-items-center">
																	<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
																	</div>
																	<div class="col-sm-12 col-md-7">
																		<div class="ceramic-coating-pading">
																			<h1>Unser neues Keramik-Material ServFaces</h1>
																			<p>In den ersten 3 Jahren unserer Arbeit mit sogenannten Keramikbeschichtungen, haben wir mit einem fernöstlichen Produkt gearbeitet. Bis wir feststellen mussten, dass sich das Material auf einmal anders verhält  und auch anders zu verarbeiten ist. Für unsere Firma ein absolutes "NO GO". <br><br> Wir konnten jedoch in kürzester Zeit eine Ersatz-Produktlinie aus deutscher Produktion finden und haben seit nunmehr 2 Jahren nur die besten Erfahrungen gemacht. Und so konnten wir unsere Garantie von 2 auf 3 Jahre erhöhen. Ab Anfang 2016 haben wir deshalb auch die Vertretung von ServFaces für die Schweiz und Deutschland übernommen.</p>
																			<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="ceramic-coting-images">
															<div class="container">
																<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-1.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-1.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-2.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-2.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-3.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-3.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a rel="lack" href="{{ asset('assets/frontend/images/auto-4.jpg') }}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-4.jpg') }}" alt="autokosmetik">
																			
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-5.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-5.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-6.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-6.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-7.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-7.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-8.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-8.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-9.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-9.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	
																</div>
																<div class="loadmore-btn col-12 text-center mt-4">
																	<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																</div>
																
															</div>
														</div>
													</div>
												</div>
												<div id="collapse2_b_2" class="collapse" data-parent="#folianMainParent">
													<div class="shadow-sm bg-white p-3">
														<div class="section-car-ceramic">
															<div class="container">
																<div class="row">
																	<div class="left col-sm-12 col-md-8">
																		<p>
																		Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und genügt für die meisten Autos. Ausserdem ist sie sehr preiswert. Wir empfehlen jedoch bei der Teflon-Versiegelung Hand- oder Lanzenwäsche, da die Waschstrasse die Versiegelung zu schnell abreibt.
																		</p>
																		<ul>
																			<li class="circle">Der Standard-Lackschutz</li>
																			<li class="circle">Haltbarkeit 6+ Monate</li>
																			<li class="circle">Preiswert</li>
																		</ul>
																	</div>
																	<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="autokosmetik">
																	</div>
																</div>
															</div>
														</div>
														<div class="services2-car-bg-2">
															<div class="container">
																<div class="row align-items-center">
																	<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																		<img class="w-100" src="{{ asset('assets/frontend/images/seal-2.jpg') }}" alt="autokosmetik">
																	</div>
																	<div class="col-sm-12 col-md-7">
																		<div class="ceramic-coating-pading">
																			<h1>Besonders geeignet für:</h1>
																			<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen. <br> </p>
																			<ul>
																				<li class="circle_2 m-0 h6">alle Autos, Motorräder, Wohnmobile, LKW's usw.</li>
																				<li class="circle_2 m-0 h6">Gebrauchtwagen</li>
																				<li class="circle_2 m-0 h6">Leasingrückgaben</li>
																			</ul>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="ceramic-coting-images">
															<div class="container">
																<div class="row">
																	<!-- Related Project -->
																	<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-3.jpg')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-3.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-4.jpg')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-4.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-5.jpg')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-5.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-6.jpg')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-6.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-7.jpg')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-7.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-8.png')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-8.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-9.png')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-9.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-10.png')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-10.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-11.png')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-11.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		
																	</div>
																	<div class="loadmore-btn col-12 text-center mt-3">
																		<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="Komplettfolierung" role="tabpanel">
											<div id="service-category-collapse-3">
												<div id="DesignMainParent" class="mt-3">
													<div id="collapse3_a" data-parent="#service-category-collapse-3">
														<div class="care-for-carousel owl-carousel owl-theme">
															<div class="item pb-4">
																<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_a_1" aria-expanded="false" aria-controls="collapse3_a_1" role="button">
																	<div class="ratio ratio-16X9">
																		<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
																	</div>
																	<div class="px-2 position-relative">
																		<a href="{{ route('bookappointment')}}" role="button">
																			<div class="align-items-center bg-white d-flex m-0 btn-service">
																				<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																				<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																			</div>
																		</a>
																		<h5 class="font-weight-bold h4 pt-3 m-0">Mono</h5>
																		<p class="h6 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda ...
																			<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																		</p>
																	</div>
																</div>
															</div>
															<div class="item pb-4">
																<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_a_2" aria-expanded="false" aria-controls="collapse3_a_2" role="button">
																	<div class="ratio ratio-16X9">
																		<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
																	</div>
																	<div class="px-2 position-relative">
																		<a href="{{ route('bookappointment')}}" role="button">
																			<div class="align-items-center bg-white d-flex m-0 btn-service">
																				<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																				<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																			</div>
																		</a>
																		<h5 class="font-weight-bold h4 pt-3 m-0">Design</h5>
																		<p class="h6 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda ...
																			<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div id="collapse3_a_1" class="collapse" data-parent="#DesignMainParent">
															<div class="shadow-sm bg-white p-3">
																<div class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-12">
																				<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur fugit iste, fuga ea, quae dolore hic facere consequatur vitae repudiandae laboriosam? Laudantium est aut expedita cumque placeat. Neque, dicta doloribus</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div id="collapse3_a_2" class="collapse" data-parent="#DesignMainParent">
															<div class="shadow-sm bg-white p-3">
																<div class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-12">
																				<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. In odio quibusdam, numquam consequuntur eum quam, fugit quis ut eveniet id corporis dolores voluptates aspernatur quos quo necessitatibus, pariatur assumenda dolorem.</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="Komplettfolierung" role="tabpanel">
											<div id="service-category-collapse-3">
												<div id="DesignMainParent" class="mt-3">
													<div id="collapse3_b" data-parent="#service-category-collapse-3">
														<div id="service-category-2" class="care-for-carousel owl-carousel owl-theme">
															<div class="item pb-4">
																<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_b_1" aria-expanded="false" aria-controls="collapse3_b_1" role="button">
																	<div class="ratio ratio-16X9">
																		<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
																	</div>
																	<div class="px-2 position-relative">
																		<a href="{{ route('bookappointment')}}" role="button">
																			<div class="align-items-center bg-white d-flex m-0 btn-service">
																				<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																				<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																			</div>
																		</a>
																		<h5 class="font-weight-bold h4 pt-3 m-0">Mono</h5>
																		<p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...
																			<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																		</p>
																	</div>
																</div>
															</div>
															<div class="item pb-4">
																<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_b_2" aria-expanded="false" aria-controls="collapse3_b_2" role="button">
																	<div class="ratio ratio-16X9">
																		<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
																	</div>
																	<div class="px-2 position-relative">
																		<a href="{{ route('bookappointment')}}" role="button">
																			<div class="align-items-center bg-white d-flex m-0 btn-service">
																				<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																				<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																			</div>
																		</a>
																		<h5 class="font-weight-bold h4 pt-3 m-0">Design</h5>
																		<p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...
																			<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div id="collapse3_b_1" class="collapse" data-parent="#DesignMainParent">
															<div class="shadow-sm bg-white p-3">
																<div class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-12">
																				<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nesciunt delectus maiores inventore quidem repellat. Natus consequatur illo blanditiis ducimus, impedit eius. Molestiae, dolorem. Quibusdam aperiam eos sapiente harum distinctio adipisci.</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div id="collapse3_b_2" class="collapse" data-parent="#DesignMainParent">
															<div class="shadow-sm bg-white p-3">
																<div class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-12">
																				<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis quam accusantium itaque nobis reprehenderit minus laborum molestiae velit esse cupiditate provident, unde excepturi voluptatum consequuntur, autem optio quos eveniet vitae.</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="pills-bike" role="tabpanel">
								<div class="tabpanel-service main-tabpanel-service">
									<ul class="nav justify-content-start nav-pills mb-3" id="pills-tab" role="tablist">
										<li class="nav-item" role="presentation">
											<button class="nav-link active" data-toggle="pill" data-target="#pills-care-bike" type="button" role="tab" aria-controls="pills-care-bike" aria-selected="false">
												Pflege
											</button>
										</li>
										<li class="nav-item" role="presentation">
											<button class="nav-link " data-toggle="pill" data-target="#pills-protect-bike" type="button" role="tab" aria-controls="pills-protect-bike" aria-selected="false">
												Schutz
											</button>
										</li>
										<li class="nav-item" role="presentation">
											<button class="nav-link " data-toggle="pill" data-target="#pills-enhance-nike" type="button" role="tab" aria-controls="pills-enhance-nike" aria-selected="false">
												Design
											</button>
										</li>
									</ul>
								</div>
								<div class="tabpanel-service">
									<div class="tab-content" id="pills-tabContent">
										<div class="tab-pane fade show active" id="pills-care-bike" role="tabpanel">
											<ul class="nav justify-content-start nav-tabs mb-3" id="pills-tab" role="tablist">
												<li class="nav-item" role="presentation">
													<button class="nav-link active" data-toggle="pill" data-target="#Aussenaufbereitung" type="button" role="tab" aria-controls="Aussenaufbereitung" aria-selected="false">
														Aussenaufbereitung
													</button>
												</li>
												<li class="nav-item" role="presentation">
													<button class="nav-link " data-toggle="pill" data-target="#Innenaufbereitung" type="button" role="tab" aria-controls="Innenaufbereitung" aria-selected="false">
														Innenaufbereitung
													</button>
												</li>
											</ul>
										</div>
										<div class="tab-pane fade" id="pills-protect-bike" role="tabpanel">
											<ul class="nav justify-content-start nav-tabs mb-3" id="pills-tab" role="tablist">
												<li class="nav-item" role="presentation">
													<button class="nav-link active" data-toggle="pill" data-target="#Folien" type="button" role="tab" aria-controls="Folien" aria-selected="false">
														Folien
													</button>
												</li>
												<li class="nav-item" role="presentation">
													<button class="nav-link " data-toggle="pill" data-target="#Beschichtung" type="button" role="tab" aria-controls="Beschichtung" aria-selected="false">
														Beschichtung
													</button>
												</li>
											</ul>
										</div>
										<div class="tab-pane fade" id="pills-enhance-nike" role="tabpanel">
											<ul class="nav justify-content-start nav-tabs mb-3" id="pills-tab" role="tablist">
												<li class="nav-item" role="presentation">
													<button class="nav-link active" data-toggle="pill" data-target="#Komplettfolierung" type="button" role="tab" aria-controls="Komplettfolierung" aria-selected="false">
														Komplettfolierung
													</button>
												</li>
												<li class="nav-item" role="presentation">
													<button class="nav-link " data-toggle="pill" data-target="#Teilfolierungen" type="button" role="tab" aria-controls="Teilfolierungen" aria-selected="false">
														Teilfolierungen
													</button>
												</li>
												<li class="nav-item" role="presentation">
													<button class="nav-link" data-toggle="pill" data-target="#Fensterfolierung" type="button" role="tab" aria-controls="Fensterfolierung" aria-selected="false">
														Fensterfolierung
													</button>
												</li>
												<li class="nav-item" role="presentation">
													<button class="nav-link " data-toggle="pill" data-target="#Interieur" type="button" role="tab" aria-controls="Interieur" aria-selected="false">
														Interieur
													</button>
												</li>
												<li class="nav-item" role="presentation">
													<button class="nav-link " data-toggle="pill" data-target="#Teilfolierungen" type="button" role="tab" aria-controls="Teilfolierungen" aria-selected="false">
														Teilfolierungen
													</button>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="tabpanel-service">
									<div class="tab-content" id="pills-tabContent">
										<div class="tab-pane fade show active" id="Aussenaufbereitung" role="tabpanel">
											<div id="collapse1_a_parent" class="mt-3 accordion px-2">
												<div class="care-for-carousel owl-carousel owl-theme">
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_a_1" aria-expanded="false" aria-controls="collapse1_a_1" role="button">
															<div class="ratio ratio-16X9">
																<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Keramikbeschichtung</h5>
																<p class="h6 py-2">Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und...
																	<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_a_2" aria-expanded="false" aria-controls="collapse1_a_2" role="button">
															<div class="ratio ratio-16X9">
																<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Teflonversiegelung</h5>
																<p class="h6 py-2">Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem...
																	<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_a_3" aria-expanded="false" aria-controls="collapse1_a_3" role="button">
															<div class="ratio ratio-16X9">	
																<img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Swissvax</h5>
																<p class="h6 py-2">In unserem Swissvax Car Care Center Liechtenstein/Rheintal ..
																	<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
												</div>

												<div id="collapse1_a_1" class="collapse" data-parent="#collapse1_a_parent">
													<div class="shadow-sm bg-white p-3">
														<div class="section-car-ceramic">
															<div class="container">
																<div class="row">
																	<div class="left col-sm-12 col-md-8">
																		<p>
																		Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und ist absolut waschstrassenfest. Besonders geeignet ist unsere Keramikbeschichtung für dunkle, grosse Autos, welche im Alltagseinsatz stehen. Jede Keramikbeschichtung erhält ein Zertifikat, welches die Haltbarkeit von Minimum 3 Jahren garantiert.
																		</p>
																		<ul>
																			<li class="circle">Der panzerglasartige Lackschutz</li>
																			<li class="circle">Haltbarkeit 36+ Monate</li>
																			<li class="circle">Waschstrassenfest</li>
																		</ul>
																	</div>
																	<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="autokosmetik"> 
																	</div>
																</div>
															</div>
														</div>
														<div class="services2-car-bg-2">
															<div class="container">
																<div class="row align-items-center">
																	<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
																	</div>
																	<div class="col-sm-12 col-md-7">
																		<div class="ceramic-coating-pading">
																			<h1>Unser neues Keramik-Material ServFaces</h1>
																			<p>In den ersten 3 Jahren unserer Arbeit mit sogenannten Keramikbeschichtungen, haben wir mit einem fernöstlichen Produkt gearbeitet. Bis wir feststellen mussten, dass sich das Material auf einmal anders verhält  und auch anders zu verarbeiten ist. Für unsere Firma ein absolutes "NO GO". <br><br> Wir konnten jedoch in kürzester Zeit eine Ersatz-Produktlinie aus deutscher Produktion finden und haben seit nunmehr 2 Jahren nur die besten Erfahrungen gemacht. Und so konnten wir unsere Garantie von 2 auf 3 Jahre erhöhen. Ab Anfang 2016 haben wir deshalb auch die Vertretung von ServFaces für die Schweiz und Deutschland übernommen.</p>
																			<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="ceramic-coting-images">
															<div class="container">
																<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-1.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-1.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-2.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-2.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-3.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-3.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-4.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-4.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-5.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-5.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-6.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-6.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-7.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-7.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-8.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-8.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-9.jpg')}}" data-fancybox="gallery">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-9.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																</div>
																<div class="loadmore-btn col-12 text-center mt-4">
																	<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div id="collapse1_a_2" class="collapse" data-parent="#collapse1_a_parent">
													<div class="shadow-sm bg-white p-3">
														<div class="section-car-ceramic">
															<div class="container">
																<div class="row">
																	<div class="left col-sm-12 col-md-8">
																		<p>
																		Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und genügt für die meisten Autos. Ausserdem ist sie sehr preiswert. Wir empfehlen jedoch bei der Teflon-Versiegelung Hand- oder Lanzenwäsche, da die Waschstrasse die Versiegelung zu schnell abreibt.
																		</p>
																		<ul>
																			<li class="circle">Der Standard-Lackschutz</li>
																			<li class="circle">Haltbarkeit 6+ Monate</li>
																			<li class="circle">Preiswert</li>
																		</ul>
																	</div>
																	<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="autokosmetik">
																	</div>
																</div>
															</div>
														</div>
														<div class="services2-car-bg-2">
															<div class="container">
																<div class="row align-items-center">
																	<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																		<img class="w-100" src="{{ asset('assets/frontend/images/seal-2.jpg') }}" alt="autokosmetik">
																	</div>
																	<div class="col-sm-12 col-md-7">
																		<div class="ceramic-coating-pading">
																			<h1>Besonders geeignet für:</h1>
																			<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen. <br> </p>
																			<ul>
																				<li class="circle_2 m-0 h6">alle Autos, Motorräder, Wohnmobile, LKW's usw.</li>
																				<li class="circle_2 m-0 h6">Gebrauchtwagen</li>
																				<li class="circle_2 m-0 h6">Leasingrückgaben</li>
																			</ul>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="ceramic-coting-images">
															<div class="container">
																<div class="row">
																	<!-- Related Project -->
																	<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-3.jpg')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-3.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-4.jpg')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-4.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-5.jpg')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-5.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-6.jpg')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-6.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-7.jpg')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-7.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-8.png')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-8.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-9.png')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-9.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-10.png')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-10.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/seal-11.png')}}" data-fancybox="gallery1_a_2">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-11.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		
																	</div>
																	<div class="loadmore-btn col-12 text-center mt-3">
																		<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div id="collapse1_a_3" class="collapse" data-parent="#collapse1_a_parent">
													<div class="shadow-sm bg-white p-3">
														<div class="section-car-ceramic">
															
															<div class="row">
																<div class="left col-sm-12 col-md-8">
																	<p> In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. </p>
																	<ul>
																		<li class="circle">hochwertigste Reinigungsmittel</li>
																		<li class="circle">optimale Langzeitpflege</li>
																		<li class="circle">ausschliesslich Swissvax-Produkte</li>
																	</ul>
																	
																</div>
																<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																	<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
																</div>
																
															</div>
															<p class="pt-5">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
															
														</div>
														<div class="services2-car-bg-2">
															<div class="container">
																<div class="row align-items-center">
																	<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																	</div>
																	<div class="col-sm-12 col-md-7">
																		<div class="ceramic-coating-pading">
																			<h1>Swissvax-Saisonalitäten</h1>
																			<ul>
																				<li class="circle">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
																				<li class="circle">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
																				<li class="circle">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
																			</ul>
																			<p>
																				Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
																				Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
																				Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
																			</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="ceramic-coting-images">
															<div class="container">
																<div class="row">
																	<!-- Related Project -->
																	<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/auto-12.jpg')}}" data-fancybox="gallery1_a_3">
																				<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/auto-13.jpg')}}" data-fancybox="gallery1_a_3">
																				<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{asset('assets/frontend/images/auto-14.jpg')}}" data-fancybox="gallery1_a_3">
																				<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																	</div>
																	<div class="loadmore-btn col-12 text-center mt-3">
																		<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="Innenaufbereitung" role="tabpanel">
											<div id="collapse1_a_parent" class="mt-3">
												<div class=" px-2">
													<div class="care-for-carousel owl-carousel owl-theme">
														<div class="item pb-4">
															<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_b_1" aria-expanded="false" aria-controls="collapse1_b_1" role="button">
																<div class="ratio ratio-16X9">
																	<img src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="" class="img-fluid">
																</div>
																<div class="px-2 position-relative">
																	<a href="{{ route('bookappointment')}}" role="button">
																		<div class="align-items-center bg-white d-flex m-0 btn-service">
																			<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																			<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																		</div>
																	</a>
																	<h5 class="font-weight-bold h4 pt-3 m-0">Herkömmlich</h5>
																	<p class="h6 py-2">Bei einer herkömmlichen Innenaufbereitung arbeiten wir mit einem...
																		<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																	</p>
																</div>
															</div>
														</div>
														<div class="item pb-4">
															<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_b_2" aria-expanded="false" aria-controls="collapse1_b_2" role="button">
																<div class="ratio ratio-16X9">
																	<img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="" class="img-fluid">
																</div>
																<div class="px-2 position-relative">
																	<a href="{{ route('bookappointment')}}" role="button">
																		<div class="align-items-center bg-white d-flex m-0 btn-service">
																			<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																			<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																		</div>
																	</a>
																	<h5 class="font-weight-bold h4 pt-3 m-0">Swissvax</h5>
																	<p class="h6 py-2">In unserem Swissvax Car Care Center Liechtenstein/Rheintal...
																		<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																	</p>
																</div>
															</div>
														</div>
														<div class="item pb-4">
															<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_b_3" aria-expanded="false" aria-controls="collapse1_b_3" role="button">
																<div class="ratio ratio-16X9">	
																	<img src="{{ asset('assets/frontend/images/special-1.jpg') }}" alt="" class="img-fluid">
																</div>
																<div class="px-2 position-relative">
																	<a href="{{ route('bookappointment')}}" role="button">
																		<div class="align-items-center bg-white d-flex m-0 btn-service">
																			<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																			<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																		</div>
																	</a>
																	<h5 class="font-weight-bold h4 pt-3 m-0">SPEZIALFÄLLE</h5>
																	<p class="h6 py-2">Bei diesen "speziellen Fällen" können wir keine Fixpreise machen, denn...
																		<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																	</p>
																</div>
															</div>
														</div>
													</div>
													<div id="collapse1_b_1" class="collapse" data-parent="#collapse1_a_parent">
														<div class="shadow-sm bg-white p-3">
															<div class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-6 col-md-8">
																			<p>
																			Bei einer herkömmlichen Innenaufbereitung arbeiten wir mit einem neutralen, ökologischen Universalreiniger. Der ganze Innenraum inkl. Kofferraum wird gesaugt, Teppiche und Sitze werden shampooniert bzw. das Leder wird gereinigt und mit einer professionellen Ledermilch eingepflegt.
																			</p>
																			<ul>
																				<li class="circle">inkl. shampoonieren</li>
																				<li class="circle">Lederreinigung und -pflege</li>
																				<li class="circle">Reinigung mit Tornador</li>
																			</ul>
																		</div>
																		<div class="right ez-animate col-sm-6 col-md-4" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-md-12">
																			<p>Bei der sogenannten "Tornadorreinigung" wird mit sehr hohem Luftdruck der Innenraum (ohne Himmel) inkl. Armaturen, allen Verkleidungen, Einstiegen, Fälzen und Schlitzen optimal gereinigt. Zum Abschluss werden die Innenscheiben professionell nur mit Wasser gereinigt. <br>Sitze und Teppiche mit problematischen Flecken werden nach dem Shampoonieren getrockenet und gegebenenfalls nochmals shampooniert oder händisch nachbearbeitet. <br>Anbei finden Sie die komplette Preisliste für die Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/auto-10.jpg') }}" data-fancybox="gallery1_b_1">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-10.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/auto-11.jpg') }}" data-fancybox="gallery1_b_1">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-11.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>															
																	</div>
																	<div class="loadmore-btn col-12 text-center mt-4">
																		<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div id="collapse1_b_2" class="collapse" data-parent="#collapse1_a_parent">
														<div class="shadow-sm bg-white p-3">
															<div class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-6 col-md-8">
																			<p> In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. </p>
																			<ul>
																				<li class="circle">hochwertigste Reinigungsmittel</li>
																				<li class="circle">optimale Langzeitpflege</li>
																				<li class="circle">ausschliesslich Swissvax-Produkte</li>
																			</ul>
																		</div>
																		<div class="right ez-animate col-sm-6 col-md-4" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="clearfix"></div>
																		<div class="col-md-12">
																			<p class="pt-5">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2">
																<div class="container">
																	<div class="row align-items-center">
																		<div class="ez-animate col-sm-6 col-md-4 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-6 col-md-8">
																			<div class="ceramic-coating-pading">
																				<h1>Swissvax-Saisonalitäten</h1>
																				<ul>
																					<li class="circle">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
																					<li class="circle">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
																					<li class="circle">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
																				</ul>
																				<p>
																					Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
																					Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
																					Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
																				</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<!-- Related Project -->
																		<div class="related-projects-carousel owl-carousel owl-theme mt-4">

																			<div class="item" data-fancybox="gallery1_b_2">
																				<a href="{{ asset('assets/frontend/images/auto-12.jpg') }}" data-fancybox="gallery1_b_2">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item" data-fancybox="gallery1_b_2">
																				<a href="{{ asset('assets/frontend/images/auto-13.jpg') }}">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/auto-14.jpg') }}">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>																
																		</div>
																		<div class="loadmore-btn col-12 text-center mt-3">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div id="collapse1_b_3" class="collapse" data-parent="#collapse1_a_parent">
														<div class="shadow-sm bg-white p-3">
															<div class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-8">
																			<p>Bei diesen "speziellen Fällen" können wir keine Fixpreise machen, denn der jeweilige Reinigungsaufwand ist von Fall zu Fall sehr unterschiedlich. Entsprechend arbeiten wir nach dem Standardstundensatz von CHF 97.10 inkl. 10% für Material und inkl. Mwst. </p>
																			<ul>
																				<li class="circle">Hundehaare</li>
																				<li class="circle">Geruchsbehandlungen</li>
																				<li class="circle">Schimmel, usw.</li>
																			</ul>
																		</div>
																		<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/special-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																		<div class="col-md-12 pt-5">
																			<p>Ozon-Geruchsbehandlung: Voraussetzung für eine Ozon-Geruchsbehandlung ist die optimale Reinigung mit anschliessender Trocknung. Erst bei einem komplett trockenen Innenraum kann eine wirksame Ozonbehandlung durchgeführt werden. <br>Die Ozonbehandlung verrechnen wir mit CHF 30.-. Teilweise sind aber mehrere Ozonbehandlungen nötig.</p>
																		</div>
																	</div>
																</div>													
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<!-- Related Project -->
																		<h2 class="h5">Beispiele von extremen Verschmutzungen die unserer Spezialbehandlung bedürfen:</h2>
																		<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/special-2.jpg') }}" data-fancybox="gallery1_b_3">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/special-2.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/special-3.jpg') }}" data-fancybox="gallery1_b_3">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/special-3.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																		</div>
																		<h2 class="h5">Vorher-Nachher Gegenüberstellungen bei Extremfällen:</h2>
																		<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																			
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/special-4.jpg') }}" data-fancybox="gallery1_b_3">
																					<img class="img-fluid" src="{{ asset('assets/frontend/images/special-4.jpg') }}" alt="autokosmetik">																		
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/special-5.png') }}" data-fancybox="gallery1_b_3">
																					<img class="img-fluid" src="{{ asset('assets/frontend/images/special-5.png') }}" alt="autokosmetik">
																				</a>
																			</div>
																			
																		</div>
																		<div class="loadmore-btn col-12 text-center mt-3">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>	
										</div>
										<div class="tab-pane fade" id="Folien" role="tabpanel">
											<div id="folianMainParent" class="mt-3">
												<div class="px-2">
													<div class="care-for-carousel owl-carousel owl-theme">
														<div class="item pb-4">
															<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_a_1" aria-expanded="false" aria-controls="collapse2_a_1" role="button">
																<div class="ratio ratio-16X9">
																	<img src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="" class="img-fluid">
																</div>
																<div class="px-2 position-relative">
																	<a href="{{ route('bookappointment')}}" role="button">
																		<div class="align-items-center bg-white d-flex m-0 btn-service">
																			<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																			<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																		</div>
																	</a>
																	<h5 class="font-weight-bold h4 pt-3 m-0">Lack</h5>
																	<p class="h6 py-2">Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschläge...
																		<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																	</p>
																</div>
															</div>
														</div>
														<div class="item pb-4">
															<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_a_2" aria-expanded="false" aria-controls="collapse2_a_2" role="button">
																<div class="ratio ratio-16X9">
																	<img src="{{ asset('assets/frontend/images/tint-10.jpg') }}" alt="" class="img-fluid">
																</div>
																<div class="px-2 position-relative">
																	<a href="{{ route('bookappointment')}}" role="button">
																		<div class="align-items-center bg-white d-flex m-0 btn-service">
																			<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																			<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																		</div>
																	</a>
																	<h5 class="font-weight-bold h4 pt-3 m-0">Scheiben</h5>
																	<p class="h6 py-2">Eine Scheibentönung sieht nicht nur gut aus, sondern schützt in mehrfacher...
																		<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																	</p>
																</div>
															</div>
														</div>
													</div>
													<div id="collapse2_a_1" class="collapse" data-parent="#folianMainParent">
														<div class="shadow-sm bg-white p-3">
															<div class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-6 col-md-8">
																			<p>Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschläge. Unsere verwendete Profifolie ist sehr dick und dadurch auch ganz besonders widerstandsfähig. Allerdings ist Sie sehr schwer zu verlegen und auch relativ kostspielig.</p>
																			<ul>
																				<li class="circle">Hochtransparent/unsichtbar</li>
																				<li class="circle">Der optimale Schutz gegen Steinschlag</li>
																				<li class="circle">Waschstrassenfest</li>
																			</ul>
																		</div>
																		<div class="right ez-animate col-sm-6 col-md-4" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2">
																<div class="container">
																	<div class="row align-items-center">
																		<div class="ez-animate col-sm-6 col-md-4 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-2.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-6 col-md-8">
																			<div class="ceramic-coating-pading">
																				<h1>Besonders geeignet für:</h1>
																				<p>Die Preise für eine Lackschutzfolie variieren sehr stark und sind abhängig von der Beschaffenheit des Autos und Anzahl jener Stellen, welche mit einer Lackschutzfolie versehen werden sollen.</p>
																				<p>Kommen Sie vorbei, wir unterbreiten Ihnen gerne ein unverbindliches Angebot.</p>
																				<ul>
																					<li class="circle">Ladekante und alle Türkanten</li>
																					<li class="circle">Stossstange und Motorhaube</li>
																					<li class="circle">Generell alle exponierten Stellen</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/paint-3.jpg') }}" data-fancybox="gallery1_b_4">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-3.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/paint-4.jpg') }}" data-fancybox="gallery1_b_4">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-4.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/paint-5.jpg') }}" data-fancybox="gallery1_b_4">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-5.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/paint-6.png') }}" data-fancybox="gallery1_b_4">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-6.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/paint-7.png') }}" data-fancybox="gallery1_b_4">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-7.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a href="{{ asset('assets/frontend/images/paint-8.png') }}" data-fancybox="gallery1_b_4">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-8.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																	</div>
																	<div class="loadmore-btn col-12 text-center mt-4">
																		<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																	</div>
																	
																</div>
															</div>
														</div>
													</div>
													<div id="collapse2_a_2" class="collapse" data-parent="#folianMainParent">
														<div class="shadow-sm bg-white p-3">
															<div class="section-car-ceramic">
																<div class="container">
																	<div class="row">
																		<div class="left col-sm-12 col-md-8">
																			<p>Eine Scheibentönung sieht nicht nur gut aus, sondern schützt in mehrfacher Hinsicht. Der Innenraum heizt weniger auf, 99% der schädlichen UV-Strahlung werden abgehalten und sie schützt vor neugierigen Blicken.<br> Wir arbeiten ausschliesslich mit Markenfolien der Firma Johnson Window Films, Folie Marathon, und gewähren 7 Jahre Garantie. Jede Scheibentönung erhält ein Zertifikat.</p>
																			<ul>
																				<li class="circle">99%-iger UV-Schutz</li>
																				<li class="circle">Hitzereduktion, Blickschutz</li>
																				<li class="circle">Edle Optik</li>
																			</ul>
																		</div>
																		<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-10.jpg') }}" alt="autokosmetik"> 
																		</div>
																	</div>
																</div>
															</div>
															<div class="services2-car-bg-2">
																<div class="container">
																	<div class="row align-items-center">
																		<div class="ez-animate col-sm-6 col-md-4 text-center" data-animation="fadeInLeft">
																			<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-11.jpg') }}" alt="autokosmetik">
																		</div>
																		<div class="col-sm-6 col-md-8">
																			<div class="ceramic-coating-pading">
																				<h1>Besonders geeignet für:</h1>
																				<p>Kofferraum oder auf der Rückbank</p>
																				<p class="pt-3">Anbei finden Sie die komplette Preisliste für fast alle Automarken - und Modelle. Sollte Ihr Auto nicht gelistet sein, bitten wir um ein kurzes Telefonat oder Mail. Wir werden Ihnen umgehend ein Angebot unterbreiten.</p>
																				<ul>
																					<li class="circle">Familienautos</li>
																					<li class="circle">Personen im Fond</li>
																					<li class="circle">Hundeautos</li>
																					<li class="circle">Wertgegenstände im</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="ceramic-coting-images">
																<div class="container">
																	<div class="row">
																		<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-1.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-1.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-2.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-2.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-3.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-3.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-4.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-4.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-5.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-5.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-6.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-6.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-7.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-7.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-8.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-8.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																			<div class="item">
																				<a href="{{ asset('assets/frontend/images/tint-9.jpg') }}" data-fancybox="gallery1_b_5">
																					<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/tint-9.jpg') }}" alt="autokosmetik">
																				</a>
																			</div>
																		</div>
																		<div class="loadmore-btn col-12 text-center mt-3">
																			<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="Beschichtung" role="tabpanel">
											<div id="folianMainParent" class="mt-3">
												<div class="care-for-carousel owl-carousel owl-theme">
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_b_1" aria-expanded="false" aria-controls="collapse2_b_1" role="button">
															<div class="ratio ratio-16X9">
																<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Keramikbeschichtung</h5>
																<p class="h6 py-2">Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und...
																	<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
													<div class="item pb-4">
														<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_b_2" aria-expanded="false" aria-controls="collapse2_b_2" role="button">
															<div class="ratio ratio-16X9">
																<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
															</div>
															<div class="px-2 position-relative">
																<a href="{{ route('bookappointment')}}" role="button">
																	<div class="align-items-center bg-white d-flex m-0 btn-service">
																		<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																		<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																	</div>
																</a>
																<h5 class="font-weight-bold h4 pt-3 m-0">Teflonversiegelung</h5>
																<p class="h6 py-2">Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und ...
																	<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																</p>
															</div>
														</div>
													</div>
												</div>
												<div id="collapse2_b_1" class="collapse" data-parent="#folianMainParent">
													<div class="shadow-sm bg-white p-3">
														<div class="section-car-ceramic">
															<div class="container">
																<div class="row">
																	<div class="left col-sm-12 col-md-8">
																		<p>
																		Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und ist absolut waschstrassenfest. Besonders geeignet ist unsere Keramikbeschichtung für dunkle, grosse Autos, welche im Alltagseinsatz stehen. Jede Keramikbeschichtung erhält ein Zertifikat, welches die Haltbarkeit von Minimum 3 Jahren garantiert.
																		</p>
																		<ul>
																			<li class="circle">Der panzerglasartige Lackschutz</li>
																			<li class="circle">Haltbarkeit 36+ Monate</li>
																			<li class="circle">Waschstrassenfest</li>
																		</ul>
																	</div>
																	<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="autokosmetik"> 
																	</div>
																</div>
															</div>
														</div>
														<div class="services2-car-bg-2">
															<div class="container">
																<div class="row align-items-center">
																	<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
																	</div>
																	<div class="col-sm-12 col-md-7">
																		<div class="ceramic-coating-pading">
																			<h1>Unser neues Keramik-Material ServFaces</h1>
																			<p>In den ersten 3 Jahren unserer Arbeit mit sogenannten Keramikbeschichtungen, haben wir mit einem fernöstlichen Produkt gearbeitet. Bis wir feststellen mussten, dass sich das Material auf einmal anders verhält  und auch anders zu verarbeiten ist. Für unsere Firma ein absolutes "NO GO". <br><br> Wir konnten jedoch in kürzester Zeit eine Ersatz-Produktlinie aus deutscher Produktion finden und haben seit nunmehr 2 Jahren nur die besten Erfahrungen gemacht. Und so konnten wir unsere Garantie von 2 auf 3 Jahre erhöhen. Ab Anfang 2016 haben wir deshalb auch die Vertretung von ServFaces für die Schweiz und Deutschland übernommen.</p>
																			<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="ceramic-coting-images">
															<div class="container">
																<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-1.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-1.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-2.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-2.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-3.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-3.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a rel="lack" href="{{ asset('assets/frontend/images/auto-4.jpg') }}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-4.jpg') }}" alt="autokosmetik">
																			
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-5.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-5.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-6.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-6.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-7.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-7.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-8.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-8.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	<div class="item">
																		<a href="{{asset('assets/frontend/images/auto-9.jpg')}}" class="fancy-box" >
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-9.jpg') }}" alt="autokosmetik">
																		</a>
																	</div>
																	
																</div>
																<div class="loadmore-btn col-12 text-center mt-4">
																	<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																</div>
																
															</div>
														</div>
													</div>
												</div>
												<div id="collapse2_b_2" class="collapse" data-parent="#folianMainParent">
													<div class="shadow-sm bg-white p-3">
														<div class="section-car-ceramic">
															<div class="container">
																<div class="row">
																	<div class="left col-sm-12 col-md-8">
																		<p>
																		Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und genügt für die meisten Autos. Ausserdem ist sie sehr preiswert. Wir empfehlen jedoch bei der Teflon-Versiegelung Hand- oder Lanzenwäsche, da die Waschstrasse die Versiegelung zu schnell abreibt.
																		</p>
																		<ul>
																			<li class="circle">Der Standard-Lackschutz</li>
																			<li class="circle">Haltbarkeit 6+ Monate</li>
																			<li class="circle">Preiswert</li>
																		</ul>
																	</div>
																	<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
																		<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="autokosmetik">
																	</div>
																</div>
															</div>
														</div>
														<div class="services2-car-bg-2">
															<div class="container">
																<div class="row align-items-center">
																	<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
																		<img class="w-100" src="{{ asset('assets/frontend/images/seal-2.jpg') }}" alt="autokosmetik">
																	</div>
																	<div class="col-sm-12 col-md-7">
																		<div class="ceramic-coating-pading">
																			<h1>Besonders geeignet für:</h1>
																			<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen. <br> </p>
																			<ul>
																				<li class="circle_2 m-0 h6">alle Autos, Motorräder, Wohnmobile, LKW's usw.</li>
																				<li class="circle_2 m-0 h6">Gebrauchtwagen</li>
																				<li class="circle_2 m-0 h6">Leasingrückgaben</li>
																			</ul>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="ceramic-coting-images">
															<div class="container">
																<div class="row">
																	<!-- Related Project -->
																	<div class="related-projects-carousel owl-carousel owl-theme mt-4">
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-3.jpg')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-3.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-4.jpg')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-4.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-5.jpg')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-5.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-6.jpg')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-6.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-7.jpg')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-7.jpg') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-8.png')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-8.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-9.png')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-9.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-10.png')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-10.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		<div class="item">
																			<a class="fancy-box" href="{{asset('assets/frontend/images/seal-11.png')}}">
																				<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-11.png') }}" alt="autokosmetik">
																			</a>
																		</div>
																		
																	</div>
																	<div class="loadmore-btn col-12 text-center mt-3">
																		<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="Komplettfolierung" role="tabpanel">
											<div id="service-category-collapse-3">
												<div id="DesignMainParent" class="mt-3">
													<div id="collapse3_a" data-parent="#service-category-collapse-3">
														<div class="care-for-carousel owl-carousel owl-theme">
															<div class="item pb-4">
																<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_a_1" aria-expanded="false" aria-controls="collapse3_a_1" role="button">
																	<div class="ratio ratio-16X9">
																		<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
																	</div>
																	<div class="px-2 position-relative">
																		<a href="{{ route('bookappointment')}}" role="button">
																			<div class="align-items-center bg-white d-flex m-0 btn-service">
																				<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																				<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																			</div>
																		</a>
																		<h5 class="font-weight-bold h4 pt-3 m-0">Mono</h5>
																		<p class="h6 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda ...
																			<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																		</p>
																	</div>
																</div>
															</div>
															<div class="item pb-4">
																<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_a_2" aria-expanded="false" aria-controls="collapse3_a_2" role="button">
																	<div class="ratio ratio-16X9">
																		<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
																	</div>
																	<div class="px-2 position-relative">
																		<a href="{{ route('bookappointment')}}" role="button">
																			<div class="align-items-center bg-white d-flex m-0 btn-service">
																				<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																				<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																			</div>
																		</a>
																		<h5 class="font-weight-bold h4 pt-3 m-0">Design</h5>
																		<p class="h6 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda ...
																			<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div id="collapse3_a_1" class="collapse" data-parent="#DesignMainParent">
															<div class="shadow-sm bg-white p-3">
																<div class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-12">
																				<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur fugit iste, fuga ea, quae dolore hic facere consequatur vitae repudiandae laboriosam? Laudantium est aut expedita cumque placeat. Neque, dicta doloribus</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div id="collapse3_a_2" class="collapse" data-parent="#DesignMainParent">
															<div class="shadow-sm bg-white p-3">
																<div class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-12">
																				<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. In odio quibusdam, numquam consequuntur eum quam, fugit quis ut eveniet id corporis dolores voluptates aspernatur quos quo necessitatibus, pariatur assumenda dolorem.</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="Komplettfolierung" role="tabpanel">
											<div id="service-category-collapse-3">
												<div id="DesignMainParent" class="mt-3">
													<div id="collapse3_b" data-parent="#service-category-collapse-3">
														<div id="service-category-2" class="care-for-carousel owl-carousel owl-theme">
															<div class="item pb-4">
																<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_b_1" aria-expanded="false" aria-controls="collapse3_b_1" role="button">
																	<div class="ratio ratio-16X9">
																		<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
																	</div>
																	<div class="px-2 position-relative">
																		<a href="{{ route('bookappointment')}}" role="button">
																			<div class="align-items-center bg-white d-flex m-0 btn-service">
																				<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																				<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																			</div>
																		</a>
																		<h5 class="font-weight-bold h4 pt-3 m-0">Mono</h5>
																		<p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...
																			<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																		</p>
																	</div>
																</div>
															</div>
															<div class="item pb-4">
																<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_b_2" aria-expanded="false" aria-controls="collapse3_b_2" role="button">
																	<div class="ratio ratio-16X9">
																		<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
																	</div>
																	<div class="px-2 position-relative">
																		<a href="{{ route('bookappointment')}}" role="button">
																			<div class="align-items-center bg-white d-flex m-0 btn-service">
																				<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																				<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
																			</div>
																		</a>
																		<h5 class="font-weight-bold h4 pt-3 m-0">Design</h5>
																		<p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...
																			<a href="javascript:void(0)" class="font-weight-bolder">Weiterlesen</a>
																		</p>
																	</div>
																</div>
															</div>
														</div>
														<div id="collapse3_b_1" class="collapse" data-parent="#DesignMainParent">
															<div class="shadow-sm bg-white p-3">
																<div class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-12">
																				<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nesciunt delectus maiores inventore quidem repellat. Natus consequatur illo blanditiis ducimus, impedit eius. Molestiae, dolorem. Quibusdam aperiam eos sapiente harum distinctio adipisci.</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div id="collapse3_b_2" class="collapse" data-parent="#DesignMainParent">
															<div class="shadow-sm bg-white p-3">
																<div class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-12">
																				<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis quam accusantium itaque nobis reprehenderit minus laborum molestiae velit esse cupiditate provident, unde excepturi voluptatum consequuntur, autem optio quos eveniet vitae.</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</section>
			</div>
			<div id="section-recentworks1" class="bg-dark-autokosmetic">
				<div class="container">
					
					<div class="title1 ez-animate col-12" data-animation="fadeInUp">
						<h6>JÜNGSTE ARBEITEN</h6>
						<h2>Unser Portfolio</h2>
						<i class="flaticon-download"></i>
					</div>
					<div class="our-portfolio owl-carousel owl-theme">
						
                              @foreach($portfolio as $portfoli)
                              <?php 
                                  $images1 = $portfoli->images; 
                                  $images  = explode(',', $images1);
      
                                  $categoryArray = array();
                                  foreach($portfoli->services_category as $value):
      								$categoryArray[] = $value->name;
      							endforeach;
                              ?>
						<div class="item">
							<!-- <a data-fancybox="gallery" href="{{asset(Storage::url(trim($images[0]))) ?? ''}}" class="fancy-box"> -->
							<a href="{{ route('portfolio')}}">
								<div class="img-container">
									<img class="img-fluid" src="{{asset(Storage::url(trim($images[0]))) ?? ''}}" alt="autokosmetik">
									<div class="overlay">
										<div class="overlay-content">
											<i></i>
											<h3>{{ $portfoli->name }}</h3>
											<p>{{ implode(',',$categoryArray) }}</p>
										</div>
									</div>
								</div>
							</a>
						</div>
                              @endforeach
						
					</div>
					
				</div>
			</div>
			<!-- <div id="section-cta1">
				<div class="container">
					<div class="row">
						<div class="left col-sm-12 col-md-9" >
							<h2>Sie brauchen eine beeindruckendere als diese, <span>kontaktiere uns jetzt</span></h2>
						</div>
						<div class="right col-sm-12 col-md-3" >
							<a href="{{ route('contact')}}" class="btn-2">Kaufen Sie dies jetzt</a>
						</div>
					</div>
				</div>
			</div> -->
			<!-- <div id="section-testimonial1" class="bg-dark-autokosmetic" style="background: url({{ asset('assets/frontend/images/index-bg.jpg')}}) no-repeat;">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="title1 ez-animate col-12" data-animation="fadeInUp">
								<h6>Kunden sagen</h6>
								<h2>Unser Kundenfeedback</h2>
								<i class="flaticon-download"></i>
							</div>
							<div class="owl-carousel owl-theme">
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial1.png') }}"alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum</h6>
										<p>Lorem ipsum</p>
									</div>
								</div>
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial2.png') }}" alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum</h6>
										<p>Lorem ipsum</p>
									</div>
								</div>
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial3.png') }}" alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum</h6>
										<p>Lorem ipsum</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> -->

			<div id="section-testimonial1" class="inner-banner-wrap">				
				<img src="{{ asset('assets/frontend/images/index-bg.jpg') }}" alt="" class="inner-page-banner">
								
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="title1 ez-animate col-12" data-animation="fadeInUp">
								<h6>Kunden sagen</h6>
								<h2>Unser Kundenfeedback</h2>
								<i class="flaticon-download"></i>
							</div>
							<div class="owl-carousel owl-theme">
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial1.png') }}"alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum</h6>
										<p>Lorem ipsum</p>
									</div>
								</div>
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial2.png') }}" alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum</h6>
										<p>Lorem ipsum</p>
									</div>
								</div>
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial3.png') }}" alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum</h6>
										<p>Lorem ipsum</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="section-contactform1">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="title1 ez-animate col-12" data-animation="fadeInUp">
								<h6>KONTAKT FORMULAR</h6>
								<h2>Kontaktieren Sie uns</h2>
								<i class="flaticon-download"></i>
							</div>
							<div class="contactform1">
                                <div class="alert alert-success alertmsg" id="contactFormSuccess" style="display: none;"></div>
                                <div class="alert alert-danger alertmsg" id="contactFormDanger" style="display: none;"></div>
								<form id="contactform1" action="" method="post">
									<div class="form-row">
										<div class="form-group col-md-6">
											<input type="text" class="form-control" id="name" placeholder="Name *" required>
                                            <div class="name_form_error" style="color: red;display: none"></div>
										</div>
										<div class="form-group col-md-6">
											<input type="email" class="form-control" id="email" placeholder="Email *" required>
                                            <div class="email_form_error" style="color: red;display: none"></div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6">
											<input type="text" class="form-control" id="phoneNumber" placeholder="Telefon *" required>
                                            <div class="number_form_error" style="color: red;display: none"></div>
										</div>
										<div class="form-group col-md-6">
											<input type="text" class="form-control" id="subject" placeholder="Betreff *" required>
                                            <div class="subject_form_error" style="color: red;display: none"></div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-12">
											<textarea class="form-control" id="message" rows="3" placeholder="Nachricht *" required></textarea>
                                            <div class="message_form_error" style="color: red;display: none"></div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group m-bot0 col-md-12">
											<button type="button" id="contactformbtn1">Senden</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.Section Contact Form 1 -->
        </div>
    </div>
@endsection

@section('script')    
<script>
$('body').on('click',"#contactformbtn1",function(){
	$('#loader').show();
    var contact_name    = $("#name").val();
    var contact_email   = $("#email").val();
    var contact_number  = $("#phoneNumber").val();
    var contact_subject = $("#subject").val();
    var contact_message = $("#message").val();
    
    $('.name_form_error,.number_form_error,.email_form_error,.subject_form_error,.message_form_error').hide();
	var count = 0;
	if(contact_name == "" || contact_name == undefined){
		$('.name_form_error').text('Bitte Name eingeben');
		$('.name_form_error').show();
		count = count +1;
	}
	if(contact_number == "" || contact_number == undefined){
		$('.number_form_error').text('Bitte Kontaktnummer eingeben');
		$('.number_form_error').show();
		count = count +1;
	}
	if(contact_email == "" || contact_email == undefined){
		$('.email_form_error').text('Bitte E-Mail eingeben');
		$('.email_form_error').show();
		count = count +1;
	}else if(!validateEmail(contact_email)){
		$('.email_form_error').text('Bitte gültige E-Mail eingeben');
		$('.email_form_error').show();
		count = count +1;
	}
   	if(contact_subject == "" || contact_subject == undefined){
		$('.subject_form_error').text('Bitte Betreff eingeben');
		$('.subject_form_error').show();
		count = count +1;
	}
   	if(contact_message == "" || contact_message == undefined){
		$('.message_form_error').text('Bitte Nachricht eingeben');
		$('.message_form_error').show();
		count = count +1;
	}
	if(count != 0){
		$('#loader').hide();
		return false;
	}else{ 
       	$.ajax({
            method: 'POST',
            url: "{{ route('ajax.contactform1') }}",
            data: {
            	"_token": "{{ csrf_token() }}",
                "name": contact_name,
                "email": contact_email,
                "phoneNumber": contact_number,
                "subject": contact_subject,
                "message": contact_message
            }
        })
        .done(function(data) {
            if(data.result == 1){
                $('#contactFormSuccess').html('Vielen Dank für Ihre Kontaktaufnahme!');
                $('#contactFormSuccess').show();
            }else{
                $('#contactFormDanger').html('Fehler im Kontaktformular. Bitte versuchen Sie es nach einiger Zeit.');
                $('#contactFormDanger').show();
            }
            $('#contactform1').trigger("reset");
			$('#loader').hide();
            setTimeout( function() {
                hideAlert();
            }, 7000);
        })
	}
});

function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}

function hideAlert(){
    $('.alertmsg').hide();
}

var $careCarousel = $('.care-for-carousel');
	$careCarousel.children().each( function( index ) {
		$(this).attr( 'data-position', index ); 
	});

	$careCarousel.owlCarousel({
		loop: true,
		margin: 30,
		center:true,
		nav: false,
		dots: false,
		responsive: {
			0: {
				items: 1,
			},
			600: {
				items: 1,
			},
			1000: {
				items: 3,
			}
		}
	});
$('.our-portfolio').owlCarousel({
	loop: true,
	margin: 30,
	autoplay: true,
	responsiveClass: true,
	nav: false,
	dots: true,
	responsive: {
		0: {
			items: 1,
			nav:false,
		},
		767: {
			items: 1,
			nav:false,
		},
		1000: {
			items: 3,
			nav:false,
		},
	}

	
});
</script>
@endsection