<?php

namespace App\Http\Controllers;

use App\ServicePortfolio;
use Illuminate\Http\Request;

class ServicePortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServicePortfolio  $servicePortfolio
     * @return \Illuminate\Http\Response
     */
    public function show(ServicePortfolio $servicePortfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServicePortfolio  $servicePortfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(ServicePortfolio $servicePortfolio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServicePortfolio  $servicePortfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServicePortfolio $servicePortfolio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServicePortfolio  $servicePortfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServicePortfolio $servicePortfolio)
    {
        //
    }
}
