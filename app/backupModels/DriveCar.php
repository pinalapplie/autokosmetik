<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriveCar extends Model
{
    protected $table ="drive_cars";
    protected $guarded = [];
    public function car_brand_name()
    {
        return $this->hasOne('App\CarBrand','id','car_brand');
    }
}
