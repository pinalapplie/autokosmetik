@section('title', 'Dienstleistungen')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<div id="main-content" class="active">
			<div id="section-breadcrumb1" class="inner-banner-wrap">
				<img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
				<div class="container">
					<div class="row">
						<div class="content col-12">
							<h1>Lack</h1>
							<ul>
                            <li><a href="{{ route('index')}}">Zuhause</a></li>
								<li><a href="{{ route('services')}}">Dienstleistungen</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="section-services2" class="section-car-ceramic">
				<div class="container">
					<div class="row">
						<div class="left col-sm-12 col-md-8">
							<p>Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschläge. Unsere verwendete Profifolie ist sehr dick und dadurch auch ganz besonders widerstandsfähig. Allerdings ist Sie sehr schwer zu verlegen und auch relativ kostspielig.</p>
							<ul>
								<li class="circle">Hochtransparent/unsichtbar</li>
								<li class="circle">Der optimale Schutz gegen Steinschlag</li>
								<li class="circle">Waschstrassenfest</li>
							</ul>
						</div>
						<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="autokosmetik"> 
						</div>
					</div>
				</div>
			</div>
			<div class="section-services2 services2-car-bg-2">
				<div class="container">
					<div class="row align-items-center">
						<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-2.jpg') }}" alt="autokosmetik">
						</div>
						<div class="col-sm-12 col-md-7">
							<div class="ceramic-coating-pading">
								<h1>Besonders geeignet für:</h1>
								<p>Die Preise für eine Lackschutzfolie variieren sehr stark und sind abhängig von der Beschaffenheit des Autos und Anzahl jener Stellen, welche mit einer Lackschutzfolie versehen werden sollen.</p>
                                <p>Kommen Sie vorbei, wir unterbreiten Ihnen gerne ein unverbindliches Angebot.</p>
                                <ul>
                                    <li>Ladekante und alle Türkanten</li>
                                    <li>Stossstange und Motorhaube</li>
                                    <li>Generell alle exponierten Stellen</li>
                                </ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="section-portfoliodetails1" class="ceramic-coting-images">
				<div class="container">
                    <div class="row">
                        <div class="related-projects col-12">
                            <div class="row">
                                <div class="item col-sm-12 col-md-4 my-3">
                                    <a href="#">
                                        <div class="img-container w-100">
                                            <img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-3.jpg') }}" alt="autokosmetik">
                                        </div>
                                    </a>
                                </div>
                                <div class="item col-sm-12 col-md-4 my-3">
                                    <a href="#">
                                        <div class="img-container w-100">
                                            <img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-4.jpg') }}" alt="autokosmetik">
                                        </div>
                                    </a>
                                </div>
                                <div class="item col-sm-12 col-md-4 my-3">
                                    <a href="#">
                                        <div class="img-container w-100">
                                            <img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-5.jpg') }}" alt="autokosmetik">
                                        </div>
                                    </a>
                                </div>
                                <div class="item col-sm-12 col-md-4 my-3">
                                    <a href="#">
                                        <div class="img-container w-100">
                                            <img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-6.png') }}" alt="autokosmetik">
                                        </div>
                                    </a>
                                </div>
                                <div class="item col-sm-12 col-md-4 my-3">
                                    <a href="#">
                                        <div class="img-container w-100">
                                            <img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-7.png') }}" alt="autokosmetik">
                                        </div>
                                    </a>
                                </div>
                                <div class="item col-sm-12 col-md-4 my-3">
                                    <a href="#">
                                        <div class="img-container w-100">
                                            <img class="img-fluid w-100" src="{{ asset('assets/frontend/images/paint-8.png') }}" alt="autokosmetik">
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center pb-5">
                            <a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
@endsection