@section('title', 'Dienstleistungen')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<div id="main-content" class="active">
			<div id="section-breadcrumb1" class="inner-banner-wrap">
				<img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
				<div class="container">
					<div class="row">
						<div class="content col-12">
							<h1>Mono</h1>
							<ul>
								<li><a href="{{ route('index')}}">ZUHAUSE</a></li>
								<li><a href="{{ route('services')}}">Dienstleistungen</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="section-services2" class="section-car-ceramic">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni cum ea consequuntur in enim exercitationem corporis suscipit, necessitatibus assumenda sint, odio voluptatem quasi, eaque labore ipsa deserunt accusantium. Sapiente, officiis!
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection    
	   
	