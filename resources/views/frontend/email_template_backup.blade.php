<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>Autokosmetik</title>
    <style>       
        * { box-sizing: border-box; -moz-box-sizing: border-box; }        
    </style>
</head>
<body style="background: #fff; margin: 0; padding: 0;font-family: 'Arial', cursive;font-size: 16px;color: #111111;">
    <table align="center" bgcolor="#fff" cellspacing="0" cellpadding="0" border="0" width="100%" style="max-width: 800px; margin: 30px auto;border: 6px solid #d0bb79;">
        <tr>
            <td style="padding: 15px;">
                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td valign="top">
                            <h1 style="font-size: 28px; font-weight: bold; margin-bottom: 0;text-transform: uppercase; margin-bottom: 0.5rem;">Rechnung:</h1>
                            <ul style="list-style: none;padding-left: 0;margin: 0;">
                                <li style="margin: 0 0 10px 0;">Autokosmetik </li>
                                <li style="margin-left: 0;">Messinastrasse 1,</li>
                                <li style="margin-left: 0;">9495 Triesen,</li>
                                <li style="margin: 0 0 10px 0;">Liechtenstein</li>
                                <li style="margin-left: 0;">
                                    <a style="color: #111111;text-decoration: underline;" href="mailto:info@autokosmetik.li">info@autokosmetik.li</a>
                                </li>
                                <li style="margin-left: 0;">
                                    <a style="color: #111111;text-decoration: underline;" href="www.autokosmetik.li">www.autokosmetik.li</a>
                                </li>
                            </ul>
                        </td>
                        <td align="right" valign="top">
                            <a href="#" target="_blank" style="color: #111111;text-decoration: none;"><img src="cid:logo_2u" alt="autokosmetik" style="max-height: 120px;"></a>
                        </td>
                        
                    </tr>
                    <tr>
                        <td height="10px"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><hr style="border: 2px dashed #d0bb79;"></td>
                        <td height="30px"></td>
                    </tr>
                </table>

                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td valign="top">
                            <table>
                                <tr>
                                    <td valign="top" style="width: 140px;" scope="col">Rechnung an: </td>
                                    <td>
                                        <ul style="margin: 0;padding: 0;list-style: none;line-height:22px;">
                                            <li style="font-weight: 700;">{{ $name ?? ''}}</li>
                                            <li>Heraweg 30</li>
                                            <li>9496 Balzers</li>
                                            <li>Liechtenstein</li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right" valign="top">
                            <table style="text-align: right;">
                                <tr>
                                    <td style="width: 140px;" scope="col">Rechnung Nr. {{$insert_id ?? ''}}</td>
                                </tr>
                                <tr>
                                     <td>{{ $today ?? '' }}</td>
                                </tr>
                                <tr><td height="15px"></td></tr>
                                <tr>
                                    <td style="font-size: 14px;"><a href="mailto:{{$email}}" style="color: #111111;">{{ $email ?? '' }}</a></td>
                                </tr>
                                <tr>
                                    <td style="font-size: 14px;"><a href="tel:{{$number}}"  style="color: #111111;">{{ $number ?? ''}}</a></td>
                                </tr>
                               <!--  <tr>
                                    <td style="width: 140px;" scope="col">Address :</td>
                                     <td>{{ $address ?? ''}} {{ $zip ?? '' }}, {{ $city??'' }}, {{ $state ??''}}</td>
                                </tr> -->
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" height="20"></td>
                    </tr>
                </table>
                @php $total = 0;@endphp
                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border: 3px solid #51151B; border-collapse: collapse;">
                    <thead>
                        <tr>
                            <th style="color: #fff; padding:5px; text-align: left;background-color: #51151B;font-size: 22px;">Dienstleistungen</th>
                            <th valign="bottom" style="color: #fff; padding:5px; text-align: right;background-color: #51151B;">Preis</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="padding:5px; font-weight: 700;" colspan="2">
                                - Pflege
                                <table cellspacing="5" cellpadding="0" border="0" width="100%" style="padding-left:50px; font-weight:400;">
                                    <tr>
                                        <td>Swissvax</td>
                                        <td align="right">210</td>
                                    </tr>
                                    <tr>
                                        <td>Spezialfälle</td>
                                        <td align="right">210</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:5px; font-weight: 700;" colspan="2">
                                - Pflege
                                <table cellspacing="5" cellpadding="0" border="0" width="100%" style="padding-left:50px; font-weight:400;">
                                    <tr>
                                        <td>Swissvax</td>
                                        <td align="right">210</td>
                                    </tr>
                                    <tr>
                                        <td>Spezialfälle</td>
                                        <td align="right">210</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <th style="color: #fff; padding:5px; text-align: left;background-color: #51151B;font-size: 22px;">Extra Service</th>
                            <th valign="bottom" style="color: #fff; padding:5px; text-align: right;background-color: #51151B;">Preis</th>
                        </tr>
                        <tr>
                            <td style="padding:5px; font-weight: 700;" colspan="2">
                                <table cellspacing="5" cellpadding="0" border="0" width="100%" style=font-weight:400;">
                                    <tr>
                                        <td>Pick up</td>
                                        <td align="right">100</td>
                                    </tr>
                                    <tr>
                                        <td>Drop</td>
                                        <td align="right">70</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                       <!--  @php $service_management = [];@endphp
                        @foreach ($service_management as $key => $value)
                            <?php //$servicePrice = getServicesWithPrice($value->id,$vehicle_condition_id,$vehicle_type_id);  ?>
                            @php $total = $total + $servicePrice->price; @endphp 
                            <tr>
                                <td style="padding:5px;">
                                    {{ $value->name}}
                                </td>
                                <td style="padding:5px; text-align: right;">
                                    {{ $servicePrice->price}}
                                </td>
                            </tr>
                        @endforeach  -->
                    </tbody>
                </table>

                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border: 3px solid #e0b4b7; border-collapse: collapse;">
                    <thead>
                        <tr>
                            <th style="padding:5px; text-align: left;background-color: #e0b4b7;">Ad On Service</th>
                            <th style="padding:5px; text-align: right;background-color: #e0b4b7;">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                         @php $addonservice = [];@endphp
                        @foreach ($addonservice as $key => $value)
                            @php $total = $total + $value->price; @endphp 
                            <tr>
                                <td style="padding:5px;">
                                    {{ $value->name}}
                                </td>
                                <td style="padding:5px; text-align: right;">
                                    {{ $value->price}}
                                </td>
                            </tr>
                        @endforeach 
                    </tbody>
                </table>
                 @php $drive_vehicle='';@endphp
                @if($drive_vehicle)
                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border: 3px solid #e0b4b7; border-collapse: collapse;">
                    <thead>
                        <tr>
                            <th style="padding:5px; text-align: left;background-color: #e0b4b7;">Scheiben t�nen</th>
                            <th style="padding:5px; text-align: right;background-color: #e0b4b7;">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                            @php $total = $total + $drive_vehicle->price; @endphp 
                            <?php 
                            $taxPer1 = 7.7+100;
                            $taxPer = 7.7;
                            $intext = $total*$taxPer/$taxPer1;
                            $finalTax = number_format($intext, 2);
                            ?>
                            <tr>
                                <td style="padding:5px;">
                                    {{ $drive_vehicle->name}}
                                </td>
                                <td style="padding:5px; text-align: right;">
                                    {{ $drive_vehicle->price}}
                                </td>
                            </tr>

                    </tbody>
                </table>
                @endif
                
                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse: collapse;">
                    <tr>
                        <td colspan="2" height="30"></td>
                    </tr>
                    <tr>
                        <td valign="top" style="font-size: 20px;">
                            <table cellspacing="0" cellpadding="5" border="0" style="border-collapse: collapse;width:100%">
                                <tr bgcolor="#e0b4b7">
                                    <td style="text-transform: uppercase; padding: 5px;" scope="col">Inclusive Tax(MWST 7.7%)</td>
                                    <td align="right">CHF {{$finalTax??''}}</td>
                                </tr>
                                <tr bgcolor="#e0b4b7">
                                    <td style="text-transform: uppercase; padding: 5px;" scope="col">Final Amount</td>
                                    <td align="right">CHF {{$total??''}}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" height="10"></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center;">
                            Invoice Powered by <a style="color: #111111;text-decoration: underline;" href="https://applie.li" target="_blank">Applie AG</a>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>    
    </table>


    <!-- <div class="page">
        <div class="subpage">Page 2/2</div>
    </div> -->

</body>
</html>