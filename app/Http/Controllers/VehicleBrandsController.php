<?php

namespace App\Http\Controllers;

use App\Http\Model\VehicleBrands;
use App\Http\Model\VehicleCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use App\Http\Model\VehicleType;
use Illuminate\Support\Facades\Storage;

class VehicleBrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicle_brands = VehicleBrands::latest()->get();
         $vehicle_type = VehicleType::get();
        return view('backend.vehicle_brands.index', compact('vehicle_brands','vehicle_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicle_category = VehicleCategory::get();
        $vehicle_type = VehicleType::get();
        return view('backend.vehicle_brands.create',compact('vehicle_category','vehicle_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
            
                $vehicle_brands = VehicleBrands::where('id',$id)->first();
                if (empty($vehicle_brands->is_active)) {
                    $vehicle_brands->is_active = 1;
                } else {
                    $vehicle_brands->is_active = 0;
                }
                    $vehicle_brands->save();
                return response()->json(['success' => 1]);
            }else{
                $this->validate($request, [
                    'name' => 'required',
                ]);
                if(!empty($request->id)){
                    $id = Crypt::decrypt($request->id);
                    $vehicle_brands = VehicleBrands::where('id',$id)->first();
                    $message = trans('message.update', ['entity' => trans('cruds.vehiclebrands.title_singular')]);
                }else{
                    $vehicle_brands = new VehicleBrands;
                    $message = trans('message.create', ['entity' => trans('cruds.vehiclebrands.title_singular')]);
                }
                    
                    if ($request->hasFile('image')) {                        
                        $storage_path = 'public/';
                        $vehicle_brands->image = Storage::disk('local')->put($storage_path, $request->file('image'));
                    }

                    $vehicle_brands->vehicle_type_ids  = implode(",",$request->vehicle_type_ids);
                    $vehicle_brands->name = $request->name;
                    //$vehicle_brands->vehicle_categorie_id = $request->vehicle_categorie_id;
                    //$vehicle_brands->vehicle_type_id = $request->vehicle_type_id;
                    $vehicle_brands->save();
                    return redirect()->route('vehiclebrands.index')->with('success',$message);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
            return redirect()->back()->with('danger', trans('global.something_wrong'));    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VehicleBrands  $vehicleBrands
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleBrands $vehicleBrands)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VehicleBrands  $vehicleBrands
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $id = Crypt::decrypt($id);
        $vehiclebrands = VehicleBrands::where('id',$id)->first();
        $vehicle_category = VehicleCategory::get();
        $vehicle_type = VehicleType::get();
        return view('backend.vehicle_brands.edit', compact('vehiclebrands','vehicle_category','vehicle_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VehicleBrands  $vehicleBrands
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleBrands $vehicleBrands)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VehicleBrands  $vehicleBrands
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                VehicleBrands::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }
}
