<?php

namespace App\Http\Controllers;


use App\Http\Model\VehicleConditions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class VehicleConditionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicle_conditions = VehicleConditions::latest()->get();
        return view('backend.vehicle_conditions.index', compact('vehicle_conditions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.vehicle_conditions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                $vehicle_conditions = VehicleConditions::where('id',$id)->first();
                if (empty($vehicle_conditions->is_active)) {
                    $vehicle_conditions->is_active = 1;
                } else {
                    $vehicle_conditions->is_active = 0;
                }
                    $vehicle_conditions->save();
                return response()->json(['success' => 1]);
            }else{
                $this->validate($request, [
                    'name' => 'required',
                ]);
                if(!empty($request->id)){
                    $id = Crypt::decrypt($request->id);
                    $vehicle_conditions = VehicleConditions::where('id',$id)->first();
                    $message = trans('message.update', ['entity' => trans('cruds.vehicleconditions.title_singular')]);
                }else{
                    $vehicle_conditions = new VehicleConditions;
                    $message = trans('message.create', ['entity' => trans('cruds.vehicleconditions.title_singular')]);
                }
                    if ($request->hasFile('image')) {                        
                        $storage_path = 'public/';
                        $vehicle_conditions->image = Storage::disk('local')->put($storage_path, $request->file('image'));
                    }
                    $vehicle_conditions->name = $request->name;
                    $vehicle_conditions->save();
                    return redirect()->route('vehicleconditions.index')->with('success',$message);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
            return redirect()->back()->with('danger', trans('global.something_wrong'));    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VehicleConditions  $vehicleConditions
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleConditions $vehicleConditions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VehicleConditions  $vehicleConditions
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $vehicleconditions = VehicleConditions::where('id',$id)->first();
        return view('backend.vehicle_conditions.edit', compact('vehicleconditions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VehicleConditions  $vehicleConditions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleConditions $vehicleConditions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VehicleConditions  $vehicleConditions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                VehicleConditions::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }
}
