<div class="form-row align-items-center justify-content-center mx-0">
@foreach ($vehicle_conditions as $key => $value) 
    <div class="col-6 text-center">
    	<div class="card select-category">
    		<input role="button" type="radio" name="step_1" class="form-control checkbox" id="step_1_{{$value->id}}" value="{{$value->id}}" v-id="{{$value->name}}">
    		<div class="card-body px-2 py-0">
    			<div class="avatar-lg mb-1">
    				<img src="{{asset(Storage::url($value->image)) ?? ''}}" alt="" class="img-fluid">
    				<p class="font-weight-bold h6 text-dark">{{$value->name}}</p>
    			</div>
                <p class="font-weight-bold h5 text-dark mt-2">{{$value->name}}</p>
    		</div>
    	</div>
    </div>
@endforeach
</div>
