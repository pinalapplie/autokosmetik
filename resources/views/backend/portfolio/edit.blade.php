@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.update') }} {{ trans('cruds.portfolio.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('portfolio.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('portfolio.store') }}" method="POST" enctype="multipart/form-data" id="add_form">
                    @csrf
                     <input type="hidden" name="id" value="{{encrypt($portfolio->id)}}">
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.portfolio.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($portfolio->name) ? $portfolio->name : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">{{ trans('cruds.portfolio.fields.description') }}</label>
                        <textarea name="description" class="form-control" id="description" rows="5">
                            {!! old('description', isset($portfolio->description) ? $portfolio->description : '') !!}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="category">{{ trans('cruds.portfolio.fields.category') }}<span class="required_class">*</span></label>
                        <select class="select2 form-control select2-multiple" name="service_category_id[]" id="category" multiple="" data-placeholder="{{ trans('cruds.portfolio.fields.select_category') }}">
                            <!--<option value="">{{ trans('cruds.portfolio.fields.select_category') }}</option>-->  
                            @foreach($service_category as $value)
                                <?php
                                    $categoryIdArray = explode(',',$portfolio->service_category_id); 
                                    $final_category_name='';
                                    if($value->parent_id == 0){
                                        $final_category_name = $value->name;
                                    }
                                ?>
                                <option value="{{$value->id}}" @if(in_array($value->id, $categoryIdArray)) selected="" @endif>{{$final_category_name}}</option>  
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="title">{{ trans('cruds.portfolio.fields.created') }}<span class="required_class">*</span></label>
                        <input type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" id="created" name="created" value="{{ old('created', isset($portfolio->created) ? $portfolio->created : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.portfolio.fields.client') }}<span class="required_class">*</span></label>
                        <input type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" id="client" name="client" value="{{ old('client', isset($portfolio->client) ? $portfolio->client : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.portfolio.fields.images') }}<span class="required_class">*</span></label>
                        <input type="file" class="form-control" id="images" name="images[]" value="{{ old('images', isset($portfolio->images) ? $portfolio->images : '') }}" multiple="">
                        <input type="hidden" name="portfolioImages" id="portfolioImages" value="{{ $portfolio->images }}" />
                    </div>
                    <div class="row mb-3">
                        <?php
                            $MK = 0; 
                            $images1 = $portfolio->images; 
                            $images  = explode(',', $images1);
                            
                            foreach($images as $image):
                            $MK++;
                        ?>
                        <div class="col-auto position-reletive" id="delete_{{ $MK }}">
                            <a class="position-absolute close-btn" href="javascript:void(0)" id="{{ trim($image) }}" onclick="deleteImg(id, {{ $MK }})"><i class="fa fa-times"></i></a>
                            <img src="{{asset(Storage::url(trim($image))) ?? ''}}" alt="" height=100 width=100>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <button type="submit" class="btn btn-primary">{{ trans('global.submit') }}</button>
                </form>
            </div>
        </div>  
    </div>
</div>
<style>
.close-btn{
    top: 0;
    right: 0;
    display: inline-flex;
    height: 28px;
    width: 28px;
    background: red;
    border-radius: 50%;
    color: white;
    font-size: 1rem;
    text-align: center;
    align-items: center;
    justify-content: center;
}
</style>  
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        $(".select2").select2();
        CKEDITOR.replace('description');
        //CKEDITOR.replace('short_description');
        $("#add_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
                // description: {
                //     required: function(textarea) {
                //         CKEDITOR.instances[textarea.id].updateElement();
                //         var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                //         return editorcontent.length === 0;
                //     }
                // },
                // service_category_id: {
                //     required: true
                // },
                // type: {
                //     required: true
                // }

            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                },
                // description: {
                //     required: "@lang('validation.required',['attribute'=>'Description'])",
                // },
                // service_category_id: {
                //     required: "@lang('validation.required',['attribute'=>'Category'])",
                // },
                // type: {
                //     required: "@lang('validation.required',['attribute'=>'Type'])",
                // }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
    
    function deleteImg(imgUrl, MK){
        var portfolioImages  = $('#portfolioImages').val();
        var portfolioImages1 = portfolioImages.split(',');
        var finalArray = [];
        
        portfolioImages1.forEach(function(portfolio) {
            // do something with `item`
            if(imgUrl != portfolio){
                finalArray.push(portfolio.trim());
            }
        });
        var portfolioImages2 = finalArray.join(",");
        $('#portfolioImages').val(portfolioImages2);
        $('#delete_'+MK).remove();
    }
</script>
@endsection