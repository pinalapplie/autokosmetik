<?php

namespace App\Http\Controllers;


use App\Http\Model\Addonservice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class AddonserviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addonservice = Addonservice::latest()->get();
        return view('backend.addonservice.index', compact('addonservice'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.addonservice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                $addonservice = Addonservice::where('id',$id)->first();
                if (empty($addonservice->is_active)) {
                    $addonservice->is_active = 1;
                } else {
                    $addonservice->is_active = 0;
                }
                    $addonservice->save();
                return response()->json(['success' => 1]);
            }else{
                $this->validate($request, [
                    'name' => 'required',
                    'price' => 'required',
                ]);
                if(!empty($request->id)){
                    $id = Crypt::decrypt($request->id);
                    $addonservice = Addonservice::where('id',$id)->first();
                    $message = trans('message.update', ['entity' => trans('cruds.addonservice.title_singular')]);
                }else{
                    $addonservice = new Addonservice();
                    $message = trans('message.create', ['entity' => trans('cruds.addonservice.title_singular')]);
                }
                    if ($request->hasFile('image')) {                        
                        $storage_path = 'public/';
                        $addonservice->image = Storage::disk('local')->put($storage_path, $request->file('image'));
                    }
                    $addonservice->name  = $request->name;
                    $addonservice->price = $request->price;
                    $addonservice->save();
                    return redirect()->route('addonservice.index')->with('success',$message);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
            return redirect()->back()->with('danger', trans('global.something_wrong'));    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Addonservice  $Addonservice
     * @return \Illuminate\Http\Response
     */
    public function show(Addonservice $Addonservice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Addonservice  $Addonservice
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $addonservice = Addonservice::where('id',$id)->first();
        return view('backend.addonservice.edit', compact('addonservice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Addonservice  $Addonservice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Addonservice $Addonservice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Addonservice  $Addonservice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                Addonservice::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }
}
