<div id="mainService31" class="all_service" style="display: none;">
    <div class="form-row align-items-center justify-content-center mx-0">
    @foreach ($vehicle_brands as $key => $value)
        <div class="col-auto text-center mt-2">
            <div class="card select-category">
                <input type="radio" role="button" name="step_7" class="form-control radio" value="{{$value->id}}" >
                <div class="card-body p-2">
                    <div class="avatar-md">
                        <img src="{{asset(Storage::url($value->image)) ?? ''}}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    </div>
</div>
<br>
@foreach ($vehicle_brands as $key => $value)
    <div id="vehicle_brands_{{$value->id}}" class="vehicleBrands" style="display: none;">
    <?php $drive_vehicle = getDriveVehicle($value->id,$vehicle_type); ?>
    @foreach ($drive_vehicle as $key => $value) 
        <div class="form-group">
            <input type="radio" name="step_21" class="form-control radio" value="{{$value->id}}" id="step_21_{{$value->id}}" p-id="{{$value->price}}" n-id="{{$value->name}}">
            <label role="button" for="step_21_{{$value->id}}"><span>{{ucfirst($value->name)}}</span>
                <span class="ml-auto">{{$value->price.' CHF'}}</span></label>
        </div>
    @endforeach
    </div>   
@endforeach