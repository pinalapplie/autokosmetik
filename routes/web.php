<?php



/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

*/

Route::get('/', 'WebsiteController@comingsoon')->name('comingsoon');


Route::get('/foo', function () {
    Artisan::call('storage:link');
});




Route::group(['prefix' => 'second_skin'], function() {

	Route::get('/',  'WebsiteController@second_skin')->name('index');

});

Route::group(['prefix' => ''], function() {

	Route::get('/', 'WebsiteController@index')->name('index');

	Route::get('/home', 'WebsiteController@index')->name('index');

	Route::get('/autoaufbereitung', 'WebsiteController@aboutUs')->name('about.us');

	Route::get('/news', 'WebsiteController@news')->name('news');

	Route::get('/news/{id}', 'WebsiteController@newsDetails')->name('newsDetails');

	Route::get('/dienstleistungen', 'WebsiteController@services')->name('services');

	Route::get('/car-cosmetics', 'WebsiteController@carCosmetics')->name('car.cosmetics');

	Route::get('/foil-technology', 'WebsiteController@foilTechnology')->name('foil.technology');

	Route::get('/rim-tire', 'WebsiteController@rimTire')->name('rim.tire');

	Route::get('/voucher', 'WebsiteController@voucher')->name('voucher');

	Route::get('/kontakt', 'WebsiteController@contact')->name('contact');

	Route::get('/projekte', 'WebsiteController@portfolio')->name('portfolio');

	Route::get('/keramikbeschichtung-servfaces', 'WebsiteController@ceramic_coating')->name('ceramic_coating');

	Route::get('/Termin-buchen', 'WebsiteController@bookappointment')->name('bookappointment');

	Route::get('/teflonversiegelung', 'WebsiteController@sealing')->name('sealing');

	Route::get('/swissvax', 'WebsiteController@swissvax')->name('swissvax');

	Route::get('/herkömmlich', 'WebsiteController@conventinal')->name('conventinal');

	Route::get('/spezialfälle', 'WebsiteController@specialcase')->name('specialcase');

	Route::get('/foiltech', 'WebsiteController@foiltech')->name('foiltech');

	Route::get('/lack', 'WebsiteController@paint')->name('paint');

	Route::get('/mono', 'WebsiteController@mono')->name('mono');
	
	Route::get('/autopflege-swissvax', 'WebsiteController@carcare_swissvax')->name('carcare_swissvax');
	Route::get('/auto-innenreinigung', 'WebsiteController@car_interior_cleaning')->name('car_interior_cleaning');
	Route::get('/auto-innenaufbereitung-spezial', 'WebsiteController@car_interior_processing_special')->name('car_interior_processing_special');
	Route::get('/steinschlagschutzfolie', 'WebsiteController@stone_impact_protection_film')->name('stone_impact_protection_film');
	Route::get('/autoscheibentoenung', 'WebsiteController@car_window_tinting')->name('car_window_tinting');
	Route::get('/lederpflege-lederrestauration', 'WebsiteController@leather_care_restoration')->name('leather_care_restoration');
	Route::get('/autofolierung-car-wrapping', 'WebsiteController@car_wrapping')->name('car_wrapping');
	Route::get('/fahrzeugbeschriftung', 'WebsiteController@vehicle_lettering')->name('vehicle_lettering');
	Route::get('/wassertransferdruck', 'WebsiteController@water_transfer_printing')->name('water_transfer_printing');
	Route::get('/verchromungen', 'WebsiteController@chrome_plating')->name('chrome_plating');
	Route::get('/interieur-design', 'WebsiteController@interior_design')->name('interior_design');

	
	

	
	


	Route::get('/email',  function () {

	   $logo = public_path().'/assets/frontend/images/logo.png';

	    return view('frontend.email_template',['name'=>'Wolfinger Fabio','insert_id'=>'9','today' =>'11. April 2021','email'=>'shravangoswami@gmail.com','number'=>'1234567890','logo' => $logo]);

	});

	Route::get('/inquiry',  function () {

	    return view('frontend.inquiry');

	})->name('inquiry');



	Route::post('/step0', 'WebsiteController@ajaxStep0')->name('ajax.step0');

	Route::post('/step1', 'WebsiteController@ajaxStep1')->name('ajax.step1');

	Route::post('/step2', 'WebsiteController@ajaxStep2')->name('ajax.step2');

	Route::post('/step20', 'WebsiteController@ajaxStep20')->name('ajax.step20');

	Route::post('/step3', 'WebsiteController@ajaxStep3')->name('ajax.step3');

	Route::post('/step4', 'WebsiteController@ajaxStep4')->name('ajax.step4');

	Route::post('/step5', 'WebsiteController@ajaxStep5')->name('ajax.step5');

	Route::post('/step6', 'WebsiteController@ajaxStep6')->name('ajax.step6');

	Route::post('/step7', 'WebsiteController@ajaxStep7')->name('ajax.step7');

	Route::post('/step8', 'WebsiteController@ajaxStep8')->name('ajax.step8');





	Route::post('/contactform1', 'WebsiteController@contactform1')->name('ajax.contactform1');

});







