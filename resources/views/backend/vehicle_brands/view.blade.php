@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
               Show Category
               <div class="float-right">
                    <a href="{{ route('category.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <div class="card-body">
                <div class="mb-2">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <!-- Tab panes -->
                            <tr>
                                <th>
                                   Title:
                                </th>
                                <td>
                                    {{ $category->title ?? '-'}}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                   Short Description:
                                </th>
                                <td>
                                    {!! $category->short_description ?? '-' !!}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                   Description:
                                </th>
                                <td>
                                    {!! $category->description ?? '-' !!}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                   Image:
                                </th>
                                <td>
                                    @if($category->image)
                                        <a href="{{ asset(Storage::url($category->image)) }}" target="_blank">
                                            <img src="{{ asset(Storage::url($category->image)) }}" alt="" height=50 width=50>
                                        </a>
                                    @else
                                        '-'
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>
                                   Parent Category:
                                </th>
                                <td>
                                    {{ ($category->category_name) ? $category->category_name->title: '-'}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection