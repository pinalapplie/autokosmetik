@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.update') }} {{ trans('cruds.drive_car.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('drivecar.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('drivecar.update',[$drivecar->id]) }}" method="POST" enctype="multipart/form-data" id="add_car_type_form">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.drive_car.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($drivecar->name) ? $drivecar->name : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="car_brand">{{ trans('cruds.drive_car.fields.car_type') }}<span class="required_class">*</span></label>
                        <select class="form-control" name="car_brand" id="car_brand">
                            <option value="">{{ trans('cruds.drive_car.fields.select_car_type') }}</option>  
                            @foreach($car_brands as $value)
                                @if (old('car_brand')==$value->id || $drivecar->car_brand == $value->id)
                                    <option value="{{$value->id}}" selected  data-type="{{$value->vehicle_category}}">{{$value->name ?? ''}}</option> 
                                @else
                                    <option value="{{$value->id}}" data-type="{{$value->vehicle_category}}">{{$value->name ?? ''}}</option> 
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="model">{{ trans('cruds.drive_car.fields.modal') }}</label>
                        <input type="text" class="form-control" id="model" name="model" value="{{ old('model', isset($drivecar->model) ? $drivecar->model : '') }}">
                    </div>
                   <!--  <div class="form-group">
                        <label for="model">{{ trans('cruds.drive_car.fields.type') }}<span class="required_class">*</span></label><br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input radio_button" type="radio" name="vehicle_category" id="car_1" value="car" 
                            @if($drivecar->vehicle_category == 'car') checked="" @endif>
                            <label class="form-check-label" for="car_1">{{ trans('cruds.drive_car.fields.car') }}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input radio_button" type="radio" name="vehicle_category" id="bike_1" value="bike"
                            @if($drivecar->vehicle_category == 'bike') checked="" @endif>
                            <label class="form-check-label" for="bike_1">{{ trans('cruds.drive_car.fields.bike') }}</label>
                        </div>
                    </div> -->
                    <div class="form-group car_window_selection" style="@if($drivecar->car_brand_name->vehicle_category == 'bike') display:none @endif">
                        <label for="model">{{ trans('cruds.drive_car.fields.window') }}<span class="required_class">*</span></label><br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="number_of_window" id="bike_3" value="3" checked="">
                            <label class="form-check-label" for="bike_3">3</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="number_of_window" id="bike_5" value="5">
                            <label class="form-check-label" for="bike_5">5</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="number_of_window" id="bike_7" value="7">
                            <label class="form-check-label" for="bike_7">7</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        $("#add_car_type_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
                car_brand: {
                    required: true
                }
            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                },
                car_brand: {
                    required: "@lang('validation.required',['attribute'=>'Car Brand'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
        $('#car_brand').on('change', function(e) {
            e.preventDefault();
            var selectedType= $(this).children("option:selected").val();
            var selectedattrValue= $(this).children("option:selected").attr('data-type');
            if(selectedType == "" || selectedattrValue == "" || selectedattrValue == "bike"){
                $('.car_window_selection').hide();
            }else{
                $('.car_window_selection').show();
            }
            
        });
        // $('body').on('click', '.radio_button', function() {
        //     var value = $(this).val();
        //     if(value == "car"){
        //         $('.car_window_selection').show();
        //     }else{
        //         $('.car_window_selection').hide();
        //     }
        // }); 
    });
</script>
@endsection