@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>Service Mgmt</h4>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('services.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add new service</a>
                </div>
            </div>
            <br>
            <div class="col-md-12  mt-3">
                @if (session('message'))
                <div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('message') }}
                </div>
                @endif
               
                 <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Type</th>
                                        <th>{{ trans('cruds.drive_car.fields.type') }}</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($services as $key => $service)
                                    <tr data-entry-id="{{ $service->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $service->name ?? ''}}</td>
                                        <?php 
                                            $category_name1="";
                                            $category_name2="";
                                            $category_name3="";
                                            if($service->service_category_name && $service->service_category_name->title !=""){
                                                $category_name1 = $service->service_category_name->title;
                                                if($service->service_category_name->category_name && $service->service_category_name->category_name->title !=""){
                                                    $category_name2 = ' -> '.$service->service_category_name->category_name->title;
                                                    if($service->service_category_name->category_name->category_name && $service->service_category_name->category_name->category_name->title !=""){
                                                        $category_name3 = ' -> '.$service->service_category_name->category_name->category_name->title;
                                                    }
                                                }
                                            }
                                        ?>
                                        <td>{{ $category_name1.$category_name2.$category_name3}}</td>
                                        <td>{{ $service->type ?? ''}}</td>
                                        <td>{{ ($service->service_category_name) ? ucfirst($service->service_category_name->vehicle_category) : '-'}}</td>
                                        <td>@if($service->image)
                                                <a href="{{ asset(Storage::url($service->image)) }}" target="_blank">
                                                    <img src="{{ asset(Storage::url($service->image)) }}" alt="" height=50 width=50>
                                                </a>
                                        @else
                                            '-'
                                        @endif
                                        </td>
                                        <td>
                                            @include('partials.switch', ['id'=>
                                            $service->id,'is_active'=>$service->is_active])
                                        </td>
                                        <td>@include('partials.actions', ['id' => $service->id])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
