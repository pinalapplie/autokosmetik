<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWindowTypeToDriveCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drive_cars', function (Blueprint $table) {
            $table->string('model')->nullable();
            $table->enum('vehicle_category', array('car', 'bike'))->default('car');
            $table->integer('number_of_window')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drive_cars', function (Blueprint $table) {
            //
        });
    }
}
