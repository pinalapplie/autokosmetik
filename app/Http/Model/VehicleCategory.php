<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleCategory extends Model
{
	use SoftDeletes;  
    protected $guarded = [];
    protected $table ="vehicle_categories";
    protected $dates = ['deleted_at'];
    protected $with = ['vehicle_type_name'];
    
    public function vehicle_type_name()
    {
        return $this->hasOne('App\Http\Model\VehicleType','id','vehicle_type_id');
    }
}
