<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarBrand extends Model
{
    protected $table ="car_brands";
    protected $guarded = [];
    public function car_type_name()
    {
        return $this->hasOne('App\CarType','id','car_type');
    }
}
