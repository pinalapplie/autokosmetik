@php $routeName = Route::currentRouteName(); @endphp
<?php if ($routeName == 'index') { ?>
    @section('title_meta', 'Autokosmetik, Fahrzeugaufbereitung & Fahrzeugveredelung Liechtenstein / Schweiz')
    @section('description_meta', 'Was wir tun, tun wir gut. Informieren Sie sich jetzt über Autokosmetik, Fahrzeugaufbereitung und Fahrzeugveredelung bei Autokosmetik.li!')
<?php } elseif ($routeName == 'about.us') { ?>
    @section('title_meta', 'Autoaufbereitung Liechtenstein / Schweiz - Autokosmetik.li')
    @section('description_meta', ' Wenn es um die Autoaufbereitung geht, sollte man sich einen kompetenten Partner ins Boot holen. Autokosmetik.li - der Profi für die Fahrzeugveredelung.')
<?php }elseif ($routeName == 'news') { ?>
    @section('title_meta', 'News - Autokosmetik.li ')
    @section('description_meta', 'Erfahren Sie, was es Neues bei Autokosmetik.li gibt. Wir machen mehr aus Ihrem Fahrzeug!')
<?php }elseif ($routeName == 'newsDetails') { ?>
    @section('title_meta', 'News - Autokosmetik.li')
    @section('description_meta', 'Erfahren Sie, was es Neues bei Autokosmetik.li gibt. Wir machen mehr aus Ihrem Fahrzeug!')
<?php }elseif ($routeName == 'services') { ?>
    @section('title_meta', 'Dienstleistungen für Auto, Motorrad und LKW')
    @section('description_meta', 'Unsere Dienstleistungen für Auto, Motorrad und LKW pflegen, schützen und schmücken Ihr Fahrzeug.')
<?php }elseif ($routeName == 'contact') { ?>
    @section('title_meta', 'Kontakt - Autokosmetik.li')
    @section('description_meta', 'Erfahren Sie, was wir für Sie und Ihr Fahrzeug tun können. Nehmen Sie jetzt Kontakt mit uns auf!')
<?php }elseif ($routeName == 'portfolio') { ?>
    @section('title_meta', 'Projekte - Autokosmetik.li')
    @section('description_meta', 'Erfahrung zahlt sich aus. Lassen Sie sich von unseren realisierten Projekten inspirieren und verlangen Sie eine Offerte!')

<?php }elseif ($routeName == 'ceramic_coating') { ?>
@section('title_meta', 'Keramikbeschichtung für Auto, Motorrad oder LKW')
@section('description_meta', 'Wenn es um einen effektiven Lackschutz geht, ist eine Keramikbeschichtung mit ServFaces die erste Wahl. Informieren Sie sich jetzt!')

<?php }elseif ($routeName == 'sealing') { ?>
@section('title_meta', 'Teflonversiegelung für Autos, Motorräder, Wohnmobile, Lastwagen')
@section('description_meta', 'Die Teflonversieglung ermöglicht einen preiswerten Lackschutz für Autos, Motorräder, Wohnmobile und Lastwagen. Jetzt informieren!')

<?php }elseif ($routeName == 'carcare_swissvax') { ?>
@section('title_meta', 'Autopflege mit Swissvax')
@section('description_meta', 'Swissvax bietet beste Qualität, wenn es um die Autopflege geht. Verlangen Sie jetzt eine Offerte!')

<?php }elseif ($routeName == 'car_interior_cleaning') { ?>
@section('title_meta', 'Auto-Innenreinigung Liechtenstein / Schweiz')
@section('description_meta', 'Eine professionelle Auto-Innenreinigung pflegt Ihr Fahrzeug und lässt es in neuem Glanz erstrahlen. Informieren Sie sich jetzt!')

<?php }elseif ($routeName == 'car_interior_processing_special') { ?>
@section('title_meta', 'Auto-Innenaufbereitung')
@section('description_meta', 'Für jedes Problem gibt es eine Lösung. Das gilt auch für Spezialfälle in der Auto-Innenaufbereitung. Fragen Sie uns jetzt an!')

<?php }elseif ($routeName == 'stone_impact_protection_film') { ?>
@section('title_meta', 'Steinschlagschutzfolie für Ihr Auto')
@section('description_meta', 'Eine Steinschlagschutzfolie schützt den Lack zuverlässig vor Kratzern und Steinschlag. Jetzt informieren!')

<?php }elseif ($routeName == 'car_window_tinting') { ?>
@section('title_meta', 'Autoscheibentönung Liechtenstein / Schweiz')
@section('description_meta', 'Eine Autoscheibentönung ist ein Blickfang und schützt die Insassen gegen UV-Strahlung und übermässiger Hitzeentwicklung. Jetzt informieren!')

<?php }elseif ($routeName == 'leather_care_restoration') { ?>
@section('title_meta', 'Lederpflege & Lederrestauration')
@section('description_meta', 'Leder-Autositze sowie Lenkräder und Schaltknüppel aus Leder benötigen Pflege. Beauftragen Sie uns mit der Lederpflege und der Lederrestauration.')

<?php }elseif ($routeName == 'car_wrapping') { ?>
@section('title_meta', 'Autofolierung / Car Wrapping Liechtenstein & Schweiz')
@section('description_meta', 'Mit einer Autofolierung erreichen Sie ein überraschendes Äusseres für Ihr Auto. Informieren Sie sich jetzt über Car Wrapping!')

<?php }elseif ($routeName == 'vehicle_lettering') { ?>
@section('title_meta', 'Fahrzeugbeschriftungen / Flottenbeschriftungen')
@section('description_meta', 'Fahrzeuge bieten eine hervorragende Werbefläche. Informieren Sie sich jetzt über Fahrzeugbeschriftungen und Flottenbeschriftungen!')

<?php }elseif ($routeName == 'water_transfer_printing') { ?>
@section('title_meta', 'Wassertransferdruck')
@section('description_meta', 'Der Wassertransferdruck ermöglicht es auch Fahrzeugelemente, ohne gerade Flächen mit einem Muster nach Wahl zu versehen. Jetzt informieren!')

<?php }elseif ($routeName == 'chrome_plating') { ?>
@section('title_meta', 'Verchromungen')
@section('description_meta', 'Neben der Neuverchromung verhelfen wir auch verblassten Chrom-Bauteilen zu neuem Glanz. Jetzt über Verchromungen informieren!')

<?php }elseif ($routeName == 'interior_design') { ?>
@section('title_meta', 'Interieur-Design für Ihr Auto')
@section('description_meta', 'Ein einzigartiges Interieur-Design für Ihr Auto. Informieren Sie sich jetzt über die Möglichkeiten!')

<?php }elseif ($routeName == 'bookappointment') { ?>
@section('title_meta', 'Autokosmetik, Fahrzeugaufbereitung & Fahrzeugveredelung Liechtenstein / Schweiz')
@section('description_meta', 'Was wir tun, tun wir gut. Informieren Sie sich jetzt über Autokosmetik, Fahrzeugaufbereitung und Fahrzeugveredelung bei Autokosmetik.li!')

<?php } ?>