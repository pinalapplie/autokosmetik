<?php

namespace App\Http\Controllers;


use App\Http\Model\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feedback = Feedback::latest()->get();
        return view('backend.feedback.index', compact('feedback'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $feedback = Feedback::get();
        return view('backend.feedback.create',compact('feedback'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                $feedback = Feedback::where('id',$id)->first();
                if (empty($feedback->is_active)) {
                    $feedback->is_active = 1;
                } else {
                    $feedback->is_active = 0;
                }
                    $feedback->save();
                return response()->json(['success' => 1]);
            }else{
                $this->validate($request, [
                    'name' => 'required','occupation' => 'required',
                    'description' => 'required'
                ]);
                if(!empty($request->id)){
                    $id = Crypt::decrypt($request->id);
                    $feedback = Feedback::where('id',$id)->first();
                    $message = trans('message.update', ['entity' => trans('cruds.feedback.title_singular')]);
                }else{
                    $feedback = new Feedback;
                    $message = trans('message.create', ['entity' => trans('cruds.feedback.title_singular')]);
                }
                    if ($request->hasFile('image')) {                        
                        $storage_path = 'public/';
                        $feedback->image = Storage::disk('local')->put($storage_path, $request->file('image'));
                    }
                    $feedback->name = $request->name;
                    $feedback->occupation = $request->occupation;
                    $feedback->description = $request->description;
                    $feedback->is_active = 1;
                    $feedback->save();
                    return redirect()->route('feedback.index')->with('success',$message);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
            return redirect()->back()->with('danger', trans('global.something_wrong'));    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feedback  $Feedback
     * @return \Illuminate\Http\Response
     */
    public function show(Feedback $Feedback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feedback  $Feedback
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $feedback = Feedback::where('id',$id)->first();
        return view('backend.feedback.edit', compact('feedback'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feedback  $Feedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feedback $Feedback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feedback  $Feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                Feedback::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }

}
