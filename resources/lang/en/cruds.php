<?php

return [
    'menu' => 'Menu',
    'setting' => 'Settings',
    'custom_required' => '*The field is required.',
    'permission'     => [
        'title'          => 'Permissions',
        'title_singular' => 'Permission',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'title'             => 'Title',
            'title_helper'      => '',
            'created_at'        => 'Created at',
            'created_at_helper' => '',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => '',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => '',
        ],
    ],
    'vehicletypes'     => [
        'title'          => 'Vehicle Types',
        'title_singular' => 'Vehicle Type',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
        ],
    ],
    'vehiclecategories'     => [
        'title'          => 'Vehicle Categories',
        'title_singular' => 'Vehicle Category',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
        ],
    ],
    'vehicleconditions'     => [
        'title'          => 'Vehicle Conditions',
        'title_singular' => 'Vehicle Condition',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
        ],
    ],
    'addonservice'     => [
        'title'          => 'Add On Service',
        'title_singular' => 'Add On Service',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
            'price' => 'Price',
        ],
    ],
    'servicecategories'     => [
        'title'          => 'Service Categories',
        'title_singular' => 'Service Category',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
            'parent_category' => 'Parent Category',
            'select_parent_category' => 'Select Parent Category',
            'vehicle_type' => 'Vehicle Type',
            'select_vehicle_type' => 'Select Vehicle Type',
            'description' => 'Description'
        ],
    ],
    'feedback'     => [
        'title'          => 'User Feedback',
        'title_singular' => 'User Feedback',
        'fields'         => [
            'id'         => 'ID',
            'name'       => 'Name',
            'occupation' => 'Occupation',
            'image'      => 'Image',
            'description' => 'Description',
            'status'     => 'Status',
            'actions'    => 'Actions'
        ],
    ], 
    'servicesubcategories'     => [
        'title'          => 'Service Sub Categories',
        'title_singular' => 'Service Sub Category',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
            'parent_category' => 'Parent Category',
            'select_parent_category' => 'Select Parent Category',
            'vehicle_type' => 'Vehicle Type',
            'select_vehicle_type' => 'Select Vehicle Type',
            'description' => 'Description'
        ],
    ],
    'vehiclebrands'     => [
        'title'          => 'Vehicle Brands',
        'title_singular' => 'Vehicle Brand',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
            'parent_category' => 'Parent Category',
            'select_parent_category' => 'Select Parent Category',
            'vehicle_category' => 'Vehicle Category',
            'select_vehicle_category' => 'Select Vehicle Category',
            'description' => 'Description'
        ],
    ],
    'drivevehicles'     => [
        'title'          => 'Drive Vehicles',
        'title_singular' => 'Drive Vehicle',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
            'parent_category' => 'Parent Category',
            'select_parent_category' => 'Select Parent Category',
            'vehicle_brand' => 'Vehicle Brand',
            'select_vehicle_brand' => 'Select Vehicle Brand',
            'vehicle_category' => 'Vehicle Category',
            'select_vehicle_category' => 'Select Vehicle Category',
            'description' => 'Description',
            'modal' => 'Model',
            'number_of_window' => 'Number Of Window',
            'price' => 'Price',
        ],
    ],
    'vehicleservices'     => [
        'title'          => 'Vehicle Services',
        'title_singular' => 'Vehicle Service',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
            'parent_category' => 'Parent Category',
            'select_parent_category' => 'Select Parent Category',
            'vehicle_brand' => 'Vehicle Brand',
            'select_vehicle_category' => 'Select Vehicle Category',
            'vehicle_category' => 'Vehicle Category',
            'service' => 'Service',
            'select_service' => 'Select Service',
        ],
    ],
    'car_type'     => [
        'title'          => 'Vehicle Types',
        'title_singular' => 'Vehicle Type',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'name'             => 'Name',
        ],
    ],
    'car_condition_type'     => [
        'title'          => 'Vehicle Conditions',
        'title_singular' => 'Vehicle Condition',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'name'             => 'Name',
        ],
    ],
    'car_brand'     => [
        'title'          => 'Vehicle Brands',
        'title_singular' => 'Vehicle Brand',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'name'             => 'Name',
            'car_type'             => 'Vehicle Type',
            'select_car_type'             => 'Select vehicle Type',
        ],
    ],
    'drive_car'     => [
        'title'          => 'Drive Vehicles',
        'title_singular' => 'Drive Vehicle',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'name'             => 'Name',
            'car_type'             => 'Vehicle Brand',
            'select_car_type'             => 'Select Vehicle Brand',
            'modal'             => 'Model',
            'type'             => 'Vehicle Category',
            'window'             => 'Number of Window',
            'car' => 'Car',
            'bike' => 'Bike'
        ],
    ],
    'servicemangement'     => [
        'title'          => 'Services Mangement',
        'title_singular' => 'Service Mangement',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
            'category'             => 'Category',
            'type'             => 'Type',
            'select_type' => 'Select Type',
            'car_type'             => 'Vehicle Type',
            'select_car_type'             => 'Select vehicle Type',
            'description' => 'Description',
            'select_category' => 'Select Category',
            'specification' =>'Specification',
            'pricechart' => 'Pricechart',
            'service_category' => 'Service Category',
            'vehicle_condition' => 'Vehicle Condition',
            'price' => 'Price',
            'subcategory' => 'Sub Category',
            'select_subcategory' => 'Select Sub Category'
        ],
    ],
    'news'     => [
        'title'          => 'News Mangement',
        'title_singular' => 'News Mangement',
        'fields'         => [
            'id'         => 'ID',
            'name'       => 'Name',
            'date'   => 'Date',
            'image'       => 'Image',
            'description' => 'Description',
            'car_type'   => 'Vehicle Type'
        ],
    ],
    'portfolio'     => [
        'title'          => 'Portfolio Mangement',
        'title_singular' => 'Portfolio Mangement',
        'fields'         => [
            'id'         => 'ID',
            'name'       => 'Name',
            'category_id'   => 'category_id',
            'images'       => 'Images',
            'description' => 'Description',
            'created'   => 'Created Date',
            'client' => 'Client Date',
            'category' => 'Category',
            'select_category' => 'Select Category',
        ],
    ],
];
