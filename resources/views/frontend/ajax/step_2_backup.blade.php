@php $temp_array = [];@endphp
@foreach ($service_category as $key => $value) 
    @if(!in_array( $value->service_category_name->id, $temp_array))
        <h3 class="pb-3">{{$value->service_category_name->name}}</h3>
        @php $temp_array[] = $value->service_category_name->id; @endphp
    @endif    
    <div class="form-group">
        <input type="checkbox" name="step_3" class="form-control checkbox" value="{{$value->id}}">
        <label role="button" >{{$value->name}}</label>
        <div class="infoicon-i" role="button" data-toggle="modal" data-target="#modal_bookappointment_{{$value->id}}">
        	<img src="{{ asset('assets/frontend/images/info-icon.svg') }} ">
    	</div>
    </div>
@endforeach