<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Autokosmetik</title>
    <link rel="shortcut icon" href="{{ asset('assets/comingsoon/img/favicon.png') }}" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@500;700&family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Anton&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('assets/comingsoon/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/comingsoon/css/main.css') }}">
</head>

<body>
    <section class="position-relative overflow-hidden">
        <div class="d-flex flex-wrap text-center text-gold">
            <button class="btn btn-menu">
                <i class="fas fa-bars text-gold"></i>
            </button>
            <div class="flex-grow-1 main py-4 min-vh-100 row justify-content-center">
                <div class="wrapper mx-auto">
                    <img src="{{ asset('assets/comingsoon/img/logo.png') }}" alt="" class="img-fluid logo mb-5">

                    <div id="timer" class="text-gold mb-5">
                        <div class="form-row">
                            <div class="col col-md-3 text-center">
                                <div class="display-4" id="days"></div>
                            </div>
                            <div class="col col-md-3 text-center">
                                <div class="display-4" id="hours"></div>
                            </div>
                            <div class="col col-md-3 text-center">
                                <div class="display-4" id="minutes"></div>
                            </div>
                            <div class="col col-md-3 text-center">
                                <div class="display-4" id="seconds"></div>
                            </div>
                        </div>

                    </div>
                    <h1 class="text-center text-gold display-4 pb-lg-5 mb-5" style="font-family: 'Anton', sans-serif;">Coming Soon</h1>
                    <p class="h5 text-light">Nebst den neuen Räumlichkeiten in Triesen, werden wir auch unseren Webauftritt auffrischen, damit wir unseren Kunden ein rundum zufriedenstellendes Erlebnis anbieten
                        können.</p>
                    <p class="pb-lg-5 mb-5 text-center text-md-right">- Pascal Nitzlnader</p>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <form action="">
                                <div class="form-row">
                                    <div class="col-12 col-md mb-2 mb-md-0">
                                        <input type="email" placeholder="E-Mail Address" required>
                                    </div>
                                    <div class="col-12 col-md-auto">
                                        <button type="submit" class="text-uppercase">Contact Us</button>
                                    </div>
                                </div>
                            </form>
                            <ul class="list-unstyled list-inline mb-2 mt-4 my-md-5">
                                <li class="list-inline-item">
                                    <a href="https://www.facebook.com/AUTOKOSMETIK/" target="_blank" class="rounded-circle d-flex align-items-center justify-content-center social-icon">
                                        <i class="fab fa-facebook-f text-gold"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="https://www.google.ch/maps/place/Autokosmetik.li/@47.218252,9.511339,15z/data=!4m5!3m4!1s0x0:0x3025938a37e377f0!8m2!3d47.218252!4d9.511339" target="_blank"
                                        class="rounded-circle d-flex align-items-center justify-content-center social-icon">
                                        <i class="fab fa-google text-gold"></i>
                                    </a>
                                </li>
                            </ul>
                            <p class="copyright">&copy; Copyright 2021 by Autokosmetik. All rights reserved</p>
                        </div>
                    </div>
                </div>
            </div>
            <aside class="sidebar py-5 d-flex position-relative">
                <div class="sidebar-content">
                    <h2 class="h2 text-center mb-5" style="font-family: 'Anton', sans-serif;">ÜBER UNS</h2>
                    <div class="h5 mb-3">Die Autokosmetik zieht um!</div>
                    <p class="text-muted">Um unseren Kundinnen und Kunden ein noch angenehmeres Kundenerlebnis zu ermöglichen, werden wir in neue Geschäftsräumlichkeiten umziehen. Diese werden im
                        Erweiterungsbau der Max Heidegger AG Unterkunft finden.</p>

                    <ul class="list-unstyled list-inline mt-auto mb-4">
                        <li class="list-inline-item">
                            <span class="address contact-icon mx-auto">
                                <i class="fas fa-map-marker-alt fa-2x"></i>
                            </span>
                            <div class="my-2 text-muted">Address</div>
                            <div>Messinastrasse 1 | 9495 Triesen | Liechtenstein</div>
                        </li>
                    </ul>

                    <ul class="list-unstyled list-inline mt-auto mb-4">
                        <li class="list-inline-item mr-lg-5">
                            <a href="tel:+4233730354" class="phone contact-icon mx-auto">
                                <i class="fas fa-phone-alt fa-2x"></i>
                            </a>
                            <div class="mt-2 text-muted">Phone</div>
                            <div class="mt-2">+423 373 03 54</div>
                        </li>
                        <li class="list-inline-item">
                            <a href="mailto:info@autokosmetik.li" class="mail contact-icon mx-auto">
                                <i class="fas fa-envelope fa-2x"></i>
                            </a>
                            <div class="mt-2 text-muted">Email</div>
                            <div class="mt-2">info@autokosmetik.li</div>
                        </li>
                    </ul>
                </div>

                <div class="overlay"></div>
            </aside>
        </div>

    </section>


    <script src="{{ asset('assets/comingsoon/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/comingsoon/js/main.js') }}"></script>
</body>

</html>