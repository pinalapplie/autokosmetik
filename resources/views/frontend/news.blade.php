@section('title', 'Nachrichten')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<div id="main-content" class="active">
		    <div id="section-breadcrumb1"  class="inner-banner-wrap">
				<img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
		    	<div class="container">
		    		<div class="row">
		    			<div class="content col-12">
		    				<h1>News</h1>
		    				<ul>
		    					<li><a href="{{ route('index')}}">ZUHAUSE</a></li>
		    					<li class="current text-light">News</li>
		    				</ul>
		    			</div>
		    		</div>
		    	</div>
		    </div>
			<div id="section-news2">
				<div class="container">
					<div class="desc-text pb-5">
						<p>Unsere News, kurz und knackig.</p>
					</div>
					<div class="row">
						<div class="contents col-12 col-sm-12 col-md-12 col-lg-8">
							
                            @foreach($news as $new)
                            <!-- Item -->
							<div class="item">
								<a href="{{ route('newsDetails', $new->id) }}"><img class="img-fluid" src="{{asset(Storage::url($new->image)) ?? ''}}" alt="{{ $new->name }}"></a><br>
								<span><?= date('F d, Y', strtotime($new->date)) ?></span>
								<a href="{{ route('newsDetails', $new->id) }}"><h2>{{ $new->name }}</h2></a>
							</div>
							<!-- /.Item -->
                            @endforeach
                            {{ $news->links() }}
							<!-- Pagination -->
							<ul class="pagination justify-content-center" style="display: none;">
								<li class="page-item disabled">
									<span class="page-link">prev</span>
								</li>
								<li class="page-item"><a class="page-link" href="#">01</a></li>
								<li class="page-item"><a class="page-link" href="#">02</a></li>
								<li class="page-item active" aria-current="page">
									<span class="page-link">
										03
										<span class="sr-only">(current)</span>
									</span>
								</li>
								<li class="page-item"><a class="page-link" href="#">...</a></li>
								<li class="page-item"><a class="page-link" href="#">10</a></li>
								<li class="page-item">
									<a class="page-link" href="#">next</a>
								</li>
							</ul>
							<!-- /.Pagination -->
						</div>
						<div class="sidebar col-12 col-sm-12 col-md-12 col-lg-4">
							<!--<div class="searchbar">
								<h3>Suchbereich</h3>
								<form action="#" method="post">
				                    <input type="text" name="search" placeholder="Search autokosmetik">
				                    <i class="flaticon-search"></i>
				                </form>
							</div>-->
							<div class="popular-news">
								<h3>Beliebte Nachrichten</h3>
                                
                                @foreach($randomNews as $new)
								<div class="list mb-3">
									<a href="{{ route('newsDetails', $new->id) }}">
                                        <div class="media">
                                            <div class="avatar-md">
                                              <img class="img-fluid" src="{{asset(Storage::url($new->image)) ?? ''}}" alt="{{ $new->name }}">
                                            </div>
                                          <div class="media-body">
                                            <p>{{ $new->name }}</p>
                                          </div>
										</div>
									</a>
								</div>
                                @endforeach
							</div>
							<div class="instagram-gallery">
								<h3>Instagram Galerie</h3>
								<div class="row">
									<div class="list">
										<a href="#">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/img-instagram1-1.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="list">
										<a href="#">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/img-instagram1-2.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="list">
										<a href="#">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/img-instagram1-3.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="list">
										<a href="#">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/img-instagram1-4.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="list">
										<a href="#">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/img-instagram1-5.jpg') }}" alt="autokosmetik">
										</a>
									</div>
									<div class="list">
										<a href="#">
											<img class="img-fluid" src="{{ asset('assets/frontend/images/img-instagram1-6.jpg') }}" alt="autokosmetik">
										</a>
									</div>
								</div>
							</div>
							<!-- <div class="category">
								<h3>Category</h3>
								<ul>
									<li><a href="#">Business</a></li>
									<li><a href="#">Tegnology</a></li>	
									<li><a href="#">Creative TF</a></li>
									<li><a href="#">Design</a></li>
									<li><a href="#">Illustrations</a></li>
									<li><a href="#">Graphic Art</a></li>
								</ul>
								<ul>
									<li><a href="#">Web developer</a></li>
									<li><a href="#">Marketing</a></li>	
									<li><a href="#">Audio edition</a></li>
									<li><a href="#">Photoshop</a></li>
								</ul>
							</div> -->
						</div>
					</div>
				</div>
			</div>
			<div id="section-subscribe1">
				<div class="container">
					<div class="row">
						<div class="title1 col-12">
							<h2><span>Jetzt Newsletter</span> abonnieren</h2>
							<i class="flaticon-download"></i>
						</div>
						<!-- <div class="content col-12">
							<p>Aliquam vehicula mollis urna vel dignissim. Integer tincidunt viverra est, non congue lorem tempor ac. Phasellus pulvinar iaculis.</p>
						</div> -->
						<div class="subscribe col-12">
							<form action="#" id="SubscribeForm">
								<input type="email" name="yourEmail" placeholder="E-Mail-Addresse" required>
								<button type="submit">Abonnieren</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection    
	