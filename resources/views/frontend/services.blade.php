@section('title', 'Dienstleistungen')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<div id="main-content" class="active">
			<div id="section-breadcrumb1" class="inner-banner-wrap">
				<img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
								
				<div class="container">
					<div class="row">
						<div class="content col-12">
							<h1>Dienstleistungen</h1>
							<ul>
								<li><a href="{{ route('index')}}">ZUHAUSE</a></li>
								<li class="current text-light">Dienstleistungen</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="section-services1" class="bg-light service-accordian-main accordion">
				<div class="container feature-car-area">
					<div class="accordion-wrap" id="accordionExample">
						<div class="row">
							<div class="col-md-4 mb-3">
								<div class="service-level-1 bg-white shadow-sm collapsed Pflege" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" style="background: url({{ asset('assets/frontend/images/car-care-bg.png')}}) no-repeat center / cover;" role="button">
									<div class="service-title-wrap">
										<div class="avatar-sm mb-3">
											<img src="{{ asset('assets/frontend/images/car-care.png')}}" alt="" class="img-fluid">
										</div>
										<h3 class="h3 font-weight-bold mb-0">Pflege</h3>
										<p>Perfekte Pflege für Ihr Fahrzeug: Auto-Innenreinigung, Lederpflege und –reparatur sowie Aussenaufbereitung.</p>
										<span class="text-primary btn-link">Mehr erfahren</span>
									</div>
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<div class="service-level-1 bg-white shadow-sm collapsed Schutz" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2" style="background: url({{ asset('assets/frontend/images/car-protect-bg.jpg')}}) no-repeat center / cover;" role="button">
									<div class="service-title-wrap">
										<div class="avatar-sm mb-3">
											<img src="{{ asset('assets/frontend/images/car-protect.png')}}" alt="" class="img-fluid">
										</div>
										<h3 class="h3 font-weight-bold mb-0">Schutz</h3>
										<p>Steinschlagschutzfolien, eine Keramikbeschichtung und eine Scheibentönung schützen Ihr Auto oder Ihr Motorrad.</p>
										<span class="text-primary btn-link">Mehr erfahren</span>
									</div>
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<div class="service-level-1 bg-white shadow-sm collapsed design" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3" style="background: url({{ asset('assets/frontend/images/paint-1.jpg')}}) no-repeat center / cover;" role="button">
									<div class="service-title-wrap">
										<div class="avatar-sm mb-3">
											<img src="{{ asset('assets/frontend/images/car-foil.png')}}" alt="" class="img-fluid">
										</div>
										<h3 class="h3 font-weight-bold mb-0">Design</h3>
										<p>Interieur-Design, Verchromungen, Wassertransferdruck und eine Autofolie machen Ihr Fahrzeug zum Blickfang.</p>
										<span class="text-primary btn-link">Mehr erfahren</span>
									</div>
								</div>
							</div>
						</div>
					
						<div class="clearfix"></div>
						<div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
							<div class="container" id="service-category">
								<div id="service-category-2" class="care-for-carousel owl-carousel owl-theme">
									<div class="item pb-4">
										<div class="bg-white shadow-sm collapsed aussenaufbereitung_a">
												<a href="{{ route('sealing')}}" class="plegelink">
													<div class="ratio ratio-16X9">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
													</div>
												</a>
												<div class="px-2 position-relative">
													<a href="{{ route('bookappointment')}}" role="button">
														<div class="align-items-center bg-white d-flex m-0 btn-service">
															<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
															<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
														</div>
													</a>
													<a href="{{ route('sealing')}}" class="plegelink">
														<h5 class="font-weight-bold h4 pt-3 m-0">Teflonversiegelung</h5>
														<p class="h6 py-2">Eine Teflon-Versiegelung ist sehr preiswert, hält 6-12 Monate und...
															<span class="font-weight-bolder">Weiterlesen</span>
														</p>
													</a>
												</div>
											
										</div>
									</div>
									<div class="item pb-4">
										<div class="bg-white shadow-sm collapsed lederpflegeRestauration">
											<a href="{{ route('leather_care_restoration')}}" class="plegelink">
												<div class="ratio ratio-16X9">
													<img src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="" class="img-fluid">
												</div>
											</a>
											<div class="px-2 position-relative">
												<a href="{{ route('bookappointment')}}" role="button">
													<div class="align-items-center bg-white d-flex m-0 btn-service">
														<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
														<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
													</div>
												</a>
												<a href="{{ route('leather_care_restoration')}}" class="plegelink">
												<h5 class="font-weight-bold h4 pt-3 m-0">Lederpflege & Restauration</h5>
												<p class="h6 py-2">Leder benötigt besondere Pflege. Das gilt besonders für stark...
													<span class="font-weight-bolder">Weiterlesen</span>
												</p>
												</a>
											</div>
										</div>
									</div>
									<div class="item pb-4">
										<div class="bg-white shadow-sm collapsed innenreinigung">
											<a href="{{ route('car_interior_cleaning')}}" class="plegelink">
											<div class="ratio ratio-16X9">
												<img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="" class="img-fluid">
											</div>
											</a>
											<div class="px-2 position-relative">
												<a href="{{ route('bookappointment')}}" role="button">
													<div class="align-items-center bg-white d-flex m-0 btn-service">
														<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
														<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
													</div>
												</a>
												<a href="{{ route('car_interior_cleaning')}}" class="plegelink">
												<h5 class="font-weight-bold h4 pt-3 m-0">Auto-Innenreinigung</h5>
												<p class="h6 py-2">Bei einer Auto-Innenreinigung arbeiten wir mit einem...
													<span class="font-weight-bolder">Weiterlesen</span>
												</p>
												</a>
											</div>
										</div>
									</div>
									<div class="item pb-4">
										<div class="bg-white shadow-sm collapsed lederpflege">
											<a href="{{ route('carcare_swissvax')}}" class="plegelink">
												<div class="ratio ratio-16X9">	
													<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
												</div>
											</a>
											<div class="px-2 position-relative">
												<a href="{{ route('bookappointment')}}" role="button">
													<div class="align-items-center bg-white d-flex m-0 btn-service">
														<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
														<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
													</div>
												</a>
												<a href="{{ route('carcare_swissvax')}}" class="plegelink">
												<h5 class="font-weight-bold h4 pt-3 m-0">Autopflege mit Swissvax</h5>
												<p class="h6 py-2">Als offizielles Swissvax Automobil Detailing Center für Liechtenstein...
													<span class="font-weight-bolder">Weiterlesen</span>
												</p>
												</a>
											</div>
										</div>
									</div>
									<div class="item pb-4">
										<div class="bg-white shadow-sm collapsed lederpflege">
											<a href="{{ route('car_interior_processing_special')}}" class="plegelink">
											<div class="ratio ratio-16X9">	
												<img src="{{ asset('assets/frontend/images/special-1.jpg') }}" alt="" class="img-fluid">
											</div>
											</a>
											<div class="px-2 position-relative">
												<a href="{{ route('bookappointment')}}" role="button">
													<div class="align-items-center bg-white d-flex m-0 btn-service">
														<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
														<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
													</div>
												</a>
												<a href="{{ route('car_interior_processing_special')}}" class="plegelink">
												<h5 class="font-weight-bold h4 pt-3 m-0">Spezialfälle</h5>
												<p class="h6 py-2">Bei "speziellen Fällen" wie diesen können wir keine Fixpreise anbieten...
													<span class="font-weight-bolder">Weiterlesen</span>
												</p>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="collapse2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
							<div class="container" id="service-category-collapse-2">
								<div id="service-category-2" class="care-for-carousel-folien owl-carousel owl-theme">
									<div class="item pb-4">
										<div class="bg-white shadow-sm scheibentönung">
											<a href="{{ route('car_window_tinting')}}" class="schutzlink">
											<div class="ratio ratio-16X9">
												<img src="{{ asset('assets/frontend/images/tint-10.jpg') }}" alt="" class="img-fluid">
											</div>
											</a>
											<div class="px-2 position-relative">
												<a href="{{ route('bookappointment')}}" role="button">
													<div class="align-items-center bg-white d-flex m-0 btn-service">
														<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
														<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
													</div>
												</a>
												<a href="{{ route('car_window_tinting')}}" class="schutzlink">
												<h5 class="font-weight-bold h4 pt-3 m-0">Autoscheibentönung</h5>
												<p class="h6 py-2">Eine Autoscheibentönung sieht nicht nur gut aus, sie schützt auch in...
													<span class="font-weight-bolder">Weiterlesen</span>
												</p>
												</a>
											</div>
										</div>
									</div>
									<div class="item pb-4">
										<div class="bg-white shadow-sm steinschlagschutzfolien">
											<a href="{{ route('stone_impact_protection_film')}}" class="schutzlink">
											<div class="ratio ratio-16X9">
												<img src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="" class="img-fluid">
											</div>
											</a>
											<div class="px-2 position-relative">
												<a href="{{ route('bookappointment')}}" role="button">
													<div class="align-items-center bg-white d-flex m-0 btn-service">
														<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
														<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
													</div>
												</a>
												<a href="{{ route('stone_impact_protection_film')}}" class="schutzlink">
												<h5 class="font-weight-bold h4 pt-3 m-0">Steinschlagschutzfolie</h5>
												<p class="h6 py-2">Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschlag...
													<span class="font-weight-bolder">Weiterlesen</span>
												</p>
												</a>
											</div>
										</div>
									</div>
									<div class="item pb-4">
										<div class="bg-white shadow-sm keramikbeschichtung">
											<a href="{{ route('ceramic_coating')}}" class="schutzlink">
											<div class="ratio ratio-16X9">
												<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
											</div>
											</a>
											<div class="px-2 position-relative">
												<a href="{{ route('bookappointment')}}" role="button">
													<div class="align-items-center bg-white d-flex m-0 btn-service">
														<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
														<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
													</div>
												</a>
												<a href="{{ route('ceramic_coating')}}" class="schutzlink">
												<h5 class="font-weight-bold h4 pt-3 m-0">Keramikbeschichtung</h5>
												<p class="h6 py-2">Sie fahren Ihr Auto täglich und es soll dabei mit möglichst wenig Aufwand...
													<span class="font-weight-bolder">Weiterlesen</span>
												</p>
												</a>
											</div>
										</div>
									</div>
								</div>
                            </div>
						</div>
						<div id="collapse3" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
							<div id="service-category-collapse-3" class="container">
								<div id="service-category-2" class="care-for-carousel-Komplettfolierung owl-carousel owl-theme">
									<div class="item pb-4">
										<div class="bg-white shadow-sm collapsed">
											<a href="{{ route('car_wrapping')}}" class="designlink">
											<div class="ratio ratio-16X9">
												<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
											</div>
											</a>
											<div class="px-2 position-relative">
												<a href="{{ route('bookappointment')}}" role="button">
													<div class="align-items-center bg-white d-flex m-0 btn-service">
														<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
														<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
													</div>
												</a>
												<a href="{{ route('car_wrapping')}}" class="designlink">
												<h5 class="font-weight-bold h4 pt-3 m-0">Autofolierung / Car Wrapping</h5>
												<p class="h6 py-2">Verschönern Sie Ihr Auto, Ihr Motorrad oder Ihren Lastwagen mit einer...
													<span class="font-weight-bolder">Weiterlesen</span>
												</p>
												</a>
											</div>
										</div>
									</div>
									<div class="item pb-4">
										<div class="bg-white shadow-sm collapsed">
											<a href="{{ route('chrome_plating')}}" class="designlink">
											<div class="ratio ratio-16X9">
												<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
											</div>
											</a>
											<div class="px-2 position-relative">
												<a href="{{ route('bookappointment')}}" role="button">
													<div class="align-items-center bg-white d-flex m-0 btn-service">
														<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
														<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
													</div>
												</a>
												<a href="{{ route('chrome_plating')}}" class="designlink">
												<h5 class="font-weight-bold h4 pt-3 m-0">Verchromungen</h5>
												<p class="h6 py-2">Mehr Glanz für Ihr Auto, Ihr Motorrad oder Ihren Lastwagen! Gerne...
													<span class="font-weight-bolder">Weiterlesen</span>
												</p>
												</a>
											</div>
										</div>
									</div>
									<div class="item pb-4">
										<div class="bg-white shadow-sm">
											<a href="{{ route('interior_design')}}" class="designlink">
												<div class="ratio ratio-16X9">
													<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
												</div>
											</a>
											<div class="px-2 position-relative">
												<a href="{{ route('bookappointment')}}" role="button">
													<div class="align-items-center bg-white d-flex m-0 btn-service">
														<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
														<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
													</div>
												</a>
												<a href="{{ route('interior_design')}}" class="designlink">
												<h5 class="font-weight-bold h4 pt-3 m-0">Interieur-Design</h5>
												<p class="h6 py-2">Fahrzeugveredelungen müssen sich nicht auf das Äussere beschränken. Wir...
													<span  class="font-weight-bolder">Weiterlesen</span>
												</p>
												</a>
											</div>
										</div>
									</div>
									<div class="item pb-4">
										<div class="bg-white shadow-sm collapsed">
											<a href="{{ route('vehicle_lettering')}}" class="designlink">
											<div class="ratio ratio-16X9">
												<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
											</div>
											</a>
											<div class="px-2 position-relative">
												<a href="{{ route('bookappointment')}}" role="button">
													<div class="align-items-center bg-white d-flex m-0 btn-service">
														<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
														<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
													</div>
												</a>
												<a href="{{ route('vehicle_lettering')}}" class="designlink">
												<h5 class="font-weight-bold h4 pt-3 m-0">Fahrzeugbeschriftung</h5>
												<p class="h6 py-2">Wer nicht wirbt, stirbt – so der Volksmund. Und es steckt durchaus Wahrheit...
													<span class="font-weight-bolder">Weiterlesen</span>
												</p>
												</a>
											</div>
										</div>
									</div>
									<div class="item pb-4">
										<div class="bg-white shadow-sm collapsed">
											<a href="{{ route('water_transfer_printing')}}" class="designlink">
											<div class="ratio ratio-16X9">
												<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
											</div>
											</a>
											<div class="px-2 position-relative">
												<a href="{{ route('bookappointment')}}" role="button">
													<div class="align-items-center bg-white d-flex m-0 btn-service">
														<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
														<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
													</div>
												</a>
												<a href="{{ route('water_transfer_printing')}}" class="designlink">
												<h5 class="font-weight-bold h4 pt-3 m-0">Wassertransferdruck</h5>
												<p class="h6 py-2">Wassertransferdruck ist ein relativ modernes Verfahren, das es möglich...
													<span class="font-weight-bolder">Weiterlesen</span>
												</p>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="services-mobile">
				<section class="mt-lg-5">
					<h3 class="h2 text-center mb-3 mb-lg-5">Unser Serviceangebot</h3>
					<div class="tabpanel-service main-tabpanel-service">
						<ul class="nav justify-content-start nav-pills mb-3" id="pills-tab" role="tablist">
							<li class="nav-item" role="presentation">
								<button class="nav-link active bg-white Pflegemobile" data-toggle="pill" data-target="#pills-care" type="button" role="tab" aria-controls="pills-care" aria-selected="false">
									<div class="service-level-1 car-care-icon bg-white p-3" role="button" data-class1="service-first">
										<div class="service-title-wrap">
											<div class="avatar-sm mb-3">
												<img src="{{ asset('assets/frontend/images/car-care.png')}}" alt="" class="img-fluid">
											</div>
											<h4 class="h6 text-center font-weight-bold mb-0 text-body">Pflege</h4>
										</div>
									</div>
								</button>
							</li>
							<li class="nav-item" role="presentation">
								<button class="nav-link bg-white Schutzmobile" data-toggle="pill" data-target="#pills-protect" type="button" role="tab" aria-controls="pills-protect" aria-selected="false">
									<div class="service-level-1 car-care-icon bg-white p-3" role="button" data-class1="service-first">
										<div class="service-title-wrap">
											<div class="avatar-sm mb-3">
												<img src="{{ asset('assets/frontend/images/car-protect.png')}}" alt="" class="img-fluid">
											</div>
											<h4 class="h6 text-center font-weight-bold mb-0 text-body">Schutz</h4>
										</div>
									</div>
								</button>
							</li>
							<li class="nav-item" role="presentation">
								<button class="nav-link bg-white designmobile" data-toggle="pill" data-target="#pills-enhance" type="button" role="tab" aria-controls="pills-enhance" aria-selected="false">
									<div class="service-level-1 car-care-icon bg-white p-3" role="button" data-class1="service-first">
										<div class="service-title-wrap">
											<div class="avatar-sm mb-3">
												<img src="{{ asset('assets/frontend/images/car-foil.png')}}" alt="" class="img-fluid">
											</div>
											<h4 class="h6 text-center font-weight-bold mb-0 text-body">Design</h4>
										</div>
									</div>
								</button>
							</li>
						</ul>
					</div>
					<div class="tabpanel-service">
						<div class="tab-content" id="pills-tabContent">
							<div class="tab-pane fade show active" id="pills-care" role="tabpanel">
								<div class="accordion px-2">
									<div class="care-for-carousel owl-carousel owl-theme">
										<div class="item pb-4">
											<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_a_2" aria-expanded="false" aria-controls="collapse1_a_2" role="button">
												<a href="{{ route('sealing')}}" class="plegemobilelink">
												<div class="ratio ratio-16X9">
													<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
												</div>
												</a>
												<div class="px-2 position-relative">
													<a href="{{ route('bookappointment')}}" role="button">
														<div class="align-items-center bg-white d-flex m-0 btn-service">
															<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
															<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
														</div>
													</a>
													<a href="{{ route('sealing')}}" class="plegemobilelink">
													<h5 class="font-weight-bold h4 pt-3 m-0">Teflonversiegelung</h5>
													<p class="h6 py-2 pb-3">Eine Teflon-Versiegelung ist sehr preiswert, hält 6-12 Monate und ist für die meisten...
														<span class="font-weight-bolder">Weiterlesen</span>
													</p>
													</a>
												</div>
											</div>
										</div>
										<div class="item pb-4">
											<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_b_1" aria-expanded="false" aria-controls="collapse1_b_1" role="button">
												<a href="{{ route('car_interior_cleaning') }}" class="plegemobilelink">
													<div class="ratio ratio-16X9">
														<img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="" class="img-fluid">
													</div>
												</a>
												<div class="px-2 position-relative">
													<a href="{{ route('bookappointment')}}" role="button">
														<div class="align-items-center bg-white d-flex m-0 btn-service">
															<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
															<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
														</div>
													</a>
													<a href="{{ route('car_interior_cleaning') }}" class="plegemobilelink">
													<h5 class="font-weight-bold h4 pt-3 m-0">Auto-Innenreinigung</h5>
													<p class="h6 py-2 pb-3">Bei einer Auto-Innenreinigung arbeiten wir mit einem neutralen, ökologischen...
														<span class="font-weight-bolder">Weiterlesen</span>
													</p>
													</a>
												</div>
											</div>
										</div>
										<div class="item pb-4">
											<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_b_2" aria-expanded="false" aria-controls="collapse1_b_2" role="button">
												<a href="{{ route('carcare_swissvax') }}" class="plegemobilelink">
													<div class="ratio ratio-16X9">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
													</div>
												</a>
														
												<div class="px-2 position-relative">
													<a href="{{ route('bookappointment')}}" role="button">
														<div class="align-items-center bg-white d-flex m-0 btn-service">
															<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
															<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
														</div>
													</a>
													<a href="{{ route('carcare_swissvax') }}" class="plegemobilelink">
													<h5 class="font-weight-bold h4 pt-3 m-0">Autopflege mit Swissvax</h5>
													<p class="h6 py-2 pb-3">Als offizielles Swissvax Automobil Detailing Center für Liechtenstein und das...
														<span class="font-weight-bolder">Weiterlesen</span>
													</p>
													</a>
												</div>
											</div>
										</div>
										<div class="item pb-4">
											<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_b_3" aria-expanded="false" aria-controls="collapse1_b_3" role="button">
												<a href="{{ route('car_interior_processing_special') }}" class="plegemobilelink">
													<div class="ratio ratio-16X9">	
														<img src="{{ asset('assets/frontend/images/special-1.jpg') }}" alt="" class="img-fluid">
													</div>
												</a>
												<div class="px-2 position-relative">
													<a href="{{ route('bookappointment')}}" role="button">
														<div class="align-items-center bg-white d-flex m-0 btn-service">
															<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
															<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
														</div>
													</a>
													<a href="{{ route('car_interior_processing_special') }}" class="plegemobilelink">
													<h5 class="font-weight-bold h4 pt-3 m-0">Spezialfälle</h5>
													<p class="h6 py-2 pb-3">Bei "speziellen Fällen" wie diesen können wir keine Fixpreise anbieten, weil...
														<span class="font-weight-bolder">Weiterlesen</span>
													</p>
													</a>
												</div>
											</div>
										</div>
										<div class="item pb-4">
											<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse1_b_4" aria-expanded="false" aria-controls="collapse1_b_4" role="button">
												<a href="{{ route('leather_care_restoration') }}" class="plegemobilelink">
													<div class="ratio ratio-16X9">	
														<img src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="" class="img-fluid">
													</div>
												</a>
												<div class="px-2 position-relative">
													<a href="{{ route('bookappointment')}}" role="button">
														<div class="align-items-center bg-white d-flex m-0 btn-service">
															<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
															<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
														</div>
													</a>
													<a href="{{ route('leather_care_restoration') }}" class="plegemobilelink">
														<h5 class="font-weight-bold h4 pt-3 m-0">Lederpflege & Restauration</h5>
														<p class="h6 py-2 pb-3">Leder benötigt besondere Pflege. Das gilt besonders für stark beanspruchte...
															<span class="font-weight-bolder">Weiterlesen</span>
														</p>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="pills-protect" role="tabpanel">
								<div class="px-2">
									<div class="care-for-carousel owl-carousel owl-theme">
										<div class="item pb-4">
											<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_a_1" aria-expanded="false" aria-controls="collapse2_a_1" role="button">
												<a href="{{ route('stone_impact_protection_film') }}" class="schutzmobilelink">
													<div class="ratio ratio-16X9">
														<img src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="" class="img-fluid">
													</div>
												</a>
												<div class="px-2 position-relative">
													<a href="{{ route('bookappointment')}}" role="button">
														<div class="align-items-center bg-white d-flex m-0 btn-service">
															<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
															<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
														</div>
													</a>
													<a href="{{ route('stone_impact_protection_film') }}" class="schutzmobilelink">
													<h5 class="font-weight-bold h4 pt-3 m-0">Steinschlagschutzfolie</h5>
													<p class="h6 py-2 pb-3">Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschlag...
														<span class="font-weight-bolder">Weiterlesen</span>
													</p>
													</a>
												</div>
											</div>
										</div>
										<div class="item pb-4">
											<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_a_2" aria-expanded="false" aria-controls="collapse2_a_2" role="button">
												<a href="{{ route('car_window_tinting') }}" class="schutzmobilelink">
												<div class="ratio ratio-16X9">
													<img src="{{ asset('assets/frontend/images/tint-10.jpg') }}" alt="" class="img-fluid">
												</div>
												</a>
												<div class="px-2 position-relative">
													<a href="{{ route('bookappointment')}}" role="button">
														<div class="align-items-center bg-white d-flex m-0 btn-service">
															<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
															<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
														</div>
													</a>
													<a href="{{ route('car_window_tinting') }}" class="schutzmobilelink">
													<h5 class="font-weight-bold h4 pt-3 m-0">Autoscheibentönung</h5>
													<p class="h6 py-2 pb-3">Eine Autoscheibentönung sieht nicht nur gut aus, sie schützt auch in mehrfacher...
														<span class="font-weight-bolder">Weiterlesen</span>
													</p>
													</a>
												</div>
											</div>
										</div>
										<div class="item pb-4">
											<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse2_b_1" aria-expanded="false" aria-controls="collapse2_b_1" role="button">
												<a href="{{ route('ceramic_coating') }}" class="schutzmobilelink">
													<div class="ratio ratio-16X9">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
													</div>
												</a>
												<div class="px-2 position-relative">
													<a href="{{ route('bookappointment')}}" role="button">
														<div class="align-items-center bg-white d-flex m-0 btn-service">
															<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
															<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
														</div>
													</a>
													<a href="{{ route('ceramic_coating') }}" class="schutzmobilelink">
													<h5 class="font-weight-bold h4 pt-3 m-0">Keramikbeschichtung</h5>
													<p class="h6 py-2 pb-3">Sie fahren Ihr Auto täglich und es soll dabei mit möglichst wenig Aufwand...
														<span class="font-weight-bolder">Weiterlesen</span>
													</p>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="pills-enhance" role="tabpanel">
								<div id="DesignMainParent">
									<div class="px-2">
										<div class="care-for-carousel owl-carousel owl-theme">
											<div class="item pb-4">
												<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_a_1" aria-expanded="false" aria-controls="collapse3_a_1" role="button">
													<a href="{{ route('car_wrapping') }}" class="designmobilelink" >
														<div class="ratio ratio-16X9">
															<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
														</div>
													</a>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<a href="{{ route('car_wrapping') }}" class="designmobilelink" >
														<h5 class="font-weight-bold h4 pt-3 m-0">Autofolierung / Car Wrapping</h5>
														<p class="h6 py-2 pb-3">Verschönern Sie Ihr Auto, Ihr Motorrad oder Ihren Lastwagen mit einer...
															<span class="font-weight-bolder">Weiterlesen</span>
														</p>
														</a>
													</div>
												</div>
											</div>
											<div class="item pb-4">
												<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_a_2" aria-expanded="false" aria-controls="collapse3_a_2" role="button">
													<a href="{{ route('chrome_plating') }}" class="designmobilelink">
														<div class="ratio ratio-16X9">
															<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
														</div>
													</a>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<a href="{{ route('chrome_plating') }}" class="designmobilelink">
														<h5 class="font-weight-bold h4 pt-3 m-0">Verchromungen</h5>
														<p class="h6 py-2 pb-3">Mehr Glanz für Ihr Auto, Ihr Motorrad oder Ihren Lastwagen! Gerne verchromen...
															<span class="font-weight-bolder">Weiterlesen</span>
														</p>
														</a>
													</div>
												</div>
											</div>
											<div class="item pb-4">
												<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_b_1" aria-expanded="false" aria-controls="collapse3_b_1" role="button">
													<a href="{{ route('interior_design') }}" class="designmobilelink">
														<div class="ratio ratio-16X9">
															<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
														</div>
													</a>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<a href="{{ route('interior_design') }}" class="designmobilelink">
														<h5 class="font-weight-bold h4 pt-3 m-0">Interieur-Design</h5>
														<p class="h6 py-2 pb-3">Fahrzeugveredelungen müssen sich nicht auf das Äussere beschränken. Wir bieten...
															<span class="font-weight-bolder">Weiterlesen</span>
														</p>
														</a>
													</div>
												</div>
											</div>
											<div class="item pb-4">
												<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_b_1" aria-expanded="false" aria-controls="collapse3_b_1" role="button">
													<a href="{{ route('vehicle_lettering') }}" class="designmobilelink">
														<div class="ratio ratio-16X9">
															<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
														</div>
													</a>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<a href="{{ route('vehicle_lettering') }}" class="designmobilelink">
														<h5 class="font-weight-bold h4 pt-3 m-0">Fahrzeugbeschriftung</h5>
														<p class="h6 py-2 pb-3">Wer nicht wirbt, stirbt – so der Volksmund. Und es steckt durchaus Wahrheit in...
															<span class="font-weight-bolder">Weiterlesen</span>
														</p>
														</a>
													</div>
												</div>
											</div>
											<div class="item pb-4">
												<div class="bg-white shadow-sm" data-toggle="collapse" data-target="#collapse3_b_2" aria-expanded="false" aria-controls="collapse3_b_2" role="button">
													<a href="{{ route('water_transfer_printing') }}" class="designmobilelink">
														<div class="ratio ratio-16X9">
															<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
														</div>
													</a>
													<div class="px-2 position-relative">
														<a href="{{ route('bookappointment')}}" role="button">
															<div class="align-items-center bg-white d-flex m-0 btn-service">
																<img src="{{ asset('assets/frontend/images/read-more.png') }}" alt="autokosmetik">
																<p class="text-dark btn-service-text pl-2 m-0">Termin buchen</p>
															</div>
														</a>
														<a href="{{ route('water_transfer_printing') }}" class="designmobilelink">
														<h5 class="font-weight-bold h4 pt-3 m-0">Wassertransferdruck</h5>
														<p class="h6 py-2 pb-3">Wassertransferdruck ist ein relativ modernes Verfahren, das es möglich macht,...
															<span class="font-weight-bolder">Weiterlesen</span>
														</p>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div id="section-services2" class="section-service-text-color">
				<div class="container">
					<div class="row align-items-center">
						<div class="left col-sm-12 col-md-6">
							<h2 class="h1 text-uppercase text-dark">Pflege</h2>
							<p>Autos, Motorräder und LKWs benötigen professionelle Pflege. Als spezialisiertes Unternehmen in diesem Bereich sorgen wir für die perfekte Pflege für Ihr Fahrzeug. Dabei kümmern wir uns sowohl um die Innen- als auch die Aussenaufbereitung.</p>
							<p>Für den Innenraum bieten wir die herkömmliche Innenreinigung inklusive Shampoonieren, Lederpflege und Reinigung mit Tornador, sowie die Pflege mit der Swissvax-Methode.</p>
							<p>Für die Aussenhülle empfehlen wir für die Pflege eine Teflonversiegelung.</p>
						</div>
						<div class="right ez-animate col-sm-12 col-md-6" data-animation="fadeInRight">
							<img class="img-fluid w-75" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
						</div>
					</div>
				</div>
			</div>
			<div class="section-services2 bg-2 bg-dark-autokosmetic">
				<div class="container">
					<div class="row align-items-center">
						<div class="right ez-animate col-sm-12 col-md-6 text-center" data-animation="fadeInLeft">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/window-filming.png') }}" alt="autokosmetik">
						</div>
						<div class="left col-sm-12 col-md-6">
							<h2 class="h1 text-uppercase">Schutz</h2>
							<p class="text-light">Besonderer Schutz für Ihr Verkehrsmittel. Eine volltransparente Lackschutzfolie schützt den darunter liegenden Lack vor Steinschlag und Kratzern. Doch auch eine Keramikbeschichtung verspricht einen hohen Schutz für den Lack. Für die Scheiben empfehlen wir eine Scheibentönung. Die entsprechenden Folien schützen ihre Kinder und Haustiere vor UV-Strahlung und sind ein Blickfang.</p>
							<!-- <p class="m-0 mb-3 text-light">INNENAUFBEREITUNG HERKÖMMLICH</p>
							<ul class="text-light">
								<li>inkl. shampoonieren</li>
								<li>Lederreinigung und -pflege</li>
								<li>Reinigung mit Tornador</li>
							</ul> -->
						</div>
					</div>
				</div>
			</div>
			<div class="section-services2 section-service-text-color">
				<div class="container">
					<div class="row align-items-center">
						<div class="left col-sm-12 col-md-6">
							<h2 class="h1 text-uppercase text-dark">Design</h2>
							<p>Eine Teil- oder Vollfolierung sorgt für ein komplett verändertes Äusseres – ganz ohne eine teure Neulackierung. Der Fantasie sind dabei nur wenige Grenzen gesetzt. Neben Matt- und Hochglanzfolien sowie Strukturfolien ist auch eine Carbon-Optik in zahlreichen aufregenden Farben möglich. Häufig nachgefragt werden auch Werbefolien. Neben dem einzigartigen Look für Ihr Fahrzeug, schützen die Folien auch den darunter liegenden Lack.</p>
							<p>Verchromungen sind ein ganz besonderer Hingucker und machen Ihr Fahrzeug einzigartig.</p>
							<p>Apropos einzigartig: Wie wäre es mit einem Interieur-Design ganz nach Ihrem Geschmack? </p>
							<!-- <p class="mb-3">Besonders geeignet für:</p>
							<ul>
								<li>Individualisten</li>
								<li>Showfahrzeuge, die besonders auffallen wollen</li>
								<li>Fahrzeuge, welche für eine</li>
							</ul> -->
						</div>
						<div class="right ez-animate col-sm-12 col-md-6" data-animation="fadeInRight">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/img-services3.png') }}" alt="autokosmetik">
						</div>
					</div>
				</div>
			</div>
			<div id="section-howwework">
				<div class="container">
					<div class="row">
						<div class="col-12 text-center">
							<i class="flaticon-multimedia"></i>
							<h1>How we <strong>Work</strong></h1>
						</div>
					</div>
				</div>
			</div>
			<!-- <div id="section-cta3">
				<div class="container">
					<div class="row ez-animate">
						<div class="title1 col-12" data-aos="fade-up" data-aos-delay="200">
							<h2><span>Immer auf der Suche nach mehr</span> Einzigartige Talente</h2>
							<i class="flaticon-download"></i>
						</div>
						<div class="cta-content col-12" data-aos="fade-up" data-aos-delay="400">
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi sequi tempora totam voluptatem natus maiores consectetur illo nobis quas fugiat commodi, aut ratione quasi blanditiis labore in doloremque quam? Adipisci! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Id ex ratione nesciunt velit! Unde repellendus, ipsam aliquid beatae nihil, aut tempora excepturi blanditiis corrupti libero odio quaerat dolor, optio itaque?</p>
							<a href="{{ route('contact')}}" class="btn-4">In Kontakt kommen</a>
						</div>
					</div>
				</div>
			</div> -->
		</div>
	</div>

@endsection

@section('script')
<script>
	$(document).ready(function () {
		if (localStorage.getItem("targetClass") === "lederpflegeRestauration" || localStorage.getItem("targetClass") === "aussenaufbereitung_a" || localStorage.getItem("targetClass") === "innenreinigung" || localStorage.getItem("targetClass") === "swissvaxFahrzeugaufbereitung" || localStorage.getItem("targetClass") === "lederpflege" || localStorage.getItem("targetClass") === "lackpflege" || localStorage.getItem("targetClass") === "metallpflege") {

			$('.Pflege').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			$('#collapse1').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			
			if(localStorage.getItem("targetClass") === "lederpflegeRestauration"){
				$('.Innenaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_b').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_b_1').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			}
			else if (localStorage.getItem("targetClass") === "aussenaufbereitung_a") { 
				$('.aussenaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a_1').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			}
			else if (localStorage.getItem("targetClass") === "innenreinigung") { 
				$('.innenreinigung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_b').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_b_2').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel-Innenaufbereitung').trigger('to.owl.carousel', 1)
			}
			else if (localStorage.getItem("targetClass") === "swissvaxFahrzeugaufbereitung") { 
				$('.aussenaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#swissvaxFahrzeugaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a_2').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel').trigger('to.owl.carousel', 1)
			}
			else if (localStorage.getItem("targetClass") === "lederpflege") { 
				$('.Innenaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_b').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.lederpflege').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_b_3').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel-Innenaufbereitung').trigger('to.owl.carousel', 2)
			}
			else if (localStorage.getItem("targetClass") === "lackpflege") { 
				$('.aussenaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.lackpflege').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a_3').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel').trigger('to.owl.carousel', 2)
			}
			else if (localStorage.getItem("targetClass") === "metallpflege") { 
				$('.aussenaufbereitung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.metallpflege').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse1_a_4').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel').trigger('to.owl.carousel', 3)
			}
			localStorage.clear();
		}
		else if (localStorage.getItem("targetClass") === "scheibentönung" || localStorage.getItem("targetClass") === "keramikbeschichtung" || localStorage.getItem("targetClass") === "steinschlagschutzfolien" || localStorage.getItem("targetClass") === "lackschutz") {
			
			$('.Schutz').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			$('#collapse2').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			
			if(localStorage.getItem("targetClass") === "scheibentönung"){
				$('.Folien').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_a_2').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			}
			else if(localStorage.getItem("targetClass") === "keramikbeschichtung"){
				$('.beschichtung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_b').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_b_1').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			}
			else if(localStorage.getItem("targetClass") === "steinschlagschutzfolien"){
				$('.Folien').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_a_1').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel-folien').trigger('to.owl.carousel', 1)
			}
			else if(localStorage.getItem("targetClass") === "lackschutz"){
				$('.Folien').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse2_a_1').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('.care-for-carousel-folien').trigger('to.owl.carousel', 2)
			}
			localStorage.clear();
		}

		else if (localStorage.getItem("targetClass") === "fahrzeugbeschriftung") {
			
			$('.design').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			$('#collapse3').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			
			if(localStorage.getItem("targetClass") === "fahrzeugbeschriftung"){
				$('.komplettfolierung').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse3_a').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
				$('#collapse3_a_2').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
			}
			localStorage.clear();
		}


		
		
	});
	
</script>

@endsection