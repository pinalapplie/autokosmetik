<?php

namespace App\Http\Controllers;

use App\Http\Model\DriveVehicle;
use App\Http\Model\VehicleBrands;
use App\Http\Model\VehicleCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;

class DriveVehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $drive_vehicles = DriveVehicle::latest()->get();
        return view('backend.driver_vehicle.index', compact('drive_vehicles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicle_brands = VehicleBrands::get();
        $vehicle_category = VehicleCategory::get();
        return view('backend.driver_vehicle.create',compact('vehicle_brands','vehicle_category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                $drive_vehicles = DriveVehicle::where('id',$id)->first();
                if (empty($drive_vehicles->is_active)) {
                    $drive_vehicles->is_active = 1;
                } else {
                    $drive_vehicles->is_active = 0;
                }
                    $drive_vehicles->save();
                return response()->json(['success' => 1]);
            }else{
                $this->validate($request, [
                    'name' => 'required',
                ]);
                if(!empty($request->id)){
                    $id = Crypt::decrypt($request->id);
                    $drive_vehicles = DriveVehicle::where('id',$id)->first();
                    $message = trans('message.update', ['entity' => trans('cruds.drivevehicles.title_singular')]);
                }else{
                    $drive_vehicles = new DriveVehicle;
                    $message = trans('message.create', ['entity' => trans('cruds.drivevehicles.title_singular')]);
                }
                    $drive_vehicles->name = $request->name;
                    $drive_vehicles->model = $request->model;
                    $drive_vehicles->price = $request->price;
                    $drive_vehicles->vehicle_categorie_id = $request->vehicle_categorie_id;
                    $drive_vehicles->vehicle_brand_id = $request->vehicle_brand_id;
                    $drive_vehicles->number_of_window = $request->number_of_window;
                    $drive_vehicles->save();
                    return redirect()->route('drivevehicles.index')->with('success',$message);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
            return redirect()->back()->with('danger', trans('global.something_wrong'));    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DriveVehicle  $driveVehicle
     * @return \Illuminate\Http\Response
     */
    public function show(DriveVehicle $driveVehicle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DriveVehicle  $driveVehicle
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $drivevehicles = DriveVehicle::where('id',$id)->first();
        //$vehicle_brands = VehicleBrands::get();
        $vehicle_brands = VehicleBrands::get();
        $vehicle_category = VehicleCategory::get();
        return view('backend.driver_vehicle.edit', compact('drivevehicles','vehicle_brands','vehicle_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DriveVehicle  $driveVehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DriveVehicle $driveVehicle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DriveVehicle  $driveVehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                DriveVehicle::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }
}
