<!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="utf-8" />
		
		@include('frontend.SEO')

		<meta name="description" content="@yield('description_meta')"/>
		<!-- Site Author -->
		<meta name="author" content="Autokosmetik.li" />
		 <!-- Page Title Here -->
		<title>@yield('title_meta')</title>
		
		<link rel="icon" href="{{ asset('assets/frontend/images/favicon.png') }}" type="image/x-icon" >

		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!-- Bootstrap 4 -->
		<link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap.min.css') }}" type="text/css">
		<!-- Swiper Slider -->
		<link rel="stylesheet" href="{{ asset('assets/frontend/css/swiper.min.css') }}" type="text/css">
		<!-- Fonts -->
		<link rel="stylesheet" href="{{ asset('assets/frontend/fonts/flaticon/flaticon.css') }}" type="text/css">
		<link rel="stylesheet" href="{{ asset('assets/frontend/fonts/fontawesome/font-awesome.min.css') }}" type="text/css">
		<!-- OWL Carousel -->
		<link rel="stylesheet" href="{{ asset('assets/frontend/css/owl.carousel.min.css') }}" type="text/css">
		<link rel="stylesheet" href="{{ asset('assets/frontend/css/owl.theme.default.min.css') }}" type="text/css">
		
		<link rel="stylesheet" href="{{ asset('assets/frontend/css/style.css') }}" type="text/css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
		<!-- REVOLUTION SLIDER -->
		<link href="http://fonts.googleapis.com/css?family=Quicksand:400%2C700%7COpen+Sans:400" rel="stylesheet" property="stylesheet" type="text/css" media="all">
		<link rel="stylesheet" href="{{ asset('assets/frontend/revslider2/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" type="text/css">
		<link rel="stylesheet" href="{{ asset('assets/frontend/revslider2/fonts/font-awesome/css/font-awesome.css') }}" type="text/css">
		<link rel="stylesheet" href="{{ asset('assets/frontend/revslider2/css/settings.css') }}" type="text/css">
		<link rel="stylesheet" href="{{ asset('assets/frontend/revslider2/css/inline-revslider2.css') }}" type="text/css">
		<link rel="stylesheet" href="{{ asset('assets/frontend/revslider2/css/revolution.addon.particles.css') }}" type="text/css" media="all" />
		<link rel="stylesheet" href="{{ asset('assets/frontend/revslider2/css/revolution.addon.polyfold.css') }}" type="text/css"  media="all" />
		<link rel="stylesheet" href="https://cdn.rawgit.com/michalsnik/aos/2.0.4/dist/aos.css" type="text/css">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

		<link rel="stylesheet" href="{{ asset('assets/frontend/css/custom.css') }}">
	</head>
	<body>
		<!-- Header.php -->
		<!-- Section Preloader -->
		<div id="section-preloader">
			<img src="{{ asset('assets/frontend/images/loader-wheel.gif') }}" alt="loader">
		</div>
		<!-- /.Section Preloader -->
		
		<div id="section-navbar1">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-md-11">
						<nav class="navbar navbar-expand-lg">
							<div class="header-logo mr-auto">
								<a href="{{ route('index')}}"><img src="{{ asset('assets/frontend/images/logo.svg') }}" alt="autokosmetik"></a>
							</div>
							<ul class="nav navbar-nav navbar1">
								<li class="nav-item">
									<div class="header-contact"><i class="fa fa-phone text-light"></i><span><a href="callto:+423 373 03 54">&nbsp;&nbsp;+423 373 03 54</a></span></div>
								</li>
								<li class="nav-item">
									<a class="nav-link btn btn-secondary" href="{{ route('bookappointment')}}">Termin buchen</a>
								</li>
								<li class="nav-item">
									<button type="button" id="sidebarCollapse" class="navbar-btn active">
										<span></span>
										<span></span>
										<span></span>
										<span></span>
									</button>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<!-- /.Section Navbar -->
		<nav id="sidebar" class="active">
			<ul class="list-unstyled components">
				<li class="{{ request()->is('Zuhause') ? 'active' : '' }}">
					<a href="{{ route('index')}}" aria-expanded="false" class="list-menu">Home</a>
				</li>
				<li class="{{ request()->is('Über-uns') ? 'active' : '' }}">
					<a href="{{ route('about.us')}}" class="list-menu">Über uns</a>
				</li>
				<li class="{{ request()->is('Dienstleistungen') ? 'active' : '' }}">
					<a href="{{ route('services')}}" class="list-menu">Dienstleistungen</a>
				</li>
				<li class="{{ request()->is('Nachrichten') ? 'active' : '' }}">
					<a href="{{ route('news')}}" class="list-menu">News</a>
				</li>
				<li class="{{ request()->is('portfolio') ? 'active' : '' }}">
					<a href="{{ route('portfolio')}}" class="list-menu">Projekte</a>
				</li>
				<li class="{{ request()->is('Kontakt') ? 'active' : '' }}">
					<a href="{{ route('contact')}}" class="list-menu">Kontakt</a>
				</li>
			</ul>
		</nav>
		<section class="main-content">
			@yield('content')
		</section>
		<footer>
			<div id="section-footer">
				<div class="footer1">
					<div class="pt-5 text-center">
						<img src="{{ asset('assets/frontend/images/footer-sign.png') }}" alt="autokosmetik" class="img-fluid px-3">						
					</div>
					<div class="footer-logo mt-5">
						<img src="{{ asset('assets/frontend/images/footer-logo.png') }}" alt="" class="img-fluid w-100">
					</div>
					<div class="py-4 text-center">
						<ul class="font-weight-bold h4 list-inline m-0">
							<li class="list-inline-item m-0">
							<div class="btn-group dropup">
								<a class="footer-dropdown" href="javascript:void(0);" role="button" id="Pflege" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pflege</a>								
								<div class="dropdown-menu" aria-labelledby="Pflege">
									<a id="leatherCareRestoration" class="dropdown-item" href="{{ route('car_interior_cleaning')}}">Auto-Innenreinigung</a>
									<a id="aussenaufbereitung" class="dropdown-item" href="{{ route('carcare_swissvax')}}">Innenaufbereitung mit Swissvax</a>
									<a id="innenreinigung" class="dropdown-item" href="{{ route('car_interior_processing_special')}}">Innenaufbereitung: Spezialfälle</a>
									<a id="swissvaxFahrzeugaufbereitung" class="dropdown-item" href="{{ route('sealing')}}">Aussenaufbereitung: Teflonversiegelung</a>
									<a id="lederpflege" class="dropdown-item" href="{{ route('leather_care_restoration')}}">Lederpflege & Restauration</a>
									<!-- <a id="lackpflege" class="dropdown-item" href="{{ route('services')}}">lackpflege</a>
									<a id="metallpflege" class="dropdown-item" href="{{ route('services')}}">metallpflege</a> -->
								</div>
							</div> | </li>
							<li class="list-inline-item m-0">
								<div class="btn-group dropup">
									<a class="footer-dropdown" href="#" role="button" id="Schutz" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Schutz</a>								
									<div class="dropdown-menu" aria-labelledby="Schutz">
										<a id="keramikbeschichtung" class="dropdown-item" href="{{ route('ceramic_coating')}}">Keramikbeschichtung</a>
										<a id="steinschlagschutzfolien" class="dropdown-item" href="{{ route('stone_impact_protection_film')}}">Steinschlagschutzfolie</a>
										<a id="lackschutz" class="dropdown-item" href="{{ route('car_window_tinting')}}">Scheibentönung</a>
									</div>
								</div> |
							</li> 
							<li class="list-inline-item m-0">
								<div class="btn-group dropup">
									<a class="footer-dropdown" href="#" role="button" id="Design" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Design</a>								
									<div class="dropdown-menu" aria-labelledby="Design">
										<a id="fahrzeugbeschriftung" class="dropdown-item" href="{{ route('car_wrapping')}}">Autofolierung / Car Wrapping </a>
										<a id="" class="dropdown-item" href="{{ route('vehicle_lettering')}}">Fahrzeugbeschriftung</a>
										<a id="" class="dropdown-item" href="{{ route('chrome_plating')}}">Verchromungen</a>
										<a id="" class="dropdown-item" href="{{ route('water_transfer_printing')}}">Wassertransferdruck</a>
										<a id="" class="dropdown-item" href="{{ route('interior_design')}}">Interieur-Design</a>
										<!-- <a id="" class="dropdown-item" href="{{ route('services')}}">voll-folierung</a>
										<a id="" class="dropdown-item" href="{{ route('services')}}">beschriftung</a>
										<a id="" class="dropdown-item" href="{{ route('services')}}">teilfolierung</a>
										<a id="" class="dropdown-item" href="{{ route('services')}}">lederlackierungen</a>
										<a id="" class="dropdown-item" href="{{ route('services')}}">beschriftungen</a> -->
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div id="section-footer" class="sec-footer-bg">
				<div class="container">
					<div class="footer2 row py-2">
						<div class="ft-copyright col-md-6">
							<p>Urheberrechte © <span id="year"></span> <a href="https://www.applie.li/" target="_blank" rel="noopener noreferrer">Applie AG.</a><br class="d-block d-sm-none">
							Alle Rechte vorbehalten.</p>
						</div>
						<div class="ft-socialmedia col-md-6">
							<div class="social-links socials-header">
								<a href="https://www.facebook.com/AUTOKOSMETIK/"><i class="fa fa-facebook fa-lg"></i></a>
								<a href="https://www.instagram.com/autokosmetik.li_nitzlnader/"><i class="fa fa-instagram fa-lg"></i></a>
								<a href="https://www.linkedin.com/company/autokosmetik-nitzlnader/about/"><i class="fa fa-linkedin fa-lg"></i></a>
								<a href="https://www.google.ch/maps/place/Autokosmetik.li/@47.218252,9.511339,15z/data=!4m5!3m4!1s0x0:0x3025938a37e377f0!8m2!3d47.218252!4d9.511339"><i class="fa fa-map-marker fa-lg"></i></a>
								<a href="mailto:info@autokosmetik.li"><i class="fa fa-envelope fa-lg"></i></a>
								<a href="callto:+4233730354"><i class="fa fa-phone fa-lg"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- <div class="car-top car-down">
			<span><img src="{{ asset('assets/frontend/images/car.png') }}" alt=""></span>
			<i class="fa fa-arrow-up"></i>
		</div> -->


		<script>
			new Date().getFullYear()
			document.getElementById("year").innerHTML = new Date().getFullYear();
		</script>
		<script src="{{ asset('assets/frontend/js/popper.min.js') }}"></script>
		<script src="{{ asset('assets/frontend/js/jquery.min.js') }}"></script>
		<script src="{{ asset('assets/frontend/js/bootstrap.min.js') }}"></script>

		<script src="{{ asset('assets/frontend/js/owl.carousel.min.js') }}"></script>
		<script src="https://huynhhuynh.github.io/owlcarousel2-filter/dist/owlcarousel2-filter.min.js"></script>
		<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>


		<!-- SLY Carousel -->
		<script src="{{ asset('assets/frontend/js/sly.min.js') }}"></script>
		<script src="{{ asset('assets/frontend/js/sly.vendor.min.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.14/swiper-bundle.min.js"></script>
		<script src="{{ asset('assets/frontend/js/scripts.js') }}"></script>
		<script src="{{ asset('assets/frontend/js/custom-2.js') }}"></script>
		<script src="{{ asset('assets/frontend/js/sly-ourportfolio.js') }}"></script>
		<script src="{{ asset('assets/frontend/revslider2/js/revolution.addon.particles.min.js') }}"></script>
		<script src="{{ asset('assets/frontend/revslider2/js/revolution.addon.snow.min.js') }}"></script>
		<script src="{{ asset('assets/frontend/revslider2/js/revolution.addon.polyfold.min.js') }}"></script>
		<!-- REVOLUTION JS FILES -->
		<script src="{{ asset('assets/frontend/revslider2/js/jquery.themepunch.tools.min.js') }}"></script>
		<script src="{{ asset('assets/frontend/revslider2/js/jquery.themepunch.revolution.min.js') }}"></script>
		<script src="{{ asset('assets/frontend/revslider1/js/inline-revslider1.js') }}"></script>
		<script src="https://cdn.jsdelivr.net/npm/simple-parallax-js@5.2.0/dist/simpleParallax.min.js"></script>
		<script src="https://cdn.rawgit.com/michalsnik/aos/2.0.4/dist/aos.js"></script>
		<script src="{{ asset('assets/frontend/js/jquery.bootstrap.wizard.min.js') }}"></script>

		<script>
			/*----------------------------------------
				LOCAL STORAGE FOR SERVICE PAGE
			-----------------------------------------*/
			if (typeof(Storage) !== "undefined") {
				/* PFLEGE CATEGORY START */
				$('#leatherCareRestoration').click(function () {
					localStorage.setItem('parentClass', 'Pflege');
					localStorage.setItem('SubClass', 'Innenaufbereitung');
					localStorage.setItem('targetClass', 'lederpflegeRestauration');
				});
				$('#aussenaufbereitung').click(function () {
					localStorage.setItem('parentClass', 'Pflege');
					localStorage.setItem('SubClass', 'aussenaufbereitung');
					localStorage.setItem('targetClass', 'aussenaufbereitung_a');
				});
				$('#innenreinigung').click(function () {
					localStorage.setItem('parentClass', 'Pflege');
					localStorage.setItem('SubClass', 'Innenaufbereitung');
					localStorage.setItem('targetClass', 'innenreinigung');
				});
				$('#swissvaxFahrzeugaufbereitung').click(function () {
					localStorage.setItem('parentClass', 'Pflege');
					localStorage.setItem('SubClass', 'aussenaufbereitung');
					localStorage.setItem('targetClass', 'swissvaxFahrzeugaufbereitung');
				});
				$('#lederpflege').click(function () {
					localStorage.setItem('parentClass', 'Pflege');
					localStorage.setItem('SubClass', 'Innenaufbereitung');
					localStorage.setItem('targetClass', 'lederpflege');
				});
				$('#lackpflege').click(function () {
					localStorage.setItem('parentClass', 'Pflege');
					localStorage.setItem('SubClass', 'Innenaufbereitung');
					localStorage.setItem('targetClass', 'lackpflege');
				});
				$('#metallpflege').click(function () {
					localStorage.setItem('parentClass', 'Pflege');
					localStorage.setItem('SubClass', 'Innenaufbereitung');
					localStorage.setItem('targetClass', 'metallpflege');
				});

				/* SCHUTZ CATEGORY START */
				$('#scheibentönung').click(function () {
					localStorage.setItem('parentClass', 'Schutz');
					localStorage.setItem('SubClass', 'Folien');
					localStorage.setItem('targetClass', 'scheibentönung');
				});
				$('#keramikbeschichtung').click(function () {
					localStorage.setItem('parentClass', 'Schutz');
					localStorage.setItem('SubClass', 'beschichtung');
					localStorage.setItem('targetClass', 'keramikbeschichtung');
				});
				$('#steinschlagschutzfolien').click(function () {
					localStorage.setItem('parentClass', 'Schutz');
					localStorage.setItem('SubClass', 'Folien');
					localStorage.setItem('targetClass', 'steinschlagschutzfolien');
				});
				$('#lackschutz').click(function () {
					localStorage.setItem('parentClass', 'Schutz');
					localStorage.setItem('SubClass', 'Folien');
					localStorage.setItem('targetClass', 'lackschutz');
				});
				/* DESIGN CATEGORY START */
				$('#fahrzeugbeschriftung').click(function () {
					localStorage.setItem('parentClass', 'design');
					localStorage.setItem('SubClass', 'Komplettfolierung');
					localStorage.setItem('targetClass', 'fahrzeugbeschriftung');
				});
				
			} else {
				$(document).text('No web storage Support.');
			}


			AOS.init({
				duration: 1000,
			});

			$('.book-appointment-slider').owlCarousel({
				loop:true,
				margin:10,
				nav:false,
				items:1,
				autoplay:true,
				dots: false,
				animateOut: 'animate__fadeOutUp',
    			animateIn: 'animate__fadeInUp',
			})
   			$(document).ready(function() {

				$('.button').click(function(){

					var buttonId = $(this).attr('id');
					$('#modal-container').removeAttr('class').addClass(buttonId);
					$('body').addClass('modal-active');
				})
				$('#modal-container').click(function(){
					$(this).addClass('out');
					$('body').removeClass('modal-active');
				});

				$('.count').each(function () {
					$(this).prop('Counter',0).animate({
						Counter: $(this).text()
					}, {
						duration: 4000,
						easing: 'swing',
						step: function (now) {
							$(this).text(Math.ceil(now));
						}
					});
				});

				$(".tooltip-main").each(function () {
					$(this).mouseover(function (e) {

						e.preventDefault();
						// $(".modal_custom").removeClass("open");
						$(this).find(".modal_custom").addClass("open");
					});
					$(this).mouseout(function (e) {
						e.preventDefault();
						$(".modal_custom").removeClass("open");
					});
					$(".close").hover(function (event) {
						event.stopPropagation();
						$(this).parents(".modal_custom").removeClass("open");
					});
				});
			});
			const navSlide = () => {
			const burger = document.querySelector('.navbar-btn');
			const nav = document.querySelector('.list-unstyled');
			const navLink = document.querySelectorAll('.list-unstyled li')

				burger.addEventListener('click', () => {

					nav.classList.toggle('nav-active');
					
					navLink.forEach((link, index) => {
						if (link.style.animation) {
							link.style.animation = ''
						} else {
							link.style.animation = `navLinkFade 2s ease forwards ${index / 7 + 0.2}s`;
						}
					});

					burger.classList.toggle('toggle');
				})

			}

			navSlide();
			$(document).ready(function(){
				$(".car-bike-1").show();
				$(".car-bike-2").hide();
				$(".car-bike-small-1").hide();
				$(".car-bike-small-2").show();

				$('.feature-new-bike-area').hide();

				$(".car-bike-small-1").click(function(){
					$(".car-bike-1").show();
					$(".car-bike-2").hide();
					$(".car-bike-small-1").hide();
					$(".car-bike-small-2").show();
				});
				$(".car-bike-small-2").click(function(){
					$(".car-bike-2").show();
					$(".car-bike-1").hide();
					$(".car-bike-small-1").show();
					$(".car-bike-small-2").hide();
				});
			});

			$(function () {
				$('[data-toggle="popover"]').popover();

				/* if (typeof(Storage) !== "undefined") {
					$('#collapse1_a').click(function () {
						localStorage.setItem('clickEventName', 'collapse1_a');
					});
				} else {
					$(document).text('No web storage Support.');
				} */
			})
			/* $(".service-wrap").each(function(){
				$(this).click(function(e){
					$(".service-wrap").removeClass("active");
					$(this).addClass("active");
				});
			}); */

			$(".car-care-icon").each(function(){
				$(this).click(function(e){
					$(".car-care-icon").removeClass("active");
					$(this).addClass("active");
				});
			});


			/* $(".service-wrap").click(function(){
				var id = $(this).attr('data-id');
				$('.tooltip-main').removeClass('active');
				$('#'+id).addClass('active');
			}); */

			$(".car-care-icon").click(function(){
				// alert
				// var id = $(this).attr('data-id');
				var class1 = $(this).attr('data-class1');

				$('.tooltip-main').removeClass('active');
				$('.'+class1).addClass('active');
			});
	
			$(document).ready(function() {
				if($(".fancybox").length > 0){
					$(".fancybox").fancybox();
				}
			});
		</script>

		<script>
			$(document).ready(function () {

				if (localStorage.getItem("parentClass") === "Pflege") {
					$('.Pflege').removeClass('collapse').collapse('show').attr("aria-expanded","true");
					$('.Schutz').removeClass('active').collapse('show').attr("aria-expanded","false");
					$('.design').removeClass('active').collapse('show').attr("aria-expanded","false");
					$('#collapse1').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
					localStorage.clear();
				}
				else if (localStorage.getItem("parentClass") === "Schutz") {
					$('.Schutz').removeClass('collapse').collapse('show').attr("aria-expanded","true");
					$('.Pflege').removeClass('active').collapse('show').attr("aria-expanded","false");
					$('.design').removeClass('active').collapse('show').attr("aria-expanded","false");
					$('#collapse2').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
					localStorage.clear();
					
				}

				else if (localStorage.getItem("parentClass") === "design") {
					$('.design').removeClass('collapse').collapse('show').attr("aria-expanded","true");
					$('.Pflege').removeClass('active').collapse('show').attr("aria-expanded","false");
					$('.Schutz').removeClass('active').collapse('show').attr("aria-expanded","false");
					$('#collapse3').removeClass('collapsed').collapse('show').attr("aria-expanded","true");
					localStorage.clear();
				}


				if (typeof(Storage) !== "undefined") {
					/* PFLEGE CATEGORY START */
					$('.plegelink').click(function () {
						localStorage.setItem('parentClass', 'Pflege');
					});
					/* Schutz CATEGORY START */
					$('.schutzlink').click(function () {
						localStorage.setItem('parentClass', 'Schutz');
					});
					/* Design CATEGORY START */
					$('.designlink').click(function () {
						localStorage.setItem('parentClass', 'design');
					});
				}
				if (localStorage.getItem("parentClass") === "Pflegemobile") {
					$('.Pflegemobile').removeClass('collapse').collapse('show').addClass('active').attr("aria-selected","true");
					$('.Schutzmobile').removeClass('active').collapse('show').addClass('collapse').attr("aria-selected","false");
					$('.designmobile').removeClass('active').collapse('show').addClass('collapse').attr("aria-selected","false");
					$('#services-mobile .tab-pane').removeClass('active show');
					$('#services-mobile #pills-care').addClass('active show');
					localStorage.clear();
				}
				else if (localStorage.getItem("parentClass") === "Schutzmobile") {
					$('.Schutzmobile').removeClass('collapse').collapse('show').addClass('active').attr("aria-selected","true");
					$('.Pflegemobile').removeClass('active').collapse('show').addClass('collapse').attr("aria-selected","false");
					$('.designmobile').removeClass('active').collapse('show').addClass('collapse').attr("aria-selected","false");
					$('#services-mobile .tab-pane').removeClass('active show');
					$('#services-mobile #pills-protect').addClass('active show');
					localStorage.clear();
				}

				else if (localStorage.getItem("parentClass") === "designmobile") {
					$('.designmobile').removeClass('collapse').collapse('show').addClass('active').attr("aria-selected","true");
					$('.Pflegemobile').removeClass('active').collapse('show').addClass('collapse').attr("aria-selected","false");
					$('.Schutzmobile').removeClass('active').collapse('show').addClass('collapse').attr("aria-selected","false");
					$('#services-mobile .tab-pane').removeClass('active show');
					$('#services-mobile #pills-enhance').addClass('active show');
					localStorage.clear();
				}
				/* PFLEGE mobile CATEGORY START */
				$('.plegemobilelink').click(function () {
					localStorage.setItem('parentClass', 'Pflegemobile');

				});
				/* Schutz mobile CATEGORY START */
				$('.schutzmobilelink').click(function () {
					localStorage.setItem('parentClass', 'Schutzmobile');
				});
				/* Design mobile CATEGORY START */
				$('.designmobilelink').click(function () {
					localStorage.setItem('parentClass', 'designmobile');
				});

			});
		</script>

		<script>
			jQuery(function($) {
				var path = window.location.href; 
				$('.plegelink,.schutzlink,.designlink,.plegemobilelink,.schutzmobilelink,.designmobilelink').each(function() {
					if (this.href === path) {
						$(this).parents('.item').addClass('active');
						setTimeout(() => {
							// $('.owl-carousel').trigger('stop.owl.autoplay');
							/* $(document).on("click", ".owl-item>div", function () {
								$('.owl-carousel').trigger("to.owl.carousel", [
									$(this).data("position"),
								]);
							});  */
						}, 1000);
						
					}
				});
				
			});
		</script>

	@yield('script')
		
	</body>
</html>


