@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.create') }} {{ trans('cruds.servicesubcategories.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('servicesubcategories.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('servicesubcategories.store') }}" method="POST" enctype="multipart/form-data" id="add_form">
                    @csrf
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.servicesubcategories.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($servicecategory->name) ? $servicecategory->name : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">{{ trans('cruds.servicesubcategories.fields.description') }}</label>
                        <textarea name="description" class="form-control" id="description" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="category">{{ trans('cruds.servicesubcategories.fields.parent_category') }}</label>
                        <select class="form-control" name="parent_id" id="category">
                            <option value="0">{{ trans('cruds.servicesubcategories.fields.select_parent_category') }}</option>  
                            @foreach($parent_categorys as $value)
                                
                                <option value="{{$value->id}}">{{$value->name}}</option>  
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="vehicle_type">{{ trans('cruds.servicesubcategories.fields.vehicle_type') }}</label>
                        <select class="form-control" name="vehicle_type_id" id="vehicle_type">
                            @foreach($vehicle_type as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>  
                            @endforeach
                        </select>
                    </div>
                    <!--<div class="form-group">
                        <label for="title">{{ trans('cruds.news.fields.image') }}<span class="required_class">*</span></label>
                        <input type="file" class="form-control" id="image" name="image" value="{{ old('image', isset($servicecategory->image) ? $servicecategory->image : '') }}">
                    </div>-->
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        CKEDITOR.replace('description');
        $("#add_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection