@foreach ($drive_vehicle as $key => $value) 
    <div class="form-group">
        <input type="radio" name="step_21" class="form-control radio" value="{{$value->id}}" id="step_21_{{$value->id}}" p-id="{{$value->price}}" n-id="{{$value->name}}">
        <label role="button" for="step_21_{{$value->id}}"><span>{{ucfirst($value->name)}}</span>
        	<span class="ml-auto">{{$value->price.' CHF'.}}</span></label>
    </div>
@endforeach        