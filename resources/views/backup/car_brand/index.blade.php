@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>{{ trans('cruds.car_brand.title_singular') }} {{ trans('global.list') }}</h4>
            </div>

            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('carbrand.create') }}" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        {{ trans('global.add') }} {{ trans('cruds.car_brand.title_singular') }}
                    </a>
                </div>
            </div>
           
            <div class="col-md-12 mt-3">
               <div class="card">
                    <div class="card-body">
                        @include("partials.alert")
                        <div class="table-responsive">
                            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                    <tr>
                                        <th>{{ trans('cruds.car_brand.fields.id') }}</th>
                                        <th>{{ trans('cruds.car_brand.fields.name') }}</th>
                                        <th>{{ trans('cruds.car_brand.fields.car_type') }}</th>
                                        <th>{{ trans('cruds.drive_car.fields.type') }}</th>
                                        <th>{{ trans('global.status') }}</th>
                                        <th>{{ trans('global.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($car_brands as $key => $car_brand)
                                    <tr data-entry-id="{{ $car_brand->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $car_brand->name ?? ''}}</td>
                                        <td>{{ $car_brand->car_type_name->name ?? ''}}</td>
                                        <td>{{ $car_brand->vehicle_category ? ucfirst($car_brand->vehicle_category) : '-'}}</td>
                                        <td>
                                            @include('partials.switch', ['id'=>
                                            $car_brand->id,'is_active'=>$car_brand->is_active])
                                        </td>
                                        <td>@include('partials.actions', ['id' => $car_brand->id])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
