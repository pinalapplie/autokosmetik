var autokosmetic = {};

(function ($) {
    "use strict";

    var $window = $(window),
        $document = $(document),
        $body = $("body"),
        $countdownTimer = $(".countdown"),
        $counter = $(".counter");

    //Check if function exists
    $.fn.exists = function () {
        return this.length > 0;
    };

    autokosmetic.scrolltotop = function () {
        var $scrolltop = $(".car-top");

        $scrolltop.on("click", function () {
            $("html,body").animate(
                {
                    scrollTop: 0,
                },
                800
            );

            /* $(this).addClass("car-run");
            setTimeout(function(){ $scrolltop.removeClass('car-run');},1000);
            return false; */
        });
        $window.on("scroll", function () {
            if ($window.scrollTop() >= 200) {
                $scrolltop.addClass("show");
                $scrolltop.addClass("car-down");
            } else {
                $scrolltop.removeClass("show");
                setTimeout(function () {
                    $scrolltop.removeClass("car-down");
                }, 300);
            }
        });
    };
    autokosmetic.sidebarfixed = function () {
        if ($(".listing-sidebar").exists()) {
            (function () {
                var reset_scroll;

                $(function () {
                    return $("[data-sticky_column]").stick_in_parent({
                        parent: "[data-sticky_parent]",
                    });
                });

                reset_scroll = function () {
                    var scroller;
                    scroller = $("body,html");
                    scroller.stop(true);
                    if ($(window).scrollTop() !== 0) {
                        scroller.animate(
                            {
                                scrollTop: 0,
                            },
                            "fast"
                        );
                    }
                    return scroller;
                };

                window.scroll_it = function () {
                    var max;
                    max = $(document).height() - $(window).height();
                    return reset_scroll()
                        .animate(
                            {
                                scrollTop: max,
                            },
                            max * 3
                        )
                        .delay(100)
                        .animate(
                            {
                                scrollTop: 0,
                            },
                            max * 3
                        );
                };
                window.scroll_it_wobble = function () {
                    var max, third;
                    max = $(document).height() - $(window).height();
                    third = Math.floor(max / 3);
                    return reset_scroll()
                        .animate(
                            {
                                scrollTop: third * 2,
                            },
                            max * 3
                        )
                        .delay(100)
                        .animate(
                            {
                                scrollTop: third,
                            },
                            max * 3
                        )
                        .delay(100)
                        .animate(
                            {
                                scrollTop: max,
                            },
                            max * 3
                        )
                        .delay(100)
                        .animate(
                            {
                                scrollTop: 0,
                            },
                            max * 3
                        );
                };

                $(window).on(
                    "resize",
                    (function (_this) {
                        return function (e) {
                            return $(document.body).trigger(
                                "sticky_kit:recalc"
                            );
                        };
                    })(this)
                );
            }.call(this));

            (function () {
                var sticky;
                if (window.matchMedia("(min-width: 768px)").matches) {
                    $(".listing-sidebar").sticky({
                        topSpacing: 0,
                    });
                }
            });
        }
    };

    $(document).ready(function () {
        autokosmetic.scrolltotop(), autokosmetic.sidebarfixed();
        ("use strict");

        $(".book-appo-wrap.test-step .button").on("click", function (e) {
            e.preventDefault();
            $(this)
                .parents(".book-appo-wrap.test-step")
                .next()
                .addClass("active");
            $(this).parents(".book-appo-wrap.test-step").removeClass("active");
        });
        /* $('.book-appo-wrap.test-step .form-group label').on('click', function(e) {
        	e.preventDefault();
        	$(this).parents('.book-appo-wrap.test-step').next().addClass('active');
        	$(this).parents('.book-appo-wrap.test-step').removeClass('active');
        }); */

        $(".test-step .prev-btn").on("click", function (e) {
            e.preventDefault();
            $(this)
                .parents(".book-appo-wrap.test-step")
                .prev()
                .addClass("active");
            $(this).parents(".book-appo-wrap.test-step").removeClass("active");
        });

        if ($("a.fancy-box").length > 0) {
            $("a.fancy-box").fancybox({
                hideOnContentClick: true,
            });
        }

        /* Owl carousel init */
        /* owl center with slide click */
        $(".Design-carousel").owlCarousel({
            loop: true,
            margin: 30,
            autoplay: false,
            responsiveClass: true,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false,
                },
                600: {
                    items: 1,
                    nav: false,
                },
                1000: {
                    items: 2,
                    nav: false,
                },
            },
        });
        
        var $careCarousel = $(".care-for-carousel");
        $careCarousel.children().each(function (index) {
            $(this).attr("data-position", index);
        });
        

        $careCarousel.owlCarousel({
            loop: true,
            margin: 30,
            center: false,
            nav: true,
            dots: false,
            autoplay:true,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                1000: {
                    items: 3,
                },
            },
        });
        /* $(document).on("click", ".owl-item>div", function () {
            var $speed = 300;
            $careCarousel.trigger("to.owl.carousel", [
                $(this).data("position"),
                $speed,
            ]);
        }); */

        var $careCarouselInnenaufbereitung = $(
            ".care-for-carousel-Innenaufbereitung"
        );
        $careCarouselInnenaufbereitung.children().each(function (index) {
            $(this).attr("data-position", index);
        });

        $careCarouselInnenaufbereitung.owlCarousel({
            loop: true,
            margin: 30,
            center: false,
            nav: true,
            dots: false,
            autoplay:true,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                1000: {
                    items: 3,
                },
            },
        });
        /* $(document).on("click", ".owl-item>div", function () {
            var $speed = 300;
            $careCarouselInnenaufbereitung.trigger("to.owl.carousel", [
                $(this).data("position"),
                $speed,
            ]);
        }); */

        var $careCarouselNew = $(".care-for-carousel-folien");
        $careCarouselNew.children().each(function (index) {
            $(this).attr("data-position", index);
        });

        $careCarouselNew.owlCarousel({
            loop: false,
            margin: 30,
            center: false,
            nav: true,
            dots: false,
            autoplay: true,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                1000: {
                    items: 3,
                },
            },
        });
        /* $(document).on("click", ".owl-item>div", function () {
            var $speed = 300;
            $careCarouselNew.trigger("to.owl.carousel", [
                $(this).data("position"),
                $speed,
            ]);
        }); */

        var $careCarouselBeschichtung = $(".care-for-carousel-Beschichtung");
        $careCarouselBeschichtung.children().each(function (index) {
            $(this).attr("data-position", index);
        });

        $careCarouselBeschichtung.owlCarousel({
            loop: true,
            margin: 30,
            center: false,
            nav: true,
            dots: false,
            autoplay:true,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                1000: {
                    items: 3,
                },
            },
        });
        /* $(document).on("click", ".owl-item>div", function () {
            var $speed = 300;
            $careCarouselBeschichtung.trigger("to.owl.carousel", [
                $(this).data("position"),
                $speed,
            ]);
        }); */

        var $careCarouselKomplettfolierung = $(
            ".care-for-carousel-Komplettfolierung"
        );
        $careCarouselKomplettfolierung.children().each(function (index) {
            $(this).attr("data-position", index);
        });

        $careCarouselKomplettfolierung.owlCarousel({
            loop: true,
            margin: 30,
            center: false,
            nav: true,
            dots: false,
            autoplay:true,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                1000: {
                    items: 3,
                },
            },
        });
        /* $(document).on("click", ".owl-item>div", function () {
            var $speed = 300;
            $careCarouselKomplettfolierung.trigger("to.owl.carousel", [
                $(this).data("position"),
                $speed,
            ]);
        }); */

        var $careCarouselTeilfolierungen = $(
            ".care-for-carousel-Teilfolierungen"
        );
        $careCarouselTeilfolierungen.children().each(function (index) {
            $(this).attr("data-position", index);
        });

        $careCarouselTeilfolierungen.owlCarousel({
            loop: true,
            margin: 30,
            center: false,
            nav: true,
            dots: false,
            autoplay:true,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                1000: {
                    items: 3,
                },
            },
        });
        /* $(document).on("click", ".owl-item>div", function () {
            var $speed = 300;
            $careCarouselTeilfolierungen.trigger("to.owl.carousel", [
                $(this).data("position"),
                $speed,
            ]);
        }); */
        /* end owl center with slide click */

        $(".related-projects-carousel").owlCarousel({
            loop: true,
            stagePadding: 50,
            center: true,
            margin: 10,
            responsiveClass: true,
            nav: false,
            dots: false,
            responsive: {
                0: {
                    items: 2,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 5,
                },
            },
        });

        

        $(".our-portfolio").owlCarousel({
            loop: true,
            margin: 30,
            autoplay: true,
            responsiveClass: true,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false,
                },
                600: {
                    items: 2,
                    nav: false,
                },
                1000: {
                    items: 3,
                    nav: false,
                },
            },
        });

        $(".our-partner").owlCarousel({
            loop: true,
            margin: 30,
            autoplay: true,
            responsiveClass: true,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false,
                },
                600: {
                    items: 2,
                    nav: false,
                },
                1000: {
                    items: 6,
                    nav: false,
                },
            },
        });
    });

    /* $( ".care-for-carousel .item .bg-white" ).each(function( index ) {
    	$(this).on('click', () => {
    		$('.care-for-carousel .item .bg-white').addClass('collapsed');
    		$(this).removeClass('collapsed');
    	});
    	
    }); */

    $("body").append("<a class='back-to-top'></a>");
    const btn = $(".back-to-top");
    $(window).on("scroll", function () {
        if ($(window).scrollTop() > 300) {
            btn.addClass("show");
        } else {
            btn.removeClass("show");
        }
    });

    btn.on("click", (e) => {
        e.preventDefault();
        $("html, body").animate(
            {
                scrollTop: 0,
            },
            "600"
        );
    });
})(jQuery);
