<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('service_management_id')->unsigned();
            $table->bigInteger('vehicle_service_id')->unsigned()->nullable();
            $table->bigInteger('vehicle_categorie_id')->unsigned();
            $table->bigInteger('vehicle_condition_id')->unsigned();
            $table->string('price')->nullable();

            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
            $table->foreign('service_management_id')->references('id')->on('service_management')->onDelete('cascade');
            $table->foreign('vehicle_service_id')->references('id')->on('vehicle_services')->onDelete('cascade');
            $table->foreign('vehicle_categorie_id')->references('id')->on('vehicle_categories')->onDelete('cascade');
            $table->foreign('vehicle_condition_id')->references('id')->on('vehicle_conditions')->onDelete('cascade');

             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_prices');
    }
}
