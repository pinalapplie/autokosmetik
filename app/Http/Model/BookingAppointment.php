<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingAppointment extends Model
{	
	use SoftDeletes;  
    protected $guarded = [];
    protected $table ="booking_appointment";
   	protected $dates = ['deleted_at'];
}
