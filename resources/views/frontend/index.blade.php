@section('title', 'Zuhause')
@extends('layouts.front_end')
@section('content')
<div class="main-wrapper index">
    <!-- Page Content  -->
    <div id="main-content" class="active">
        <div class="banner-carousel owl-carousel owl-theme">
            <div class="item">
                <img src="{{ asset('assets/frontend/revslider1/assets/Website_Autokosmetik_YZ_01.png') }}" alt="">
                <div class="banner-details">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-lg-11">
                                <div class="h1"><strong>VISION</strong></div>
                                <div class="h3">Ihre Ideen und Wünsche werden mit modernsten Methoden umgesetzt.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="{{ asset('assets/frontend/revslider1/assets/Website_Autokosmetik_YZ_03.png') }}" alt="">
                <div class="banner-details">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-lg-11">
                                <div class="h1"><strong>PASSION</strong></div>
                                <div class="h3">Qualität und Präzision spiegelt sich in unseren Arbeiten wider.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="{{ asset('assets/frontend/revslider1/assets/Website_Autokosmetik_YZ_02.png') }}" alt="">
                <div class="banner-details">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-lg-11">
                                <div class="h1"><strong>WISSEN</strong></div>
                                <div class="h3">Begeisterung und kompetentes Wissen ist für die Ausübung unseres
                                    Handwerks unerlässlich.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="{{ asset('assets/frontend/revslider1/assets/Website_Autokosmetik_YZ_04.png') }}" alt="">
                <div class="banner-details">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-lg-11">
                                <div class="h1"><strong>Mission</strong></div>
                                <div class="h3">Auf nachhaltigen Schutz und schonendste Reinigungsmethoden, legen wir
                                    grössten Wert!</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="{{ asset('assets/frontend/revslider1/assets/banner-new.png') }}" alt="">
                <div class="banner-details">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-lg-11">
                                <div class="h1"><strong>LEIDENSCHAFT</strong></div>
                                <div class="h3">Die passende Infrastruktur bietet die Grundlage zur Ausübung unserer'
                                    Leidenschaft.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Setion About us -->
        <div class="bg-dark-autokosmetic">
            <div class="container overflow-hidden">
                <div class="row align-items-center">
                    <div class="left col-sm-12 col-md-6 mt-4 mt-md-0" data-aos="fade-up">
                        <div class="text-white">Autokosmetik.li - Herzlich willkommen!</div>
                        <h1 class="text-white">Ihr Partner für Autokosmetik, Fahrzeugaufbereitung & Fahrzeugveredelung
                        </h1>
                        <p class="text-light">Wir lieben Ihr Auto oder Motorrad fast genauso wie Sie! Ganz gleich ob es
                            um Pflege, Schutz oder Design geht, wir machen mehr aus Ihrem Fahrzeug.</p>
                        <p class="text-light">Im Jahr 2008 begannen wir als einer der ersten Anbieter mit der
                            Fahrzeugaufbereitung in einer Garage in Gamprin. Heute bieten wir alle Dienstleistungen im
                            Bereich der Autokosmetik, der Fahrzeugpflege und der Fahrzeugveredelung an und betreuen
                            unsere Kundschaft im Fürstentum Liechtenstein und in der Schweiz / Rheintal.</p>
                        <p class="text-light">Was wir tun, tun wir gut. Erwarten Sie beste Qualität zu einem fairen
                            Preis. Immer.</p>
                        <p class="text-light">Rufen Sie uns jetzt an oder schauen Sie einfach auf einen Kaffee an
                            unserem neuen Standort in Triesen vorbei.</p>
                        <a href="{{ route('about.us')}}" class="btn-3">Mehr <i class="fa fa-chevron-right"></i></a>
                    </div>
                    <div class="right ez-animate col-sm-12 col-md-6" data-aos="fade-left">
                        <img class="img-fluid w-100" src="{{ asset('assets/frontend/images/FotoPascal.png') }}"
                            alt="FotoPascal">
                    </div>
                    <!-- <div class="bottom ez-animate col-12" data-aos="fade-right">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/img-aboutus-2.png') }}" alt="autokosmetik">
							<label>Gründer Autokosmetik </label>
						</div> -->
                </div>
            </div>
        </div>
        <!-- /.Setion About us -->
        <div class="h1 pt-5 text-center pb-3">Unsere Partner</div>
        <div class="our-partner owl-carousel owl-theme container">
            <div class="item">
                <div class="img-container">
                    <img class="img-fluid" src="https://via.placeholder.com/150x150.png?text=Partner" alt="">
                </div>
            </div>
            <div class="item">
                <div class="img-container">
                    <img class="img-fluid" src="https://via.placeholder.com/150x150.png?text=Partner" alt="">
                </div>
            </div>
            <div class="item">
                <div class="img-container">
                    <img class="img-fluid" src="https://via.placeholder.com/150x150.png?text=Partner" alt="">
                </div>
            </div>
            <div class="item">
                <div class="img-container">
                    <img class="img-fluid" src="https://via.placeholder.com/150x150.png?text=Partner" alt="">
                </div>
            </div>
            <div class="item">
                <div class="img-container">
                    <img class="img-fluid" src="https://via.placeholder.com/150x150.png?text=Partner" alt="">
                </div>
            </div>
            <div class="item">
                <div class="img-container">
                    <img class="img-fluid" src="https://via.placeholder.com/150x150.png?text=Partner" alt="">
                </div>
            </div>
        </div>

        <!-- Section miscellaneous 1 -->
        <div id="accordionExample">
            <div class="container-fluid pb-4">
				<div class="row">
                    <div class="col-md-12">
                        <div class="home-service-title py-3 py-lg-5 text-center">
                            <div class="h1 pt-4">Unsere Dienstleistungen</div>
                            <!-- <p class="h6"> Wir bieten einen kompletten Service für Autoreparatur und Wartung </p> -->
                        </div>
                    </div>
                </div>

				<div class="form-row justify-content-center mb-md-5 mb-3">
					<div class="col-md-auto">
						<div class="form-row accordion">
							<div id="headingOne" class="col-md-auto">
								<a class="btn mb-2 mb-lg-4 d-flex border justify-content-center" type="button"
									data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
									aria-controls="collapseOne">Neuwagen & Gebrauchtwagen</a>
							</div>
							<div id="headingTwo" class="col-md">
								<a class="btn mb-2 mb-lg-4 d-flex collapsed border justify-content-center" type="button"
									data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
									aria-controls="collapseTwo">Oldtimer, Youngtimer & Show-Cars</a>
							</div>
						</div>
					</div>
				</div>
				<div class="collapse show" id="collapseOne" aria-labelledby="headingOne"
					data-parent="#accordionExample">
					<div class="car-bike-1 row">
						<div class="col-md-4">
							<div id="section-services1" class="feature-car-area pt-2">
								<div class="container">
									<div class="row modal-main service-accordian-main">
										<div class="col-12 mb-3">
											<div class="service-level-1 car-care-icon bg-white shadow-sm"
												style="background: url({{ asset('assets/frontend/images/car-care-bg.png')}}) no-repeat center / cover;"
												role="button" data-class1="service-first">
												<div class="align-items-center d-flex flex-wrap service-title-wrap">
													<div class="avatar-sm mb-3 ml-0 mr-2">
														<img src="{{ asset('assets/frontend/images/car-care.png')}}"
															alt="" class="img-fluid">
													</div>
													<h3 class="h3 font-weight-bold mb-0">Pflege</h3>
													<div>Perfekte Pflege für Ihr Fahrzeug: Auto-Innenreinigung,
														Lederpflege und –reparatur sowie Aussenaufbereitung.</div>
												</div>
											</div>
										</div>
										<div class="col-12 mb-3">
											<div class="service-level-1 car-care-icon bg-white shadow-sm"
												style="background: url({{ asset('assets/frontend/images/car-protect-bg.jpg')}}) no-repeat center / cover;"
												role="button" data-class1="service-second">
												<div class="align-items-center d-flex flex-wrap service-title-wrap">
													<div class="avatar-sm mb-3 ml-0 mr-2">
														<img src="{{ asset('assets/frontend/images/car-protect.png')}}"
															alt="" class="img-fluid">
													</div>
													<h3 class="h3 font-weight-bold mb-0">Schutz</h3>
													<div>Steinschlagschutzfolien, eine Keramikbeschichtung und eine
														Scheibentönung schützen Ihr Auto oder Ihr Motorrad.</div>
												</div>
											</div>
										</div>
										<div class="col-12 mb-3">
											<div class="service-level-1 car-care-icon bg-white shadow-sm"
												style="background: url({{ asset('assets/frontend/images/paint-1.jpg')}}) no-repeat center / cover;"
												role="button" data-class1="service-third">
												<div class="align-items-center d-flex flex-wrap service-title-wrap">
													<div class="avatar-sm mb-3 ml-0 mr-2">
														<img src="{{ asset('assets/frontend/images/car-foil.png')}}"
															alt="" class="img-fluid">
													</div>
													<h3 class="h3 font-weight-bold mb-0">Design</h3>
													<div>Interieur-Design, Verchromungen, Wassertransferdruck und eine
														Autofolie machen Ihr Fahrzeug zum Blickfang.</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-8">
							<div id="section-miscellaneous1-1" class="feature-car-area">
								<div class="container">
									<div class="row">
										<div class="col-md-12">
											<div class="home-service-image">
												<img src="{{ asset('assets/frontend/images/car-service.png') }}"
													class="w-100 img-fluid" alt="autokosmetik">
											</div>

											<div class="tooltip-main service-first car-new-pflege-1"
												data-info="Innenreinigung">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_right">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-7">
																				<p class="h6">Als offizielles Swissvax
																					Automobil Detailing Center für
																					Liechtenstein und das Rheintal
																					(Schweiz) stellen wir die optimale
																					Pflege mit den herausragenden
																					Swissvax-Produkten sicher. Da jeder
																					Auto-Eigentümer individuelle Wünsche
																					und Vorstellungen darüber hat, was
																					er an seinem „Liebling“ verbessert
																					oder perfektioniert haben möchte,
																					können wir für die Autopflege mit
																					Swissvax nicht mit Pauschalen
																					arbeiten.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">
																						Hochwertigste Reinigungsmittel
																					</li>
																					<li class="circle m-0 h6">Optimale
																						Langzeitpflege</li>
																					<li class="circle m-0 h6">
																						Ausschliesslich
																						Swissvax-Produkte</li>
																				</ul>
																			</div>
																			<div class="right col-sm-12 col-md-5"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<p class="h6">Entsprechend ist die
																						vorgängige Begutachtung des
																						Autos und das Kundengespräch die
																						Basis für unser definitives
																						Angebot. Die Begutachtung kann
																						vor Ort beim Kunden oder aber
																						auch bei uns in Triesen
																						erfolgen. Anschliessend erhält
																						der Kunde ein schriftliches
																						Angebot mit einer Beschreibung
																						der auszuführenden Arbeiten und
																						dem verbindlichen Preis.
																						Schlussendlich setzt sich der
																						Preis aus dem Stundensatz von
																						CHF 148.--, inkl. 15%
																						Swissvax-Materialaufwand und
																						7.7% MwSt. zusammen.</p>
																					<p class="h6">Selbstverständlich
																						bieten wir auch einen Hol- und
																						Bringservice an, welcher sich
																						nach dem jeweiligen Aufwand
																						berechnet. Hochglanz- und
																						Schleifpolituren,
																						Innenaufbereitungen,
																						Lederreparaturen,
																						Concoursaufbereitungen, usw.
																						ausschliesslich gegen vorgängige
																						Besichtigung und Offerte.</p>
																					<p class="h6">Fragen Sie uns jetzt
																						für eine Autopflege mit Swissvax
																						an!</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-first car-new-pflege-2"
												data-info="Lederpflege & Restauration">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom">
														<div class="modal-content">
															<div class="modal-body">
																<div class="services2-car-bg-2 py-3">
																	<div class="container">
																		<div class="row">
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<p class="h6">
																						Leder benötigt besondere Pflege.
																						Das gilt besonders für stark
																						beanspruchte Autositze aus
																						Leder. Doch auch Lenkrad,
																						Schaltknüppel und Armaturen sind
																						oft mit Leder überzogen und
																						können sich durch den Gebrauch
																						abnutzen.
																					</p>
																					<p class="h6">Vertrauen Sie uns die
																						Lederpflege Ihres Autos an. Bei
																						einer Grundreinigung mit Bürste
																						und Staubsauger entfernen wir
																						groben Schmutz. Anschliessend
																						geht es an die Tiefenreinigung.
																						Hierfür verwenden wir eine milde
																						Pflegemilch, welche die
																						Rückfettung ausgetrockneter
																						Stellen sicherstellt. Unser
																						Mittel enthält zudem einen
																						UV-Schutz, um das Material gegen
																						das Ausbleichen durch die Sonne
																						zu schützen.</p>
																				</div>
																			</div>
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="ez-animate col-sm-12 col-md-5 text-center pt-3"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<p class="h6">
																						Wenn Leder unansehnlich geworden
																						ist, muss es nicht zwingend
																						ersetzt werden. Kleine Risse und
																						Löcher können wir mit
																						Flüssigleder füllen.
																						Anschliessend glätten wir die
																						Oberflächen und gleichen die
																						Farben an. Zum Abschluss der
																						Lederreparatur versiegeln wir
																						die Oberflächen, um das Leder
																						gegen Abrieb und Kratzer zu
																						schützen.
																					</p>
																					<p class="h6">Unsere erstklassigen
																						Reinigungsprodukte helfen
																						übrigens, auch wenn stark
																						beanspruchtes Leder einfach
																						unansehnlich geworden ist. Durch
																						Färben können wir wieder ein
																						einheitliches Äusseres
																						herstellen.</p>
																					<p class="h6">Lederpflege und
																						Lederrestauration gehören in die
																						Hände von Profis. Fragen Sie uns
																						jetzt an!</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a href="#">
																							<div class="img-container">
																								<a data-fancybox="gallery-17"
																									href="{{ asset('assets/frontend/images/auto-1.jpg') }} ">
																									<img class="w-100"
																										src="{{ asset('assets/frontend/images/auto-1.jpg') }} "
																										alt="autokosmetik">
																								</a>
																							</div>
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a href="#">
																							<div class="img-container">
																								<a data-fancybox="gallery-17"
																									href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																									<img class="w-100"
																										src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																										alt="autokosmetik">
																								</a>
																							</div>
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a href="#">
																							<div class="img-container">
																								<a data-fancybox="gallery-17"
																									href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																									<img class="w-100"
																										src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																										alt="autokosmetik">
																								</a>
																							</div>
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a href="#">
																							<div class="img-container">
																								<a data-fancybox="gallery-17"
																									href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																									<img class="w-100"
																										src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																										alt="autokosmetik">
																								</a>
																							</div>
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a href="#">
																							<div class="img-container">
																								<a data-fancybox="gallery-17"
																									href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																									<img class="w-100"
																										src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																										alt="autokosmetik">
																								</a>
																							</div>
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a href="#">
																							<div class="img-container">
																								<a data-fancybox="gallery-17"
																									href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																									<img class="w-100"
																										src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																										alt="autokosmetik">
																								</a>
																							</div>
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a href="#">
																							<div class="img-container">
																								<a data-fancybox="gallery-17"
																									href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																									<img class="w-100"
																										src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																										alt="autokosmetik">
																								</a>
																							</div>
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a href="#">
																							<div class="img-container">
																								<a data-fancybox="gallery-17"
																									href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																									<img class="w-100"
																										src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																										alt="autokosmetik">
																								</a>
																							</div>
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a href="#">
																							<div class="img-container">
																								<a data-fancybox="gallery-17"
																									href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																									<img class="w-100"
																										src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																										alt="autokosmetik">
																								</a>
																							</div>
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-first car-new-pflege-3"
												data-info="Aussenaufbereitung">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-7">
																				<p class="h6">Eine Teflon-Versiegelung
																					ist sehr preiswert, hält 6-12 Monate
																					und ist für die meisten Autos
																					geeignet. Nach einer
																					Teflon-Versiegelung empfehlen wir
																					jedoch Hand- oder Lanzenwäsche, da
																					die Waschstrasse die Versiegelung zu
																					schnell abreibt.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">Der
																						Standard-Lackschutz</li>
																					<li class="circle m-0 h6">
																						Haltbarkeit 6+ Monate</li>
																					<li class="circle m-0 h6">Preiswert
																					</li>
																				</ul>
																			</div>
																			<div class="right col-sm-12 col-md-5"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/seal-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/seal-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<h3 class="h4">Besonders geeignet
																						für:</h3>
																					<ul class="m-0">
																						<li class="circle m-0 h6">Autos,
																							Motorräder, Wohnmobile, LKWs
																							usw.</li>
																						<li class="circle m-0 h6">
																							Gebrauchtwagen</li>
																						<li class="circle m-0 h6">
																							Leasingrückgaben</li>
																					</ul>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/seal-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/seal-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/seal-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/seal-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/seal-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/seal-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/seal-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/seal-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/seal-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/seal-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/seal-8.png') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/seal-8.png') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/seal-9.png') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/seal-9.png') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/seal-10.png') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/seal-10.png') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/seal-11.png') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/seal-11.png') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-second car-new-schutz-1"
												data-info="Scheibentönung">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-7">
																				<p class="h6">Eine Autoscheibentönung
																					sieht nicht nur gut aus, sie schützt
																					auch in mehrfacher Hinsicht. Der
																					Innenraum heizt weniger auf, 99% der
																					schädlichen UV-Strahlung wird
																					abgehalten und die Autofolie schützt
																					vor neugierigen Blicken. Wir
																					arbeiten ausschliesslich mit der
																					hervorragenden Folie Marathon der
																					Firma Johnson Window Films, welche
																					darauf Lebenslange Garantie gewährt.
																					Jede Scheibentönung erhält ein
																					Zertifikat.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">99%iger
																						UV-Schutz</li>
																					<li class="circle m-0 h6">
																						Hitzereduktion</li>
																					<li class="circle m-0 h6">
																						Blickschutz</li>
																					<li class="circle m-0 h6">Edle Optik
																					</li>
																				</ul>
																			</div>
																			<div class="right col-sm-12 col-md-5"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/tint-10.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/tint-11.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<h3 class="h4">Besonders geeignet
																						für:</h3>
																					<ul class="m-0 mb-2">
																						<li class="circle m-0 h6">
																							Familienautos und besonders
																							die Kinder</li>
																						<li class="circle m-0 h6">
																							Personen im Fond</li>
																						<li class="circle m-0 h6">
																							Personen mit Hunden</li>
																						<li class="circle m-0 h6">Beim
																							Transport von
																							Wertgegenständen</li>
																					</ul>
																					<p class="h6">Jetzt Termin für eine
																						Autoscheibentönung vereinbaren
																					</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/tint-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/tint-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/tint-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/tint-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/tint-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/tint-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/tint-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/tint-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/tint-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/tint-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/tint-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/tint-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/tint-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/tint-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/tint-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/tint-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery-1"
																							href="{{ asset('assets/frontend/images/tint-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/tint-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-second car-new-schutz-2"
												data-info="Keramikbeschichtung">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_right">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-7">
																				<p class="h6">Sie fahren Ihr Auto
																					täglich und es soll dabei mit
																					möglichst wenig Aufwand schön
																					aussehen? Dann ist die
																					Keramikbeschichtung durch die
																					easy-to-clean Beschichtung das
																					richtige für Sie. Sie hält im
																					Minimum 36 Monate und ist
																					waschstrassenfest. Für jede
																					Keramikbeschichtung stellen wir ein
																					Zertifikat aus, welches eine
																					Haltbarkeit von minimal 3 Jahren
																					garantiert.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">Der
																						panzerglasartige Lackschutz</li>
																					<li class="circle m-0 h6">
																						Haltbarkeit 36+ Monate</li>
																					<li class="circle m-0 h6">
																						Waschstrassenfest</li>
																				</ul>
																			</div>
																			<div class="right col-sm-12 col-md-5"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<h3 class="h4">ServFaces - das
																						perfekte Keramik-Material</h3>
																					<p class="h6">Mit ServFaces haben
																						wir einen hervorragenden Partner
																						in Deutschland. Aufgrund der in
																						jeglicher Hinsicht überragenden
																						Resultate mit ServFaces bieten
																						wir unseren Kunden eine Garantie
																						von drei Jahren auf eine
																						Keramikbeschichtung aus unserem
																						Hause.</p>
																					<p class="h6">Seit Anfang 2016 haben
																						wir übrigens auch die offizielle
																						Vertretung von ServFaces für die
																						Schweiz und Deutschland
																						übernommen.</p>
																					<p class="h6">Jetzt Termin
																						vereinbaren</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-second car-new-schutz-3"
												data-info="Steinschlagschutzfolien">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_right">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-7">
																				<p class="h6">Eine volltransparente
																					Lackschutzfolie ist der beste Schutz
																					gegen Steinschlag. Die von uns
																					verwendete Steinschlagschutzfolie
																					ist sehr dick und dadurch auch ganz
																					besonders widerstandsfähig.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">
																						Hochtransparent/unsichtbar</li>
																					<li class="circle m-0 h6">Der
																						optimale Schutz gegen
																						Steinschlag</li>
																					<li class="circle m-0 h6">
																						Waschstrassenfest</li>
																				</ul>
																			</div>
																			<div class="right col-sm-12 col-md-5"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<h3 class="h4">Besonders geeignet
																						für:</h3>
																					<ul class="m-0 mb-2">
																						<li class="circle m-0 h6">
																							Ladekante und alle Türkanten
																						</li>
																						<li class="circle m-0 h6">
																							Stossstange und Motorhaube
																						</li>
																						<li class="circle m-0 h6">
																							Generell alle exponierten
																							Stellen</li>
																					</ul>
																					<p class="h6">Die Preise für eine
																						Steinschlagschutzfolie
																						variieren. Sie sind abhängig von
																						der Beschaffenheit des Autos und
																						der Anzahl der Stellen, welche
																						mit einer Lackschutzfolie
																						ausgestattet werden sollen.
																						Kommen Sie vorbei, wir
																						unterbreiten Ihnen gerne ein
																						unverbindliches Angebot!</p>
																					<p class="h6">Jetzt Termin
																						vereinbaren</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-third car-new-design-1"
												data-info="Fahrzeugbeschriftung">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-7">
																				<p class="h6">Wer nicht wirbt, stirbt –
																					so der Volksmund. Und es steckt
																					durchaus Wahrheit in diesem Spruch.
																					Neben On- und Offlinewerbung in
																					Medien verschiedenster Art bieten
																					Firmenfahrzeuge eine preiswerte und
																					sehr effektive Werbefläche. Gerade
																					Fahrzeuge, welche oft unterwegs
																					sind, können sehr viel
																					Aufmerksamkeit erwecken – zu einem
																					vergleichsweise tiefen Preis. Dies
																					ist auch der Grund, dass sich
																					Autowerbung einer derart hohen
																					Beliebtheit erfreut.</p>
																			</div>
																			<div class="right col-sm-12 col-md-5"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<p class="h6">Beauftragen Sie uns
																						mit der Autobeschriftung oder
																						Lastwagenbeschriftung.
																						Fahrzeugbeschriftungen in der
																						Form von einer Fahrzeugfolierung
																						können sowohl auf Teilflächen
																						(Teilfolierung) als auch über
																						das gesamte Fahrzeug
																						(Vollfolierung) erstellt werden.
																					</p>
																					<p class="h6">Übrigens: Für
																						Fahrzeugbeschriftungen einer
																						ganzen Flotte
																						(Flottenbeschriftung) gewähren
																						wir attraktive Ermässigungen.
																						Fragen Sie uns jetzt an!</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-third car-new-design-2"
												data-info="Car Wrapping">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_right">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-7">
																				<p class="h6">Verschönern Sie Ihr Auto,
																					Ihr Motorrad oder Ihren Lastwagen
																					mit einer individuellen Folierung.
																					Das auch als Car Wrapping bekannte
																					Verfahren schafft einen
																					einzigartigen Look für Ihr Fahrzeug.
																					Gerne beraten wir Sie individuell
																					über die Möglichkeiten:</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">Ein- oder
																						Mehrfarbig</li>
																					<li class="circle m-0 h6">Matte
																						Folien</li>
																					<li class="circle m-0 h6">
																						Hochglanzfolien</li>
																					<li class="circle m-0 h6">
																						Texturierte Brushed- &
																						Carbon-Style-Folien</li>
																					<li class="circle m-0 h6">
																						Werbefolien</li>
																					<li class="circle m-0 h6">
																						Digitaldrucke</li>
																				</ul>
																			</div>
																			<div class="right col-sm-12 col-md-5"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<p class="h6">Dabei können Sie
																						sowohl einzelne Teile folieren
																						lassen, wie zum Beispiel die
																						Motorhaube oder die Türen
																						(Teilfolierung) oder auch das
																						ganze Fahrzeug (Vollfolierung).
																						Es ist übrigens auch möglich
																						Elemente des Innenraumes zu
																						folieren, wie zum Beispiel
																						Zierleisten im Cockpit.</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-third car-new-design-3"
												data-info="Wassertransferdruck">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_left">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-7">
																				<p class="h6">Wassertransferdruck ist
																					ein relativ modernes Verfahren, das
																					es möglich macht, auch Gegenstände,
																					ohne gerade Flächen vollständig mit
																					einem attraktiven Muster nach Wahl
																					zu versehen. Dabei können fast alle
																					Materialien mit diesem Verfahren
																					behandelt werden, wie zum Beispiel
																					Metall, Kunststoff, Leder oder Glas.
																				</p>
																				<p class="h6">Wir bieten Ihnen eine
																					grosse Auswahl an Dekors, wie zum
																					Beispiel die beliebte Carbon-Optik.
																				</p>
																				<p class="h6">Interessiert? Fragen Sie
																					uns jetzt über die Möglichkeiten mit
																					dem Wassertransferdruck an!</p>

																			</div>
																			<div class="right col-sm-12 col-md-5"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images mt-3">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-third car-new-design-4"
												data-info="Verchromungen">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_left">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-7">
																				<p class="h6">Mehr Glanz für Ihr Auto,
																					Ihr Motorrad oder Ihren Lastwagen!
																					Gerne verchromen wir einzelne Teile
																					Ihres Fahrzeugs und machen es so zu
																					einem Unikat.</p>
																				<p class="h6">Neben der Neuverchromung
																					einzelner Bauteile können wir auch
																					ausgebleichte verchromte Bauteile in
																					neuem Glanz erstrahlen lassen. Für
																					den besonderen Wow-Effekt!</p>
																				<p class="h6">Jetzt informieren!</p>
																			</div>
																			<div class="right col-sm-12 col-md-5"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images mt-3">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-third car-new-design-5"
												data-info="Interieur Design">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_left">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-7">
																				<p class="h6">Fahrzeugveredelungen
																					müssen sich nicht auf das Äussere
																					beschränken. Wir bieten Ihnen
																					individuelle Lösungen für das
																					Interieur Ihres Fahrzeugs. Neben
																					einer Auffrischung oder Erneuerung
																					der Sitzflächen ermöglichen die
																					Autofolierung und der
																					Wassertransferdruck ein
																					einzigartiges Interieur-Design.</p>
																				<p class="h6">Für ein Auto, das so
																					einzigartig wie Sie selbst ist.</p>
																				<p class="h6">Jetzt anfragen</p>
																			</div>
																			<div class="right col-sm-12 col-md-5"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images mt-3">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="bg-white pt-5 page-section-pt car-bike-small-1" role="button">
								<div class="container-fluid feature-car-area">
									<div class="row">
										<div class="col-md-3">
											<div class="home-service-image">
												<img src="{{ asset('assets/frontend/images/car-service.png') }}"
													class="w-100 img-fluid" alt="autokosmetik">
											</div>

										</div>
									</div>
								</div>
							</div>
							<div class="bg-white page-section-pt pb-5 white-bg car-bike-small-2" role="button">
								<div class="container-fluid feature-car-area">
									<div class="row">
										<div class="col-md-2">
											<div class="home-service-image">
												<img src="{{ asset('assets/frontend/images/bike-2.png') }}"
													class="w-100 img-fluid" alt="autokosmetik">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
					<div class="car-bike-2 row">
						<div class="col-md-4">
							<div id="section-services1" class="feature-car-area">
								<div class="container">
									<div class="row modal-main service-accordian-main">
										<div class="col-12 mb-3">
											<div class="service-level-1 car-care-icon  bg-white shadow-sm active"
												style="background: url({{ asset('assets/frontend/images/car-care-bg.png')}}) no-repeat center / cover;"
												role="button" data-class1="service-four">
												<div class="align-items-center d-flex flex-wrap service-title-wrap">
													<div class="avatar-sm mb-3 ml-0 mr-2">
														<img src="{{ asset('assets/frontend/images/car-care.png')}}"
															alt="" class="img-fluid">
													</div>
													<h4 class="h3 font-weight-bold mb-0">Pflege</h4>
													<p>Perfekte Pflege für Ihr Fahrzeug: Auto-Innenreinigung,
														Lederpflege und –reparatur sowie Aussenaufbereitung.</p>
													<!-- <span class="text-primary btn-link">Read more</span> -->
												</div>
											</div>
										</div>
										<div class="col-12 mb-3">
											<div class="service-level-1 car-care-icon  bg-white shadow-sm"
												style="background: url({{ asset('assets/frontend/images/car-protect-bg.jpg')}}) no-repeat center / cover;"
												role="button" data-class1="service-five">
												<div class="align-items-center d-flex flex-wrap service-title-wrap">
													<div class="avatar-sm mb-3 ml-0 mr-2">
														<img src="{{ asset('assets/frontend/images/car-protect.png')}}"
															alt="" class="img-fluid">
													</div>
													<h4 class="h3 font-weight-bold mb-0">Schutz</h4>
													<p>Steinschlagschutzfolien, eine Keramikbeschichtung und eine
														Scheibentönung schützen Ihr Auto oder Ihr Motorrad.</p>
													<!-- <span class="text-primary btn-link">Read more</span> -->
												</div>
											</div>
										</div>
										<div class="col-12 mb-3">
											<div class="service-level-1 car-care-icon  bg-white shadow-sm"
												style="background: url({{ asset('assets/frontend/images/car-enhance-bg.png')}}) no-repeat center / cover;"
												role="button" data-class1="service-six">
												<div class="align-items-center d-flex flex-wrap service-title-wrap">
													<div class="avatar-sm mb-3 ml-0 mr-2">
														<img src="{{ asset('assets/frontend/images/car-enhance.png')}}"
															alt="" class="img-fluid">
													</div>
													<h4 class="h3 font-weight-bold mb-0">Design</h4>
													<p>Interieur-Design, Verchromungen, Wassertransferdruck und eine
														Autofolie machen Ihr Fahrzeug zum Blickfang.</p>
													<!-- <span class="text-primary btn-link">Read more</span> -->
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-8">
							<div id="section-miscellaneous1-1" class="feature-bike-area">
								<div class="container">
									<div class="row justify-content-center">
										<div class="col-md-10">
											<div class="home-service-image">
												<img src="{{ asset('assets/frontend/images/bike-2.png') }}"
													class="w-100 img-fluid" alt="autokosmetik">
											</div>
											<div class="tooltip-main service-four bike-new-pflege-1"
												data-info="lackpflege">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_right">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-6">
																				<p class="h6">Unser neuester Hit, eine
																					Keramikbeschichtung hält Minimum 36
																					Monate und ist absolut
																					waschstrassenfest. Besonders
																					geeignet ist unsere
																					Keramikbeschichtung für dunkle,
																					grosse Autos, welche im
																					Alltagseinsatz stehen. Jede
																					Keramikbeschichtung erhält ein
																					Zertifikat, welches die Haltbarkeit
																					von Minimum 3 Jahren garantiert.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">Der
																						panzerglasartige Lackschutz</li>
																					<li class="circle m-0 h6">
																						Haltbarkeit 36+ Monate</li>
																					<li class="circle m-0 h6">
																						Waschstrassenfest</li>
																				</ul>
																			</div>
																			<div class="right col-sm-12 col-md-6"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<h1 class="h4">Unser neues
																						Keramik-Material ServFaces</h1>
																					<p class="h6">In den ersten 3 Jahren
																						unserer Arbeit mit sogenannten
																						Keramikbeschichtungen, haben wir
																						mit einem fernöstlichen Produkt
																						gearbeitet. Bis wir feststellen
																						mussten, dass sich das Material
																						auf einmal anders verhält und
																						auch anders zu verarbeiten ist.
																						Für unsere Firma ein absolutes
																						"NO GO". Wir konnten jedoch in
																						kürzester Zeit eine
																						Ersatz-Produktlinie aus
																						deutscher Produktion finden und
																						haben seit nunmehr 2 Jahren nur
																						die besten Erfahrungen gemacht.
																						Und so konnten wir unsere
																						Garantie von 2 auf 3 Jahre
																						erhöhen. Ab Anfang 2016 haben
																						wir deshalb auch die Vertretung
																						von ServFaces für die Schweiz
																						und Deutschland übernommen.</p>
																					<p class="pt-3 h6">Anbei finden Sie
																						die komplette Preisliste für die
																						Aussen- und Innenaufbereitung
																						inklusive detaillierter
																						Beschreibung unserer Leistungen.
																					</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-four bike-new-pflege-2"
												data-info="lederpflege">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_right">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-7">
																				<p class="h6">Leder benötigt besondere
																					Pflege. Das gilt besonders für stark
																					beanspruchte Autositze aus Leder.
																					Doch auch Lenkrad, Schaltknüppel und
																					Armaturen sind oft mit Leder
																					überzogen und können sich durch den
																					Gebrauch abnutzen.</p>
																				<p class="h6">Vertrauen Sie uns die
																					Lederpflege Ihres Autos an. Bei
																					einer Grundreinigung mit Bürste und
																					Staubsauger entfernen wir groben
																					Schmutz. Anschliessend geht es an
																					die Tiefenreinigung. Hierfür
																					verwenden wir eine milde
																					Pflegemilch, welche die Rückfettung
																					ausgetrockneter Stellen
																					sicherstellt. Unser Mittel enthält
																					zudem einen UV-Schutz, um das
																					Material gegen das Ausbleichen durch
																					die Sonne zu schützen.</p>
																			</div>
																			<div class="right col-sm-12 col-md-5"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<p class="h6">Wenn Leder
																						unansehnlich geworden ist, muss
																						es nicht zwingend ersetzt
																						werden. Kleine Risse und Löcher
																						können wir mit Flüssigleder
																						füllen. Anschliessend glätten
																						wir die Oberflächen und gleichen
																						die Farben an. Zum Abschluss der
																						Lederreparatur versiegeln wir
																						die Oberflächen, um das Leder
																						gegen Abrieb und Kratzer zu
																						schützen.</p>
																					<p class="h6">Unsere erstklassigen
																						Reinigungsprodukte helfen
																						übrigens, auch wenn stark
																						beanspruchtes Leder einfach
																						unansehnlich geworden ist. Durch
																						Färben können wir wieder ein
																						einheitliches Äusseres
																						herstellen.</p>
																					<p class="h6">Lederpflege und
																						Lederrestauration gehören in die
																						Hände von Profis. Fragen Sie uns
																						jetzt an!</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-five bike-new-schutz-1"
												data-info="keramikbeschichtung">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_right">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-7">
																				<p class="h6">Sie fahren Ihr Auto
																					täglich und es soll dabei mit
																					möglichst wenig Aufwand schön
																					aussehen? Dann ist die
																					Keramikbeschichtung durch die
																					easy-to-clean Beschichtung das
																					richtige für Sie. Sie hält im
																					Minimum 36 Monate und ist
																					waschstrassenfest. Für jede
																					Keramikbeschichtung stellen wir ein
																					Zertifikat aus, welches eine
																					Haltbarkeit von minimal 3 Jahren
																					garantiert.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">Der
																						panzerglasartige Lackschutz</li>
																					<li class="circle m-0 h6">
																						Haltbarkeit 36+ Monate</li>
																					<li class="circle m-0 h6">
																						Waschstrassenfest</li>
																				</ul>
																			</div>
																			<div class="right col-sm-12 col-md-5"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<h3 class="h4">ServFaces - das
																						perfekte Keramik-Material</h3>
																					<p class="h6">Mit ServFaces haben
																						wir einen hervorragenden Partner
																						in Deutschland. Aufgrund der in
																						jeglicher Hinsicht überragenden
																						Resultate mit ServFaces bieten
																						wir unseren Kunden eine Garantie
																						von drei Jahren auf eine
																						Keramikbeschichtung aus unserem
																						Hause.</p>
																					<p class="h6">Seit Anfang 2016 haben
																						wir übrigens auch die offizielle
																						Vertretung von ServFaces für die
																						Schweiz und Deutschland
																						übernommen.</p>
																					<p class="h6">Jetzt Termin
																						vereinbaren</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-five bike-new-schutz-2"
												data-info="lackschutzfolie">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_right">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-7">
																				<p class="h6">Eine volltransparente
																					Lackschutzfolie ist der beste Schutz
																					gegen Steinschlag. Die von uns
																					verwendete Steinschlagschutzfolie
																					ist sehr dick und dadurch auch ganz
																					besonders widerstandsfähig.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">
																						Hochtransparent/unsichtbar</li>
																					<li class="circle m-0 h6">Der
																						optimale Schutz gegen
																						Steinschlag</li>
																					<li class="circle m-0 h6">
																						Waschstrassenfest</li>
																				</ul>
																			</div>
																			<div class="right col-sm-12 col-md-5"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<h3 class="h4">Besonders geeignet
																						für:</h3>
																					<ul class="m-0 mb-2">
																						<li class="circle m-0 h6">
																							Ladekante und alle Türkanten
																						</li>
																						<li class="circle m-0 h6">
																							Stossstange und Motorhaube
																						</li>
																						<li class="circle m-0 h6">
																							Generell alle exponierten
																							Stellen</li>
																					</ul>
																					<p class="h6">Die Preise für eine
																						Steinschlagschutzfolie
																						variieren. Sie sind abhängig von
																						der Beschaffenheit des Autos und
																						der Anzahl der Stellen, welche
																						mit einer Lackschutzfolie
																						ausgestattet werden sollen.
																						Kommen Sie vorbei, wir
																						unterbreiten Ihnen gerne ein
																						unverbindliches Angebot!</p>
																					<p class="h6">Jetzt Termin
																						vereinbaren</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-4 my-1">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5 mt-3">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-six bike-new-design-1"
												data-info="voll-folierung">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_right">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-6">
																				<p class="h6">Unser neuester Hit, eine
																					Keramikbeschichtung hält Minimum 36
																					Monate und ist absolut
																					waschstrassenfest. Besonders
																					geeignet ist unsere
																					Keramikbeschichtung für dunkle,
																					grosse Autos, welche im
																					Alltagseinsatz stehen. Jede
																					Keramikbeschichtung erhält ein
																					Zertifikat, welches die Haltbarkeit
																					von Minimum 3 Jahren garantiert.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">Der
																						panzerglasartige Lackschutz</li>
																					<li class="circle m-0 h6">
																						Haltbarkeit 36+ Monate</li>
																					<li class="circle m-0 h6">
																						Waschstrassenfest</li>
																				</ul>
																			</div>
																			<div class="right col-sm-12 col-md-6"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<h1 class="h4">Unser neues
																						Keramik-Material ServFaces</h1>
																					<p class="h6">In den ersten 3 Jahren
																						unserer Arbeit mit sogenannten
																						Keramikbeschichtungen, haben wir
																						mit einem fernöstlichen Produkt
																						gearbeitet. Bis wir feststellen
																						mussten, dass sich das Material
																						auf einmal anders verhält und
																						auch anders zu verarbeiten ist.
																						Für unsere Firma ein absolutes
																						"NO GO". Wir konnten jedoch in
																						kürzester Zeit eine
																						Ersatz-Produktlinie aus
																						deutscher Produktion finden und
																						haben seit nunmehr 2 Jahren nur
																						die besten Erfahrungen gemacht.
																						Und so konnten wir unsere
																						Garantie von 2 auf 3 Jahre
																						erhöhen. Ab Anfang 2016 haben
																						wir deshalb auch die Vertretung
																						von ServFaces für die Schweiz
																						und Deutschland übernommen.</p>
																					<p class="pt-3 h6">Anbei finden Sie
																						die komplette Preisliste für die
																						Aussen- und Innenaufbereitung
																						inklusive detaillierter
																						Beschreibung unserer Leistungen.
																					</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-six bike-new-design-2"
												data-info="beschriftung">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_right">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-6">
																				<p class="h6">Unser neuester Hit, eine
																					Keramikbeschichtung hält Minimum 36
																					Monate und ist absolut
																					waschstrassenfest. Besonders
																					geeignet ist unsere
																					Keramikbeschichtung für dunkle,
																					grosse Autos, welche im
																					Alltagseinsatz stehen. Jede
																					Keramikbeschichtung erhält ein
																					Zertifikat, welches die Haltbarkeit
																					von Minimum 3 Jahren garantiert.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">Der
																						panzerglasartige Lackschutz</li>
																					<li class="circle m-0 h6">
																						Haltbarkeit 36+ Monate</li>
																					<li class="circle m-0 h6">
																						Waschstrassenfest</li>
																				</ul>
																			</div>
																			<div class="right col-sm-12 col-md-6"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<h1 class="h4">Unser neues
																						Keramik-Material ServFaces</h1>
																					<p class="h6">In den ersten 3 Jahren
																						unserer Arbeit mit sogenannten
																						Keramikbeschichtungen, haben wir
																						mit einem fernöstlichen Produkt
																						gearbeitet. Bis wir feststellen
																						mussten, dass sich das Material
																						auf einmal anders verhält und
																						auch anders zu verarbeiten ist.
																						Für unsere Firma ein absolutes
																						"NO GO". Wir konnten jedoch in
																						kürzester Zeit eine
																						Ersatz-Produktlinie aus
																						deutscher Produktion finden und
																						haben seit nunmehr 2 Jahren nur
																						die besten Erfahrungen gemacht.
																						Und so konnten wir unsere
																						Garantie von 2 auf 3 Jahre
																						erhöhen. Ab Anfang 2016 haben
																						wir deshalb auch die Vertretung
																						von ServFaces für die Schweiz
																						und Deutschland übernommen.</p>
																					<p class="pt-3 h6">Anbei finden Sie
																						die komplette Preisliste für die
																						Aussen- und Innenaufbereitung
																						inklusive detaillierter
																						Beschreibung unserer Leistungen.
																					</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tooltip-main service-six bike-new-design-3"
												data-info="teilfolierung">
												<div class="tooltip">
													<div id="myModalCustom" class="modal_custom custom_modal_right">
														<div class="modal-content">
															<div class="modal-body">
																<div id="" class="section-car-ceramic">
																	<div class="container">
																		<div class="row">
																			<div class="left col-sm-12 col-md-6">
																				<p class="h6">Unser neuester Hit, eine
																					Keramikbeschichtung hält Minimum 36
																					Monate und ist absolut
																					waschstrassenfest. Besonders
																					geeignet ist unsere
																					Keramikbeschichtung für dunkle,
																					grosse Autos, welche im
																					Alltagseinsatz stehen. Jede
																					Keramikbeschichtung erhält ein
																					Zertifikat, welches die Haltbarkeit
																					von Minimum 3 Jahren garantiert.</p>
																				<ul class="m-0">
																					<li class="circle m-0 h6">Der
																						panzerglasartige Lackschutz</li>
																					<li class="circle m-0 h6">
																						Haltbarkeit 36+ Monate</li>
																					<li class="circle m-0 h6">
																						Waschstrassenfest</li>
																				</ul>
																			</div>
																			<div class="right col-sm-12 col-md-6"
																				data-animation="fadeInRight">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																					alt="autokosmetik">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="services2-car-bg-2 py-5">
																	<div class="container">
																		<div class="row">
																			<div class="ez-animate col-sm-12 col-md-5 text-center"
																				data-animation="fadeInLeft">
																				<img class="img-fluid"
																					src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																					alt="autokosmetik">
																			</div>
																			<div class="col-sm-12 col-md-7">
																				<div class="ceramic-coating-pading">
																					<h1 class="h4">Unser neues
																						Keramik-Material ServFaces</h1>
																					<p class="h6">In den ersten 3 Jahren
																						unserer Arbeit mit sogenannten
																						Keramikbeschichtungen, haben wir
																						mit einem fernöstlichen Produkt
																						gearbeitet. Bis wir feststellen
																						mussten, dass sich das Material
																						auf einmal anders verhält und
																						auch anders zu verarbeiten ist.
																						Für unsere Firma ein absolutes
																						"NO GO". Wir konnten jedoch in
																						kürzester Zeit eine
																						Ersatz-Produktlinie aus
																						deutscher Produktion finden und
																						haben seit nunmehr 2 Jahren nur
																						die besten Erfahrungen gemacht.
																						Und so konnten wir unsere
																						Garantie von 2 auf 3 Jahre
																						erhöhen. Ab Anfang 2016 haben
																						wir deshalb auch die Vertretung
																						von ServFaces für die Schweiz
																						und Deutschland übernommen.</p>
																					<p class="pt-3 h6">Anbei finden Sie
																						die komplette Preisliste für die
																						Aussen- und Innenaufbereitung
																						inklusive detaillierter
																						Beschreibung unserer Leistungen.
																					</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="ceramic-coting-images">
																	<div class="container">
																		<div class="row">
																			<div class="related-projects col-12">
																				<div class="row">
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																					<div
																						class="item col-sm-12 col-md-3 my-3">
																						<a data-fancybox="gallery"
																							href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																							<img class="w-100"
																								src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																								alt="autokosmetik">
																						</a>
																					</div>
																				</div>
																			</div>
																			<div class="col-12 text-center pb-5">
																				<a href="{{ route('bookappointment')}}"
																					class="btn-1">Einen Termin
																					verabreden</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="bg-white pt-5 page-section-pt car-bike-small-1" role="button">
									<div class="container-fluid feature-car-area">
										<div class="row">
											<div class="col-md-3">
												<div class="home-service-image">
													<img src="{{ asset('assets/frontend/images/car-service.png') }}"
														class="w-100 img-fluid" alt="autokosmetik">
												</div>

											</div>
										</div>
									</div>
								</div>
								<div class="bg-white page-section-pt pb-5 white-bg car-bike-small-2" role="button">
									<div class="container-fluid feature-car-area">
										<div class="row">
											<div class="col-md-2">
												<div class="home-service-image">
													<img src="{{ asset('assets/frontend/images/bike-2.png') }}"
														class="w-100 img-fluid" alt="autokosmetik">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordionExample">
						<div class="car-bike-1 row">
							<div class="col-md-4">
								<div id="section-services1" class="feature-car-area pt-2">
									<div class="container">
										<div class="row modal-main service-accordian-main">
											<div class="col-12 mb-3">
												<div class="service-level-1 car-care-icon bg-white shadow-sm"
													style="background: url({{ asset('assets/frontend/images/car-care-bg.png')}}) no-repeat center / cover;"
													role="button" data-class1="service-first">
													<div class="align-items-center d-flex flex-wrap service-title-wrap">
														<div class="avatar-sm mb-3 ml-0 mr-2">
															<img src="{{ asset('assets/frontend/images/car-care.png')}}"
																alt="" class="img-fluid">
														</div>
														<h4 class="h3 font-weight-bold mb-0">Pflege</h4>
														<p>Perfekte Pflege für Ihr Fahrzeug: Auto-Innenreinigung,
															Lederpflege und –reparatur sowie Aussenaufbereitung.</p>
													</div>
												</div>
											</div>
											<div class="col-12 mb-3">
												<div class="service-level-1 car-care-icon bg-white shadow-sm"
													style="background: url({{ asset('assets/frontend/images/car-protect-bg.jpg')}}) no-repeat center / cover;"
													role="button" data-class1="service-five">
													<div class="align-items-center d-flex flex-wrap service-title-wrap">
														<div class="avatar-sm mb-3 ml-0 mr-2">
															<img src="{{ asset('assets/frontend/images/car-protect.png')}}"
																alt="" class="img-fluid">
														</div>
														<h4 class="h3 font-weight-bold mb-0">Schutz</h4>
														<p>Steinschlagschutzfolien, eine Keramikbeschichtung und eine
															Scheibentönung schützen Ihr Auto oder Ihr Motorrad.</p>
													</div>
												</div>
											</div>
											<div class="col-12 mb-3">
												<div class="service-level-1 car-care-icon bg-white shadow-sm"
													style="background: url({{ asset('assets/frontend/images/paint-1.jpg')}}) no-repeat center / cover;"
													role="button" data-class1="service-six">
													<div class="align-items-center d-flex flex-wrap service-title-wrap">
														<div class="avatar-sm mb-3 ml-0 mr-2">
															<img src="{{ asset('assets/frontend/images/car-foil.png')}}"
																alt="" class="img-fluid">
														</div>
														<h4 class="h3 font-weight-bold mb-0">Design</h4>
														<p>Interieur-Design, Verchromungen, Wassertransferdruck und eine
															Autofolie machen Ihr Fahrzeug zum Blickfang.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div id="section-miscellaneous1-1" class="bg-white page-section-pt feature-car-area">
									<div class="container">
										<div class="row">
											<div class="col-md-12">
												<div class="home-service-image">
													<img src="{{ asset('assets/frontend/images/vintage-car.png') }}"
														class="w-100 img-fluid" alt="autokosmetik">
												</div>
												<div class="tooltip-main service-first car-old-pflege-1"
													data-info="swissvax fahrzeugaufbereitung">
													<div class="tooltip">
														<div id="myModalCustom" class="modal_custom">
															<div class="modal-content">
																<div class="modal-body">
																	<div class="services2-car-bg-2 py-3">
																		<div class="container">
																			<div class="row">
																				<div class="ez-animate col-sm-12 col-md-5 text-center"
																					data-animation="fadeInLeft">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}"
																						alt="autokosmetik">
																				</div>
																				<div class="col-sm-12 col-md-7">
																					<div class="ceramic-coating-pading">
																						<h1 class="h4">INNENAUFBEREITUNG
																							HERKÖMMLICH</h1>
																						<p class="h6"> Bei einer
																							herkömmlichen Innenaufbereitung
																							arbeiten wir mit einem
																							neutralen, ökologischen
																							Universalreiniger. Der ganze
																							Innenraum inkl. Kofferraum wird
																							gesaugt, Teppiche und Sitze
																							werden shampooniert bzw. das
																							Leder wird gereinigt und mit
																							einer professionellen Ledermilch
																							eingepflegt.<br>
																						</p>
																						<ul>
																							<li class="circle_2 m-0 h6">
																								inkl. shampoonieren</li>
																							<li class="circle_2 m-0 h6">
																								Lederreinigung und -pflege
																							</li>
																							<li class="circle_2 m-0 h6">
																								Reinigung mit Tornador</li>
																						</ul>
																					</div>
																				</div>
																				<div class="pt-3 col-md-12">
																					<p class="h6">Bei der sogenannten
																						"Tornadorreinigung" wird mit sehr
																						hohem Luftdruck der Innenraum (ohne
																						Himmel) inkl. Armaturen, allen
																						Verkleidungen, Einstiegen, Fälzen
																						und Schlitzen optimal gereinigt. Zum
																						Abschluss werden die Innenscheiben
																						professionell nur mit Wasser
																						gereinigt. <br>Sitze und Teppiche
																						mit problematischen Flecken werden
																						nach dem Shampoonieren getrockenet
																						und gegebenenfalls nochmals
																						shampooniert oder händisch
																						nachbearbeitet. <br>Anbei finden Sie
																						die komplette Preisliste für die
																						Innenaufbereitung inklusive
																						detaillierter Beschreibung unserer
																						Leistungen.</p>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="ceramic-coting-images">
																		<div class="container">
																			<div class="row">
																				<div class="related-projects col-12">
																					<h2 class="h5">Reinigung mit Tornador
																					</h2>
																					<div class="row">
																						<div
																							class="item col-sm-12 col-md-3 my-3">
																							<a href="#">
																								<div class="img-container">
																									<a data-fancybox="gallery-17"
																										href="{{ asset('assets/frontend/images/auto-10.jpg') }} ">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-10.jpg') }} "
																											alt="autokosmetik">
																									</a>
																								</div>
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-3 my-3">
																							<a href="#">
																								<div class="img-container">
																									<a data-fancybox="gallery-17"
																										href="{{ asset('assets/frontend/images/auto-11.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-11.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																							</a>
																						</div>
																					</div>
																				</div>
																				<div class="col-12 text-center pb-5">
																					<a href="{{ route('bookappointment')}}"
																						class="btn-1">Einen Termin
																						verabreden</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="tooltip-main service-first car-old-pflege-2"
													data-info="laderpflege und reperatur">
													<div class="tooltip">
														<div id="myModalCustom" class="modal_custom custom_modal_right">
															<div class="modal-content">
																<div class="modal-body">
																	<div id="" class="section-car-ceramic">
																		<div class="container">
																			<div class="row">
																				<div class="left col-sm-12 col-md-6">
																					<p class="h6">Unser neuester Hit, eine
																						Keramikbeschichtung hält Minimum 36
																						Monate und ist absolut
																						waschstrassenfest. Besonders
																						geeignet ist unsere
																						Keramikbeschichtung für dunkle,
																						grosse Autos, welche im
																						Alltagseinsatz stehen. Jede
																						Keramikbeschichtung erhält ein
																						Zertifikat, welches die Haltbarkeit
																						von Minimum 3 Jahren garantiert.</p>
																					<ul class="m-0">
																						<li class="circle m-0 h6">Der
																							panzerglasartige Lackschutz</li>
																						<li class="circle m-0 h6">
																							Haltbarkeit 36+ Monate</li>
																						<li class="circle m-0 h6">
																							Waschstrassenfest</li>
																					</ul>
																				</div>
																				<div class="right col-sm-12 col-md-6"
																					data-animation="fadeInRight">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																						alt="autokosmetik">
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="services2-car-bg-2 py-5">
																		<div class="container">
																			<div class="row">
																				<div class="ez-animate col-sm-12 col-md-5 text-center"
																					data-animation="fadeInLeft">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																						alt="autokosmetik">
																				</div>
																				<div class="col-sm-12 col-md-7">
																					<div class="ceramic-coating-pading">
																						<h1 class="h4">Unser neues
																							Keramik-Material ServFaces</h1>
																						<p class="h6">In den ersten 3 Jahren
																							unserer Arbeit mit sogenannten
																							Keramikbeschichtungen, haben wir
																							mit einem fernöstlichen Produkt
																							gearbeitet. Bis wir feststellen
																							mussten, dass sich das Material
																							auf einmal anders verhält und
																							auch anders zu verarbeiten ist.
																							Für unsere Firma ein absolutes
																							"NO GO". Wir konnten jedoch in
																							kürzester Zeit eine
																							Ersatz-Produktlinie aus
																							deutscher Produktion finden und
																							haben seit nunmehr 2 Jahren nur
																							die besten Erfahrungen gemacht.
																							Und so konnten wir unsere
																							Garantie von 2 auf 3 Jahre
																							erhöhen. Ab Anfang 2016 haben
																							wir deshalb auch die Vertretung
																							von ServFaces für die Schweiz
																							und Deutschland übernommen.</p>
																						<p class="pt-3 h6">Anbei finden Sie
																							die komplette Preisliste für die
																							Aussen- und Innenaufbereitung
																							inklusive detaillierter
																							Beschreibung unserer Leistungen.
																						</p>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="ceramic-coting-images">
																		<div class="container">
																			<div class="row">
																				<div class="related-projects col-12">
																					<div class="row">
																						<div
																							class="item col-sm-12 col-md-3 my-3">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-3 my-3">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-3 my-3">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-3 my-3">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-3 my-3">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-3 my-3">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-3 my-3">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-3 my-3">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-3 my-3">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																					</div>
																				</div>
																				<div class="col-12 text-center pb-5">
																					<a href="{{ route('bookappointment')}}"
																						class="btn-1">Einen Termin
																						verabreden</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="tooltip-main service-five car-old-schutz-1"
													data-info="lackschutz">
													<div class="tooltip">
														<div id="myModalCustom" class="modal_custom">
															<div class="modal-content">
																<div class="modal-body">
																	<div class="services2-car-bg-2 py-3">
																		<div class="container">
																			<div class="row">
																				<div class="ez-animate col-sm-12 col-md-5 text-center"
																					data-animation="fadeInLeft">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}"
																						alt="autokosmetik">
																				</div>
																				<div class="col-sm-12 col-md-7">
																					<div class="ceramic-coating-pading">
																						<h1 class="h4">INNENAUFBEREITUNG
																							HERKÖMMLICH</h1>
																						<p class="h6"> Bei einer
																							herkömmlichen Innenaufbereitung
																							arbeiten wir mit einem
																							neutralen, ökologischen
																							Universalreiniger. Der ganze
																							Innenraum inkl. Kofferraum wird
																							gesaugt, Teppiche und Sitze
																							werden shampooniert bzw. das
																							Leder wird gereinigt und mit
																							einer professionellen Ledermilch
																							eingepflegt.<br>
																						</p>
																						<ul>
																							<li class="circle_2 m-0 h6">
																								inkl. shampoonieren</li>
																							<li class="circle_2 m-0 h6">
																								Lederreinigung und -pflege
																							</li>
																							<li class="circle_2 m-0 h6">
																								Reinigung mit Tornador</li>
																						</ul>
																					</div>
																				</div>
																				<div class="pt-3 col-md-12">
																					<p class="h6">Bei der sogenannten
																						"Tornadorreinigung" wird mit sehr
																						hohem Luftdruck der Innenraum (ohne
																						Himmel) inkl. Armaturen, allen
																						Verkleidungen, Einstiegen, Fälzen
																						und Schlitzen optimal gereinigt. Zum
																						Abschluss werden die Innenscheiben
																						professionell nur mit Wasser
																						gereinigt. <br>Sitze und Teppiche
																						mit problematischen Flecken werden
																						nach dem Shampoonieren getrockenet
																						und gegebenenfalls nochmals
																						shampooniert oder händisch
																						nachbearbeitet. <br>Anbei finden Sie
																						die komplette Preisliste für die
																						Innenaufbereitung inklusive
																						detaillierter Beschreibung unserer
																						Leistungen.</p>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="ceramic-coting-images">
																		<div class="container">
																			<div class="row">
																				<div class="related-projects col-12">
																					<h2 class="h5">Reinigung mit Tornador
																					</h2>
																					<div class="row">
																						<div
																							class="item col-sm-12 col-md-3 my-3">
																							<a href="#">
																								<div class="img-container">
																									<a data-fancybox="gallery-17"
																										href="{{ asset('assets/frontend/images/auto-10.jpg') }} ">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-10.jpg') }} "
																											alt="autokosmetik">
																									</a>
																								</div>
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-3 my-3">
																							<a href="#">
																								<div class="img-container">
																									<a data-fancybox="gallery-17"
																										href="{{ asset('assets/frontend/images/auto-11.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-11.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																							</a>
																						</div>
																					</div>
																				</div>
																				<div class="col-12 text-center pb-5">
																					<a href="{{ route('bookappointment')}}"
																						class="btn-1">Einen Termin
																						verabreden</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="tooltip-main service-five car-old-schutz-2"
													data-info="keramikbeschichtung">
													<div class="tooltip">
														<div id="myModalCustom" class="modal_custom custom_modal_right">
															<div class="modal-content">
																<div class="modal-body">
																	<div id="" class="section-car-ceramic">
																		<div class="container">
																			<div class="row">
																				<div class="left col-sm-12 col-md-7">
																					<p class="h6">Sie fahren Ihr Auto
																						täglich und es soll dabei mit
																						möglichst wenig Aufwand schön
																						aussehen? Dann ist die
																						Keramikbeschichtung durch die
																						easy-to-clean Beschichtung das
																						richtige für Sie. Sie hält im
																						Minimum 36 Monate und ist
																						waschstrassenfest. Für jede
																						Keramikbeschichtung stellen wir ein
																						Zertifikat aus, welches eine
																						Haltbarkeit von minimal 3 Jahren
																						garantiert.</p>
																					<ul class="m-0">
																						<li class="circle m-0 h6">Der
																							panzerglasartige Lackschutz</li>
																						<li class="circle m-0 h6">
																							Haltbarkeit 36+ Monate</li>
																						<li class="circle m-0 h6">
																							Waschstrassenfest</li>
																					</ul>
																				</div>
																				<div class="right col-sm-12 col-md-5"
																					data-animation="fadeInRight">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																						alt="autokosmetik">
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="services2-car-bg-2 py-5">
																		<div class="container">
																			<div class="row">
																				<div class="ez-animate col-sm-12 col-md-5 text-center"
																					data-animation="fadeInLeft">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																						alt="autokosmetik">
																				</div>
																				<div class="col-sm-12 col-md-7">
																					<div class="ceramic-coating-pading">
																						<h3 class="h4">ServFaces - das
																							perfekte Keramik-Material</h3>
																						<p class="h6">Mit ServFaces haben
																							wir einen hervorragenden Partner
																							in Deutschland. Aufgrund der in
																							jeglicher Hinsicht überragenden
																							Resultate mit ServFaces bieten
																							wir unseren Kunden eine Garantie
																							von drei Jahren auf eine
																							Keramikbeschichtung aus unserem
																							Hause.</p>
																						<p class="h6">Seit Anfang 2016 haben
																							wir übrigens auch die offizielle
																							Vertretung von ServFaces für die
																							Schweiz und Deutschland
																							übernommen.</p>
																						<p class="h6">Jetzt Termin
																							vereinbaren</p>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="ceramic-coting-images">
																		<div class="container">
																			<div class="row">
																				<div class="related-projects col-12">
																					<div class="row">
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																					</div>
																				</div>
																				<div class="col-12 text-center pb-5 mt-3">
																					<a href="{{ route('bookappointment')}}"
																						class="btn-1">Einen Termin
																						verabreden</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="tooltip-main service-six car-old-design-1"
													data-info="beschriftungen">
													<div class="tooltip">
														<div id="myModalCustom" class="modal_custom custom_modal_right">
															<div class="modal-content">
																<div class="modal-body">
																	<div id="" class="section-car-ceramic">
																		<div class="container">
																			<div class="row">
																				<div class="left col-sm-12 col-md-7">
																					<p class="h6">Wer nicht wirbt, stirbt –
																						so der Volksmund. Und es steckt
																						durchaus Wahrheit in diesem Spruch.
																						Neben On- und Offlinewerbung in
																						Medien verschiedenster Art bieten
																						Firmenfahrzeuge eine preiswerte und
																						sehr effektive Werbefläche. Gerade
																						Fahrzeuge, welche oft unterwegs
																						sind, können sehr viel
																						Aufmerksamkeit erwecken – zu einem
																						vergleichsweise tiefen Preis. Dies
																						ist auch der Grund, dass sich
																						Autowerbung einer derart hohen
																						Beliebtheit erfreut.</p>
																				</div>
																				<div class="right col-sm-12 col-md-5"
																					data-animation="fadeInRight">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																						alt="autokosmetik">
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="services2-car-bg-2 py-5">
																		<div class="container">
																			<div class="row">
																				<div class="ez-animate col-sm-12 col-md-5 text-center"
																					data-animation="fadeInLeft">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																						alt="autokosmetik">
																				</div>
																				<div class="col-sm-12 col-md-7">
																					<div class="ceramic-coating-pading">
																						<p class="h6">Beauftragen Sie uns
																							mit der Autobeschriftung oder
																							Lastwagenbeschriftung.
																							Fahrzeugbeschriftungen in der
																							Form von einer Fahrzeugfolierung
																							können sowohl auf Teilflächen
																							(Teilfolierung) als auch über
																							das gesamte Fahrzeug
																							(Vollfolierung) erstellt werden.
																						</p>
																						<p class="h6">Übrigens: Für
																							Fahrzeugbeschriftungen einer
																							ganzen Flotte
																							(Flottenbeschriftung) gewähren
																							wir attraktive Ermässigungen.
																							Fragen Sie uns jetzt an!</p>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="ceramic-coting-images">
																		<div class="container">
																			<div class="row">
																				<div class="related-projects col-12">
																					<div class="row">
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																					</div>
																				</div>
																				<div class="col-12 text-center pb-5 mt-3">
																					<a href="{{ route('bookappointment')}}"
																						class="btn-1">Einen Termin
																						verabreden</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="bg-white pt-5 page-section-pt car-bike-small-1" role="button">
									<div class="container-fluid feature-car-area">
										<div class="row">
											<div class="col-md-3">
												<div class="home-service-image">
													<img src="{{ asset('assets/frontend/images/vintage-car.png') }}"
														class="w-100 img-fluid" alt="autokosmetik">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="bg-white page-section-pt pb-5 white-bg car-bike-small-2" role="button">
									<div class="container-fluid feature-car-area">
										<div class="row">
											<div class="col-md-2">
												<div class="home-service-image">
													<img src="{{ asset('assets/frontend/images/vinatge-bike.png') }}"
														class="w-100 img-fluid" alt="autokosmetik">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="car-bike-2 row">
							<div class="col-md-4">
								<div id="section-services1" class="feature-car-area">
									<div class="container">
										<div class="row modal-main service-accordian-main">

											<div class="col-12 mb-3">
												<div class="service-level-1 car-care-icon  bg-white shadow-sm active"
													style="background: url({{ asset('assets/frontend/images/car-care-bg.png')}}) no-repeat center / cover;"
													role="button" data-class1="service-four">
													<div class="align-items-center d-flex flex-wrap service-title-wrap">
														<div class="avatar-sm mb-3 ml-0 mr-2">
															<img src="{{ asset('assets/frontend/images/car-care.png')}}"
																alt="" class="img-fluid">
														</div>
														<h4 class="h3 font-weight-bold mb-0">Pflege</h4>
														<p>Perfekte Pflege für Ihr Fahrzeug: Auto-Innenreinigung,
															Lederpflege und –reparatur sowie Aussenaufbereitung.</p>
														<!-- <span class="text-primary btn-link">Read more</span> -->
													</div>
												</div>
											</div>
											<div class="col-12 mb-3">
												<div class="service-level-1 car-care-icon  bg-white shadow-sm"
													style="background: url({{ asset('assets/frontend/images/car-protect-bg.jpg')}}) no-repeat center / cover;"
													role="button" data-class1="service-five">
													<div class="align-items-center d-flex flex-wrap service-title-wrap">
														<div class="avatar-sm mb-3 ml-0 mr-2">
															<img src="{{ asset('assets/frontend/images/car-protect.png')}}"
																alt="" class="img-fluid">
														</div>
														<h4 class="h3 font-weight-bold mb-0">Schutz</h4>
														<p>Steinschlagschutzfolien, eine Keramikbeschichtung und eine
															Scheibentönung schützen Ihr Auto oder Ihr Motorrad.</p>
														<!-- <span class="text-primary btn-link">Read more</span> -->
													</div>
												</div>
											</div>
											<div class="col-12 mb-3">
												<div class="service-level-1 car-care-icon  bg-white shadow-sm"
													style="background: url({{ asset('assets/frontend/images/car-enhance-bg.png')}}) no-repeat center / cover;"
													role="button" data-class1="service-six">
													<div class="align-items-center d-flex flex-wrap service-title-wrap">
														<div class="avatar-sm mb-3 ml-0 mr-2">
															<img src="{{ asset('assets/frontend/images/car-enhance.png')}}"
																alt="" class="img-fluid">
														</div>
														<h4 class="h3 font-weight-bold mb-0">Design</h4>
														<p>Interieur-Design, Verchromungen, Wassertransferdruck und eine
															Autofolie machen Ihr Fahrzeug zum Blickfang.</p>
														<!-- <span class="text-primary btn-link">Read more</span> -->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div id="section-miscellaneous1-1" class="bg-white page-section-pt">
									<div class="container">
										<div class="row justify-content-center">
											<div class="col-md-10">
												<div class="home-service-image">
													<img src="{{ asset('assets/frontend/images/vinatge-bike.png') }}"
														class="w-100 img-fluid" alt="autokosmetik">
												</div>
												<div class="tooltip-main service-four bike-old-pflege-1"
													data-info="lederpflege">
													<div class="tooltip">
														<div id="myModalCustom" class="modal_custom">
															<div class="modal-content">
																<div class="modal-body">
																	<div id="" class="section-car-ceramic">
																		<div class="container">
																			<div class="row">
																				<div class="left col-sm-12 col-md-7">
																					<p class="h6">Leder benötigt besondere
																						Pflege. Das gilt besonders für stark
																						beanspruchte Autositze aus Leder.
																						Doch auch Lenkrad, Schaltknüppel und
																						Armaturen sind oft mit Leder
																						überzogen und können sich durch den
																						Gebrauch abnutzen.</p>
																					<p class="h6">Vertrauen Sie uns die
																						Lederpflege Ihres Autos an. Bei
																						einer Grundreinigung mit Bürste und
																						Staubsauger entfernen wir groben
																						Schmutz. Anschliessend geht es an
																						die Tiefenreinigung. Hierfür
																						verwenden wir eine milde
																						Pflegemilch, welche die Rückfettung
																						ausgetrockneter Stellen
																						sicherstellt. Unser Mittel enthält
																						zudem einen UV-Schutz, um das
																						Material gegen das Ausbleichen durch
																						die Sonne zu schützen.</p>
																				</div>
																				<div class="right col-sm-12 col-md-5"
																					data-animation="fadeInRight">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																						alt="autokosmetik">
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="services2-car-bg-2 py-5">
																		<div class="container">
																			<div class="row">
																				<div class="ez-animate col-sm-12 col-md-5 text-center"
																					data-animation="fadeInLeft">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																						alt="autokosmetik">
																				</div>
																				<div class="col-sm-12 col-md-7">
																					<div class="ceramic-coating-pading">
																						<p class="h6">Wenn Leder
																							unansehnlich geworden ist, muss
																							es nicht zwingend ersetzt
																							werden. Kleine Risse und Löcher
																							können wir mit Flüssigleder
																							füllen. Anschliessend glätten
																							wir die Oberflächen und gleichen
																							die Farben an. Zum Abschluss der
																							Lederreparatur versiegeln wir
																							die Oberflächen, um das Leder
																							gegen Abrieb und Kratzer zu
																							schützen.</p>
																						<p class="h6">Unsere erstklassigen
																							Reinigungsprodukte helfen
																							übrigens, auch wenn stark
																							beanspruchtes Leder einfach
																							unansehnlich geworden ist. Durch
																							Färben können wir wieder ein
																							einheitliches Äusseres
																							herstellen.</p>
																						<p class="h6">Lederpflege und
																							Lederrestauration gehören in die
																							Hände von Profis. Fragen Sie uns
																							jetzt an!</p>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="ceramic-coting-images">
																		<div class="container">
																			<div class="row">
																				<div class="related-projects col-12">
																					<div class="row">
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																					</div>
																				</div>
																				<div class="col-12 text-center pb-5 mt-3">
																					<a href="{{ route('bookappointment')}}"
																						class="btn-1">Einen Termin
																						verabreden</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="tooltip-main service-four bike-old-pflege-2"
													data-info="lackpflege">
													<div class="tooltip">
														<div class="info-main info-wash">
															<div class="info modal_animation_main custom_modal_right_info">
																<h5 class="text-uppercase">LOREM IPSUM</h5>
																<ul class="m-0 pl-4">
																	<li>Lorem, ipsum.</li>
																	<li>Lorem, ipsum.</li>
																	<li>Lorem, ipsum.</li>
																</ul>
																<div id="myModalCustom"
																	class="modal_custom custom_modal_right">
																	<div class="modal-content">
																		<div class="modal-body">
																			<div id="" class="section-car-ceramic">
																				<div class="container">
																					<div class="row">
																						<div
																							class="left col-sm-12 col-md-6">
																							<p class="h6">Unser neuester
																								Hit, eine
																								Keramikbeschichtung hält
																								Minimum 36 Monate und ist
																								absolut waschstrassenfest.
																								Besonders geeignet ist
																								unsere Keramikbeschichtung
																								für dunkle, grosse Autos,
																								welche im Alltagseinsatz
																								stehen. Jede
																								Keramikbeschichtung erhält
																								ein Zertifikat, welches die
																								Haltbarkeit von Minimum 3
																								Jahren garantiert.</p>
																							<ul class="m-0">
																								<li class="circle m-0 h6">
																									Der panzerglasartige
																									Lackschutz</li>
																								<li class="circle m-0 h6">
																									Haltbarkeit 36+ Monate
																								</li>
																								<li class="circle m-0 h6">
																									Waschstrassenfest</li>
																							</ul>
																						</div>
																						<div class="right col-sm-12 col-md-6"
																							data-animation="fadeInRight">
																							<img class="img-fluid"
																								src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																								alt="autokosmetik">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="services2-car-bg-2 py-5">
																				<div class="container">
																					<div class="row">
																						<div class="ez-animate col-sm-12 col-md-5 text-center"
																							data-animation="fadeInLeft">
																							<img class="img-fluid"
																								src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																								alt="autokosmetik">
																						</div>
																						<div class="col-sm-12 col-md-7">
																							<div
																								class="ceramic-coating-pading">
																								<h1 class="h4">Unser neues
																									Keramik-Material
																									ServFaces</h1>
																								<p class="h6">In den ersten
																									3 Jahren unserer Arbeit
																									mit sogenannten
																									Keramikbeschichtungen,
																									haben wir mit einem
																									fernöstlichen Produkt
																									gearbeitet. Bis wir
																									feststellen mussten,
																									dass sich das Material
																									auf einmal anders
																									verhält und auch anders
																									zu verarbeiten ist. Für
																									unsere Firma ein
																									absolutes "NO GO". Wir
																									konnten jedoch in
																									kürzester Zeit eine
																									Ersatz-Produktlinie aus
																									deutscher Produktion
																									finden und haben seit
																									nunmehr 2 Jahren nur die
																									besten Erfahrungen
																									gemacht. Und so konnten
																									wir unsere Garantie von
																									2 auf 3 Jahre erhöhen.
																									Ab Anfang 2016 haben wir
																									deshalb auch die
																									Vertretung von ServFaces
																									für die Schweiz und
																									Deutschland übernommen.
																								</p>
																								<p class="pt-3 h6">Anbei
																									finden Sie die komplette
																									Preisliste für die
																									Aussen- und
																									Innenaufbereitung
																									inklusive detaillierter
																									Beschreibung unserer
																									Leistungen.</p>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="ceramic-coting-images">
																				<div class="container">
																					<div class="row">
																						<div
																							class="related-projects col-12">
																							<div class="row">
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																							</div>
																						</div>
																						<div
																							class="col-12 text-center pb-5">
																							<a href="{{ route('bookappointment')}}"
																								class="btn-1">Einen Termin
																								verabreden</a>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="tooltip-main service-four bike-old-pflege-3"
													data-info="metallpflege">
													<div class="tooltip">
														<div class="info-main info-wash">
															<div class="info modal_animation_main custom_modal_right_info">
																<h5 class="text-uppercase">LOREM IPSUM</h5>
																<ul class="m-0 pl-4">
																	<li>Lorem, ipsum.</li>
																	<li>Lorem, ipsum.</li>
																	<li>Lorem, ipsum.</li>
																</ul>
																<div id="myModalCustom"
																	class="modal_custom custom_modal_right">
																	<div class="modal-content">
																		<div class="modal-body">
																			<div id="" class="section-car-ceramic">
																				<div class="container">
																					<div class="row">
																						<div
																							class="left col-sm-12 col-md-6">
																							<p class="h6">Unser neuester
																								Hit, eine
																								Keramikbeschichtung hält
																								Minimum 36 Monate und ist
																								absolut waschstrassenfest.
																								Besonders geeignet ist
																								unsere Keramikbeschichtung
																								für dunkle, grosse Autos,
																								welche im Alltagseinsatz
																								stehen. Jede
																								Keramikbeschichtung erhält
																								ein Zertifikat, welches die
																								Haltbarkeit von Minimum 3
																								Jahren garantiert.</p>
																							<ul class="m-0">
																								<li class="circle m-0 h6">
																									Der panzerglasartige
																									Lackschutz</li>
																								<li class="circle m-0 h6">
																									Haltbarkeit 36+ Monate
																								</li>
																								<li class="circle m-0 h6">
																									Waschstrassenfest</li>
																							</ul>
																						</div>
																						<div class="right col-sm-12 col-md-6"
																							data-animation="fadeInRight">
																							<img class="img-fluid"
																								src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																								alt="autokosmetik">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="services2-car-bg-2 py-5">
																				<div class="container">
																					<div class="row">
																						<div class="ez-animate col-sm-12 col-md-5 text-center"
																							data-animation="fadeInLeft">
																							<img class="img-fluid"
																								src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																								alt="autokosmetik">
																						</div>
																						<div class="col-sm-12 col-md-7">
																							<div
																								class="ceramic-coating-pading">
																								<h1 class="h4">Unser neues
																									Keramik-Material
																									ServFaces</h1>
																								<p class="h6">In den ersten
																									3 Jahren unserer Arbeit
																									mit sogenannten
																									Keramikbeschichtungen,
																									haben wir mit einem
																									fernöstlichen Produkt
																									gearbeitet. Bis wir
																									feststellen mussten,
																									dass sich das Material
																									auf einmal anders
																									verhält und auch anders
																									zu verarbeiten ist. Für
																									unsere Firma ein
																									absolutes "NO GO". Wir
																									konnten jedoch in
																									kürzester Zeit eine
																									Ersatz-Produktlinie aus
																									deutscher Produktion
																									finden und haben seit
																									nunmehr 2 Jahren nur die
																									besten Erfahrungen
																									gemacht. Und so konnten
																									wir unsere Garantie von
																									2 auf 3 Jahre erhöhen.
																									Ab Anfang 2016 haben wir
																									deshalb auch die
																									Vertretung von ServFaces
																									für die Schweiz und
																									Deutschland übernommen.
																								</p>
																								<p class="pt-3 h6">Anbei
																									finden Sie die komplette
																									Preisliste für die
																									Aussen- und
																									Innenaufbereitung
																									inklusive detaillierter
																									Beschreibung unserer
																									Leistungen.</p>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="ceramic-coting-images">
																				<div class="container">
																					<div class="row">
																						<div
																							class="related-projects col-12">
																							<div class="row">
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																							</div>
																						</div>
																						<div
																							class="col-12 text-center pb-5">
																							<a href="{{ route('bookappointment')}}"
																								class="btn-1">Einen Termin
																								verabreden</a>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="tooltip-main service-five bike-old-schutz-1"
													data-info="keramikbeschichtung">
													<div class="tooltip">
														<div id="myModalCustom" class="modal_custom custom_modal_right">
															<div class="modal-content">
																<div class="modal-body">
																	<div id="" class="section-car-ceramic">
																		<div class="container">
																			<div class="row">
																				<div class="left col-sm-12 col-md-7">
																					<p class="h6">Sie fahren Ihr Auto
																						täglich und es soll dabei mit
																						möglichst wenig Aufwand schön
																						aussehen? Dann ist die
																						Keramikbeschichtung durch die
																						easy-to-clean Beschichtung das
																						richtige für Sie. Sie hält im
																						Minimum 36 Monate und ist
																						waschstrassenfest. Für jede
																						Keramikbeschichtung stellen wir ein
																						Zertifikat aus, welches eine
																						Haltbarkeit von minimal 3 Jahren
																						garantiert.</p>
																					<ul class="m-0">
																						<li class="circle m-0 h6">Der
																							panzerglasartige Lackschutz</li>
																						<li class="circle m-0 h6">
																							Haltbarkeit 36+ Monate</li>
																						<li class="circle m-0 h6">
																							Waschstrassenfest</li>
																					</ul>
																				</div>
																				<div class="right col-sm-12 col-md-5"
																					data-animation="fadeInRight">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																						alt="autokosmetik">
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="services2-car-bg-2 py-5">
																		<div class="container">
																			<div class="row">
																				<div class="ez-animate col-sm-12 col-md-5 text-center"
																					data-animation="fadeInLeft">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																						alt="autokosmetik">
																				</div>
																				<div class="col-sm-12 col-md-7">
																					<div class="ceramic-coating-pading">
																						<h3 class="h4">ServFaces - das
																							perfekte Keramik-Material</h3>
																						<p class="h6">Mit ServFaces haben
																							wir einen hervorragenden Partner
																							in Deutschland. Aufgrund der in
																							jeglicher Hinsicht überragenden
																							Resultate mit ServFaces bieten
																							wir unseren Kunden eine Garantie
																							von drei Jahren auf eine
																							Keramikbeschichtung aus unserem
																							Hause.</p>
																						<p class="h6">Seit Anfang 2016 haben
																							wir übrigens auch die offizielle
																							Vertretung von ServFaces für die
																							Schweiz und Deutschland
																							übernommen.</p>
																						<p class="h6">Jetzt Termin
																							vereinbaren</p>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="ceramic-coting-images">
																		<div class="container">
																			<div class="row">
																				<div class="related-projects col-12">
																					<div class="row">
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																					</div>
																				</div>
																				<div class="col-12 text-center pb-5 mt-3">
																					<a href="{{ route('bookappointment')}}"
																						class="btn-1">Einen Termin
																						verabreden</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="tooltip-main service-six bike-old-design-1"
													data-info="lederlackierungen">
													<div class="tooltip">
														<div class="info-main info-wash">
															<div class="info modal_animation_main custom_modal_right_info">
																<h5 class="text-uppercase">LOREM IPSUM</h5>
																<ul class="m-0 pl-4">
																	<li>Lorem, ipsum.</li>
																	<li>Lorem, ipsum.</li>
																	<li>Lorem, ipsum.</li>
																</ul>
																<div id="myModalCustom"
																	class="modal_custom custom_modal_right">
																	<div class="modal-content">
																		<div class="modal-body">
																			<div id="" class="section-car-ceramic">
																				<div class="container">
																					<div class="row">
																						<div
																							class="left col-sm-12 col-md-6">
																							<p class="h6">Unser neuester
																								Hit, eine
																								Keramikbeschichtung hält
																								Minimum 36 Monate und ist
																								absolut waschstrassenfest.
																								Besonders geeignet ist
																								unsere Keramikbeschichtung
																								für dunkle, grosse Autos,
																								welche im Alltagseinsatz
																								stehen. Jede
																								Keramikbeschichtung erhält
																								ein Zertifikat, welches die
																								Haltbarkeit von Minimum 3
																								Jahren garantiert.</p>
																							<ul class="m-0">
																								<li class="circle m-0 h6">
																									Der panzerglasartige
																									Lackschutz</li>
																								<li class="circle m-0 h6">
																									Haltbarkeit 36+ Monate
																								</li>
																								<li class="circle m-0 h6">
																									Waschstrassenfest</li>
																							</ul>
																						</div>
																						<div class="right col-sm-12 col-md-6"
																							data-animation="fadeInRight">
																							<img class="img-fluid"
																								src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																								alt="autokosmetik">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="services2-car-bg-2 py-5">
																				<div class="container">
																					<div class="row">
																						<div class="ez-animate col-sm-12 col-md-5 text-center"
																							data-animation="fadeInLeft">
																							<img class="img-fluid"
																								src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																								alt="autokosmetik">
																						</div>
																						<div class="col-sm-12 col-md-7">
																							<div
																								class="ceramic-coating-pading">
																								<h1 class="h4">Unser neues
																									Keramik-Material
																									ServFaces</h1>
																								<p class="h6">In den ersten
																									3 Jahren unserer Arbeit
																									mit sogenannten
																									Keramikbeschichtungen,
																									haben wir mit einem
																									fernöstlichen Produkt
																									gearbeitet. Bis wir
																									feststellen mussten,
																									dass sich das Material
																									auf einmal anders
																									verhält und auch anders
																									zu verarbeiten ist. Für
																									unsere Firma ein
																									absolutes "NO GO". Wir
																									konnten jedoch in
																									kürzester Zeit eine
																									Ersatz-Produktlinie aus
																									deutscher Produktion
																									finden und haben seit
																									nunmehr 2 Jahren nur die
																									besten Erfahrungen
																									gemacht. Und so konnten
																									wir unsere Garantie von
																									2 auf 3 Jahre erhöhen.
																									Ab Anfang 2016 haben wir
																									deshalb auch die
																									Vertretung von ServFaces
																									für die Schweiz und
																									Deutschland übernommen.
																								</p>
																								<p class="pt-3 h6">Anbei
																									finden Sie die komplette
																									Preisliste für die
																									Aussen- und
																									Innenaufbereitung
																									inklusive detaillierter
																									Beschreibung unserer
																									Leistungen.</p>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="ceramic-coting-images">
																				<div class="container">
																					<div class="row">
																						<div
																							class="related-projects col-12">
																							<div class="row">
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																								<div
																									class="item col-sm-12 col-md-3 my-3">
																									<a data-fancybox="gallery"
																										href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																										<img class="w-100"
																											src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																											alt="autokosmetik">
																									</a>
																								</div>
																							</div>
																						</div>
																						<div
																							class="col-12 text-center pb-5">
																							<a href="{{ route('bookappointment')}}"
																								class="btn-1">Einen Termin
																								verabreden</a>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="tooltip-main service-six bike-old-design-2"
													data-info="beschriftungen">
													<div class="tooltip">
														<div id="myModalCustom" class="modal_custom custom_modal_right">
															<div class="modal-content">
																<div class="modal-body">
																	<div id="" class="section-car-ceramic">
																		<div class="container">
																			<div class="row">
																				<div class="left col-sm-12 col-md-7">
																					<p class="h6">Wer nicht wirbt, stirbt –
																						so der Volksmund. Und es steckt
																						durchaus Wahrheit in diesem Spruch.
																						Neben On- und Offlinewerbung in
																						Medien verschiedenster Art bieten
																						Firmenfahrzeuge eine preiswerte und
																						sehr effektive Werbefläche. Gerade
																						Fahrzeuge, welche oft unterwegs
																						sind, können sehr viel
																						Aufmerksamkeit erwecken – zu einem
																						vergleichsweise tiefen Preis. Dies
																						ist auch der Grund, dass sich
																						Autowerbung einer derart hohen
																						Beliebtheit erfreut.</p>
																				</div>
																				<div class="right col-sm-12 col-md-5"
																					data-animation="fadeInRight">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
																						alt="autokosmetik">
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="services2-car-bg-2 py-5">
																		<div class="container">
																			<div class="row">
																				<div class="ez-animate col-sm-12 col-md-5 text-center"
																					data-animation="fadeInLeft">
																					<img class="img-fluid"
																						src="{{ asset('assets/frontend/images/img-car-2.jpg') }}"
																						alt="autokosmetik">
																				</div>
																				<div class="col-sm-12 col-md-7">
																					<div class="ceramic-coating-pading">
																						<p class="h6">Beauftragen Sie uns
																							mit der Autobeschriftung oder
																							Lastwagenbeschriftung.
																							Fahrzeugbeschriftungen in der
																							Form von einer Fahrzeugfolierung
																							können sowohl auf Teilflächen
																							(Teilfolierung) als auch über
																							das gesamte Fahrzeug
																							(Vollfolierung) erstellt werden.
																						</p>
																						<p class="h6">Übrigens: Für
																							Fahrzeugbeschriftungen einer
																							ganzen Flotte
																							(Flottenbeschriftung) gewähren
																							wir attraktive Ermässigungen.
																							Fragen Sie uns jetzt an!</p>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="ceramic-coting-images">
																		<div class="container">
																			<div class="row">
																				<div class="related-projects col-12">
																					<div class="row">
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-1.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-1.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-2.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-2.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-3.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-3.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-4.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-4.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-5.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-5.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-6.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-6.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-7.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-7.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-8.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-8.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																						<div
																							class="item col-sm-12 col-md-4 my-1">
																							<a data-fancybox="gallery"
																								href="{{ asset('assets/frontend/images/auto-9.jpg') }}">
																								<img class="w-100"
																									src="{{ asset('assets/frontend/images/auto-9.jpg') }}"
																									alt="autokosmetik">
																							</a>
																						</div>
																					</div>
																				</div>
																				<div class="col-12 text-center pb-5 mt-3">
																					<a href="{{ route('bookappointment')}}"
																						class="btn-1">Einen Termin
																						verabreden</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="bg-white pt-5 page-section-pt car-bike-small-1" role="button">
									<div class="container-fluid feature-car-area">
										<div class="row">
											<div class="col-md-3">
												<div class="home-service-image">
													<img src="{{ asset('assets/frontend/images/vintage-car.png') }}"
														class="w-100 img-fluid" alt="autokosmetik">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="bg-white page-section-pt pb-5 white-bg car-bike-small-2" role="button">
									<div class="container-fluid feature-car-area">
										<div class="row">
											<div class="col-md-2">
												<div class="home-service-image">
													<img src="{{ asset('assets/frontend/images/vinatge-bike.png') }}"
														class="w-100 img-fluid" alt="autokosmetik">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
					
					
				</div>
                
                
            </div>

        </div>

        <div id="services-mobile">
            <section class="mt-lg-5">
                <div class="collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#services-mobile">
                    <div class="tabpanel-service main-tabpanel-service">
                        <ul class="nav justify-content-start align-items-center nav-pills mb-5" id="pills-tab"
                            role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active show p-0 border-0" data-toggle="pill"
                                    data-target="#pills-car" type="button" role="tab" aria-controls="pills-car"
                                    aria-selected="false">
                                    <div class="card select-category border-0 rounded-0">
                                        <input role="button" type="radio" name="step_0" class="form-control checkbox">
                                        <div class="card-body px-2 py-0">
                                            <img src="{{ asset('assets/frontend/images/car-service.png') }}" alt=""
                                                class="img-fluid">
                                        </div>
                                    </div>
                                </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link p-0 border-0" data-toggle="pill" data-target="#pills-bike"
                                    type="button" role="tab" aria-controls="pills-bike" aria-selected="false">
                                    <div class="card select-category border-0 rounded-0">
                                        <input role="button" type="radio" name="step_0" class="form-control checkbox">
                                        <div class="card-body px-2 py-0">
                                            <!-- <div class="avatar-md">
													<img src="{{ asset('assets/frontend/images/form-bike.png') }}" alt="" class="img-fluid">
												</div>
												<p class="font-weight-bold h5 text-dark m-0 mb-2">Motorrad</p> -->
                                            <img src="{{ asset('assets/frontend/images/bike-2.png') }}" alt=""
                                                class="img-fluid">
                                        </div>
                                    </div>
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div class="tabpanel-service main-tabpanel-service tab-content">
                        <div class="tab-pane show active" id="pills-car" role="tabpanel">
                            <ul class="nav justify-content-center nav-pills mb-3 px-3" id="pills-tab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active bg-white" data-toggle="pill"
                                        data-target="#pills-care" type="button" role="tab" aria-controls="pills-care"
                                        aria-selected="false">
                                        <div class="service-level-1 car-care-icon bg-white p-3" role="button"
                                            data-class1="service-first">
                                            <div class="service-title-wrap">
                                                <div class="avatar-sm mb-3">
                                                    <img src="{{ asset('assets/frontend/images/car-care.png')}}" alt=""
                                                        class="img-fluid">
                                                </div>
                                                <h4 class="h6 text-center font-weight-bold mb-0 text-body">Pflege</h4>
                                            </div>
                                        </div>
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link bg-white" data-toggle="pill" data-target="#pills-protect"
                                        type="button" role="tab" aria-controls="pills-protect" aria-selected="false">
                                        <div class="service-level-1 car-care-icon bg-white p-3" role="button"
                                            data-class1="service-first">
                                            <div class="service-title-wrap">
                                                <div class="avatar-sm mb-3">
                                                    <img src="{{ asset('assets/frontend/images/car-protect.png')}}"
                                                        alt="" class="img-fluid">
                                                </div>
                                                <h4 class="h6 text-center font-weight-bold mb-0 text-body">Schutz</h4>
                                            </div>
                                        </div>
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link bg-white" data-toggle="pill" data-target="#pills-enhance"
                                        type="button" role="tab" aria-controls="pills-enhance" aria-selected="false">
                                        <div class="service-level-1 car-care-icon bg-white p-3" role="button"
                                            data-class1="service-first">
                                            <div class="service-title-wrap">
                                                <div class="avatar-sm mb-3">
                                                    <img src="{{ asset('assets/frontend/images/car-foil.png')}}" alt=""
                                                        class="img-fluid">
                                                </div>
                                                <h4 class="h6 text-center font-weight-bold mb-0 text-body">Design</h4>
                                            </div>
                                        </div>
                                    </button>
                                </li>
                            </ul>
                            <div class="tabpanel-service">
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-care" role="tabpanel">
                                        <div class="accordion px-2">
                                            <div class="care-for-carousel owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm" data-toggle="collapse"
                                                        data-target="#collapse1_b_1" aria-expanded="false"
                                                        aria-controls="collapse1_b_1" role="button">
                                                        <a href="{{ route('car_interior_cleaning') }}"
                                                            class="plegemobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('car_interior_cleaning') }}"
                                                                class="plegemobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Innenreinigung
                                                                </h5>
                                                                <p class="h6 py-2 pb-3">Bei einer Auto-Innenreinigung
                                                                    arbeiten wir mit einem neutralen, ökologischen...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm" data-toggle="collapse"
                                                        data-target="#collapse1_a_2" aria-expanded="false"
                                                        aria-controls="collapse1_a_2" role="button">
                                                        <a href="{{ route('sealing')}}" class="plegemobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('sealing')}}" class="plegemobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">
                                                                    Aussenaufbereitung</h5>
                                                                <p class="h6 py-2 pb-3">Eine Teflon-Versiegelung ist
                                                                    sehr preiswert, hält 6-12 Monate und ist für die
                                                                    meisten...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm" data-toggle="collapse"
                                                        data-target="#collapse1_b_4" aria-expanded="false"
                                                        aria-controls="collapse1_b_4" role="button">
                                                        <a href="{{ route('leather_care_restoration') }}"
                                                            class="plegemobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('leather_care_restoration') }}"
                                                                class="plegemobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Lederpflege &
                                                                    Restauration</h5>
                                                                <p class="h6 py-2 pb-3">Leder benötigt besondere Pflege.
                                                                    Das gilt besonders für stark beanspruchte...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-protect" role="tabpanel">
                                        <div class="px-2">
                                            <div class="care-for-carousel owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm scheibentönung">
                                                        <a href="{{ route('car_window_tinting')}}"
                                                            class="schutzmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/tint-10.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('car_window_tinting')}}"
                                                                class="schutzmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Scheibentönung
                                                                </h5>
                                                                <p class="h6 py-2">Eine Autoscheibentönung sieht nicht
                                                                    nur gut aus, sie schützt auch in...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm keramikbeschichtung">
                                                        <a href="{{ route('ceramic_coating')}}"
                                                            class="schutzmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('ceramic_coating')}}"
                                                                class="schutzmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">
                                                                    Keramikbeschichtung</h5>
                                                                <p class="h6 py-2">Sie fahren Ihr Auto täglich und es
                                                                    soll dabei mit möglichst wenig Aufwand...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm steinschlagschutzfolien">
                                                        <a href="{{ route('stone_impact_protection_film')}}"
                                                            class="schutzmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/paint-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('stone_impact_protection_film')}}"
                                                                class="schutzmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">
                                                                    Steinschlagschutzfolien</h5>
                                                                <p class="h6 py-2">Eine volltransparente Lackschutzfolie
                                                                    ist der beste Schutz gegen Steinschlag...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-enhance" role="tabpanel">
                                        <div class="px-2">
                                            <div class="care-for-carousel owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm collapsed">
                                                        <a href="{{ route('vehicle_lettering')}}"
                                                            class="designmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('vehicle_lettering')}}"
                                                                class="designmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">
                                                                    Fahrzeugbeschriftung</h5>
                                                                <p class="h6 py-2">Wer nicht wirbt, stirbt – so der
                                                                    Volksmund. Und es steckt durchaus Wahrheit...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm collapsed">
                                                        <a href="{{ route('car_wrapping')}}" class="designmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('car_wrapping')}}"
                                                                class="designmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Car Wrapping
                                                                </h5>
                                                                <p class="h6 py-2">Verschönern Sie Ihr Auto, Ihr
                                                                    Motorrad oder Ihren Lastwagen mit einer...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm collapsed">
                                                        <a href="{{ route('water_transfer_printing')}}"
                                                            class="designmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/seal-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('water_transfer_printing')}}"
                                                                class="designmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">
                                                                    Wassertransferdruck</h5>
                                                                <p class="h6 py-2">Wassertransferdruck ist ein relativ
                                                                    modernes Verfahren, das es möglich...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm collapsed">
                                                        <a href="{{ route('chrome_plating')}}" class="designmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/seal-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('chrome_plating')}}"
                                                                class="designmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Verchromungen
                                                                </h5>
                                                                <p class="h6 py-2">Mehr Glanz für Ihr Auto, Ihr Motorrad
                                                                    oder Ihren Lastwagen! Gerne...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm">
                                                        <a href="{{ route('interior_design')}}"
                                                            class="designmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('interior_design')}}"
                                                                class="designmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Interieur
                                                                    Design</h5>
                                                                <p class="h6 py-2">Fahrzeugveredelungen müssen sich
                                                                    nicht auf das Äussere beschränken. Wir...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="pills-bike" role="tabpanel">
                            <ul class="nav justify-content-center nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active bg-white" data-toggle="pill"
                                        data-target="#pills-care-bike" type="button" role="tab"
                                        aria-controls="pills-care-bike" aria-selected="false">
                                        <div class="service-level-1 car-care-icon bg-white p-3" role="button"
                                            data-class1="service-first">
                                            <div class="service-title-wrap">
                                                <div class="avatar-sm mb-3">
                                                    <img src="{{ asset('assets/frontend/images/car-care.png')}}" alt=""
                                                        class="img-fluid">
                                                </div>
                                                <h4 class="h6 text-center font-weight-bold mb-0 text-body">Pflege</h4>
                                            </div>
                                        </div>
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link bg-white" data-toggle="pill"
                                        data-target="#pills-protect-bike" type="button" role="tab"
                                        aria-controls="pills-protect" aria-selected="false">
                                        <div class="service-level-1 car-care-icon bg-white p-3" role="button"
                                            data-class1="service-first">
                                            <div class="service-title-wrap">
                                                <div class="avatar-sm mb-3">
                                                    <img src="{{ asset('assets/frontend/images/car-protect.png')}}"
                                                        alt="" class="img-fluid">
                                                </div>
                                                <h4 class="h6 text-center font-weight-bold mb-0 text-body">Schutz</h4>
                                            </div>
                                        </div>
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link bg-white" data-toggle="pill"
                                        data-target="#pills-enhance-bike" type="button" role="tab"
                                        aria-controls="pills-enhance" aria-selected="false">
                                        <div class="service-level-1 car-care-icon bg-white p-3" role="button"
                                            data-class1="service-first">
                                            <div class="service-title-wrap">
                                                <div class="avatar-sm mb-3">
                                                    <img src="{{ asset('assets/frontend/images/car-foil.png')}}" alt=""
                                                        class="img-fluid">
                                                </div>
                                                <h4 class="h6 text-center font-weight-bold mb-0 text-body">Design</h4>
                                            </div>
                                        </div>
                                    </button>
                                </li>
                            </ul>
                            <div class="tabpanel-service">
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-care-bike" role="tabpanel">
                                        <div class="accordion px-2">
                                            <div class="care-for-carousel owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm collapsed lederpflegeRestauration">
                                                        <a href="{{ route('leather_care_restoration')}}"
                                                            class="plegemobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('leather_care_restoration')}}"
                                                                class="plegemobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Lederpflege
                                                                </h5>
                                                                <p class="h6 py-2">Leder benötigt besondere Pflege. Das
                                                                    gilt besonders für stark...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm collapsed lackpflege">
                                                        <a href="#" class="plegemobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="#" class="plegemobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Lackpflege</h5>
                                                                <p class="h6 py-2">Lorem ipsum dolor sit amet,
                                                                    consectetur adipiscing elit. Integer...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-protect-bike" role="tabpanel">
                                        <div class="px-2">
                                            <div class="care-for-carousel owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm keramikbeschichtung">
                                                        <a href="{{ route('ceramic_coating')}}"
                                                            class="schutzmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('ceramic_coating')}}"
                                                                class="schutzmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">
                                                                    Keramikbeschichtung</h5>
                                                                <p class="h6 py-2">Sie fahren Ihr Auto täglich und es
                                                                    soll dabei mit möglichst wenig Aufwand...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm steinschlagschutzfolien">
                                                        <a href="{{ route('stone_impact_protection_film')}}"
                                                            class="schutzmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/paint-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('stone_impact_protection_film')}}"
                                                                class="schutzmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Lackschutzfolie
                                                                </h5>
                                                                <p class="h6 py-2">Eine volltransparente Lackschutzfolie
                                                                    ist der beste Schutz gegen Steinschlag...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-enhance-bike" role="tabpanel">
                                        <div class="px-2">
                                            <div class="care-for-carousel owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm vollfolierung">
                                                        <a href="#" class="designmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="#" class="designmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Voll-folierung
                                                                </h5>
                                                                <p class="h6 py-2">Lorem ipsum dolor sit amet,
                                                                    consectetur adipiscing elit. Integer...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm beschriftung">
                                                        <a href="#" class="designmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="#" class="designmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Beschriftung
                                                                </h5>
                                                                <p class="h6 py-2">Lorem ipsum dolor sit amet,
                                                                    consectetur adipiscing elit. Integer...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm teilfolierung">
                                                        <a href="#" class="designmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="#" class="designmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Teilfolierung
                                                                </h5>
                                                                <p class="h6 py-2">Lorem ipsum dolor sit amet,
                                                                    consectetur adipiscing elit. Integer...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#services-mobile">
                    <div class="tabpanel-service main-tabpanel-service">
                        <ul class="nav justify-content-start align-items-center nav-pills mb-5" id="pills-tab"
                            role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active show p-0 border-0" data-toggle="pill"
                                    data-target="#pills-car-old" type="button" role="tab" aria-controls="pills-car"
                                    aria-selected="false">
                                    <div class="card select-category border-0 rounded-0">
                                        <input role="button" type="radio" name="step_0" class="form-control checkbox">
                                        <div class="card-body px-2 py-0">
                                            <img src="{{ asset('assets/frontend/images/car-service.png') }}" alt=""
                                                class="img-fluid">
                                        </div>
                                    </div>
                                </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link p-0 border-0" data-toggle="pill" data-target="#pills-bike-old"
                                    type="button" role="tab" aria-controls="pills-bike" aria-selected="false">
                                    <div class="card select-category border-0 rounded-0">
                                        <input role="button" type="radio" name="step_0" class="form-control checkbox">
                                        <div class="card-body px-2 py-0">
                                            <!-- <div class="avatar-md">
													<img src="{{ asset('assets/frontend/images/form-bike.png') }}" alt="" class="img-fluid">
												</div>
												<p class="font-weight-bold h5 text-dark m-0 mb-2">Motorrad</p> -->
                                            <img src="{{ asset('assets/frontend/images/bike-2.png') }}" alt=""
                                                class="img-fluid">
                                        </div>
                                    </div>
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div class="tabpanel-service main-tabpanel-service tab-content">
                        <div class="tab-pane show active" id="pills-car-old" role="tabpanel">
                            <ul class="nav justify-content-center nav-pills mb-3 px-3" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active bg-white" data-toggle="pill"
                                        data-target="#pills-care-old" type="button" role="tab"
                                        aria-controls="pills-care" aria-selected="false">
                                        <div class="service-level-1 car-care-icon bg-white p-3" role="button"
                                            data-class1="service-first">
                                            <div class="service-title-wrap">
                                                <div class="avatar-sm mb-3">
                                                    <img src="{{ asset('assets/frontend/images/car-care.png')}}" alt=""
                                                        class="img-fluid">
                                                </div>
                                                <h4 class="h6 text-center font-weight-bold mb-0 text-body">Pflege</h4>
                                            </div>
                                        </div>
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link bg-white" data-toggle="pill"
                                        data-target="#pills-protect-old" type="button" role="tab"
                                        aria-controls="pills-protect" aria-selected="false">
                                        <div class="service-level-1 car-care-icon bg-white p-3" role="button"
                                            data-class1="service-first">
                                            <div class="service-title-wrap">
                                                <div class="avatar-sm mb-3">
                                                    <img src="{{ asset('assets/frontend/images/car-protect.png')}}"
                                                        alt="" class="img-fluid">
                                                </div>
                                                <h4 class="h6 text-center font-weight-bold mb-0 text-body">Schutz</h4>
                                            </div>
                                        </div>
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link bg-white" data-toggle="pill"
                                        data-target="#pills-enhance-old" type="button" role="tab"
                                        aria-controls="pills-enhance" aria-selected="false">
                                        <div class="service-level-1 car-care-icon bg-white p-3" role="button"
                                            data-class1="service-first">
                                            <div class="service-title-wrap">
                                                <div class="avatar-sm mb-3">
                                                    <img src="{{ asset('assets/frontend/images/car-foil.png')}}" alt=""
                                                        class="img-fluid">
                                                </div>
                                                <h4 class="h6 text-center font-weight-bold mb-0 text-body">Design</h4>
                                            </div>
                                        </div>
                                    </button>
                                </li>
                            </ul>
                            <div class="tabpanel-service">
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-care-old" role="tabpanel">
                                        <div class="accordion px-2">
                                            <div class="care-for-carousel owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm swissvaxfahrzeugaufbereitung">
                                                        <a href="#" class="plegemobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="#" class="plegemobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Swissvax
                                                                    Fahrzeugaufbereitung</h5>
                                                                <p class="h6 py-2">Lorem ipsum dolor sit amet,
                                                                    consectetur adipiscing elit. Integer...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm laderpflegereperatur">
                                                        <a href="#" class="plegemobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="#" class="plegemobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Laderpflege und
                                                                    Reperatur</h5>
                                                                <p class="h6 py-2">Lorem ipsum dolor sit amet,
                                                                    consectetur adipiscing elit. Integer...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-protect-old" role="tabpanel">
                                        <div class="px-2">
                                            <div class="care-for-carousel owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm lackschutz">
                                                        <a href="#" class="schutzmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="#" class="schutzmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Lackschutz</h5>
                                                                <p class="h6 py-2">Lorem ipsum dolor sit amet,
                                                                    consectetur adipiscing elit. Integer...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm keramikbeschichtung">
                                                        <a href="{{ route('ceramic_coating')}}"
                                                            class="schutzmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('ceramic_coating')}}"
                                                                class="schutzmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">
                                                                    Keramikbeschichtung</h5>
                                                                <p class="h6 py-2">Sie fahren Ihr Auto täglich und es
                                                                    soll dabei mit möglichst wenig Aufwand...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-enhance-old" role="tabpanel">
                                        <div class="px-2">
                                            <div class="care-for-carousel owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm collapsed beschriftungen">
                                                        <a href="{{ route('vehicle_lettering')}}"
                                                            class="designmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('vehicle_lettering')}}"
                                                                class="designmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Beschriftungen
                                                                </h5>
                                                                <p class="h6 py-2">Wer nicht wirbt, stirbt – so der
                                                                    Volksmund. Und es steckt durchaus Wahrheit...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="pills-bike-old" role="tabpanel">
                            <ul class="nav justify-content-center nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active bg-white" data-toggle="pill"
                                        data-target="#pills-care-bike-old" type="button" role="tab"
                                        aria-controls="pills-care-bike" aria-selected="false">
                                        <div class="service-level-1 car-care-icon bg-white p-3" role="button"
                                            data-class1="service-first">
                                            <div class="service-title-wrap">
                                                <div class="avatar-sm mb-3">
                                                    <img src="{{ asset('assets/frontend/images/car-care.png')}}" alt=""
                                                        class="img-fluid">
                                                </div>
                                                <h4 class="h6 text-center font-weight-bold mb-0 text-body">Pflege</h4>
                                            </div>
                                        </div>
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link bg-white" data-toggle="pill"
                                        data-target="#pills-protect-bike-old" type="button" role="tab"
                                        aria-controls="pills-protect" aria-selected="false">
                                        <div class="service-level-1 car-care-icon bg-white p-3" role="button"
                                            data-class1="service-first">
                                            <div class="service-title-wrap">
                                                <div class="avatar-sm mb-3">
                                                    <img src="{{ asset('assets/frontend/images/car-protect.png')}}"
                                                        alt="" class="img-fluid">
                                                </div>
                                                <h4 class="h6 text-center font-weight-bold mb-0 text-body">Schutz</h4>
                                            </div>
                                        </div>
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link bg-white" data-toggle="pill"
                                        data-target="#pills-enhance-bike-old" type="button" role="tab"
                                        aria-controls="pills-enhance" aria-selected="false">
                                        <div class="service-level-1 car-care-icon bg-white p-3" role="button"
                                            data-class1="service-first">
                                            <div class="service-title-wrap">
                                                <div class="avatar-sm mb-3">
                                                    <img src="{{ asset('assets/frontend/images/car-foil.png')}}" alt=""
                                                        class="img-fluid">
                                                </div>
                                                <h4 class="h6 text-center font-weight-bold mb-0 text-body">Design</h4>
                                            </div>
                                        </div>
                                    </button>
                                </li>
                            </ul>
                            <div class="tabpanel-service">
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-care-bike-old" role="tabpanel">
                                        <div class="accordion px-2">
                                            <div class="care-for-carousel owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm collapsed lederpflegeRestauration">
                                                        <a href="{{ route('leather_care_restoration')}}"
                                                            class="plegemobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('leather_care_restoration')}}"
                                                                class="plegemobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Lederpflege
                                                                </h5>
                                                                <p class="h6 py-2">Leder benötigt besondere Pflege. Das
                                                                    gilt besonders für stark...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm collapsed lackpflege">
                                                        <a href="#" class="plegemobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="#" class="plegemobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Lackpflege</h5>
                                                                <p class="h6 py-2">Lorem ipsum dolor sit amet,
                                                                    consectetur adipiscing elit. Integer...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm collapsed metallpflege">
                                                        <a href="#" class="plegemobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="#" class="plegemobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Metallpflege
                                                                </h5>
                                                                <p class="h6 py-2">Lorem ipsum dolor sit amet,
                                                                    consectetur adipiscing elit. Integer...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-protect-bike-old" role="tabpanel">
                                        <div class="px-2">
                                            <div class="care-for-carousel owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm keramikbeschichtung">
                                                        <a href="{{ route('ceramic_coating')}}"
                                                            class="schutzmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('ceramic_coating')}}"
                                                                class="schutzmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">
                                                                    Keramikbeschichtung</h5>
                                                                <p class="h6 py-2">Sie fahren Ihr Auto täglich und es
                                                                    soll dabei mit möglichst wenig Aufwand...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-enhance-bike-old" role="tabpanel">
                                        <div class="px-2">
                                            <div class="care-for-carousel owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm lederlackierungen">
                                                        <a href="#" class="designmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="#" class="designmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">
                                                                    Lederlackierungen</h5>
                                                                <p class="h6 py-2">Lorem ipsum dolor sit amet,
                                                                    consectetur adipiscing elit. Integer...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm collapsed beschriftungen">
                                                        <a href="{{ route('vehicle_lettering')}}"
                                                            class="designmobilelink">
                                                            <div class="ratio ratio-16X9">
                                                                <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                                    alt="" class="img-fluid">
                                                            </div>
                                                        </a>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <a href="{{ route('vehicle_lettering')}}"
                                                                class="designmobilelink">
                                                                <h5 class="font-weight-bold h4 pt-3 m-0">Beschriftungen
                                                                </h5>
                                                                <p class="h6 py-2">Wer nicht wirbt, stirbt – so der
                                                                    Volksmund. Und es steckt durchaus Wahrheit...
                                                                    <span class="font-weight-bolder">Weiterlesen</span>
                                                                </p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div id="services-mobile" style="display:none;">
            <div class="car-bike-1">
                <div id="section-services1" class="">
                    <div class="container">
                        <div class="row modal-main service-accordian-main">
                            <div class="col-md-12">
                                <div class="home-service-title pb-3 text-center">
                                    <h2 class="h1 pt-0">Unsere Dienstleistungen</h2>
                                    <!-- <p class="h6"> Wir bieten einen kompletten Service für Autoreparatur und Wartung </p> -->
                                </div>
                            </div>

                            <div class="col-md-4 mb-3 col-4 px-2">
                                <div class="service-level-1 car-care-icon bg-white shadow-sm p-3" role="button"
                                    data-class1="service-first">
                                    <div class="service-title-wrap">
                                        <div class="avatar-sm mb-3">
                                            <img src="{{ asset('assets/frontend/images/car-care.png')}}" alt=""
                                                class="img-fluid">
                                        </div>
                                        <h4 class="h6 text-center font-weight-bold mb-0">Pflege</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3 col-4 px-2">
                                <div class="service-level-1 car-care-icon bg-white shadow-sm p-3" role="button"
                                    data-class1="service-second">
                                    <div class="service-title-wrap">
                                        <div class="avatar-sm mb-3">
                                            <img src="{{ asset('assets/frontend/images/car-protect.png')}}" alt=""
                                                class="img-fluid">
                                        </div>
                                        <h4 class="h6 text-center font-weight-bold mb-0">Schutz</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3 col-4 px-2">
                                <div class="service-level-1 car-care-icon bg-white shadow-sm p-3" role="button"
                                    data-class1="service-third">
                                    <div class="service-title-wrap">
                                        <div class="avatar-sm mb-3">
                                            <img src="{{ asset('assets/frontend/images/car-foil.png')}}" alt=""
                                                class="img-fluid">
                                        </div>
                                        <h4 class="h6 text-center font-weight-bold mb-0">Design</h4>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="section-miscellaneous1-1" class="bg-white pt-5 page-section-pt ">
                    <div class="container">
                        <div class="row overflow-hidden">
                            <div class="col-md-12">
                                <div class="home-service-image">
                                    <img src="{{ asset('assets/frontend/images/car-service.png') }}"
                                        class="w-100 img-fluid" alt="autokosmetik">
                                </div>
                                <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <div class="tooltip-main service-first second" data-toggle="pill"
                                            data-target="#Aussenaufbereitung" type="button" role="tab"
                                            aria-controls="Aussenaufbereitung" aria-selected="false"></div>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <div class="tooltip-main service-first first" data-toggle="pill"
                                            data-target="#Innenaufbereitung" type="button" role="tab"
                                            aria-controls="Innenaufbereitung" aria-selected="false"></div>
                                    </li>
                                    <!-- <li class="nav-item" role="presentation">
											<div class="tooltip-main service-second third">
										</li>

										<li class="nav-item" role="presentation">
											<div class="tooltip-main service-second four">
										</li>
										<li class="nav-item" role="presentation">
											<div class="tooltip-main service-third five">
										</li>
										<li class="nav-item" role="presentation">
											<div class="tooltip-main service-third six">
										</li>
										<li class="nav-item" role="presentation">
											<div class="tooltip-main service-third seven">
										</li> -->
                                </ul>

                            </div>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="Aussenaufbereitung" role="tabpanel">
                                    <div id="collapse1_a_parent" class="mt-3 accordion px-2">
                                        <div class="care-for-carousel owl-carousel owl-theme">
                                            <div class="item pb-4">
                                                <div class="bg-white shadow-sm" data-toggle="collapse"
                                                    data-target="#collapse1_a_1" aria-expanded="false"
                                                    aria-controls="collapse1_a_1" role="button">
                                                    <div class="ratio ratio-16X9">
                                                        <img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}"
                                                            alt="" class="img-fluid">
                                                    </div>
                                                    <div class="px-2 position-relative">
                                                        <a href="{{ route('bookappointment')}}" role="button">
                                                            <div
                                                                class="align-items-center bg-white d-flex m-0 btn-service">
                                                                <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                    alt="autokosmetik">
                                                                <p class="text-dark btn-service-text pl-2 m-0">Termin
                                                                    buchen</p>
                                                            </div>
                                                        </a>
                                                        <h5 class="font-weight-bold h4 pt-3 m-0">Keramikbeschichtung
                                                        </h5>
                                                        <p class="h6 py-2">Unser neuester Hit, eine Keramikbeschichtung
                                                            hält Minimum 36 Monate und...
                                                            <a href="{{ route('ceramic_coating')}}"
                                                                class="font-weight-bolder">Weiterlesen</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item pb-4">
                                                <div class="bg-white shadow-sm" data-toggle="collapse"
                                                    data-target="#collapse1_a_2" aria-expanded="false"
                                                    aria-controls="collapse1_a_2" role="button">
                                                    <div class="ratio ratio-16X9">
                                                        <img src="{{ asset('assets/frontend/images/seal-1.jpg') }}"
                                                            alt="" class="img-fluid">
                                                    </div>
                                                    <div class="px-2 position-relative">
                                                        <a href="{{ route('bookappointment')}}" role="button">
                                                            <div
                                                                class="align-items-center bg-white d-flex m-0 btn-service">
                                                                <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                    alt="autokosmetik">
                                                                <p class="text-dark btn-service-text pl-2 m-0">Termin
                                                                    buchen</p>
                                                            </div>
                                                        </a>
                                                        <h5 class="font-weight-bold h4 pt-3 m-0">Teflonversiegelung</h5>
                                                        <p class="h6 py-2">Eine Teflon-Versiegelung hält Minimum 6
                                                            Monate bis zu einem...
                                                            <a href="{{ route('sealing')}}"
                                                                class="font-weight-bolder">Weiterlesen</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item pb-4">
                                                <div class="bg-white shadow-sm" data-toggle="collapse"
                                                    data-target="#collapse1_a_3" aria-expanded="false"
                                                    aria-controls="collapse1_a_3" role="button">
                                                    <div class="ratio ratio-16X9">
                                                        <img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}"
                                                            alt="" class="img-fluid">
                                                    </div>
                                                    <div class="px-2 position-relative">
                                                        <a href="{{ route('bookappointment')}}" role="button">
                                                            <div
                                                                class="align-items-center bg-white d-flex m-0 btn-service">
                                                                <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                    alt="autokosmetik">
                                                                <p class="text-dark btn-service-text pl-2 m-0">Termin
                                                                    buchen</p>
                                                            </div>
                                                        </a>
                                                        <h5 class="font-weight-bold h4 pt-3 m-0">Swissvax</h5>
                                                        <p class="h6 py-2">In unserem Swissvax Car Care Center
                                                            Liechtenstein/Rheintal ..
                                                            <a href="{{ route('swissvax')}}"
                                                                class="font-weight-bolder">Weiterlesen</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="Innenaufbereitung" role="tabpanel">
                                    <div id="collapse1_a_parent" class="mt-3">
                                        <div class=" px-2">
                                            <div class="care-for-carousel owl-carousel owl-theme">
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm" data-toggle="collapse"
                                                        data-target="#collapse1_b_1" aria-expanded="false"
                                                        aria-controls="collapse1_b_1" role="button">
                                                        <div class="ratio ratio-16X9">
                                                            <img src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}"
                                                                alt="" class="img-fluid">
                                                        </div>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <h5 class="font-weight-bold h4 pt-3 m-0">Herkömmlich</h5>
                                                            <p class="h6 py-2">Bei einer herkömmlichen Innenaufbereitung
                                                                arbeiten wir mit einem...
                                                                <a href="{{ route('conventinal') }}"
                                                                    class="font-weight-bolder">Weiterlesen</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm" data-toggle="collapse"
                                                        data-target="#collapse1_b_2" aria-expanded="false"
                                                        aria-controls="collapse1_b_2" role="button">
                                                        <div class="ratio ratio-16X9">
                                                            <img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}"
                                                                alt="" class="img-fluid">
                                                        </div>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <h5 class="font-weight-bold h4 pt-3 m-0">Swissvax</h5>
                                                            <p class="h6 py-2">In unserem Swissvax Car Care Center
                                                                Liechtenstein/Rheintal...
                                                                <a href="{{ route('swissvax') }}"
                                                                    class="font-weight-bolder">Weiterlesen</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item pb-4">
                                                    <div class="bg-white shadow-sm" data-toggle="collapse"
                                                        data-target="#collapse1_b_3" aria-expanded="false"
                                                        aria-controls="collapse1_b_3" role="button">
                                                        <div class="ratio ratio-16X9">
                                                            <img src="{{ asset('assets/frontend/images/special-1.jpg') }}"
                                                                alt="" class="img-fluid">
                                                        </div>
                                                        <div class="px-2 position-relative">
                                                            <a href="{{ route('bookappointment')}}" role="button">
                                                                <div
                                                                    class="align-items-center bg-white d-flex m-0 btn-service">
                                                                    <img src="{{ asset('assets/frontend/images/read-more.png') }}"
                                                                        alt="autokosmetik">
                                                                    <p class="text-dark btn-service-text pl-2 m-0">
                                                                        Termin buchen</p>
                                                                </div>
                                                            </a>
                                                            <h5 class="font-weight-bold h4 pt-3 m-0">SPEZIALFÄLLE</h5>
                                                            <p class="h6 py-2">Bei diesen "speziellen Fällen" können wir
                                                                keine Fixpreise machen, denn...
                                                                <a href="{{ route('specialcase') }}"
                                                                    class="font-weight-bolder">Weiterlesen</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div id="section-miscellaneous1-1" class="bg-white pt-5 page-section-pt car-bike-small-1" role="button">
                <div class="container-fluid ">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="home-service-image">
                                <img src="{{ asset('assets/frontend/images/car-service.png') }}" class="w-100 img-fluid"
                                    alt="autokosmetik">
                            </div>
                        </div>
                    </div>
                </div>
            </div>	

            <div id="section-miscellaneous1-1" class="bg-white page-section-pt pb-5 white-bg car-bike-small-2"
                role="button">
                <div class="container-fluid mt-5">
                    <div class="row">
                        <div class="col-5">
                            <div class="home-service-image">
                                <img src="{{ asset('assets/frontend/images/bike-2.png') }}" class="w-100 img-fluid"
                                    alt="autokosmetik">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="section-recentworks1" class="bg-dark-autokosmetic">
            <div class="container">

                <div class="title1 ez-animate col-12" data-animation="fadeInUp">
                    <div class="h6">JÜNGSTE ARBEITEN</div>
                    <div class="h2">Unser Portfolio</div>
                    <i class="flaticon-download"></i>
                </div>
                <div class="our-portfolio owl-carousel owl-theme">

                    @foreach($portfolio as $portfoli)
                    <?php 
                                  $images1 = $portfoli->images; 
                                  $images  = explode(',', $images1);
      
                                  $categoryArray = array();
                                  foreach($portfoli->services_category as $value):
      								$categoryArray[] = $value->name;
      							endforeach;
                              ?>
                    <div class="item">
                        <!-- <a data-fancybox="gallery" href="{{asset(Storage::url(trim($images[0]))) ?? ''}}" class="fancy-box"> -->
                        <a href="{{ route('portfolio')}}">
                            <div class="img-container">
                                <img class="img-fluid" src="{{asset(Storage::url(trim($images[0]))) ?? ''}}"
                                    alt="autokosmetik">
                                <div class="overlay">
                                    <div class="overlay-content">
                                        <i></i>
                                        <h3>{{ $portfoli->name }}</h3>
                                        <p>{{ implode(',',$categoryArray) }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>

        <!-- <div id="section-cta1">
				<div class="container">
					<div class="row">
						<div class="left col-sm-12 col-md-9" >
							<h2>Sie brauchen eine beeindruckendere als diese, <span>kontaktiere uns jetzt</span></h2>
						</div>
						<div class="right col-sm-12 col-md-3" >
							<a href="{{ route('contact')}}" class="btn-2">Kaufen Sie dies jetzt</a>
						</div>
					</div>
				</div>
			</div> -->
        <!-- <div id="section-testimonial1" class="bg-dark-autokosmetic" style="background: url({{ asset('assets/frontend/images/index-bg.jpg')}}) no-repeat;">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="title1 ez-animate col-12" data-animation="fadeInUp">
								<h6>Kunden sagen</h6>
								<h2>Unser Kundenfeedback</h2>
								<i class="flaticon-download"></i>
							</div>
							<div class="owl-carousel owl-theme">
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial1.png') }}"alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum</h6>
										<p>Lorem ipsum</p>
									</div>
								</div>
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial2.png') }}" alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum</h6>
										<p>Lorem ipsum</p>
									</div>
								</div>
								<div class="item col content">
									<div class="comment">
										<p><strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. In consequat commo- do lorem sed sodales. Etiam condimentum, <strong>dui quis suscipit tincidunt</strong>, lectus arcu viverra neque, eu eleifend.</p>
									</div>
									<div class="image">
										<img src="{{ asset('assets/frontend/images/img-testimonial3.png') }}" alt="autokosmetik">
									</div>
									<div class="client">
										<h6>Lorem ipsum</h6>
										<p>Lorem ipsum</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> -->

			<div id="section-testimonial1" class="inner-banner-wrap">				
				<img src="{{ asset('assets/frontend/images/index-bg.jpg') }}" alt="" class="inner-page-banner">
								
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="title1 ez-animate col-12" data-animation="fadeInUp">
								<div class="h6">Was Kunden über uns sagen</div>
								<div class="h2">Kundenfeedback</div>
								<i class="flaticon-download"></i>
							</div>
							<div class="owl-carousel owl-theme">
								@foreach($feedback as $feed)
                                <div class="item col content">
									<div class="comment">
										<p>{!!html_entity_decode($feed->description)!!}</p>
									</div>
									<div class="image">
                                        @if(!empty($feed->image))
                                        <img src="{{asset(Storage::url($feed->image)) ?? ''}}" alt="" style="max-height: 62px;background: #fff;">
									    @else
                                        <img src="{{ asset('assets/frontend/images/img-testimonial1.png') }}"alt="autokosmetik">
                                        @endif
                                    </div>
									<div class="client">
										<h6>{{ $feed->name }}</h6>
										<p>{{ $feed->occupation }}</p>
									</div>
								</div>
                                @endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="section-contactform1">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="title1 ez-animate col-12" data-animation="fadeInUp">
								<div class="h6">Kontaktformular</div>
								<div class="h2">Kontaktieren Sie uns</div>
								<i class="flaticon-download"></i>
							</div>
							<div class="contactform1">
                                <div class="alert alert-success alertmsg" id="contactFormSuccess" style="display: none;"></div>
                                <div class="alert alert-danger alertmsg" id="contactFormDanger" style="display: none;"></div>
								<form id="contactform1" action="" method="post">
									<div class="form-row">
										<div class="form-group col-md-6">
											<input type="text" class="form-control" id="name" placeholder="Name *" required>
                                            <div class="name_form_error" style="color: red;display: none"></div>
										</div>
										<div class="form-group col-md-6">
											<input type="email" class="form-control" id="email" placeholder="Email *" required>
                                            <div class="email_form_error" style="color: red;display: none"></div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6">
											<input type="text" class="form-control" id="phoneNumber" placeholder="Telefon *" required>
                                            <div class="number_form_error" style="color: red;display: none"></div>
										</div>
										<div class="form-group col-md-6">
											<input type="text" class="form-control" id="subject" placeholder="Betreff *" required>
                                            <div class="subject_form_error" style="color: red;display: none"></div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-12">
											<textarea class="form-control" id="message" rows="3" placeholder="Nachricht *" required></textarea>
                                            <div class="message_form_error" style="color: red;display: none"></div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group m-bot0 col-md-12">
											<button type="button" id="contactformbtn1">Senden</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--<div id="section-map" class="overflow-hidden">
				<div class="row">
					<div class="col-6 p-0 d-flex">
						<div class="detail">
							<div class="marker-img">
								<img src="{{ asset('assets/frontend/images/marker.png') }}" alt="autokosmetik">
							</div>
							<div class="description">
								<p> Messinastrasse 1<br>
								9495 Triesen<br>
								Liechtenstein</p>
							</div>
						</div>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2714.9507204632764!2d9.517502515859576!3d47.11963722948226!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479b33e8d3f444ed%3A0x2c32a32d5a5bc7dd!2sMessinastrasse%201%2C%209495%20Triesen%2C%20Liechtenstein!5e0!3m2!1sen!2sin!4v1617771666494!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" class="d-flex h-100"></iframe>
					</div>
					<div class="col-6 p-0">
						<img src="{{ asset('assets/frontend/images/banner-new.png') }}" alt="" class="img-fluid">
					</div>
				</div>
			</div>-->
			<!-- /.Section Contact Form 1 -->

        <div id="section-map" class="overflow-hidden">
            <div class="row">
                <div class="col-6 p-0 d-flex">
                    <div class="detail">
                        <div class="marker-img">
                            <img src="{{ asset('assets/frontend/images/marker.png') }}" alt="autokosmetik">
                        </div>
                        <div class="description">
                            <p> Messinastrasse 1<br>
                                9495 Triesen<br>
                                Liechtenstein</p>
                        </div>
                    </div>
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2714.9507204632764!2d9.517502515859576!3d47.11963722948226!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479b33e8d3f444ed%3A0x2c32a32d5a5bc7dd!2sMessinastrasse%201%2C%209495%20Triesen%2C%20Liechtenstein!5e0!3m2!1sen!2sin!4v1617771666494!5m2!1sen!2sin"
                        width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""
                        aria-hidden="false" tabindex="0" class="d-flex h-100"></iframe>
                </div>
                <div class="col-6 p-0">
                    <img src="{{ asset('assets/frontend/images/banner-new.png') }}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
        <!-- /.Section Contact Form 1 -->
    </div>
</div>
@endsection

@section('script')
<script>
$('body').on('click', "#contactformbtn1", function() {
    $('#loader').show();
    var contact_name = $("#name").val();
    var contact_email = $("#email").val();
    var contact_number = $("#phoneNumber").val();
    var contact_subject = $("#subject").val();
    var contact_message = $("#message").val();

    $('.name_form_error,.number_form_error,.email_form_error,.subject_form_error,.message_form_error').hide();
    var count = 0;
    if (contact_name == "" || contact_name == undefined) {
        $('.name_form_error').text('Bitte Name eingeben');
        $('.name_form_error').show();
        count = count + 1;
    }
    if (contact_number == "" || contact_number == undefined) {
        $('.number_form_error').text('Bitte Kontaktnummer eingeben');
        $('.number_form_error').show();
        count = count + 1;
    }
    if (contact_email == "" || contact_email == undefined) {
        $('.email_form_error').text('Bitte E-Mail eingeben');
        $('.email_form_error').show();
        count = count + 1;
    } else if (!validateEmail(contact_email)) {
        $('.email_form_error').text('Bitte gültige E-Mail eingeben');
        $('.email_form_error').show();
        count = count + 1;
    }
    if (contact_subject == "" || contact_subject == undefined) {
        $('.subject_form_error').text('Bitte Betreff eingeben');
        $('.subject_form_error').show();
        count = count + 1;
    }
    if (contact_message == "" || contact_message == undefined) {
        $('.message_form_error').text('Bitte Nachricht eingeben');
        $('.message_form_error').show();
        count = count + 1;
    }
    if (count != 0) {
        $('#loader').hide();
        return false;
    } else {
        $.ajax({
                method: 'POST',
                url: "{{ route('ajax.contactform1') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "name": contact_name,
                    "email": contact_email,
                    "phoneNumber": contact_number,
                    "subject": contact_subject,
                    "message": contact_message
                }
            })
            .done(function(data) {
                if (data.result == 1) {
                    $('#contactFormSuccess').html('Vielen Dank für Ihre Kontaktaufnahme!');
                    $('#contactFormSuccess').show();
                } else {
                    $('#contactFormDanger').html(
                        'Fehler im Kontaktformular. Bitte versuchen Sie es nach einiger Zeit.');
                    $('#contactFormDanger').show();
                }
                $('#contactform1').trigger("reset");
                $('#loader').hide();
                setTimeout(function() {
                    hideAlert();
                }, 7000);
            })
    }
});

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}

function hideAlert() {
    $('.alertmsg').hide();
}

var $careCarousel = $('.care-for-carousel');
$careCarousel.children().each(function(index) {
    $(this).attr('data-position', index);
});

$careCarousel.owlCarousel({
    loop: true,
    margin: 30,
    center: false,
    autoplay: true,
    nav: true,
    dots: false,
    responsive: {
        0: {
            items: 1,
        },
        600: {
            items: 1,
        },
        1000: {
            items: 3,
        }
    }
});
$('.our-portfolio').owlCarousel({
    loop: true,
    margin: 30,
    autoplay: true,
    responsiveClass: true,
    nav: false,
    dots: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
        },
        767: {
            items: 1,
            nav: false,
        },
        1000: {
            items: 3,
            nav: false,
        },
    }
});

$('.banner-carousel').owlCarousel({
    loop: true,
    margin: 0,
    nav: false,
    dots: false,
    autoplay: true,
    items: 1,
    animateOut: 'animate__fadeOut',
    animateIn: 'animate__fadeIn',
    smartSpeed: 450
})
</script>
<style>
#headingOne a,
#headingTwo a {
    background-color: #401219;
    color: #fff;
}

#headingOne .collapsed,
#headingTwo .collapsed {
    background-color: #0000;
    color: #401219;
}
</style>
@endsection