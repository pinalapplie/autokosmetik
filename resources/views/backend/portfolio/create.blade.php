@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.create') }} {{ trans('cruds.portfolio.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('portfolio.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('portfolio.store') }}" method="POST" enctype="multipart/form-data" id="add_form">
                    @csrf
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.portfolio.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($servicemanagement->name) ? $servicemanagement->name : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">{{ trans('cruds.portfolio.fields.description') }}</label>
                        <textarea name="description" class="form-control" id="description" rows="5">
                            {!! old('description', isset($servicemanagement->description) ? $servicemanagement->description : '') !!}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="category">{{ trans('cruds.portfolio.fields.category') }}<span class="required_class">*</span></label>
                        <select class="select2 form-control select2-multiple" name="service_category_id[]" id="category" multiple="" data-placeholder="{{ trans('cruds.portfolio.fields.select_category') }}"> 
                            @foreach($service_category as $value)
                                <?php 
                                    $final_category_name='';
                                    if($value->parent_id == 0){
                                        $final_category_name = $value->name;
                                    }
                                ?>
                                <option value="{{$value->id}}">{{ $final_category_name }}</option>  
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.portfolio.fields.created') }}<span class="required_class">*</span></label>
                        <input type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" id="created" name="created" value="{{ old('created', isset($portfolio->created) ? $portfolio->created : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.portfolio.fields.client') }}<span class="required_class">*</span></label>
                        <input type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" id="client" name="client" value="{{ old('client', isset($portfolio->client) ? $portfolio->client : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.portfolio.fields.images') }}<span class="required_class">*</span></label>
                        <input type="file" class="form-control" id="images" name="images[]" value="{{ old('images', isset($portfolio->images) ? $portfolio->images : '') }}" multiple="">
                    </div>
                    <button type="submit" class="btn btn-primary">{{ trans('global.submit') }}</button>
                </form>
            </div>
        </div>  
    </div>
</div>  
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        $(".select2").select2();
        CKEDITOR.replace('description');
        //CKEDITOR.replace('short_description');
        $('#select_type').on('change', function(e) {
            e.preventDefault();
            var selectedType= $(this).children("option:selected").val();
            var selectedattrValue= $(this).children("option:selected").attr('value');
            if(selectedType == "" || selectedattrValue == "" || selectedattrValue == "pricechart"){
                $('.price_text_box').hide();
            }else{
                $('.price_text_box').show();
            }
            
        });
        $("#add_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
                // description: {
                //     required: function(textarea) {
                //         CKEDITOR.instances[textarea.id].updateElement();
                //         var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                //         return editorcontent.length === 0;
                //     }
                // },
                service_category_id: {
                    required: true
                },
                type: {
                    required: true
                }

            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                },
                description: {
                    required: "@lang('validation.required',['attribute'=>'Description'])",
                },
                service_category_id: {
                    required: "@lang('validation.required',['attribute'=>'Category'])",
                },
                type: {
                    required: "@lang('validation.required',['attribute'=>'Type'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection