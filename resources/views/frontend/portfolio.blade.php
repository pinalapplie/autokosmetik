@section('title', 'Portfolio')
@extends('layouts.front_end')

@section('content')
	<div class="main-wrapper">
    <div id="main-content" class="active">
			<div id="section-breadcrumb1" class="inner-banner-wrap">
                <img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
				<div class="container">
					<div class="row">
						<div class="content p-bot89 col-12">
							<h1>Projekte</h1>
							<ul>
								<li><a href="{{ route('index')}}">ZUHAUSE</a></li>
								<li class="current text-light">Projekte</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- <a href="#related-projects-link" class="related_project_link" >test</a> -->
			<div id="section-portfoliodetails1" class="pb-4">
				<div class="container">
					<div class="desc-text pb-5">
						<p>Ein Bild sagt mehr als tausend Worte. Schauen Sie sich ein paar unserer realisierten Projekte an. Überzeugt? <a href="{{ route('contact')}}">Kontaktieren Sie uns jetzt!</a></p>	
					</div>
                    <div id="project-terms" class="mb-3 mb-lg-5">
                        <a data-owl-filter="*" class="btn btn-link item active" href="javascript:void(0)">All</a>
                        @foreach($serviceCategory as $category)
                            @if($category->parent_id == 0)
                            <a  data-owl-filter=".{{ str_replace(' ', '', $category->name) }}" class="item btn btn-link" href="javascript:void(0)">{{ $category->name }}</a>
                            @endif
                        @endforeach
                    </div>
					</div>
				<div class="container-fluid">
					<div class="owl-carousel owl-theme" id="projects-carousel">
                        
                        @foreach($portfolio as $portfoli)
                        <?php 
                            $images1 = $portfoli->images; 
                            $images  = explode(',', $images1);

                            $categoryArray = array();
                            $categoryArray1 = array();
                            foreach($portfoli->services_category as $value):
								$categoryArray[] = $value->name;
                                $categoryArray1[] = str_replace(' ', '', $value->name);
							endforeach;
                        ?>
                        
						<div class="item {{ implode(' ',$categoryArray1) }}">
							<div class="row">
								<div class="list-thumbnail col-2 col-md-2">
                                    @foreach($images as $image)
									<div class="item">
										<a href="javascript:void(0);">
											<img src="{{asset(Storage::url(trim($image))) ?? ''}}" alt="autokosmetik">
										</a>
									</div>
                                    @endforeach
								</div>
								<div class="contents mb-0 col-10 col-md-10">
									<div class="main-image-detail">
										<ul>
											<li>
												<img class="img-fluid src-image" src="{{asset(Storage::url(trim($images[0]))) ?? ''}}" alt="autokosmetik">
											</li>
										</ul>
									</div>
									<div class="row">
										<div class="project-description col-12 col-sm-8 pt-3">
											<h3>{{ $portfoli->name }}</h3>
											<p>{!! $portfoli->description ?? '-' !!}</p>
										</div>
										<div class="sidebar-description col-12 col-sm-4 pt-3">
											<div class="item">
												<h4>Created:</h4>
												<p><?= date('F d, Y', strtotime($portfoli->created)) ?></p>
											</div>
											<div class="item">
												<h4>Client:</h4>
												<p><?= date('F d, Y', strtotime($portfoli->client)) ?></p>
											</div>
											<div class="item">
												<h4>Category:</h4>
												<p>{{ implode(',',$categoryArray) }} </p>
											</div>
										</div>
                                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                        <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-512f41a471458451"></script>
                                        <div class="addthis_inline_share_toolbox_tis0"></div> -->
										<!-- <div class="shared ez-animate col-12" data-animation="fadeInUp">
											<h4>Shared:</h4>
											<div class="social-links">
												<a href="#"><i class="fa fa-facebook fa-lg"></i></a>
												<a href="#"><i class="fa fa-twitter fa-lg"></i></a>
												<a href="#"><i class="fa fa-instagram fa-lg"></i></a>
												<a href="#"><i class="fa fa-dribbble fa-lg"></i></a>
											</div>
										</div> -->
									</div>
								</div>
							</div>
						</div>
                        @endforeach
					</div>
                    <div id="projects-hidden" class="hide d-none"></div>
				</div>
			</div>
			<div class="mb-5">
				<div class="container">
					
					<div class="related-projects col-12" id="related-projects-link">
						<h2>Related Portfolio</h2>
						<div class="column-wrap">
                            @foreach($portfolio as $key => $portfoli)
                            <?php 
                                $images1 = $portfoli->images; 
                                $images  = explode(',', $images1);
                            ?>
                             
                            <div class="custom-thumb-carousel" data-value="{{$key+1}}">
								<a href="#section-portfoliodetails1">
                                	<img class="img-fluid" src="{{asset(Storage::url(trim($images[0]))) ?? ''}}" alt="{{ $portfoli->name }}" role="button">
								</a>
                            </div>
                             
                            @endforeach
						</div>
						<div class="mt-4 pagination-wrap">
							<?php echo $portfolio->links(); ?>
						</div>
					</div>	
					<!-- <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                            </li>
                        </ul>
                    </nav> -->
				</div>
			</div>

		</div>
	</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		//var page = '<?php //echo $_REQUEST['page'];?>';
		//alert(page);
		// if(page != ''){
		// 	$('.related_project_link').trigger();
		// }

		var projectCarousel = $('#projects-carousel');
		projectCarousel.owlCarousel({
			stagePadding: 360,
			margin:10,
			nav:false,
			items:3,
			dots: true,
			autoplay:true,
			URLhashListener:true,
			autoplayHoverPause:true,
			startPosition: 'URLHash',
			responsive:{
				0:{
					items:1,
					stagePadding: 20,
				},
				600:{
					items:1,
					stagePadding: 20,
				},
				1280:{
					items:1,
					stagePadding: 200,
				},
				1920:{
					items:1
				}
			}
		});

		$( '#project-terms' ).on( 'click', '.item', function() {
			var $item = $(this);
			$('#project-terms a').removeClass('active');
			$(this).addClass('active');
			var filter = $item.data( 'owl-filter' )
			projectCarousel.owlcarousel2_filter( filter );
			if($("#projects-carousel .active .list-thumbnail .item").length > 0){	
			$("#projects-carousel .active .list-thumbnail .item").each(function(){
				$("#projects-carousel .active .list-thumbnail .item a img").each(function(e) {
					$(this).click(function(){
						let img_src = this.src;
						$('#projects-carousel .active .src-image').attr('src', img_src);
					});
				});
			});
		}

		});

		$('body').on('click','.custom-thumb-carousel',function(){
			var value = $(this).attr('data-value');
			$('.owl-dots .owl-dot:nth-child('+value+')').click();
		});

		projectCarousel.on('dragged.owl.carousel', function() {
		    if($("#projects-carousel .active .list-thumbnail .item").length > 0){	
				$("#projects-carousel .active .list-thumbnail .item").each(function(){
					$("#projects-carousel .active .list-thumbnail .item a img").each(function(e) {
						$(this).click(function(){
							let img_src = this.src;
							$('#projects-carousel .active .src-image').attr('src', img_src);
						});
					});
				});
			}
		})


		projectCarousel.on('translated.owl.carousel', function() {
		    if($("#projects-carousel .active .list-thumbnail .item").length > 0){
				$("#projects-carousel .active .list-thumbnail .item").each(function(){
					$("#projects-carousel .active .list-thumbnail .item a img").each(function(e) {
						$(this).click(function(){
							let img_src = this.src;
							$('#projects-carousel .active .src-image').attr('src', img_src);
						});
					});
				});
			}
		})

		if($("#projects-carousel .active .list-thumbnail .item").length > 0){
			$("#projects-carousel .active .list-thumbnail .item").each(function(){
				$("#projects-carousel .active .list-thumbnail .item a img").each(function(e) {
					$(this).click(function(){
						let img_src = this.src;					
						$('#projects-carousel .active .src-image').attr('src', img_src);
					});
				});
			});
		}
	});	
</script>
@endsection