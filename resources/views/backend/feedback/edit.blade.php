@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.update') }} {{ trans('cruds.feedback.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('feedback.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('feedback.store') }}" method="POST" enctype="multipart/form-data" id="add_form">
                    @csrf
                     <input type="hidden" name="id" value="{{encrypt($feedback->id)}}">
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.feedback.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($feedback->name) ? $feedback->name : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.feedback.fields.occupation') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="occupation" name="occupation" value="{{ old('name', isset($feedback->occupation) ? $feedback->occupation : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">{{ trans('cruds.feedback.fields.description') }}</label>
                        <textarea name="description" class="form-control" id="description" rows="5">
                            {!! $feedback->description !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.news.fields.image') }}</label>
                        <input type="file" class="form-control" id="image" name="image" value="{{ old('image', isset($feedback->image) ? $feedback->image : '') }}"><br>
                        <img src="{{asset(Storage::url($feedback->image)) ?? ''}}" alt="" height=100 width=100>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        CKEDITOR.replace('description');
        $("#add_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection