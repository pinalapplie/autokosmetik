@section('title', 'Dienstleistungen')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<div id="main-content" class="active">
			<div id="section-breadcrumb1" class="inner-banner-wrap">
				<img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
				<div class="container">
					<div class="row">
						<div class="content col-12">
							<h1>HERKÖMMLICH</h1>
							<ul>
								<li><a href="{{ route('index')}}">Zuhause</a></li>
								<li><a href="{{ route('services')}}">Dienstleistungen</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="section-services2" class="section-car-ceramic">
				<div class="container">
					<div class="row">
						<div class="left col-sm-12 col-md-8">
                            <h1>INNENAUFBEREITUNG HERKÖMMLICH</h1>
							<p>
                            Bei einer herkömmlichen Innenaufbereitung arbeiten wir mit einem neutralen, ökologischen Universalreiniger. Der ganze Innenraum inkl. Kofferraum wird gesaugt, Teppiche und Sitze werden shampooniert bzw. das Leder wird gereinigt und mit einer professionellen Ledermilch eingepflegt.
							</p>
							<ul>
								<li class="circle">inkl. shampoonieren</li>
								<li class="circle">Lederreinigung und -pflege</li>
								<li class="circle">Reinigung mit Tornador</li>
							</ul>
						</div>
						<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
                            <img class="img-fluid" src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="autokosmetik">
						</div>
                        <div class="col-md-12">
                            <p>Bei der sogenannten "Tornadorreinigung" wird mit sehr hohem Luftdruck der Innenraum (ohne Himmel) inkl. Armaturen, allen Verkleidungen, Einstiegen, Fälzen und Schlitzen optimal gereinigt. Zum Abschluss werden die Innenscheiben professionell nur mit Wasser gereinigt. <br>Sitze und Teppiche mit problematischen Flecken werden nach dem Shampoonieren getrockenet und gegebenenfalls nochmals shampooniert oder händisch nachbearbeitet. <br>Anbei finden Sie die komplette Preisliste für die Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
                        </div>
					</div>
				</div>
			</div>
			<div id="section-portfoliodetails1" class="ceramic-coting-images">
				<div class="container">
					<div class="row">
						<!-- Related Project -->
						<div class="related-projects col-12">
                            <h2 class="h5">Reinigung mit Tornador</h2>
                            <div class="row">
                                <div class="item col-sm-12 col-md-4 my-3">
                                    <a href="#">
                                        <div class="img-container">
                                            <img class="img-fluid" src="{{ asset('assets/frontend/images/auto-10.jpg') }}" alt="autokosmetik">
                                        </div>
                                    </a>
                                </div>
                                <div class="item col-sm-12 col-md-4 my-3">
                                    <a href="#">
                                        <div class="img-container">
                                            <img class="img-fluid" src="{{ asset('assets/frontend/images/auto-11.jpg') }}" alt="autokosmetik">
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center pb-5">
                            <a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection    
	