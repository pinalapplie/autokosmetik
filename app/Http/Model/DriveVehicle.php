<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DriveVehicle extends Model
{
    use SoftDeletes;  
    protected $guarded = [];
    protected $table ="drive_vehicles";
    protected $dates = ['deleted_at'];

    protected $with = ['vehicle_brand_name'];

    public function vehicle_brand_name()
    {
        return $this->hasOne('App\Http\Model\VehicleBrands','id','vehicle_brand_id');
    }

    public function vehicle_category_name()
    {
        return $this->hasOne('App\Http\Model\VehicleCategory','id','vehicle_categorie_id');
    }
}
