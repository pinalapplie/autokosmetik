<?php


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/



Route::group(['middleware' => ['web']], function () {


	Route::get('clear', function (){
    	\Artisan::call('config:cache');
        \Artisan::call('cache:clear');
        \Artisan::call('route:clear');
        \Artisan::call('view:clear');
        \Artisan::call('storage:link');
        echo "ok";
	});

	// Route::get('/link', function () {        
 //   		$target = '/kunden/351322_8001/web/2709369/A2709369/autokosmetik.li/storage/app/public';
 //   		//echo public_path();
 //   		$shortcut = '/kunden/351322_8001/web/2709369/A2709369/autokosmetik.li/public/storage';
 //   		symlink($target, $shortcut);
	// });

	
	Route::get('/register',function () {return redirect('admin/login'); });
	/* Authentication Routes... */
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('login', 'Auth\LoginController@login');
	Route::post('logout', 'Auth\LoginController@logout')->name('logout');
	  
	/* Registration Routes... */
	//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
	//Route::post('register', 'Auth\RegisterController@register');
	  
	/* Password Reset Routes... */
	// Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
	// Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	// Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
	// Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
	  
	/* Email Verification Routes... */
	// Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
	// Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
	// Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

	Route::get('/', function () {
	     return redirect('admin/vehicletypes');
	});
	Route::group(['middleware' => 'auth'], function () {
		/*Vehicle Type Route*/
		Route::resource('/vehicletypes', 'VehicleTypeController');  
		Route::resource('/servicecategories', 'ServiceCategoryController');
		Route::resource('/servicesubcategories', 'ServiceSubCategoryController');
        Route::resource('/vehiclecategories', 'VehicleCategoryController');  
		Route::resource('/vehicleconditions', 'VehicleConditionsController');  
		Route::resource('/vehiclebrands', 'VehicleBrandsController');  
		Route::resource('/drivevehicles', 'DriveVehicleController');
		Route::resource('/vehicleservices', 'VehicleServicesController');  
		Route::resource('/servicemangement', 'ServiceManagementController');
        Route::resource('/portfolio', 'PortfolioController');
        Route::resource('/news', 'NewsController');
        Route::resource('/addonservice', 'AddonserviceController');
        Route::resource('/feedback', 'FeedbackController');
	});    
});