@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.update') }} {{ trans('cruds.vehiclebrands.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('vehiclebrands.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('vehiclebrands.store') }}" method="POST" enctype="multipart/form-data" id="add_form">
                    @csrf
                     <input type="hidden" name="id" value="{{encrypt($vehiclebrands->id)}}">
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.vehiclebrands.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($vehiclebrands->name) ? $vehiclebrands->name : '') }}">
                    </div>
                    <!-- <div class="form-group">
                        <label for="category">{{ trans('cruds.vehiclebrands.fields.vehicle_category') }}</label>
                        <select class="form-control" name="vehicle_categorie_id" id="category">
                           <option value="">{{ trans('cruds.vehiclebrands.fields.select_vehicle_category') }}</option>  
                            @foreach($vehicle_category as $value)
                                @if($value->id == $vehiclebrands->vehicle_categorie_id)
                                    <option value="{{$value->id}}" selected="">{{$value->name}}</option>  
                                @else
                                    <option value="{{$value->id}}">{{$value->name}}</option>  
                                @endif
                            @endforeach

                        </select>
                    </div> -->
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.news.fields.image') }}</label>
                        <input type="file" class="form-control" id="image" name="image" value="{{ old('image', isset($vehiclebrands->image) ? $vehiclebrands->image : '') }}"><br>
                        <img src="{{asset(Storage::url($vehiclebrands->image)) ?? ''}}" alt="" height=100 width=100>
                    </div>
                    <div class="form-group">
                        <label for="vehicle_type">{{ trans('cruds.servicecategories.fields.vehicle_type') }}</label>
                        <!-- <select class="form-control" name="vehicle_type_id" id="vehicle_type">
                            @foreach($vehicle_type as $value)
                                @if(old('vehicle_type_id') == $value->id || $value->id == $vehiclebrands->vehicle_type_id)
                                    <option value="{{$value->id}}" selected="">{{$value->name}}</option>  
                                @else
                                    <option value="{{$value->id}}">{{$value->name}}</option>  
                                @endif
                            @endforeach
                        </select> -->
                         <select name="vehicle_type_ids[]"  class="select2 form-control select2-multiple" multiple="multiple" data-placeholder="select vehicle types">
                            <optgroup label="select vehicle types">
                                @php $temp_array = explode(",",$vehiclebrands->vehicle_type_ids); @endphp
                                @foreach($vehicle_type as $value)
                                    @if(in_array($value->id,$temp_array))
                                        <option value="{{$value->id}}" selected="">{{$value->name}}</option>  
                                    @else
                                        <option value="{{$value->id}}">{{$value->name}}</option>  
                                    @endif    
                                @endforeach
                            </optgroup>
                        </select>
                    </div>    
                    

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        $(".select2").select2();
        $("#add_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
                "vehicle_type_ids[]": {
                         required: true
                }
            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                },
                "vehicle_type_ids[]": {
                    required: "@lang('validation.required',['attribute'=> 'Vehicle Type'])"
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection