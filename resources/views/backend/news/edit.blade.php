@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.update') }} {{ trans('cruds.news.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('news.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('news.store') }}" method="POST" enctype="multipart/form-data" id="add_form">
                    @csrf
                     <input type="hidden" name="id" value="{{encrypt($news->id)}}">
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.news.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($news->name) ? $news->name : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">{{ trans('cruds.news.fields.description') }}</label>
                        <textarea name="description" class="form-control" id="description" rows="5">
                            {!! old('description', isset($news->description) ? $news->description : '') !!}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.news.fields.date') }}<span class="required_class">*</span></label>
                        <input type="date" class="form-control" id="date" name="date" value="{{ old('date', isset($news->date) ? $news->date : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.news.fields.image') }}<span class="required_class">*</span></label>
                        <input type="file" class="form-control" id="image" name="image" value="{{ old('image', isset($news->image) ? $news->image : '') }}">
                    </div>
                    <div class="mb-3">
                        <img src="{{asset(Storage::url($news->image)) ?? ''}}" alt="" height=100 width=100>
                    </div>
                    <button type="submit" class="btn btn-primary">{{ trans('global.submit') }}</button>
                </form>
            </div>
        </div>  
    </div>
</div>  
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        CKEDITOR.replace('description');
        //CKEDITOR.replace('short_description');
        $("#add_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
                // description: {
                //     required: function(textarea) {
                //         CKEDITOR.instances[textarea.id].updateElement();
                //         var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                //         return editorcontent.length === 0;
                //     }
                // },
                // service_category_id: {
                //     required: true
                // },
                // type: {
                //     required: true
                // }

            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                },
                // description: {
                //     required: "@lang('validation.required',['attribute'=>'Description'])",
                // },
                // service_category_id: {
                //     required: "@lang('validation.required',['attribute'=>'Category'])",
                // },
                // type: {
                //     required: "@lang('validation.required',['attribute'=>'Type'])",
                // }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection