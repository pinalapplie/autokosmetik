
<table class="table border">
    <thead>
        <tr>
            <th scope="row" class="w-75">Dienst-Typ</th>
            <th scope="row">Preis (CHF)</th>
        </tr>
    </thead>
    <tbody>
    	@php $total = 0;@endphp
    	@foreach ($service_management as $key => $value)
    		@php $total = $total + $value->price; @endphp 
	     	<tr>
	     	 	<td class="border-0">
	     	 	 	{{ $value->name}}
	     	 	</td>
	     	 	<td class="border-0">
	     	 	 	{{ $value->price}}
	     	 	</td>
	     	</tr>
     	@endforeach	
    </tbody>
    <tfoot>
    	<tr>
     	 	<th>Total</th>
     	 	<th>{{$total}}</th>
     	</tr>
    </tfoot>
</table>
    