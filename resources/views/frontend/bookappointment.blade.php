@section('title', 'Book Appointment')
@extends('layouts.front_end')
@section('content')

<style>
#loader {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: rgba(0, 0, 0, 0.9);
    z-index: 999999;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
}
#loader .loading {
    border: 10px solid #fff;
    border-radius: 50%;
    border-top: 10px solid transparent;
    width: 60px;
    height: 60px;
    animation: spin 1s linear infinite;
    background: transparent;
}
@keyframes spin {
  100% {
    transform: rotate(360deg);
  }
}
</style>

	<div class="main-wrapper bg-light">
		<div id="main-content">
			<!-- Section Breadcrumb 1 -->
			<div id="section-breadcrumb1" class="inner-banner-wrap" style="z-index: 1;">
				<img src="{{ asset('assets/frontend/images/bookappointment.jpg') }}" alt="" class="inner-page-banner">
				<div class="container">/
					<div class="row">
						<div class="content col-12">
							<h1>Termin buchen</h1>
							<ul>
								<li><a href="{{ route('index')}}">HOME</a></li>
								<li class="current text-light">Termin buchen</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="position-realtive">
				<div class="container-fluid py-lg-5 position-realtive">
					<!-- <div class="service-img-book">
						<img src="{{ asset('assets/frontend/images/repairing-service.png') }} " alt="">
					</div> -->
					<div class="row my-5 no-gutters flex-lg-row-reverse flex-column-reverse">
						<div class="col-md-6 wizard-container">
							<div class="h-100 step-form-wrap">
								<div class="book-appo-wrap border-right p-lg-5 ">
									<div class="slide-right-anim align-self-center">
										<div class="step-block">
											<div class="row m-0">
												<div class="col-md-8">
													<h3 class="h3 font-weight-bold text-capitalize pb-3">Rechnung</h3>
													<p class="h6 font-weight-bold m-0 text-capitalize" id="fname"></p>
                                                    <p class="h6 font-weight-bold m-0" id="faddress"></p>
                                                    <span class="h6 font-weight-bold" id="fzip"></span>
                                                    <span class="h6 font-weight-bold" id="fcity"></span>
													<!-- <p class="h6 font-weight-bold m-0" id="femail"></p> -->
                                                    <!-- <p class="h6 font-weight-bold m-0" id="fnumber"></p> -->
                                                    <p class="h6 font-weight-bold" id="fstate"></p>
												</div>
												<div class="col-md-4 text-right">
													<img src="{{ asset('assets/frontend/images/logo-bkp.png') }}" alt="" class="img-fluid">
													<p class="h6 font-weight-bold pt-2 m-0">Autokosmetik AG</p>
													<p class="h6 font-weight-bold">Messinastrasse 1 <br> 9495 Triesen <!-- <br> Liechtenstein --></p>
												</div>
												<div class="col-md-12">
                                                    <ul class="list-unstyled list-inline font-weight-bold">
                                                        <li class="list-inline-item h5" id="yourVehicle"></li>
                                                        <li class="list-inline-item" id="yourVehicleConditionId"></li>
                                                    </ul>
												</div>
												<div class="col-md-12">
													<table class="table border">
														<thead>
															<tr>
																<th scope="row">
																	<ul class="list-unstyled justify-content-between d-flex w-100">
																		<li>Dienstleistungen</li>
																		<li>Preis</li>
																	</ul>
																</th>
															</tr>
														</thead>
														<tbody id="selectService">
														</tbody>
                                                        <tbody>
                                                            <tr><td class="pb-1" id="glassSerice"></td></tr>
                                                            <tr><td class="pb-1" >Extra Service<div id="addOnServices"></div></td></tr>
                                                            <tr>
                                                                <td>
                                                                    <ul class="list-unstyled justify-content-between d-flex w-100">
                                                                        <li class="font-weight-bold">Inklusive 7.7% MwSt.</li>
                                                                        <li id="serviceInclusiveTax"></li>
                                                                    </ul>
                                                                    <ul class="list-unstyled justify-content-between d-flex w-100">
                                                                        <li class="font-weight-bold">Total</li>
                                                                        <li id="serviceTotal"></li>
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                        </tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>
						<div class="col-md-6 position-relative">
							<form action="" class="d-flex flex-column h-100 wizard-container" id="js-wizard-form">
								<div class="progress" id="js-progress">
									<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
										<span class="progress-val">0%</span>
									</div>
								</div>
								<ul class="nav nav-tab">
									<li class="progress_nav_tab progress_nav_tab_0 active">
										<a  href="#" data-toggle="tab"></a>
									</li>
									<li class="progress_nav_tab progress_nav_tab_1">
										<a href="#" data-toggle="tab"></a>
									</li>
									<li class="progress_nav_tab progress_nav_tab_2 ">
										<a href="#" data-toggle="tab"></a>
									</li>
									<li class="progress_nav_tab progress_nav_tab_2_0 ">
										<a href="#" data-toggle="tab"></a>
									</li>
									<li class="progress_nav_tab progress_nav_tab_3">
										<a href="#" data-toggle="tab"></a>
									</li>
									<li class="progress_nav_tab progress_nav_tab_4">
										<a href="#" data-toggle="tab"></a>
									</li>
									<li class="progress_nav_tab progress_nav_tab_5">
										<a href="#" data-toggle="tab"></a>
									</li>
									<li class="progress_nav_tab progress_nav_tab_6">
										<a href="#" data-toggle="tab"></a>
									</li>
									<li class="progress_nav_tab progress_nav_tab_7">
										<a href="#" data-toggle="tab"></a>
									</li>
									<li class="progress_nav_tab progress_nav_tab_8">
										<a href="#" data-toggle="tab"></a>
									</li>
									<li class="progress_nav_tab progress_nav_tab_9">
										<a href="#" data-toggle="tab"></a>
									</li>
									<li class="progress_nav_tab progress_nav_tab_10">
										<a href="#" data-toggle="tab"></a>
									</li>
								</ul>
								

								<div class="h-100 step-form-wrap tab-content w-100" style="position: relative;">
									<div class="alert alert-danger section_error" style="display: none;position: absolute;width: 100%;text-align: center;">
										 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										Bitte wählen Sie mindestens eine Auswahl
									</div>

									<div class="tab-pane step_0 active">
										<div class="book-appo-wrap p-lg-5 py-3">
											<div class="slide-right-anim">
												<h3 class="pb-3 h3 text-capitalize text-center">Wählen Sie Ihr Fahrzeug</h3>
												<div class="step-block">
													<div class="form-row align-items-center justify-content-center mx-0">
														
														@foreach($vehicle_type as $value)
															<div class="col-6 text-center" >
																<div class="card select-category">
																	<input role="button" type="radio" name="step_0" class="form-control checkbox" id="{{$value->name}}" value="{{$value->id}}">
																	<div class="card-body px-2 py-0">
																		<div class="avatar-lg mb-1">
																			<img src="{{asset(Storage::url($value->image)) ?? ''}}" alt="" class="img-fluid">
																		</div>
																		<p class="font-weight-bold h5 text-dark mt-2">{{$value->name}}</p>
																	</div>
																</div>
															</div>
														@endforeach
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane step_1 ">
										<div class="book-appo-wrap p-lg-5 py-3">
											<div class="slide-right-anim">
												<div class="btn-next-con d-flex">
													<a href="javascript:void(0)" class="btn-back backBtn comman-btn button  " data-current="step_1" data-last="step_0"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Vorherige</a>
												</div>
												<h3 class="pb-3 h3 text-capitalize text-center">Fahrzeugzustand</h3>
												<div class="step_1_block">

												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane step_2 ">
										<div class="book-appo-wrap p-lg-5 p-3">
											<div class="slide-right-anim">
												<div class="btn-next-con d-flex">
													<a href="javascript:void(0)" class="btn-back backBtn comman-btn button  " data-current="step_2" data-last="step_1"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Vorherige</a>
												</div>
												<h3>Fahrzeugtyp wählen</h3>
												<div class="step-block">
													<div class="step_2_block">
														<div class="form-row align-items-center justify-content-center mx-0">

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane step_3 step_4 step_6 step_22">
										<div class="book-appo-wrap p-lg-5 p-3">
											<div class="slide-right-anim">
												<div class="btn-next-con d-flex">
													<a href="javascript:void(0)" class="btn-back backBtn comman-btn button  " data-current="step_4" data-last="step_2"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Vorherige</a>
												</div>
												<h3 class="pb-3 h3 text-capitalize text-center">Dienste auswählen</h3>
												<div class="step-block service-accordian-main" id="accordionExample">
                                                    <div class="step_3_block">

                                                    </div>
                                                    <div class="row align-items-center justify-content-center mx-0">
                                                        <div class="col-md-12 my-5 step_4_block">
                                                        
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row align-items-center justify-content-center mx-0">
                                                        <div class="col-md-12 step_6_block">
                                                        
                                                        </div>
                                                    </div>
                                                    
                                                   <!--  <div class="row align-items-center justify-content-center mx-0">
                                                        <div class="col-md-12 my-5 step_22_block">
                                                        
                                                        </div>
                                                    </div> -->
                                                    <a class="btn-next button comman-btn d-block w-50 mx-auto step_btn_4" role="button">Weiter</a>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane step_5 ">
										<div class="book-appo-wrap p-lg-5 p-3">
											<div class="slide-right-anim">
												<div class="btn-next-con d-flex">
													<a href="javascript:void(0)" class="btn-back backBtn comman-btn button  " data-current="step_5" data-last="step_4"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Vorherige</a>
												</div>
												<h3>Extra Service</h3>
												<div class="step-block">
													<div class="step_5_block">

													</div>
                                                    <a class="btn-next button comman-btn d-block w-50 mx-auto step_btn_5" role="button">Weiter</a>
												</div>
											</div>
										</div>
									</div>                                    
									<div class="tab-pane  step_8" >
										<div class="book-appo-wrap p-lg-5 p-3">
											<div class="slide-right-anim">
												<div class="btn-next-con d-flex">
													<a href="javascript:void(0)" class="btn-back backBtn comman-btn button "  data-current="step_8" data-last="step_5" href="javascript:void(0)"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Vorherige</a>
												</div>
												<h3 class="h3 text-capitalize pb-3">Termin buchen</h3>
												<div class="step-block container-fluid">
													<form action="">
														<div class="mb-3">
															<label for="">Vollständiger Name*</label>
															<input type="text" name="" id="contact_name" class="form-control bookAppointment"  required>
															<div class="name_form_error" style="color: red;display: none"></div>
														</div>
														<div class="mb-3">
															<label for="">Kontakt Nummer*</label>
															<input type="tel" name="" id="contact_number" class="form-control bookAppointment" required>
															<div class="number_form_error" style="color: red;display: none"></div>
														</div>
														<div class="mb-3">
															<label for="">E-Mail*</label>
															<input type="email" name="" id="contact_email" class="form-control bookAppointment" required>
															<div class="email_form_error" style="color: red;display: none"></div>
														</div>
														<div class="mb-3">
															<label for="">Strasse und Nummer*</label><br>
															<input type="text" name="" id="contact_address" class="form-control bookAppointment"  required>
															<div class="address_form_error" style="color: red;display: none"></div>
														</div>
                                                        <div class="mb-3  row">
                                                            <div class="col-md-6">
                                                                <label for="zip">Plz*</label>
                                                                <input type="text" name="zip" id="contact_zip" class="form-control bookAppointment" required>
                                                                <div class="zip_form_error" style="color: red;display: none"></div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label for="city">Stadt*</label>
                                                                <input type="text" name="city" id="contact_city" class="form-control bookAppointment" required>
                                                                <div class="city_form_error" style="color: red;display: none"></div>
                                                            </div>
                                                        </div>
                                                        <div class="mb-3">
                                                        	<label for="state">Land*</label>
                                                            <input type="text" name="state" id="contact_state" class="form-control bookAppointment" required>
                                                            <div class="state_form_error" style="color: red;display: none"></div>
                                                        </div>
														<div class="mb-3 text-right">
															<button type="button" class="btn-next comman-btn button final_submit_btn w-100" role="button">Termin bestätigen</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane step_10" >
										<div class="book-appo-wrap align-items-center justify-content-center flex-column">
											<div class="mb-3 avatar-md">
												<img src="{{ asset('assets/frontend/images/tick.png') }}" alt="" class="img-fluid">
											</div>
											<div>
												<h6 class="text-center h1 font-weight-bold mb-0" style="color: #44c4a1;">Dankeschön</h6>
												<p>Unser Team wird sich innerhalb von <span class="font-weight-bold">24</span> Stunden melden.</p>
											</div>
										</div>
									</div> 

								</div>
							</form>
							<div id="loader" style="display:none"> 
								<span class="loading"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@include('frontend.modal')  <!-- All modal in this blade file  -->
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
	   var yourVehicleCondition = [];
		var _token1 = $('meta[name="csrf-token"]').attr('content');
		$('body').on('click','.backBtn',function(){
			var current = $(this).attr('data-current');
			var last = $(this).attr('data-last');
			
			$('.'+current).removeClass('active');
			$('.'+last).addClass('active');
			$('.section_error').hide();
		});
		$('body').on('click',"input[name='step_0']",function(){
			$('#loader').show();
			var step_0 = $("input[name='step_0']:checked").val();
			//alert(step_1);
			if(step_0 == "" || step_0 == undefined){
				$('.section_error').show();
			}else{
				var step_0_checkbox = [];  
	           	$("input[name='step_0']").each(function(){  
	                if($(this).is(":checked"))  
	                {  
	                	step_0_checkbox.push($(this).val());  
	                }  
	           });  
	           step_0_checkbox = step_0_checkbox.toString(); 
	           	
	           	$.ajax({
                    method: 'POST',
                    url: "{{ route('ajax.step0') }}",
                    data: {
                    	"_token": "{{ csrf_token() }}",
                        "id": step_0_checkbox,
                    }
                })
                .done(function(data) {
                	if(data.result == 1){
                   		$('.step_1 .step_1_block').html(data.html);
	                }else{
	                	$('.step_1 .step_1_block').html("<div><center>No data found.<center></div>");
                   	}
                   	$('.section_error').hide();
					$('.step_0,.progress_nav_tab').removeClass('active');
					$('.step_1,.progress_nav_tab_1').addClass('active');
					$('.progress-bar').width('10%');
					$('.progress-val').text('10%');
                   	
                    $('#yourVehicle').html($("input[name='step_0']:checked").attr('id')+' : ');
                })
			}
			$('#loader').hide();
		});
		$('body').on('click',"input[name='step_1']",function(){
			$('#loader').show();
			var step_1 = $("input[name='step_0']:checked").val();
			//alert(step_1);
			if(step_1 == "" || step_1 == undefined){
				$('.section_error').show();
				$('#loader').hide();
			}else{
				// var step_1_checkbox = [];  
	   //         	$("input[name='step_1']").each(function(){  
	   //              if($(this).is(":checked"))  
	   //              {  
	   //              	step_1_checkbox.push($(this).val());  
	   //              }  
	   //         });  
	   //         step_1_checkbox = step_1_checkbox.toString(); 
                //var step_1 = $("input[name='step_1']:checked").val();
	           	$.ajax({
                    method: 'POST',
                    url: "{{ route('ajax.step2') }}",
                    data: {
                    	"_token": "{{ csrf_token() }}",
                        "id": step_1,
                    }
                })
                .done(function(data) {
                   	if(data.result == 1){
                   		$('.step_2 .step_2_block').html(data.html);
	                }else{
	                	$('.step_2 .step_2_block').html("<div><center>No data found.<center></div>");
                   	}
                   	
                	$('.section_error').hide();
					$('.step_1,.progress_nav_tab').removeClass('active');
					$('.step_2,.progress_nav_tab_2').addClass('active');
					$('.progress-bar').width('20%');
					$('.progress-val').text('20%');
                    
                    yourVehicleCondition = [];                    
                    yourVehicleCondition.push($("input[name='step_1']:checked").attr('v-id'));
                    $('#yourVehicleConditionId').html(yourVehicleCondition.join(", "));
                })
			}
			$('#loader').hide();
		});
		$('body').on('click',"input[name='step_20']",function(){
			$('#loader').show();
			var step_20 = $("input[name='step_20']:checked").val();
			//alert(step_1);
			if(step_20 == "" || step_20 == undefined){
				$('.section_error').show();
			}else{
				var step_20_checkbox = [];  
	           	$("input[name='step_20']").each(function(){  
	                if($(this).is(":checked"))  
	                {  
	                	step_20_checkbox.push($(this).val());  
	                }  
	           });  
	           step_20_checkbox = step_20_checkbox.toString(); 
	           	
	           	$.ajax({
                    method: 'POST',
                    url: "{{ route('ajax.step20') }}",
                    data: {
                    	"_token": "{{ csrf_token() }}",
                        "id": step_20_checkbox,
                    }
                })
                .done(function(data) {
                   	if(data.result == 1){
                   	    $('.step_4_block').html('');
                        $('#selectService').html('');
                   		$('.step_3 .step_3_block').html(data.html);
	                }else{
	                	$('.step_3 .step_3_block').html("<div><center>No data found.<center></div>");
                   	}
                   	$('.section_error').hide();
					$('.step_2,.progress_nav_tab').removeClass('active');
					$('.step_3,.progress_nav_tab_3').addClass('active');
					$('.progress-bar').width('30%');
					$('.progress-val').text('30%');
                    
                    getAllServices();
                    getGlassServices();
                    
                    yourVehicleCondition.push($("input[name='step_20']:checked").attr('v-id'));
                    $('#yourVehicleConditionId').html(yourVehicleCondition.join(", "));
                })
			}
			$('#loader').hide();
		});

		/*$('body').on('click',"input[name='step_3']",function(){
            var gid = $(this).attr('g-id');
            if(gid == 1){
                if($(this).is(":checked")) {
        			$('#loader').show();
                   	var step_0_checkbox = $("input[name='step_0']:checked").val();
                   	$.ajax({
                        method: 'POST',
                        url: "{{ route('ajax.step6') }}",
                        data: {
                        	"_token": "{{ csrf_token() }}",
                            "id": step_0_checkbox
                        }
                    })
                    .done(function(data) {
                       	if(data.result == 1){
                            //$('.step_6 .step_6_block').html(data.html);
                            $('.step_4 .step_4_block').html(data.html);
                        }else{
                        	//$('.step_6 .step_6_block').html("<div><center>No data found.<center></div>");
                            $('.step_4 .step_4_block').html("<div><center>No data found.<center></div>");                            
                       	}
                       	//$('.step_6 .step_6_block').html(data);
                    	$('.section_error').hide();
    					$('.step_3,.progress_nav_tab').removeClass('active');
    					$('.step_4,.progress_nav_tab_4').addClass('active');
        				$('.progress-bar').width('60%');
        				$('.progress-val').text('60%');
                        
                        //$('#selectService').html('');
                        priceCal();                        
                    });
        			$('#loader').hide();
                } else {
                    $('.step_6_block').html('');
                    $('.step_22_block').html('');
                    $('#glassSerice').html('');
                    priceCal();
                }
            }else{
                if($(this).is(":checked")) {
                    $('#loader').show();
                    var step_0 = $("input[name='step_0']:checked").val(); //Choose Your Vehicle
                    var step_2 = $("input[name='step_1']:checked").val(); //Your Vehicle Condition
                    var step_20 = $("input[name='step_20']:checked").val();//Select vehicle type
        			
                    var step_3 = $(this).val();
        			
        			if(step_3 == "" || step_3 == undefined){
        				$('.section_error').show();
        			}else{
        				var step_3_checkbox = [];  
        	           	$("input[name='step_3']").each(function(){  
        	                if($(this).is(":checked"))  
        	                {  
        	                	step_3_checkbox.push($(this).val());  
        	                }  
        	           });  
        	           step_3_checkbox = step_3_checkbox.toString(); 
        	           	
        	           	$.ajax({
                            method: 'POST',
                            url: "{{ route('ajax.step3') }}",
                            data: {
                            	"_token": "{{ csrf_token() }}",
                                "id": step_3,
                                "vehicle": step_0,
                                "vehicle_condition": step_2,
                                "vehicle_type": step_20,
                            }
                        })
                        .done(function(data) {
                           	if(data.result == 1){
                           		$('.step_4 .step_4_block').html(data.html);
        	                }else{
        	                	$('.step_4 .step_4_block').html("<div><center>No data found.<center></div>");
                           	}
        					//$('.step_4 .step_4_block').html(data);
                        	$('.section_error').hide();
        					$('.step_3,.progress_nav_tab').removeClass('active');
        					$('.step_4,.progress_nav_tab_4').addClass('active');
        					$('.progress-bar').width('40%');
        					$('.progress-val').text('40%');
                            //$('#selectService').html('');
                            $('#glassSerice').html('');
                            $('.step_22_block').html('');
                            selectedServices();
                            priceCal();
                        })
        			}
        			$('#loader').hide(); 
                }else{
                    var step_3 = $(this).val();
                    $('#mainService'+step_3).remove();
                    $('#service_'+step_3).remove();
                    
                    //var cid = $(this).val();
                    $('#tdservice_'+step_3).remove(); 
                }               
            }
            
		});*/
        
        function getAllServices(){
            $('#loader').show();
            var step_0 = $("input[name='step_0']:checked").val(); //Choose Your Vehicle
            var step_2 = $("input[name='step_1']:checked").val(); //Your Vehicle Condition
            var step_20 = $("input[name='step_20']:checked").val();//Select vehicle type
               	
            $.ajax({
                method: 'POST',
                url: "{{ route('ajax.step3') }}",
                data: {
                	"_token": "{{ csrf_token() }}",
                    "vehicle": step_0,
                    "vehicle_condition": step_2,
                    "vehicle_type": step_20,
                }
            })
            .done(function(data) {
               	if(data.result == 1){
               		$('.step_4 .step_4_block').append(data.html);
                }else{
                	$('.step_4 .step_4_block').html("<div><center>No data found.<center></div>");
               	}
            	//$('.step_4 .step_4_block').html(data);
            	$('.section_error').hide();
            	//$('.step_3,.progress_nav_tab').removeClass('active');
            	//$('.step_4,.progress_nav_tab_4').addClass('active');
            	$('.progress-bar').width('40%');
            	$('.progress-val').text('40%');
                //$('#selectService').html('');
                //$('#glassSerice').html('');
                //$('.step_22_block').html('');
                //selectedServices();
                //priceCal();
            });
    		
    		$('#loader').hide();             
        }
        function getGlassServices(){
    		$('#loader').show();
           	var step_0_checkbox = $("input[name='step_0']:checked").val();
           	var step_20 = $("input[name='step_20']:checked").val();//Select vehicle type

           	$.ajax({
                method: 'POST',
                url: "{{ route('ajax.step6') }}",
                data: {
                	"_token": "{{ csrf_token() }}",
                    "id": step_0_checkbox,
                    "vehicle_type": step_20
                }
            })
            .done(function(data) {
               	if(data.result == 1){
                    //$('.step_6 .step_6_block').html(data.html);
                    $('.step_4 .step_4_block').append(data.html);
                }else{
                	//$('.step_6 .step_6_block').html("<div><center>No data found.<center></div>");
                    $('.step_4 .step_4_block').html("<div><center>No data found.<center></div>");                            
               	}
               	//$('.step_6 .step_6_block').html(data);
            	$('.section_error').hide();
    			//$('.step_3,.progress_nav_tab').removeClass('active');
    			//$('.step_4,.progress_nav_tab_4').addClass('active');
    			$('.progress-bar').width('60%');
    			$('.progress-val').text('60%');
                
                //$('#selectService').html('');
                //priceCal();                        
            });
    		$('#loader').hide();
        }
        $('body').on('click',"input[name='step_3']",function(){
            if($(this).is(":checked")) {
                var step_3 = $(this).val();
                $('.all_service').hide();
                $('.vehicleBrands').hide();
                $('#mainService'+step_3).show();
                
                selectedServices();
                priceCal();
            }
        });
		/*$('body').on('click',"input[name='step_4']",function(){
			$('#loader').show();
			var step_4 = $("input[name='step_4']:checked").val();
			//alert(step_1);
			if(step_4 == "" || step_4 == undefined){
				$('.section_error').show();
			}else{
				var step_4_checkbox = [];  
	           	$("input[name='step_1']").each(function(){  
	                if($(this).is(":checked"))  
	                {  
	                	step_4_checkbox.push($(this).val());  
	                }  
	           });  
	           step_4_checkbox = step_4_checkbox.toString(); 
	           	
	           	$.ajax({
                    method: 'POST',
                    url: "{{ route('ajax.step4') }}",
                    data: {
                    	"_token": "{{ csrf_token() }}",
                        "id": step_4_checkbox,
                    }
                })
                .done(function(data) {
                   	if(data.result == 1){
                   		$('.step_5 .step_5_block').html(data.html);
	                }else{
	                	$('.step_5 .step_5_block').html("<div><center>No data found.<center></div>");
                   	}
                   	//$('.step_5 .step_5_block').html(data);
                	$('.section_error').hide();
					$('.step_4,.progress_nav_tab').removeClass('active');
					$('.step_5,.progress_nav_tab_5').addClass('active');
					$('.progress-bar').width('50%');
					$('.progress-val').text('50%');
                    
                    yourVehicleCondition.push($("input[name='step_4']:checked").attr('v-id'));
                    $('#yourVehicleConditionId').html(yourVehicleCondition.join(", "));
                })
			}
			$('#loader').hide();
		});*/
		$('body').on('click',".step_btn_4",function(){
			$('#loader').show();
           	var step_4_checkbox = '';
           	$.ajax({
                method: 'POST',
                url: "{{ route('ajax.step4') }}",
                data: {
                	"_token": "{{ csrf_token() }}",
                    "id": step_4_checkbox
                }
            })
            .done(function(data) {
               	if(data.result == 1){
               		$('.step_5 .step_5_block').html(data.html);
                }else{
                	$('.step_5 .step_5_block').html("<div><center>No data found.<center></div>");
               	}
               	//$('.step_6 .step_6_block').html(data);
            	$('.section_error').hide();
				$('.step_4,.progress_nav_tab').removeClass('active');
				$('.step_5,.progress_nav_tab_6').addClass('active');
				$('.progress-bar').width('60%');
				$('.progress-val').text('60%');
            });
			
			$('#loader').hide();
		});
		$('body').on('click',".step_btn_6",function(){
			$('#loader').show();
			var step_6 = $("input[name='step_6']:checked").val();
			//alert(step_1);
			if(step_6 == "" || step_6 == undefined){
				$('.section_error').show();
			}else{
				var step_6_checkbox = [];  
	           	$("input[name='step_6']").each(function(){  
	                if($(this).is(":checked"))  
	                {  
	                	step_6_checkbox.push($(this).val());  
	                }  
	           });  
	           step_6_checkbox = step_6_checkbox.toString(); 
	           	
	           	$.ajax({
                    method: 'POST',
                    url: "{{ route('ajax.step6') }}",
                    data: {
                    	"_token": "{{ csrf_token() }}",
                        "id": step_6_checkbox,
                    }
                })
                .done(function(data) {
                   	if(data.result == 1){
                   		$('.step_7 .step_7_block').html(data.html);
	                }else{
	                	$('.step_7 .step_7_block').html("<div><center>No data found.<center></div>");
                   	}
                   	//$('.step_7 .step_7_block').html(data);
                	$('.section_error').hide();
					$('.step_6,.progress_nav_tab').removeClass('active');
					$('.step_7,.progress_nav_tab_7').addClass('active');
					$('.progress-bar').width('70%');
					$('.progress-val').text('70%');
                })
			}
			$('#loader').hide();
		});
		/*$('body').on('click',"input[name='step_7']",function(){
			$('#loader').show();
			var step_7 = $("input[name='step_7']:checked").val();
			//alert(step_7);
			if(step_7 == "" || step_7 == undefined){
				$('.section_error').show();
			}else{
				var step_7_checkbox = [];  
	           	$("input[name='step_7']").each(function(){  
	                if($(this).is(":checked"))  
	                {  
	                	step_7_checkbox.push($(this).val());  
	                }  
	           });  
	           step_7_checkbox = step_7_checkbox.toString(); 
	           	
	           	$.ajax({
                    method: 'POST',
                    url: "{{ route('ajax.step7') }}",
                    data: {
                    	"_token": "{{ csrf_token() }}",
                        "id": step_7_checkbox,
                    }
                })
                .done(function(data) {
                   	if(data.result == 1){
                   		$('.step_22 .step_22_block').html(data.html);
	                }else{
	                	$('.step_22 .step_22_block').html("<div><center>No data found.<center></div>");
                   	}
                   	//$('.step_8 .step_8_block').html(data);
                	$('.section_error').hide();
					//$('.step_7,.progress_nav_tab').removeClass('active');
					$('.step_22,.progress_nav_tab_8').addClass('active');
					$('.progress-bar').width('80%');
					$('.progress-val').text('80%');
                })
			}
			$('#loader').hide();
		});*/
        $('body').on('click',"input[name='step_7']",function(){
            if($(this).is(":checked")) {
                var step_7 = $(this).val();
                $('.vehicleBrands').hide();
                $('#vehicle_brands_'+step_7).show();
            }
        });
		$('body').on('click',".step_btn_5",function(){
        	$('.section_error').hide();
    		$('.step_5,.progress_nav_tab').removeClass('active');
    		$('.step_8,.progress_nav_tab_8').addClass('active');
    		$('.progress-bar').width('80%');
    		$('.progress-val').text('80%');
		});
		// $('body').on('click',"input[name='step_8']",function(){
		// 	var step_8 = $("input[name='step_8']:checked").val();
		// 	//alert(step_8);
		// 	if(step_8 == "" || step_8 == undefined){
		// 		$('.section_error').show();
		// 	}else{
		// 		var step_8_checkbox = [];  
	 //           	$("input[name='step_8']").each(function(){  
	 //                if($(this).is(":checked"))  
	 //                {  
	 //                	step_8_checkbox.push($(this).val());  
	 //                }  
	 //           });  
	 //           step_8_checkbox = step_8_checkbox.toString(); 
	           	
	 //           	$.ajax({
  //                   method: 'POST',
  //                   url: "{{ route('ajax.step8') }}",
  //                   data: {
  //                   	"_token": "{{ csrf_token() }}",
  //                       "id": step_8_checkbox,
  //                   }
  //               })
  //               .done(function(data) {
  //                  	$('.step_9 .step_9_block').html(data);
  //               	$('.section_error').hide();
		// 			$('.step_8,.progress_nav_tab').removeClass('active');
		// 			$('.step_9,.progress_nav_tab_9').addClass('active');
		// 			$('.progress-bar').width('90%');
		// 			$('.progress-val').text('90%');
  //               })
		// 	}
		// });
		$('body').on('click',".final_submit_btn",function(){
			$('#loader').show();
			var contact_name = $("#contact_name").val();
			var contact_number = $("#contact_number").val();
			var contact_email = $("#contact_email").val();
			var contact_address = $("#contact_address").val();
            var contact_zip = $("#contact_zip").val();
            var contact_city = $("#contact_city").val();
            var contact_state = $("#contact_state").val();
            $('.name_form_error,.number_form_error,.email_form_error,.address_form_error,.zip_form_error,.city_form_error,.state_form_error').hide();
			var count = 0;
			if(contact_name == "" || contact_name == undefined){
				$('.name_form_error').text('Bitte Name eingeben');
				$('.name_form_error').show();
				count = count +1;
			}
			if(contact_number == "" || contact_number == undefined){
				$('.number_form_error').text('Bitte Kontaktnummer eingeben');
				$('.number_form_error').show();
				count = count +1;
			}
			if(contact_email == "" || contact_email == undefined){
				$('.email_form_error').text('Bitte E-Mail eingeben');
				$('.email_form_error').show();
				count = count +1;
			}else if(!validateEmail(contact_email)){
				$('.email_form_error').text('Bitte gültige E-Mail eingeben');
				$('.email_form_error').show();
				count = count +1;
			}
			if(contact_address == "" || contact_address == undefined){
				$('.address_form_error').text('Bitte Adresse eingeben');
				$('.address_form_error').show();
				count = count +1;
			}
			if(contact_zip == "" || contact_zip == undefined){
				$('.zip_form_error').text('Bitte Postleitzahl eingeben');
				$('.zip_form_error').show();
				count = count +1;
			}
			if(contact_city == "" || contact_city == undefined){
				$('.city_form_error').text('Bitte Stadt eingeben');
				$('.city_form_error').show();
				count = count +1;
			}
			if(contact_state == "" || contact_state == undefined){
				$('.state_form_error').text('Bitte Zustand eingeben');
				$('.state_form_error').show();
				count = count +1;
			}
			if(count != 0){
				$('#loader').hide();
				return false;
			}else{
				var step_8_checkbox = [];  
	           	$("input[name='step_4']").each(function(){  
	                if($(this).is(":checked"))  
	                {  
	                	step_8_checkbox.push($(this).val());  
	                }  
	           	});  
	           	step_8_checkbox = step_8_checkbox.toString(); 
                
				var step_5_checkbox = [];  
	           	$("input[name='step_5']").each(function(){  
	                if($(this).is(":checked"))  
	                {  
	                	step_5_checkbox.push($(this).val());  
	                }  
	           	});  
	           	step_5_checkbox = step_5_checkbox.toString(); 
                var step_21 = $("input[name='step_21']:checked").val();
                var step_2 = $("input[name='step_1']:checked").val(); //Your Vehicle Condition
                var step_20 = $("input[name='step_20']:checked").val();//Select vehicle type
                
	           	$.ajax({
                    method: 'POST',
                    url: "{{ route('ajax.step8') }}",
                    data: {
                    	"_token": "{{ csrf_token() }}",
                        "id": step_8_checkbox,
                        "contact_name":contact_name,
                        "contact_number":contact_number,
                        "contact_email":contact_email,
                        "contact_address":contact_address,
                        "contact_zip":contact_zip,
                        "contact_city":contact_city,
                        "contact_state":contact_state,
                        "step_5_checkbox": step_5_checkbox,
                        "step_21": step_21,
                        "vehicle_condition": step_2,
                        "vehicle_type": step_20,
                    }
                })
                .done(function(data) {
                 	$('.section_error').hide();
					$('.step_8,.progress_nav_tab').removeClass('active');
					$('.step_10,.progress_nav_tab_10').addClass('active');
					
					$('.progress-bar').width('100%');
					$('.progress-val').text('100%');

					$('#loader').hide();
                    setTimeout(function(){ location.reload(); }, 5000);
                })
			}
		});
		function validateEmail($email) {
		  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		  return emailReg.test( $email );
		}
        
        function selectedServices() {
            var step_3_html = '';

            $("input[name='step_3']").each(function(){
                if($(this).is(":checked"))  
                {
                    var gid = $(this).attr('g-id');
                    if(gid == 0){
                        var cid = $(this).val();
                        if (document.getElementById('tdservice_'+cid) == null){
                            step_3_html += '<tr id="tdservice_'+cid+'"><td class="pb-1">'+$(this).attr('id')+'<ul id="service_'+cid+'"></ul></td></tr>';
                            $('#selectService').append(step_3_html);         
                        }
                    }
                }  
            });
        }
        $('body').on('click',"input[name='step_4']",function(){
            //var step_6_html = '';
            $("input[name='step_4']").each(function(){
                if($(this).is(":checked"))  
                {
                    var sSubVal = $(this).attr('s-id');
                    var pid = $(this).attr('p-id');
                    var sid = $(this).attr('s-id');
                    var cid = $(this).val();
    
                    if (document.getElementById('SubVal_'+sid) == null){
                        var step_4_html = '<li id="SubVal_'+sSubVal+'">'+$(this).attr('s-name');
                        step_4_html += '</li>';
                        
                        if (document.getElementById('SubVal_'+sid) == null){
                            $('#service_'+pid).append(step_4_html);     
                        }
                          
                    } else {
                        //alert('gk1');
                    }
                    
                    var step_6_html = '<ul class="list-unstyled justify-content-between d-flex w-100 pl-4" id="subService_'+cid+'"><li class="" id="SubTwoVal_'+sid+'">'+$(this).attr('c-name')+'</li><li class="sPrices" p-id="'+$(this).attr('price')+'" id="pService_'+sSubVal+'">CHF '+$(this).attr('price')+'</li></ul>';
                    if (document.getElementById('subService_'+cid) == null){
                        $('#SubVal_'+sid).append(step_6_html);     
                    } 
                } else {
                    var cid = $(this).val();
                    $('#subService_'+cid).remove();
                }
            });
            priceCal();
        });
        
        $('body').on('click',"input[name='step_5']",function(){
            $("input[name='step_5']").each(function(){
                var step_5_html = '';
                if($(this).is(":checked"))  
                {
                    var id = $(this).val();
                    var price = $(this).attr('p-id');
                    var name = $(this).attr('n-id');
                    
                	step_5_html += '<ul class="list-unstyled justify-content-between d-flex w-100 pl-4" id="addOnS_'+id+'"><li class="">'+name+'</li><li class="sPrices" p-id="'+price+'">CHF '+price+'</li></ul>'; 
                    
                    if (document.getElementById('addOnS_'+id) == null){
                       $('#addOnServices').append(step_5_html);     
                    }  
                    
                }else{
                    var id = $(this).val();
                    $('#addOnS_'+id).remove();    
                }
            });
            priceCal();
        });
        
        $('body').on('click',"input[name='step_21']",function(){
            $("input[name='step_21']").each(function(){
                var step_21_html = '';
                if($(this).is(":checked"))  
                {
                    var id = $(this).val();
                    var price = $(this).attr('p-id');
                    var name = $(this).attr('n-id');
                    
                	step_21_html += 'Scheiben tönen <ul class="list-unstyled justify-content-between d-flex w-100 pl-4" id="glassS_'+id+'"><li class="">'+name+'</li><li class="sPrices" p-id="'+price+'">CHF '+price+'</li></ul>'; 
                    
                    $('#glassSerice').html(step_21_html); 
                    
                }
            });
            priceCal();
        });
        
        $('body').on('click',"input[name='step_6']",function(){
            $("input[name='step_6']").each(function(){
                var step_6_html = '';
                if($(this).is(":checked"))  
                {
                    var sid = $(this).attr('s-id');
                    var sSubVal = $(this).val();
                	step_6_html += '<ul class="list-unstyled justify-content-between d-flex w-100 pl-4" id="subService_'+$(this).val()+'"><li class="sPrices" id="SubTwoVal_'+sSubVal+'">'+$(this).attr('v-id')+'</li></ul>'; 
                    
                    if (document.getElementById('subService_'+sSubVal) == null){
                       $('#SubVal_'+sid).append(step_6_html);     
                    }  
                    
                }else{
                    var sSubVal = $(this).val();
                    $('#subService_'+sSubVal).remove();    
                }
            });

        });
        
        /*$('body').on('click',"input[name='step_7']",function(){
            $("input[name='step_7']").each(function(){
                var step_7_html = '';
                if($(this).is(":checked"))  
                {
                    var sid = $(this).attr('s-id');
                    var pid = $(this).attr('p-id');
                    var sSubVal = $(this).val();
                	step_7_html += '<li class="sPrices" p-id="'+$(this).attr('p-id')+'" id="pService_'+sSubVal+'">CHF '+$(this).attr('p-id')+'</li>'; 
                    
                    if (document.getElementById('pService_'+sSubVal) == null){
                       $('#subService_'+sid).append(step_7_html);     
                    } 
                }else{
                    var sSubVal = $(this).val();
                    $('#pService_'+sSubVal).remove();   
                }
            });
            var lastPrice = 0;
            $(".sPrices").each(function(){
                var price = $(this).attr('p-id');
                lastPrice += Number(price);
            });
            $('#serviceTotal').html("CHF "+lastPrice);
        });*/
        
        $(".bookAppointment").blur(function(){
            var contact_name = $('#contact_name').val();
            var contact_address = $('#contact_address').val();
            var contact_zip = $('#contact_zip').val();
            var contact_city = $('#contact_city').val();
            var contact_state = $('#contact_state').val();
            //var contact_number = $('#contact_number').val();
            //var contact_email = $('#contact_email').val();
            
            if(contact_name != ''){
                $('#fname').html(contact_name);
            }
            
            // if(contact_email != ''){
            //    $('#femail').html(contact_email); 
            // }
            
            // if(contact_number != ''){
            //    $('#fnumber').html(contact_number); 
            // }
            
            if(contact_address != ''){
               $('#faddress').html(contact_address); 
            }
            
            if(contact_zip != ''){
               $('#fzip').html(contact_zip); 
            }
            
            if(contact_city != ''){
               $('#fcity').html(contact_city); 
            }
            
            if(contact_state != ''){
               $('#fstate').html(contact_state); 
            }
        });
        
        function priceCal(){
            var lastPrice = 0;
            $(".sPrices").each(function(){
                var price = $(this).attr('p-id');
                lastPrice += Number(price);
            });
            $('#serviceTotal').html("CHF "+lastPrice);
            
            var taxPer1 = 7.7+100;
            var taxPer = 7.7;
            var intext = lastPrice*taxPer/taxPer1;
            
            var finalTax = intext.toFixed(2);
            $('#serviceInclusiveTax').html(finalTax);
        }
	});
</script>
@endsection