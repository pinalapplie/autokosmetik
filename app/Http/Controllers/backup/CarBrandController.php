<?php

namespace App\Http\Controllers;

use App\CarBrand;
use App\CarType;
use Illuminate\Http\Request;

class CarBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $car_brands = CarBrand::with(['car_type_name'])->latest()->get();
        return view('car_brand.index', compact('car_brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $car_types = CarType::where('is_active',1)->get();
        return view('car_brand.create',compact('car_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $carbrand = new CarBrand;
            $carbrand->name = $request->name;
            $carbrand->car_type = $request->car_type;
            $carbrand->vehicle_category = $request->vehicle_category;
            $carbrand->save();
            return redirect()->route('carbrand.index')->with('success',trans('message.car_brand.create'));
        }catch(\Exception $e){
            return redirect()->back()->with('danger', trans('global.something_wrong'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
     */
    public function show(CarBrand $carBrand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
     */
    public function edit(CarBrand $carbrand)
    {
         $car_types = CarType::where('is_active',1)->get();
        return view('car_brand.edit', compact('carbrand','car_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarBrand  $carbrand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CarBrand $carbrand)
    {
        try{
            $carbrand->name = $request->name;
            $carbrand->car_type = $request->car_type;
            $carbrand->vehicle_category = $request->vehicle_category;
            $carbrand->save();
            return redirect()->route('carbrand.index')->with('success',trans('message.car_brand.update'));
        }catch(\Exception $e){
            return redirect()->back()->with('danger', trans('global.something_wrong'));
        }    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
     */
   public function destroy(Request $request)
    {
        $carbrand = CarBrand::find(request('ids'));
        $carbrand->delete();
        return response()->noContent();
    }
    public function switchUpdate(Request $request)
    {
        $carbrand = CarBrand::find($request->ids);
        if (empty($carbrand->is_active)) {
            $carbrand->is_active = 1;
        } else {
            $carbrand->is_active = 0;
        }
        $carbrand->save();
        return response()->noContent();
    }
}
