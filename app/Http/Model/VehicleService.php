<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class VehicleService extends Model
{
    use SoftDeletes; 
    protected $guarded = [];
    protected $table ="vehicle_services";
    protected $dates = ['deleted_at'];
    protected $with = ['service_management_name'];

    public function service_management_name()
    {
        return $this->hasOne('App\Http\Model\ServiceManagement','id','service_management_id');
    }
}
