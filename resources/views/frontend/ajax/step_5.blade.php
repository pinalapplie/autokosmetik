@php $temp_array = [];@endphp
@foreach ($service_category as $key => $value) 
    @if(!in_array( $value->service_category_name->id, $temp_array))
       <div class="h3">{{ucfirst($value->service_category_name->name)}}</div>
        @php $temp_array[] = $value->service_category_name->id; @endphp
    @endif    
    <div class="form-group">
        <input type="checkbox" name="step_6" class="form-control checkbox" value="{{$value->id}}" id="step_6_{{$value->id}}" s-id="{{$value->parent_id}}" v-id="{{ucfirst($value->name)}}">
        <label role="button" for="step_6_{{$value->id}}">{{ucfirst($value->name)}}</label>
    </div>
@endforeach

@php $temp_array1 = [];@endphp

@foreach ($service_with_sub_category as $key => $value) 
    @if(!in_array( $value->id, $temp_array1))
       <div class="h3">{{$value->name}}</div>
        @php $temp_array1[] = $value->id; @endphp
    @endif    
    <div class="form-group">
        <input type="checkbox" name="step_6" class="form-control checkbox" value="{{$value->id}}" id="step_6_temp_{{$value->id}}" s-id="{{$value->parent_id}}" v-id="{{ucfirst($value->name)}}">
        <label role="button" for="step_6_temp_{{$value->id}}">{{ucfirst($value->name)}}</label>
    </div>
@endforeach  
