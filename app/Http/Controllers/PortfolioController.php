<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use App\Http\Model\VehicleService;
use App\Http\Model\VehicleConditions;
use Illuminate\Support\Facades\Storage;
use File;

use App\Http\Model\Portfolio;
use App\Http\Model\ServiceCategory;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolio = Portfolio::latest()->get();
        return view('backend.portfolio.index', compact('portfolio'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $service_category = ServiceCategory::get();
        return view('backend.portfolio.create',compact('service_category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                $portfolio = Portfolio::where('id',$id)->first();
                if (empty($portfolio->is_active)) {
                    $portfolio->is_active = 1;
                } else {
                    $portfolio->is_active = 0;
                }
                    $portfolio->save();
                return response()->json(['success' => 1]);
            }else{
                if(!empty($request->id)){
                    $this->validate($request, [
                        'name' => 'required',
                        'client' => 'required',                        
                        'created' => 'required',
                        'description' => 'required',
                        'service_category_id' => 'required'
                    ]);
                    $id = Crypt::decrypt($request->id);

                    $path = explode(',', $request->portfolioImages);
                    if ($request->hasFile('images')) {
                        $images = $request->file('images');
                        $gk = 0;
                        foreach ($images as $image){
                            $gk++;
                            $filename  = time().$gk . '.' .$image->getClientOriginalExtension();
                            Storage::disk('public')->put('portfolio/'.$filename,  File::get($image));
                            $path[] = 'portfolio/'.$filename; 
                        }
                    }
                    
                    $portfolio = Portfolio::where('id',$id)->first();    
                    $portfolio->images = implode(',', $path);
                    $portfolio->name = $request->name;
                    $portfolio->description = $request->description;
                    $portfolio->created = date('Y-m-d', strtotime($request->created));
                    $portfolio->client = date('Y-m-d', strtotime($request->client));
                    $portfolio->service_category_id = implode(',', $request->service_category_id);
                    $portfolio->save();
   
                    $message = trans('message.update', ['entity' => trans('cruds.portfolio.title_singular')]);
                }else{
                    $this->validate($request, [
                        'name' => 'required',
                        'client' => 'required',
                        'images' => 'required',                        
                        'created' => 'required',
                        'description' => 'required',
                        'service_category_id' => 'required'
                    ]);
                    
                    $path = array();
                    if ($request->hasFile('images')) {                        
                        $images = $request->file('images');
                        $gk = 0;
                        foreach ($images as $image){
                            $gk++;
                            $filename  = time().$gk . '.' .$image->getClientOriginalExtension();
                            Storage::disk('public')->put('portfolio/'.$filename,  File::get($image));
                            $path[] = 'portfolio/'.$filename; 
                        }
                    }
                    
                    $portfolio = new Portfolio;
                    $portfolio->name = $request->name;
                    $portfolio->description = $request->description;
                    $portfolio->created = date('Y-m-d', strtotime($request->created));
                    $portfolio->client = date('Y-m-d', strtotime($request->client));
                    $portfolio->images = implode(', ', $path);
                    $portfolio->service_category_id = implode(', ', $request->service_category_id);
                    $portfolio->save();
                    $message = trans('message.create', ['entity' => trans('cruds.portfolio.title_singular')]);
                }
                
                return redirect()->route('portfolio.index')->with('success',$message);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
            return redirect()->back()->with('danger', $e->getMessage());    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceManagement  $serviceManagement
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceManagement $servicemanagement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceManagement  $serviceManagement
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $portfolio = Portfolio::where('id',$id)->first();
        $service_category = ServiceCategory::get();
        return view('backend.portfolio.edit', compact('portfolio','service_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceManagement  $serviceManagement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceManagement $serviceManagement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceManagement  $serviceManagement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                Portfolio::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }
}
