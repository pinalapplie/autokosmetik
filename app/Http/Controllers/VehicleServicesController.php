<?php

namespace App\Http\Controllers;

use App\Http\Model\VehicleService;
use App\Http\Model\ServiceCategory;
use App\Http\Model\ServiceManagement;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;

class VehicleServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicle_service = VehicleService::latest()->get();
        return view('backend.vehicle_services.index', compact('vehicle_service'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent_categorys = ServiceManagement::where('type','pricechart')->get();
        return view('backend.vehicle_services.create',compact('parent_categorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                $vehicle_service = VehicleService::where('id',$id)->first();
                if (empty($vehicle_service->is_active)) {
                    $vehicle_service->is_active = 1;
                } else {
                    $vehicle_service->is_active = 0;
                }
                    $vehicle_service->save();
                return response()->json(['success' => 1]);
            }else{
                $this->validate($request, [
                    'name' => 'required',
                ]);
                if(!empty($request->id)){
                    $id = Crypt::decrypt($request->id);
                    $vehicle_service = VehicleService::where('id',$id)->first();
                    $message = trans('message.update', ['entity' => trans('cruds.vehicleservices.title_singular')]);
                }else{
                    $vehicle_service = new VehicleService;
                    $message = trans('message.create', ['entity' => trans('cruds.vehicleservices.title_singular')]);
                }
                    $vehicle_service->name = $request->name;
                    $vehicle_service->service_management_id = $request->service_management_id;
                    $vehicle_service->save();
                    return redirect()->route('vehicleservices.index')->with('success',$message);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
            return redirect()->back()->with('danger', trans('global.something_wrong'));    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VehicleServiceType  $vehicleServiceType
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleServiceType $vehicleServiceType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VehicleServiceType  $vehicleServiceType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $vehicleservices = VehicleService::where('id',$id)->first();
        $parent_categorys = ServiceManagement::where('type','pricechart')->get();
        return view('backend.vehicle_services.edit', compact('vehicleservices','parent_categorys'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VehicleServiceType  $vehicleServiceType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleServiceType $vehicleServiceType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VehicleServiceType  $vehicleServiceType
     * @return \Illuminate\Http\Response
     */
   public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                VehicleService::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }
}
