<div class="modal fade modal-bookappointment" id="modal_bookappointment_26" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="services2-car-bg-2 py-3">
					<div class="container">
						<div class="row">
							<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/car-cosmetic-4.jpg') }}" alt="autokosmetik">
							</div>
							<div class="col-sm-12 col-md-7">
								<div class="ceramic-coating-pading">
									<h1 class="h4">INNENAUFBEREITUNG HERKÖMMLICH</h1>
									<p class="h6"> Bei einer herkömmlichen Innenaufbereitung arbeiten wir mit einem neutralen, ökologischen Universalreiniger. Der ganze Innenraum inkl. Kofferraum wird gesaugt, Teppiche und Sitze werden shampooniert bzw. das Leder wird gereinigt und mit einer professionellen Ledermilch eingepflegt.<br>
									</p>
									<ul>
										<li class="circle_2 m-0 h6">inkl. shampoonieren</li>
										<li class="circle_2 m-0 h6">Lederreinigung und -pflege</li>
										<li class="circle_2 m-0 h6">Reinigung mit Tornador</li>
									</ul>
								</div>
							</div>
							<div class="pt-3 col-md-12">
								<p class="h6">Bei der sogenannten "Tornadorreinigung" wird mit sehr hohem Luftdruck der Innenraum (ohne Himmel) inkl. Armaturen, allen Verkleidungen, Einstiegen, Fälzen und Schlitzen optimal gereinigt. Zum Abschluss werden die Innenscheiben professionell nur mit Wasser gereinigt. <br>Sitze und Teppiche mit problematischen Flecken werden nach dem Shampoonieren getrockenet und gegebenenfalls nochmals shampooniert oder händisch nachbearbeitet. <br>Anbei finden Sie die komplette Preisliste für die Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="ceramic-coting-images">
					<div class="container">
						<div class="row">
							<div class="related-projects col-12">
								<h2 class="h5">Reinigung mit Tornador</h2>
								<div class="row">
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-10.jpg') }} ">
													<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-10.jpg') }} " alt="autokosmetik">
												</a>
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<a class="fancybox" rel="group" href="{{ asset('assets/frontend/images/auto-11.jpg') }}">
													<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-11.jpg') }}" alt="autokosmetik">
												</a>
											</div>
										</a>
									</div>
								</div>
							</div>
							<!-- <div class="col-12 text-center pb-5">
								<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_27" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Swissvax</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="services2-car-bg-2 pt-2 pb-3">
					<div class="container">
						<div class="row">
							<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
							</div>
							<div class="col-sm-12 col-md-7">
								<div class="ceramic-coating-pading">
									<h1 class="h4">INNENAUFBEREITUNG NACH SWISSVAX</h1>
									<p class="h6">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte.<br></p>
									<ul class="m-0">
										<li class="circle_2 m-0 h6">hochwertigste Reinigungsmittel</li>
										<li class="circle_2 m-0 h6">optimale Langzeitpflege</li>
										<li class="circle_2 m-0 h6">ausschliesslich Swissvax-Produkte</li>
									</ul>
								</div>
							</div>
							<div class="pt-3 col-md-12">
								<p class="h6">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
							</div>
						</div>
					</div>
				</div>
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-12">
								<h1 class="h4">Swissvax-Saisonalitäten</h1>
								<ul class="m-0">
									<li class="circle m-0 h6">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
									<li class="circle m-0 h6">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
									<li class="circle m-0 h6">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
								</ul>
								<p class="h6 py-3">
									Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
									Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
									Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="ceramic-coting-images">
					<div class="container">
						<div class="row">
							<div class="related-projects col-12">
								<div class="row">
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_31" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Swissvax</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="services2-car-bg-2 pt-2 pb-3">
					<div class="container">
						<div class="row">
							<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
							</div>
							<div class="col-sm-12 col-md-7">
								<div class="ceramic-coating-pading">
									<h1 class="h4">INNENAUFBEREITUNG NACH SWISSVAX</h1>
									<p class="h6">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte.<br></p>
									<ul class="m-0">
										<li class="circle_2 m-0 h6">hochwertigste Reinigungsmittel</li>
										<li class="circle_2 m-0 h6">optimale Langzeitpflege</li>
										<li class="circle_2 m-0 h6">ausschliesslich Swissvax-Produkte</li>
									</ul>
								</div>
							</div>
							<div class="pt-3 col-md-12">
								<p class="h6">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
							</div>
						</div>
					</div>
				</div>
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-12">
								<h1 class="h4">Swissvax-Saisonalitäten</h1>
								<ul class="m-0">
									<li class="circle m-0 h6">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
									<li class="circle m-0 h6">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
									<li class="circle m-0 h6">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
								</ul>
								<p class="h6 py-3">
									Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
									Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
									Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="ceramic-coting-images">
					<div class="container">
						<div class="row">
							<div class="related-projects col-12">
								<div class="row">
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_28" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">SPEZIALFÄLLE</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-6">
								<p class="h6">Bei diesen "speziellen Fällen" können wir keine Fixpreise machen, denn der jeweilige Reinigungsaufwand ist von Fall zu Fall sehr unterschiedlich. Entsprechend arbeiten wir nach dem Standardstundensatz von CHF 97.10 inkl. 10% für Material und inkl. Mwst. </p>
								<ul class="m-0">
									<li class="circle m-0 h6">Hundehaare</li>
									<li class="circle m-0 h6">Geruchsbehandlungen</li>
									<li class="circle m-0 h6">Schimmel, usw.</li>
								</ul>
							</div>
							<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/special-1.jpg') }}" alt="autokosmetik"> 
							</div>
							<div class="col-md-12">
								<p class="py-3 h6">Ozon-Geruchsbehandlung: Voraussetzung für eine Ozon-Geruchsbehandlung ist die optimale Reinigung mit anschliessender Trocknung. Erst bei einem komplett trockenen Innenraum kann eine wirksame Ozonbehandlung durchgeführt werden. <br>Die Ozonbehandlung verrechnen wir mit CHF 30.-. Teilweise sind aber mehrere Ozonbehandlungen nötig.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="ceramic-coting-images">
					<div class="container">
						<div class="row">
							<div class="related-projects col-12">
								<h2 class="h5">Beispiele von extremen Verschmutzungen die unserer Spezialbehandlung bedürfen:</h2>
								<div class="row">
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/special-2.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/special-3.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
								</div>
							</div>
							<div class="related-projects col-12">
								<h2 class="h5">Vorher-Nachher Gegenüberstellungen bei Extremfällen:</h2>
								<div class="row">
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/special-4.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/special-5.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_29" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">KERAMIKBESCHICHTUNG</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-6">
								<p class="h6">Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und ist absolut waschstrassenfest. Besonders geeignet ist unsere Keramikbeschichtung für dunkle, grosse Autos, welche im Alltagseinsatz stehen. Jede Keramikbeschichtung erhält ein Zertifikat, welches die Haltbarkeit von Minimum 3 Jahren garantiert.</p>
								<ul class="m-0">
									<li class="circle m-0 h6">Der panzerglasartige Lackschutz</li>
									<li class="circle m-0 h6">Haltbarkeit 36+ Monate</li>
									<li class="circle m-0 h6">Waschstrassenfest</li>
								</ul>
							</div>
							<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="autokosmetik"> 
							</div>
						</div>
					</div>
				</div>
				<div class="services2-car-bg-2 py-5">
					<div class="container">
						<div class="row">
							<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
							</div>
							<div class="col-sm-12 col-md-7">
								<div class="ceramic-coating-pading">
									<h1 class="h4">Unser neues Keramik-Material ServFaces</h1>
									<p class="h6">In den ersten 3 Jahren unserer Arbeit mit sogenannten Keramikbeschichtungen, haben wir mit einem fernöstlichen Produkt gearbeitet. Bis wir feststellen mussten, dass sich das Material auf einmal anders verhält  und auch anders zu verarbeiten ist. Für unsere Firma ein absolutes "NO GO". Wir konnten jedoch in kürzester Zeit eine Ersatz-Produktlinie aus deutscher Produktion finden und haben seit nunmehr 2 Jahren nur die besten Erfahrungen gemacht. Und so konnten wir unsere Garantie von 2 auf 3 Jahre erhöhen. Ab Anfang 2016 haben wir deshalb auch die Vertretung von ServFaces für die Schweiz und Deutschland übernommen.</p>
									<p class="pt-3 h6">Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="ceramic-coting-images">
					<div class="container">
						<div class="row">
							<div class="related-projects col-12">
								<div class="row">
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-1.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-2.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-3.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-4.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-5.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-6.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-7.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-8.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-9.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_30" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">TEFLONVERSIEGELUNG</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-6">
								<p class="h6">Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und genügt für die meisten Autos. Ausserdem ist sie sehr preiswert. Wir empfehlen jedoch bei der Teflon-Versiegelung Hand- oder Lanzenwäsche, da die Waschstrasse die Versiegelung zu schnell abreibt.</p>
								<ul class="m-0">
									<li class="circle m-0 h6">Der Standard-Lackschutz</li>
									<li class="circle m-0 h6">Haltbarkeit 6+ Monate</li>
									<li class="circle m-0 h6">Preiswert</li>
								</ul>
							</div>
							<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="autokosmetik"> 
							</div>
						</div>
					</div>
				</div>
				<div class="services2-car-bg-2 py-5">
					<div class="container">
						<div class="row">
							<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-2.jpg') }}" alt="autokosmetik">
							</div>
							<div class="col-sm-12 col-md-7">
								<div class="ceramic-coating-pading">
									<h1 class="h4">Besonders geeignet für:</h1>
									<p class="h6">Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.
									</p>
									<ul>
										<li class="circle_2 m-0 h6">alle Autos, Motorräder, Wohnmobile, LKW's usw.</li>
										<li class="circle_2 m-0 h6">Gebrauchtwagen</li>
										<li class="circle_2 m-0 h6">Leasingrückgaben</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="ceramic-coting-images">
					<div class="container">
						<div class="row">
							<div class="related-projects col-12">
								<div class="row">
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-3.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-4.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-5.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-6.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-7.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-8.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-9.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-10.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_12" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Foliendesign</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-6">
								<p class="h6">Besonders Individualisten wissen die vielfältigen Möglichkeiten von Autofolien zu schätzen. Dem Design und der individuellen Gestaltung sind fast keine Grenzen gesetzt. Gerne beraten wir diesbezüglich unsere anspruchsvollen Kunden. Sowohl betreffend die verwendeten Folien als auch in Bezug auf unsere Arbeit können Sie höchste Qualität erwarten. Bei allen Folien gilt eine Garantie von 7 Jahren.</p>
								<ul class="m-0">
									<li class="circle m-0 h6">Individuelle Optik</li>
									<li class="circle m-0 h6">Designberatung</li>
									<li class="circle m-0 h6">Waschstrassenfest</li>
								</ul>
							</div>
							<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-1.jpg') }}" alt="autokosmetik"> 
							</div>
						</div>
					</div>
				</div>
				<div class="services2-car-bg-2 py-5">
					<div class="container">
						<div class="row">
							<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-2.jpg') }}" alt="autokosmetik">
							</div>
							<div class="col-sm-12 col-md-7">
								<div class="ceramic-coating-pading">
									<h1 class="h4">Besonders geeignet für:</h1>
									<p class="h6">begrenzte Zeit eine andere Farbe brauchen</p>
									<ul class="m-0">
										<li class="circle m-0 h6">Individualisten</li>
										<li class="circle m-0 h6">Showfahrzeuge, die besonders auffallen wollen</li>
										<li class="circle m-0 h6">Fahrzeuge, welche für eine </li>
									</ul>
								</div>
							</div>
							<div class="col-md-12 pt-3">
								<p class="h6">Für das Foliendesign gibt es keine generelle Preisliste. Rechnen Sie bei einer Vollfolierung für einen Golf mit Minimum CHF 3.000,- bis CHF 3.600,-,  ein SUV wird Minimum CHF 4.000.- bis CHF 5.000 kosten. Je nach speziellen Anforderungen und Features können die Preise auch noch darüber liegen.</p>
								<p class="pt-3 h6">Teilfolierungen kosten entsprechend weniger und beginnen bereits bei wenigen hundert Franken.</p>
								<p class="pt-3 h6">Achtung: Es gibt immer mehr „Möchte-Gern-Folierer“, welche neben der schlechten Verklebung auch dem Lack sehr viel Schaden zufügen können. Auch werden teilweise sehr billige, schlechte Folien verwendet, bei denen keinerlei Garantie gewährt wird.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="ceramic-coting-images">
					<div class="container">
						<div class="row">
							<div class="related-projects col-12">
								<div class="row">
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-3.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-4.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-5.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-6.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-7.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-8.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-9.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-10.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/foil-11.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_32" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">LACK</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-6">
								<p class="h6">Eine volltransparente Lackschutzfolie ist der beste Schutz gegen Steinschläge. Unsere verwendete Profifolie ist sehr dick und dadurch auch ganz besonders widerstandsfähig. Allerdings ist Sie sehr schwer zu verlegen und auch relativ kostspielig.</p>
								<ul class="m-0">
									<li class="circle m-0 h6">Hochtransparent/unsichtbar</li>
									<li class="circle m-0 h6">Der optimale Schutz gegen Steinschlag</li>
									<li class="circle m-0 h6">Waschstrassenfest</li>
								</ul>
							</div>
							<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-1.jpg') }}" alt="autokosmetik"> 
							</div>
						</div>
					</div>
				</div>
				<div class="services2-car-bg-2 py-5">
					<div class="container">
						<div class="row">
							<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-2.jpg') }}" alt="autokosmetik">
							</div>
							<div class="col-sm-12 col-md-7">
								<div class="ceramic-coating-pading">
									<h1 class="h4">Besonders geeignet für:</h1>
									<p class="h6">Die Preise für eine Lackschutzfolie variieren sehr stark und sind abhängig von der Beschaffenheit des Autos und Anzahl jener Stellen, welche mit einer Lackschutzfolie versehen werden sollen. </p>
									<p class="h6">Kommen Sie vorbei, wir unterbreiten Ihnen gerne ein unverbindliches Angebot.</p>
									<ul class="m-0">
										<li class="circle m-0 h6">Ladekante und alle Türkanten</li>
										<li class="circle m-0 h6">Stossstange und Motorhaube</li>
										<li class="circle m-0 h6">Generell alle exponierten Stellen</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="ceramic-coting-images">
					<div class="container">
						<div class="row">
							<div class="related-projects col-12">
								<div class="row">
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-3.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-4.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-5.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-6.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-7.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/paint-8.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_33" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">SCHEIBEN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-6">
								<p class="h6">Eine Scheibentönung sieht nicht nur gut aus, sondern schützt in mehrfacher Hinsicht. Der Innenraum heizt weniger auf, 99% der schädlichen UV-Strahlung werden abgehalten und sie schützt vor neugierigen Blicken.</p>
								<p class="h6 pt-2">Wir arbeiten ausschliesslich mit Markenfolien der Firma Johnson Window Films, Folie Marathon, und gewähren 7 Jahre Garantie. Jede Scheibentönung erhält ein Zertifikat.</p>
								<ul class="m-0">
									<li class="circle m-0 h6">99%-iger UV-Schutz</li>
									<li class="circle m-0 h6">Hitzereduktion, Blickschutz</li>
									<li class="circle m-0 h6">Edle Optik</li>
								</ul>
							</div>
							<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-10.jpg') }}" alt="autokosmetik"> 
							</div>
						</div>
					</div>
				</div>
				<div class="services2-car-bg-2 py-5">
					<div class="container">
						<div class="row">
							<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-11.jpg') }}" alt="autokosmetik">
							</div>
							<div class="col-sm-12 col-md-7">
								<div class="ceramic-coating-pading">
									<h1 class="h4">Besonders geeignet für:</h1>
									<p class="h6">Kofferraum oder auf der Rückbank</p>
									<p class="pt-3">Anbei finden Sie die komplette Preisliste für fast alle Automarken - und Modelle. Sollte Ihr Auto nicht gelistet sein, bitten wir um ein kurzes Telefonat oder Mail. Wir werden Ihnen umgehend ein Angebot unterbreiten.</p>
									<ul class="m-0">
										<li class="circle m-0 h6">Familienautos</li>
										<li class="circle m-0 h6">Personen im Fond</li>
										<li class="circle m-0 h6">Hundeautos</li>
										<li class="circle m-0 h6">Wertgegenstände im</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="ceramic-coting-images">
					<div class="container">
						<div class="row">
							<div class="related-projects col-12">
								<div class="row">
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-1.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-2.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-3.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-4.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-5.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-6.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-7.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-8.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/tint-9.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_34" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">KERAMIKBESCHICHTUNG</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-6">
								<p class="h6">Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und ist absolut waschstrassenfest. Besonders geeignet ist unsere Keramikbeschichtung für dunkle, grosse Autos, welche im Alltagseinsatz stehen. Jede Keramikbeschichtung erhält ein Zertifikat, welches die Haltbarkeit von Minimum 3 Jahren garantiert.</p>
								<ul class="m-0">
									<li class="circle m-0 h6">Der panzerglasartige Lackschutz</li>
									<li class="circle m-0 h6">Haltbarkeit 36+ Monate</li>
									<li class="circle m-0 h6">Waschstrassenfest</li>
								</ul>
							</div>
							<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="autokosmetik"> 
							</div>
						</div>
					</div>
				</div>
				<div class="services2-car-bg-2 py-5">
					<div class="container">
						<div class="row">
							<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
							</div>
							<div class="col-sm-12 col-md-7">
								<div class="ceramic-coating-pading">
									<h1 class="h4">Unser neues Keramik-Material ServFaces</h1>
									<p class="h6">In den ersten 3 Jahren unserer Arbeit mit sogenannten Keramikbeschichtungen, haben wir mit einem fernöstlichen Produkt gearbeitet. Bis wir feststellen mussten, dass sich das Material auf einmal anders verhält  und auch anders zu verarbeiten ist. Für unsere Firma ein absolutes "NO GO". Wir konnten jedoch in kürzester Zeit eine Ersatz-Produktlinie aus deutscher Produktion finden und haben seit nunmehr 2 Jahren nur die besten Erfahrungen gemacht. Und so konnten wir unsere Garantie von 2 auf 3 Jahre erhöhen. Ab Anfang 2016 haben wir deshalb auch die Vertretung von ServFaces für die Schweiz und Deutschland übernommen.</p>
									<p class="pt-3 h6">Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="ceramic-coting-images">
					<div class="container">
						<div class="row">
							<div class="related-projects col-12">
								<div class="row">
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-1.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-2.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-3.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-4.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-5.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-6.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-7.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-8.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-9.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_35" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">TEFLONVERSIEGELUNG</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-6">
								<p class="h6">Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und genügt für die meisten Autos. Ausserdem ist sie sehr preiswert. Wir empfehlen jedoch bei der Teflon-Versiegelung Hand- oder Lanzenwäsche, da die Waschstrasse die Versiegelung zu schnell abreibt.</p>
								<ul class="m-0">
									<li class="circle m-0 h6">Der Standard-Lackschutz</li>
									<li class="circle m-0 h6">Haltbarkeit 6+ Monate</li>
									<li class="circle m-0 h6">Preiswert</li>
								</ul>
							</div>
							<div class="right col-sm-12 col-md-6" data-animation="fadeInRight">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="autokosmetik"> 
							</div>
						</div>
					</div>
				</div>
				<div class="services2-car-bg-2 py-5">
					<div class="container">
						<div class="row">
							<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
								<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-2.jpg') }}" alt="autokosmetik">
							</div>
							<div class="col-sm-12 col-md-7">
								<div class="ceramic-coating-pading">
									<h1 class="h4">Besonders geeignet für:</h1>
									<p class="h6">Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.
									</p>
									<ul>
										<li class="circle_2 m-0 h6">alle Autos, Motorräder, Wohnmobile, LKW's usw.</li>
										<li class="circle_2 m-0 h6">Gebrauchtwagen</li>
										<li class="circle_2 m-0 h6">Leasingrückgaben</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="ceramic-coting-images">
					<div class="container">
						<div class="row">
							<div class="related-projects col-12">
								<div class="row">
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-3.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-4.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-5.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-6.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-7.jpg') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-8.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-9.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
									<div class="item col-sm-12 col-md-3 my-3">
										<a href="#">
											<div class="img-container">
												<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-10.png') }}" alt="autokosmetik">
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_36" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">MONO</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-12">
								<p class="h6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, rerum nihil provident optio tenetur debitis. Nulla similique dolorum a quaerat atque porro sapiente, quod, vero officia ipsam ut. Unde, vitae?</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_37" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">DESIGN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-12">
								<p class="h6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, rerum nihil provident optio tenetur debitis. Nulla similique dolorum a quaerat atque porro sapiente, quod, vero officia ipsam ut. Unde, vitae?</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_38" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">MONO</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-12">
								<p class="h6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, rerum nihil provident optio tenetur debitis. Nulla similique dolorum a quaerat atque porro sapiente, quod, vero officia ipsam ut. Unde, vitae?</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-bookappointment" id="modal_bookappointment_39" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">DESIGN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="" class="section-car-ceramic">
					<div class="container">
						<div class="row">
							<div class="left col-sm-12 col-md-12">
								<p class="h6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, rerum nihil provident optio tenetur debitis. Nulla similique dolorum a quaerat atque porro sapiente, quod, vero officia ipsam ut. Unde, vitae?</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>