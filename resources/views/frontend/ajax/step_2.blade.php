<div class="form-row align-items-center justify-content-center mx-0">
@foreach ($vehicle_categorys as $key => $value)    
    <div class="col-4 text-center mt-2">
        <div class="card select-category">
            <input type="radio" role="button" name="step_20" class="form-control radio" value="{{$value->id}}" v-id="{{$value->name}}">
            <div class="card-body p-2">
                <div class="mb-1">
                    <img src="{{asset(Storage::url($value->image)) ?? ''}}" alt="" class="img-fluid">
                </div>
                <p class="font-weight-bold h5 text-dark mt-2">{{$value->name}}</p>
            </div>
        </div>
    </div>
@endforeach
</div>
