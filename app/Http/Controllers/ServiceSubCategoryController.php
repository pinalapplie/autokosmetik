<?php

namespace App\Http\Controllers;

use App\Http\Model\ServiceCategory;
use App\Http\Model\VehicleType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class ServiceSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service_category = ServiceCategory::where('parent_id', '!=',0)->latest()->get();
        return view('backend.service_subcategories.index', compact('service_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent_categorys = ServiceCategory::where('parent_id',0)->get();
        //dd($parent_categorys);
        $vehicle_type = VehicleType::get();
        return view('backend.service_subcategories.create',compact('parent_categorys','vehicle_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                $service_category = ServiceCategory::where('id',$id)->first();
                if (empty($service_category->is_active)) {
                    $service_category->is_active = 1;
                } else {
                    $service_category->is_active = 0;
                }
                    $service_category->save();
                return response()->json(['success' => 1]);
            }else{
                $this->validate($request, [
                    'name' => 'required',
                ]);
                if(!empty($request->id)){
                    $id = Crypt::decrypt($request->id);
                    $service_category = ServiceCategory::where('id',$id)->first();
                    $message = trans('message.update', ['entity' => trans('cruds.servicecategories.title_singular')]);
                }else{
                    $service_category = new ServiceCategory;
                    $message = trans('message.create', ['entity' => trans('cruds.servicecategories.title_singular')]);
                }
                    /*if ($request->hasFile('image')) {                        
                        $storage_path = 'public/';
                        $service_category->image = Storage::disk('local')->put($storage_path, $request->file('image'));
                    }*/
                    $service_category->name = $request->name;
                    $service_category->description = $request->description;
                    $service_category->parent_id = $request->parent_id;
                    $service_category->vehicle_type_id = $request->vehicle_type_id;
                    $service_category->save();
                    return redirect()->route('servicecategories.index')->with('success',$message);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
            return redirect()->back()->with('danger', trans('global.something_wrong'));    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceCategory  $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceCategory $serviceCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceCategory  $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $servicecategory = ServiceCategory::where('id',$id)->first();
        $parent_categorys = ServiceCategory::where('parent_id',0)->get();
        $vehicle_type = VehicleType::get();
        return view('backend.service_subcategories.edit', compact('servicecategory','parent_categorys','vehicle_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceCategory  $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceCategory $serviceCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceCategory  $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                ServiceCategory::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }

}
