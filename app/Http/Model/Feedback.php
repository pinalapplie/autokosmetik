<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
    use SoftDeletes; 
    protected $guarded = [];
    protected $dates = ['deleted_at'];
    protected $table = "feedback";

    //protected $with = ['service_category_name','service_price'];
}
