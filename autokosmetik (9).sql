-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2021 at 07:08 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autokosmetik`
--

-- --------------------------------------------------------

--
-- Table structure for table `addonservice`
--

CREATE TABLE `addonservice` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addonservice`
--

INSERT INTO `addonservice` (`id`, `name`, `image`, `price`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Pick Up', 'public//24JyVTfkRraaYsU08ihJL6eEd37KjReJIoB0Vfr5.png', '100', 1, '2021-04-07 05:40:16', '2021-04-07 05:40:16', NULL),
(2, 'Drop', NULL, '70', 1, '2021-04-08 04:31:22', '2021-04-08 04:31:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `booking_appointment`
--

CREATE TABLE `booking_appointment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_ids` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `booking_appointment`
--

INSERT INTO `booking_appointment` (`id`, `name`, `number`, `email`, `address`, `zip`, `city`, `state`, `service_ids`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'pratik', '9925941767', 'only.shahpratik266@gmail.com', NULL, NULL, NULL, NULL, '5,8,10', 1, '2021-04-03 10:11:54', '2021-04-03 10:11:54', NULL),
(2, 'gaurang', '987654321', 'gkpharmacy@gmail.com', NULL, NULL, NULL, NULL, '26,27', 1, '2021-04-08 13:27:09', '2021-04-08 13:27:09', NULL),
(3, 'gaurang', '+919727352482', 'kathiriyagaurang@gmail.com', NULL, NULL, NULL, NULL, '26,27,28,29,30,31,39', 1, '2021-04-09 06:24:51', '2021-04-09 06:24:51', NULL),
(4, 'gaurang', '+919727352482', 'kathiriyagaurang@gmail.com', NULL, NULL, NULL, NULL, '38,39', 1, '2021-04-09 12:14:06', '2021-04-09 12:14:06', NULL),
(5, 'Fabio Wolfinger', '0340030234423', 'drsgrsg@esfef.de', NULL, NULL, NULL, NULL, '32,33,34,35', 1, '2021-04-09 13:44:11', '2021-04-09 13:44:11', NULL),
(6, 'gaurang', '+919727352482', 'kathiriyagaurang@gmail.com', '714 / 4726 Gujarat Housing Board, Nr patel wadi, bhidbhanjan road\nbapunagar, ahmedabad', '380024', 'Ahmedabad', 'Gujarat', '26,27,32,33,36', 1, '2021-04-10 09:00:22', '2021-04-10 09:00:22', NULL),
(7, 'gaurang', '+919727352482', 'kathiriyagaurang@gmail.com', '714 / 4726 Gujarat Housing Board, Nr patel wadi, bhidbhanjan road\nbapunagar, ahmedabad', '380024', 'Ahmedabad', 'Gujarat', '26,32,36', 1, '2021-04-10 09:34:47', '2021-04-10 09:34:47', NULL),
(8, 'Fabio Wolfinger', '0765002418', 'tools@applie.li', 'Floraweg 18', '9490', 'Vaduz', 'Liechtenstein', '26,27,29,30,32,33,37', 1, '2021-04-10 09:39:52', '2021-04-10 09:39:52', NULL),
(9, 'Fabio Wolfinger', '0765002418', 'fabio.wolfinger@applie.li', 'Heraweg 30', '9496', 'Balzers', 'Liechtenstein', '26,27,28,32,33,34,36,38', 1, '2021-04-11 17:10:40', '2021-04-11 17:10:40', NULL),
(10, 'shravan', '123456789', 'shravan1104@gmail.com', 'Solapur, Maharashtra, India', '380013', 'Ahmedabad', 'Gujarat', '26,27', 1, '2021-04-12 04:47:46', '2021-04-12 04:47:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `drive_vehicles`
--

CREATE TABLE `drive_vehicles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_of_window` int(11) DEFAULT NULL,
  `vehicle_categorie_id` bigint(20) UNSIGNED NOT NULL,
  `vehicle_brand_id` bigint(20) UNSIGNED NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `drive_vehicles`
--

INSERT INTO `drive_vehicles` (`id`, `name`, `model`, `number_of_window`, `vehicle_categorie_id`, `vehicle_brand_id`, `price`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BMW 5 Series', '1', 3, 1, 1, '110', 1, '2021-04-02 00:29:01', '2021-04-08 13:26:00', NULL),
(2, 'BMW X3', '2', 3, 1, 1, '100', 1, '2021-04-02 09:00:01', '2021-04-08 13:25:55', NULL),
(3, 'BMW X6', '3', 3, 1, 1, '120', 1, '2021-04-02 09:00:16', '2021-04-08 13:25:50', NULL),
(4, 'CUPRA', 'CUPRA GT380', 5, 5, 4, NULL, 1, '2021-04-05 18:20:02', '2021-04-05 18:20:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2021_03_23_125812_create_vehicle_types_table', 1),
(4, '2021_03_24_072450_create_service_categories_table', 1),
(5, '2021_03_24_085521_create_vehicle_categories_table', 1),
(6, '2021_03_24_104246_create_vehicle_conditions_table', 1),
(7, '2021_03_24_110021_create_vehicle_brands_table', 1),
(8, '2021_03_24_113350_create_drive_vehicles_table', 1),
(9, '2021_03_24_125828_create_service_management_table', 1),
(10, '2021_03_25_045518_create_vehicle_services_table', 1),
(11, '2021_03_25_055743_create_service_prices_table', 1),
(12, '2021_03_31_112515_create_booking_appointment_table', 1),
(13, '2021_04_01_054401_create_news_table', 1),
(15, '2021_04_01_091815_create_portfolio_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name`, `date`, `image`, `description`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'GK Pharmacy', '2021-04-03', 'news/phpF522.tmp.png', '<p>fg dsgdfgdgdf sgf</p>', 1, '2021-04-01 04:29:27', '2021-04-01 23:53:18', '2021-04-01 23:53:18'),
(2, 'Free Shipping', '2021-04-02', 'news/1617283389.png', '<p>gdfgd gdfgdfgf</p>', 1, '2021-04-01 07:53:09', '2021-04-01 23:53:15', '2021-04-01 23:53:15'),
(3, 'FENSTERTÖNUNG MIT TEMPERATURRABATT', '2019-09-27', 'news/1617341235.jpg', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 1, '2021-04-01 23:57:15', '2021-04-03 05:55:40', NULL),
(4, 'OFFENER TAG, 18. MÄRZ, 10 UHR - 16.00 Uhr', '2019-09-25', 'news/1617341281.jpg', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 1, '2021-04-01 23:58:01', '2021-04-01 23:58:01', NULL),
(5, 'KERAMIKBESCHICHTUNG FÜR MOTORRÄDER - WINTERFÖRDERUNG', '2020-09-29', 'news/1617341334.png', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>', 1, '2021-04-01 23:58:54', '2021-04-01 23:58:54', NULL),
(6, 'FENSTERFARBKAMPAGNE JANUAR 2017', '2019-09-12', 'news/1617341372.jpg', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>', 1, '2021-04-01 23:59:32', '2021-04-01 23:59:32', NULL),
(7, 'AUTO-LACK-KAMPAGNE', '2020-09-19', 'news/1617341416.png', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 1, '2021-04-02 00:00:16', '2021-04-02 00:00:16', NULL),
(8, 'MERCEDES FOILING', '2020-09-24', 'news/1617341447.jpg', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 1, '2021-04-02 00:00:47', '2021-04-02 00:00:47', NULL),
(9, 'VERTRETUNG VON DIENSTLEISTUNGEN', '2020-09-24', 'news/1617341478.jpg', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 1, '2021-04-02 00:01:18', '2021-04-02 00:01:18', NULL),
(10, 'aaaaaa', '2021-04-02', 'news/1617429652.jpg', '<p>aaaaaaaa</p>', 1, '2021-04-03 06:00:12', '2021-04-03 06:01:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_category_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` date DEFAULT NULL,
  `client` date DEFAULT NULL,
  `images` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`id`, `name`, `service_category_id`, `created`, `client`, `images`, `description`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Free Shipping', '2,3', '2021-04-02', '2021-04-16', 'portfolio/php9044.tmp.jpg, portfolio/php9045.tmp.jpg', '<p>sdfsd dsfgdfgds gd</p>', 1, '2021-04-01 22:01:51', '2021-04-01 22:22:34', '2021-04-01 22:22:34'),
(2, 'Project 1', '2, 3', '2021-04-05', '2021-04-06', 'portfolio/16173574101.jpg, portfolio/16173574102.jpg, portfolio/16173574103.jpg', '<p>The PSD file includes a smart layer so all you have to do is place your design inside it and you&rsquo;ll be done in no time. Aliquam vehicula mollis urna vel dignissim.</p>', 1, '2021-04-01 22:24:03', '2021-04-01 22:56:50', NULL),
(3, 'Project 2', '2, 3', '2021-04-22', '2021-04-24', 'portfolio/16173574611.jpg, portfolio/16173574612.jpg, portfolio/16173574613.jpg', '<p>The PSD file includes a smart layer so all you have to do is place your design inside it and you&rsquo;ll be done in no time. Aliquam vehicula mollis urna vel dignissim.</p>', 1, '2021-04-01 22:24:30', '2021-04-01 22:57:41', NULL),
(4, 'Project 3', '1', '2021-04-27', '2021-04-28', 'portfolio/16173574261.jpg, portfolio/16173574262.jpg, portfolio/16173574263.jpg', '<p>The PSD file includes a smart layer so all you have to do is place your design inside it and you&rsquo;ll be done in no time. Aliquam vehicula mollis urna vel dignissim.</p>', 1, '2021-04-01 22:25:02', '2021-04-03 05:58:07', NULL),
(5, '1111111', '1', '2021-04-01', '2021-04-03', 'portfolio/16180455151.jpg,portfolio/16180455521.jpg,portfolio/16180455522.jpg,portfolio/16180455523.jpg', '<p>1111111111111111111111111111111111111111111111</p>', 1, '2021-04-03 06:03:41', '2021-04-10 09:05:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_categories`
--

CREATE TABLE `service_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `vehicle_type_id` bigint(20) UNSIGNED NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_glass` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_categories`
--

INSERT INTO `service_categories` (`id`, `name`, `image`, `description`, `parent_id`, `vehicle_type_id`, `is_active`, `is_glass`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Pflege', 'public//jNA47cJkz46Sru5auWwyL11kFTqFTV5Nh2JvLAEV.png', '<p>test</p>', 0, 1, 1, 0, '2021-03-24 19:19:07', '2021-04-08 13:06:39', NULL),
(2, 'Schutz', 'public//7YYQ1xx7qVPzZTp6HaFNNMjUXpxdO0NywmuCLEOX.png', '<p>test</p>', 0, 1, 1, 0, '2021-03-24 19:19:18', '2021-04-08 13:06:25', NULL),
(3, 'Design', 'public//QLPyzkdbqjelJsWx7Y1fZiBtTiyIRuXgN9KB0D93.png', NULL, 0, 1, 1, 0, '2021-03-24 19:19:35', '2021-04-08 13:06:17', NULL),
(4, 'INNENAUFBEREITUNG', NULL, NULL, 1, 1, 1, 0, '2021-03-24 19:19:47', '2021-03-24 19:19:47', NULL),
(5, 'AUSSENAUFBEREITUNG', NULL, NULL, 1, 1, 1, 0, '2021-03-24 19:20:01', '2021-03-24 19:20:01', NULL),
(12, 'FOLIEN', NULL, NULL, 2, 1, 1, 0, '2021-03-24 22:04:57', '2021-03-24 22:04:57', NULL),
(13, 'BESCHICHTUNG', NULL, NULL, 2, 1, 1, 0, '2021-03-24 22:05:10', '2021-03-24 22:05:10', NULL),
(18, 'KOMPLETTFOLIERUNG', NULL, NULL, 3, 1, 1, 0, '2021-03-24 22:09:30', '2021-03-24 22:09:30', NULL),
(19, 'TEILFOLIERUNG', NULL, NULL, 3, 1, 1, 0, '2021-03-24 22:09:58', '2021-03-24 22:09:58', NULL),
(20, 'FENSTERFOLIERUNG', NULL, NULL, 3, 1, 1, 0, '2021-03-24 22:10:11', '2021-03-24 22:10:11', NULL),
(21, 'INTERIEUR', NULL, NULL, 3, 1, 1, 0, '2021-03-24 22:10:28', '2021-03-24 22:10:28', NULL),
(22, 'BESCHRIFTUNG', NULL, NULL, 3, 1, 1, 0, '2021-03-24 22:10:34', '2021-03-24 22:10:34', NULL),
(27, 'Window Cleaning test', NULL, NULL, 0, 1, 1, 0, '2021-04-06 05:45:40', '2021-04-07 07:17:48', '2021-04-07 07:17:48'),
(28, 'A1', NULL, NULL, 0, 1, 1, 0, '2021-04-06 05:56:05', '2021-04-07 07:17:45', '2021-04-07 07:17:45'),
(31, 'Tönen', 'public//j5d1nWhcanaDQ1JakDAoXmXFALlSJh52TpycZ5Bn.png', NULL, 0, 1, 1, 1, '2021-04-08 13:05:17', '2021-04-12 03:20:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_management`
--

CREATE TABLE `service_management` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_category_id` bigint(20) UNSIGNED NOT NULL,
  `type` enum('specification','pricechart') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'specification',
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_management`
--

INSERT INTO `service_management` (`id`, `name`, `service_category_id`, `type`, `description`, `image`, `price`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(19, 'INTERIEUR', 21, 'specification', NULL, NULL, '12', 1, '2021-03-30 20:14:01', '2021-04-07 01:30:51', '2021-04-07 01:30:51'),
(20, 'BESCHRIFTUNG', 22, 'specification', NULL, NULL, '12', 1, '2021-03-30 20:14:20', '2021-04-07 01:30:48', '2021-04-07 01:30:48'),
(21, 'Hitesh', 3, 'pricechart', NULL, NULL, NULL, 1, '2021-04-05 18:21:44', '2021-04-07 01:30:45', '2021-04-07 01:30:45'),
(23, 'Test Window', 27, 'specification', NULL, NULL, '100', 1, '2021-04-06 05:47:14', '2021-04-07 01:30:42', '2021-04-07 01:30:42'),
(24, 'Test tttt', 27, 'pricechart', NULL, NULL, NULL, 1, '2021-04-06 05:49:32', '2021-04-07 01:30:34', '2021-04-07 01:30:34'),
(26, 'Herkömmlich', 4, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:32:43', '2021-04-07 01:32:43', NULL),
(27, 'Swissvax', 4, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:33:15', '2021-04-07 01:33:15', NULL),
(28, 'SPEZIALFÄLLE', 4, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:33:50', '2021-04-07 01:33:50', NULL),
(29, 'KERAMIKBESCHICHTUNG', 5, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:34:31', '2021-04-07 01:34:31', NULL),
(30, 'TEFLONVERSIEGELUNG', 5, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:35:05', '2021-04-07 01:35:05', NULL),
(31, 'SWISSVAX', 5, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:35:48', '2021-04-07 01:35:48', NULL),
(32, 'LACK', 12, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:36:43', '2021-04-07 01:36:43', NULL),
(33, 'SCHEIBEN', 12, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:37:26', '2021-04-07 01:37:26', NULL),
(34, 'KERAMIKBESCHICHTUNG', 13, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:38:05', '2021-04-07 01:38:05', NULL),
(35, 'TEFLONBECHICHTUNG', 13, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:38:50', '2021-04-07 01:38:50', NULL),
(36, 'MONO', 18, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:39:53', '2021-04-07 01:39:53', NULL),
(37, 'DESIGN', 18, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:40:25', '2021-04-07 01:40:25', NULL),
(38, 'MONO', 19, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:41:02', '2021-04-07 01:41:02', NULL),
(39, 'DESIGN', 19, 'specification', NULL, NULL, NULL, 1, '2021-04-07 01:41:36', '2021-04-07 01:41:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_prices`
--

CREATE TABLE `service_prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_management_id` bigint(20) UNSIGNED NOT NULL,
  `vehicle_service_id` bigint(20) UNSIGNED DEFAULT NULL,
  `vehicle_categorie_id` bigint(20) UNSIGNED NOT NULL,
  `vehicle_condition_id` bigint(20) UNSIGNED NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_prices`
--

INSERT INTO `service_prices` (`id`, `service_management_id`, `vehicle_service_id`, `vehicle_categorie_id`, `vehicle_condition_id`, `price`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(45, 26, NULL, 7, 2, '100', 1, '2021-04-07 01:32:43', '2021-04-07 01:32:43', NULL),
(46, 26, NULL, 7, 1, '110', 1, '2021-04-07 01:32:43', '2021-04-07 01:32:43', NULL),
(47, 26, NULL, 6, 2, '120', 1, '2021-04-07 01:32:43', '2021-04-07 01:32:43', NULL),
(48, 26, NULL, 6, 1, '130', 1, '2021-04-07 01:32:43', '2021-04-07 01:32:43', NULL),
(49, 26, NULL, 5, 2, '140', 1, '2021-04-07 01:32:43', '2021-04-07 01:32:43', NULL),
(50, 26, NULL, 5, 1, '150', 1, '2021-04-07 01:32:43', '2021-04-07 01:32:43', NULL),
(51, 26, NULL, 3, 2, '160', 1, '2021-04-07 01:32:43', '2021-04-07 01:32:43', NULL),
(52, 26, NULL, 3, 1, '170', 1, '2021-04-07 01:32:43', '2021-04-07 01:32:43', NULL),
(53, 26, NULL, 2, 2, '180', 1, '2021-04-07 01:32:43', '2021-04-07 01:32:43', NULL),
(54, 26, NULL, 2, 1, '190', 1, '2021-04-07 01:32:43', '2021-04-07 01:32:43', NULL),
(55, 26, NULL, 1, 2, '200', 1, '2021-04-07 01:32:43', '2021-04-07 01:32:43', NULL),
(56, 26, NULL, 1, 1, '210', 1, '2021-04-07 01:32:43', '2021-04-07 01:32:43', NULL),
(57, 27, NULL, 7, 2, '100', 1, '2021-04-07 01:33:15', '2021-04-07 01:33:15', NULL),
(58, 27, NULL, 7, 1, '110', 1, '2021-04-07 01:33:15', '2021-04-07 01:33:15', NULL),
(59, 27, NULL, 6, 2, '1120', 1, '2021-04-07 01:33:15', '2021-04-07 01:33:15', NULL),
(60, 27, NULL, 6, 1, '130', 1, '2021-04-07 01:33:15', '2021-04-07 01:33:15', NULL),
(61, 27, NULL, 5, 2, '140', 1, '2021-04-07 01:33:15', '2021-04-07 01:33:15', NULL),
(62, 27, NULL, 5, 1, '150', 1, '2021-04-07 01:33:15', '2021-04-07 01:33:15', NULL),
(63, 27, NULL, 3, 2, '160', 1, '2021-04-07 01:33:15', '2021-04-07 01:33:15', NULL),
(64, 27, NULL, 3, 1, '170', 1, '2021-04-07 01:33:15', '2021-04-07 01:33:15', NULL),
(65, 27, NULL, 2, 2, '180', 1, '2021-04-07 01:33:15', '2021-04-07 01:33:15', NULL),
(66, 27, NULL, 2, 1, '190', 1, '2021-04-07 01:33:15', '2021-04-07 01:33:15', NULL),
(67, 27, NULL, 1, 2, '200', 1, '2021-04-07 01:33:15', '2021-04-07 01:33:15', NULL),
(68, 27, NULL, 1, 1, '210', 1, '2021-04-07 01:33:15', '2021-04-07 01:33:15', NULL),
(69, 28, NULL, 7, 2, '100', 1, '2021-04-07 01:33:50', '2021-04-07 01:33:50', NULL),
(70, 28, NULL, 7, 1, '110', 1, '2021-04-07 01:33:50', '2021-04-07 01:33:50', NULL),
(71, 28, NULL, 6, 2, '120', 1, '2021-04-07 01:33:50', '2021-04-07 01:33:50', NULL),
(72, 28, NULL, 6, 1, '130', 1, '2021-04-07 01:33:50', '2021-04-07 01:33:50', NULL),
(73, 28, NULL, 5, 2, '140', 1, '2021-04-07 01:33:50', '2021-04-07 01:33:50', NULL),
(74, 28, NULL, 5, 1, '150', 1, '2021-04-07 01:33:50', '2021-04-07 01:33:50', NULL),
(75, 28, NULL, 3, 2, '160', 1, '2021-04-07 01:33:50', '2021-04-07 01:33:50', NULL),
(76, 28, NULL, 3, 1, '170', 1, '2021-04-07 01:33:50', '2021-04-07 01:33:50', NULL),
(77, 28, NULL, 2, 2, '180', 1, '2021-04-07 01:33:50', '2021-04-07 01:33:50', NULL),
(78, 28, NULL, 2, 1, '190', 1, '2021-04-07 01:33:50', '2021-04-07 01:33:50', NULL),
(79, 28, NULL, 1, 2, '200', 1, '2021-04-07 01:33:50', '2021-04-07 01:33:50', NULL),
(80, 28, NULL, 1, 1, '210', 1, '2021-04-07 01:33:50', '2021-04-07 01:33:50', NULL),
(81, 29, NULL, 7, 2, '100', 1, '2021-04-07 01:34:31', '2021-04-07 01:34:31', NULL),
(82, 29, NULL, 7, 1, '110', 1, '2021-04-07 01:34:31', '2021-04-07 01:34:31', NULL),
(83, 29, NULL, 6, 2, '120', 1, '2021-04-07 01:34:31', '2021-04-07 01:34:31', NULL),
(84, 29, NULL, 6, 1, '130', 1, '2021-04-07 01:34:31', '2021-04-07 01:34:31', NULL),
(85, 29, NULL, 5, 2, '140', 1, '2021-04-07 01:34:31', '2021-04-07 01:34:31', NULL),
(86, 29, NULL, 5, 1, '150', 1, '2021-04-07 01:34:31', '2021-04-07 01:34:31', NULL),
(87, 29, NULL, 3, 2, '160', 1, '2021-04-07 01:34:31', '2021-04-07 01:34:31', NULL),
(88, 29, NULL, 3, 1, '170', 1, '2021-04-07 01:34:31', '2021-04-07 01:34:31', NULL),
(89, 29, NULL, 2, 2, '180', 1, '2021-04-07 01:34:31', '2021-04-07 01:34:31', NULL),
(90, 29, NULL, 2, 1, '190', 1, '2021-04-07 01:34:31', '2021-04-07 01:34:31', NULL),
(91, 29, NULL, 1, 2, '200', 1, '2021-04-07 01:34:31', '2021-04-07 01:34:31', NULL),
(92, 29, NULL, 1, 1, '210', 1, '2021-04-07 01:34:31', '2021-04-07 01:34:31', NULL),
(93, 30, NULL, 7, 2, '100', 1, '2021-04-07 01:35:05', '2021-04-07 01:35:05', NULL),
(94, 30, NULL, 7, 1, '110', 1, '2021-04-07 01:35:05', '2021-04-07 01:35:05', NULL),
(95, 30, NULL, 6, 2, '120', 1, '2021-04-07 01:35:05', '2021-04-07 01:35:05', NULL),
(96, 30, NULL, 6, 1, '130', 1, '2021-04-07 01:35:05', '2021-04-07 01:35:05', NULL),
(97, 30, NULL, 5, 2, '140', 1, '2021-04-07 01:35:05', '2021-04-07 01:35:05', NULL),
(98, 30, NULL, 5, 1, '150', 1, '2021-04-07 01:35:05', '2021-04-07 01:35:05', NULL),
(99, 30, NULL, 3, 2, '160', 1, '2021-04-07 01:35:05', '2021-04-07 01:35:05', NULL),
(100, 30, NULL, 3, 1, '170', 1, '2021-04-07 01:35:05', '2021-04-07 01:35:05', NULL),
(101, 30, NULL, 2, 2, '180', 1, '2021-04-07 01:35:05', '2021-04-07 01:35:05', NULL),
(102, 30, NULL, 2, 1, '190', 1, '2021-04-07 01:35:05', '2021-04-07 01:35:05', NULL),
(103, 30, NULL, 1, 2, '200', 1, '2021-04-07 01:35:05', '2021-04-07 01:35:05', NULL),
(104, 30, NULL, 1, 1, '210', 1, '2021-04-07 01:35:05', '2021-04-07 01:35:05', NULL),
(105, 31, NULL, 7, 2, '100', 1, '2021-04-07 01:35:48', '2021-04-07 01:35:48', NULL),
(106, 31, NULL, 7, 1, '110', 1, '2021-04-07 01:35:48', '2021-04-07 01:35:48', NULL),
(107, 31, NULL, 6, 2, '120', 1, '2021-04-07 01:35:48', '2021-04-07 01:35:48', NULL),
(108, 31, NULL, 6, 1, '130', 1, '2021-04-07 01:35:48', '2021-04-07 01:35:48', NULL),
(109, 31, NULL, 5, 2, '140', 1, '2021-04-07 01:35:48', '2021-04-07 01:35:48', NULL),
(110, 31, NULL, 5, 1, '150', 1, '2021-04-07 01:35:48', '2021-04-07 01:35:48', NULL),
(111, 31, NULL, 3, 2, '160', 1, '2021-04-07 01:35:48', '2021-04-07 01:35:48', NULL),
(112, 31, NULL, 3, 1, '170', 1, '2021-04-07 01:35:48', '2021-04-07 01:35:48', NULL),
(113, 31, NULL, 2, 2, '180', 1, '2021-04-07 01:35:48', '2021-04-07 01:35:48', NULL),
(114, 31, NULL, 2, 1, '190', 1, '2021-04-07 01:35:48', '2021-04-07 01:35:48', NULL),
(115, 31, NULL, 1, 2, '200', 1, '2021-04-07 01:35:48', '2021-04-07 01:35:48', NULL),
(116, 31, NULL, 1, 1, '210', 1, '2021-04-07 01:35:48', '2021-04-07 01:35:48', NULL),
(117, 32, NULL, 7, 2, '100', 1, '2021-04-07 01:36:43', '2021-04-07 01:36:43', NULL),
(118, 32, NULL, 7, 1, '110', 1, '2021-04-07 01:36:43', '2021-04-07 01:36:43', NULL),
(119, 32, NULL, 6, 2, '120', 1, '2021-04-07 01:36:43', '2021-04-07 01:36:43', NULL),
(120, 32, NULL, 6, 1, '130', 1, '2021-04-07 01:36:43', '2021-04-07 01:36:43', NULL),
(121, 32, NULL, 5, 2, '140', 1, '2021-04-07 01:36:43', '2021-04-07 01:36:43', NULL),
(122, 32, NULL, 5, 1, '150', 1, '2021-04-07 01:36:43', '2021-04-07 01:36:43', NULL),
(123, 32, NULL, 3, 2, '160', 1, '2021-04-07 01:36:43', '2021-04-07 01:36:43', NULL),
(124, 32, NULL, 3, 1, '170', 1, '2021-04-07 01:36:43', '2021-04-07 01:36:43', NULL),
(125, 32, NULL, 2, 2, '180', 1, '2021-04-07 01:36:43', '2021-04-07 01:36:43', NULL),
(126, 32, NULL, 2, 1, '190', 1, '2021-04-07 01:36:43', '2021-04-07 01:36:43', NULL),
(127, 32, NULL, 1, 2, '200', 1, '2021-04-07 01:36:43', '2021-04-07 01:36:43', NULL),
(128, 32, NULL, 1, 1, '210', 1, '2021-04-07 01:36:43', '2021-04-07 01:36:43', NULL),
(129, 33, NULL, 7, 2, '100', 1, '2021-04-07 01:37:26', '2021-04-07 01:37:26', NULL),
(130, 33, NULL, 7, 1, '110', 1, '2021-04-07 01:37:26', '2021-04-07 01:37:26', NULL),
(131, 33, NULL, 6, 2, '120', 1, '2021-04-07 01:37:26', '2021-04-07 01:37:26', NULL),
(132, 33, NULL, 6, 1, '130', 1, '2021-04-07 01:37:26', '2021-04-07 01:37:26', NULL),
(133, 33, NULL, 5, 2, '140', 1, '2021-04-07 01:37:26', '2021-04-07 01:37:26', NULL),
(134, 33, NULL, 5, 1, '150', 1, '2021-04-07 01:37:26', '2021-04-07 01:37:26', NULL),
(135, 33, NULL, 3, 2, '160', 1, '2021-04-07 01:37:26', '2021-04-07 01:37:26', NULL),
(136, 33, NULL, 3, 1, '170', 1, '2021-04-07 01:37:26', '2021-04-07 01:37:26', NULL),
(137, 33, NULL, 2, 2, '180', 1, '2021-04-07 01:37:26', '2021-04-07 01:37:26', NULL),
(138, 33, NULL, 2, 1, '190', 1, '2021-04-07 01:37:26', '2021-04-07 01:37:26', NULL),
(139, 33, NULL, 1, 2, '200', 1, '2021-04-07 01:37:26', '2021-04-07 01:37:26', NULL),
(140, 33, NULL, 1, 1, '210', 1, '2021-04-07 01:37:26', '2021-04-07 01:37:26', NULL),
(141, 34, NULL, 7, 2, '100', 1, '2021-04-07 01:38:05', '2021-04-07 01:38:05', NULL),
(142, 34, NULL, 7, 1, '110', 1, '2021-04-07 01:38:05', '2021-04-07 01:38:05', NULL),
(143, 34, NULL, 6, 2, '120', 1, '2021-04-07 01:38:05', '2021-04-07 01:38:05', NULL),
(144, 34, NULL, 6, 1, '130', 1, '2021-04-07 01:38:05', '2021-04-07 01:38:05', NULL),
(145, 34, NULL, 5, 2, '140', 1, '2021-04-07 01:38:05', '2021-04-07 01:38:05', NULL),
(146, 34, NULL, 5, 1, '150', 1, '2021-04-07 01:38:05', '2021-04-07 01:38:05', NULL),
(147, 34, NULL, 3, 2, '160', 1, '2021-04-07 01:38:05', '2021-04-07 01:38:05', NULL),
(148, 34, NULL, 3, 1, '170', 1, '2021-04-07 01:38:05', '2021-04-07 01:38:05', NULL),
(149, 34, NULL, 2, 2, '180', 1, '2021-04-07 01:38:05', '2021-04-07 01:38:05', NULL),
(150, 34, NULL, 2, 1, '190', 1, '2021-04-07 01:38:05', '2021-04-07 01:38:05', NULL),
(151, 34, NULL, 1, 2, '200', 1, '2021-04-07 01:38:05', '2021-04-07 01:38:05', NULL),
(152, 34, NULL, 1, 1, '210', 1, '2021-04-07 01:38:05', '2021-04-07 01:38:05', NULL),
(153, 35, NULL, 7, 2, '100', 1, '2021-04-07 01:38:50', '2021-04-07 01:38:50', NULL),
(154, 35, NULL, 7, 1, '110', 1, '2021-04-07 01:38:50', '2021-04-07 01:38:50', NULL),
(155, 35, NULL, 6, 2, '120', 1, '2021-04-07 01:38:50', '2021-04-07 01:38:50', NULL),
(156, 35, NULL, 6, 1, '130', 1, '2021-04-07 01:38:50', '2021-04-07 01:38:50', NULL),
(157, 35, NULL, 5, 2, '140', 1, '2021-04-07 01:38:50', '2021-04-07 01:38:50', NULL),
(158, 35, NULL, 5, 1, '150', 1, '2021-04-07 01:38:50', '2021-04-07 01:38:50', NULL),
(159, 35, NULL, 3, 2, '160', 1, '2021-04-07 01:38:50', '2021-04-07 01:38:50', NULL),
(160, 35, NULL, 3, 1, '170', 1, '2021-04-07 01:38:50', '2021-04-07 01:38:50', NULL),
(161, 35, NULL, 2, 2, '180', 1, '2021-04-07 01:38:50', '2021-04-07 01:38:50', NULL),
(162, 35, NULL, 2, 1, '190', 1, '2021-04-07 01:38:50', '2021-04-07 01:38:50', NULL),
(163, 35, NULL, 1, 2, '200', 1, '2021-04-07 01:38:50', '2021-04-07 01:38:50', NULL),
(164, 35, NULL, 1, 1, '210', 1, '2021-04-07 01:38:50', '2021-04-07 01:38:50', NULL),
(165, 36, NULL, 7, 2, '100', 1, '2021-04-07 01:39:54', '2021-04-07 01:39:54', NULL),
(166, 36, NULL, 7, 1, '110', 1, '2021-04-07 01:39:54', '2021-04-07 01:39:54', NULL),
(167, 36, NULL, 6, 2, '120', 1, '2021-04-07 01:39:54', '2021-04-07 01:39:54', NULL),
(168, 36, NULL, 6, 1, '130', 1, '2021-04-07 01:39:54', '2021-04-07 01:39:54', NULL),
(169, 36, NULL, 5, 2, '140', 1, '2021-04-07 01:39:54', '2021-04-07 01:39:54', NULL),
(170, 36, NULL, 5, 1, '150', 1, '2021-04-07 01:39:54', '2021-04-07 01:39:54', NULL),
(171, 36, NULL, 3, 2, '160', 1, '2021-04-07 01:39:54', '2021-04-07 01:39:54', NULL),
(172, 36, NULL, 3, 1, '170', 1, '2021-04-07 01:39:54', '2021-04-07 01:39:54', NULL),
(173, 36, NULL, 2, 2, '180', 1, '2021-04-07 01:39:54', '2021-04-07 01:39:54', NULL),
(174, 36, NULL, 2, 1, '190', 1, '2021-04-07 01:39:54', '2021-04-07 01:39:54', NULL),
(175, 36, NULL, 1, 2, '200', 1, '2021-04-07 01:39:54', '2021-04-07 01:39:54', NULL),
(176, 36, NULL, 1, 1, '210', 1, '2021-04-07 01:39:54', '2021-04-07 01:39:54', NULL),
(177, 37, NULL, 7, 2, '100', 1, '2021-04-07 01:40:25', '2021-04-07 01:40:25', NULL),
(178, 37, NULL, 7, 1, '110', 1, '2021-04-07 01:40:25', '2021-04-07 01:40:25', NULL),
(179, 37, NULL, 6, 2, '120', 1, '2021-04-07 01:40:25', '2021-04-07 01:40:25', NULL),
(180, 37, NULL, 6, 1, '130', 1, '2021-04-07 01:40:25', '2021-04-07 01:40:25', NULL),
(181, 37, NULL, 5, 2, '140', 1, '2021-04-07 01:40:25', '2021-04-07 01:40:25', NULL),
(182, 37, NULL, 5, 1, '150', 1, '2021-04-07 01:40:25', '2021-04-07 01:40:25', NULL),
(183, 37, NULL, 3, 2, '160', 1, '2021-04-07 01:40:25', '2021-04-07 01:40:25', NULL),
(184, 37, NULL, 3, 1, '170', 1, '2021-04-07 01:40:25', '2021-04-07 01:40:25', NULL),
(185, 37, NULL, 2, 2, '180', 1, '2021-04-07 01:40:25', '2021-04-07 01:40:25', NULL),
(186, 37, NULL, 2, 1, '190', 1, '2021-04-07 01:40:25', '2021-04-07 01:40:25', NULL),
(187, 37, NULL, 1, 2, '200', 1, '2021-04-07 01:40:25', '2021-04-07 01:40:25', NULL),
(188, 37, NULL, 1, 1, '210', 1, '2021-04-07 01:40:25', '2021-04-07 01:40:25', NULL),
(189, 38, NULL, 7, 2, '100', 1, '2021-04-07 01:41:02', '2021-04-07 01:41:02', NULL),
(190, 38, NULL, 7, 1, '110', 1, '2021-04-07 01:41:02', '2021-04-07 01:41:02', NULL),
(191, 38, NULL, 6, 2, '120', 1, '2021-04-07 01:41:02', '2021-04-07 01:41:02', NULL),
(192, 38, NULL, 6, 1, '130', 1, '2021-04-07 01:41:02', '2021-04-07 01:41:02', NULL),
(193, 38, NULL, 5, 2, '140', 1, '2021-04-07 01:41:02', '2021-04-07 01:41:02', NULL),
(194, 38, NULL, 5, 1, '150', 1, '2021-04-07 01:41:02', '2021-04-07 01:41:02', NULL),
(195, 38, NULL, 3, 2, '160', 1, '2021-04-07 01:41:02', '2021-04-07 01:41:02', NULL),
(196, 38, NULL, 3, 1, '170', 1, '2021-04-07 01:41:02', '2021-04-07 01:41:02', NULL),
(197, 38, NULL, 2, 2, '180', 1, '2021-04-07 01:41:02', '2021-04-07 01:41:02', NULL),
(198, 38, NULL, 2, 1, '190', 1, '2021-04-07 01:41:02', '2021-04-07 01:41:02', NULL),
(199, 38, NULL, 1, 2, '200', 1, '2021-04-07 01:41:02', '2021-04-07 01:41:02', NULL),
(200, 38, NULL, 1, 1, '210', 1, '2021-04-07 01:41:02', '2021-04-07 01:41:02', NULL),
(201, 39, NULL, 7, 2, '100', 1, '2021-04-07 01:41:36', '2021-04-07 01:41:36', NULL),
(202, 39, NULL, 7, 1, '110', 1, '2021-04-07 01:41:36', '2021-04-07 01:41:36', NULL),
(203, 39, NULL, 6, 2, '120', 1, '2021-04-07 01:41:36', '2021-04-07 01:41:36', NULL),
(204, 39, NULL, 6, 1, '130', 1, '2021-04-07 01:41:36', '2021-04-07 01:41:36', NULL),
(205, 39, NULL, 5, 2, '140', 1, '2021-04-07 01:41:36', '2021-04-07 01:41:36', NULL),
(206, 39, NULL, 5, 1, '150', 1, '2021-04-07 01:41:36', '2021-04-07 01:41:36', NULL),
(207, 39, NULL, 3, 2, '160', 1, '2021-04-07 01:41:36', '2021-04-07 01:41:36', NULL),
(208, 39, NULL, 3, 1, '170', 1, '2021-04-07 01:41:36', '2021-04-07 01:41:36', NULL),
(209, 39, NULL, 2, 2, '180', 1, '2021-04-07 01:41:36', '2021-04-07 01:41:36', NULL),
(210, 39, NULL, 2, 1, '190', 1, '2021-04-07 01:41:36', '2021-04-07 01:41:36', NULL),
(211, 39, NULL, 1, 2, '200', 1, '2021-04-07 01:41:36', '2021-04-07 01:41:36', NULL),
(212, 39, NULL, 1, 1, '210', 1, '2021-04-07 01:41:36', '2021-04-07 01:41:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Liebreiz', 'admin@admin.com', '2021-04-02 00:05:16', '$2y$10$umdOzebLZ7Z4gFFaRkxBhe52cvX8Sbh4RkgUcwn9VVL0MMKY22bi.', 'rLi0jCQBov', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_brands`
--

CREATE TABLE `vehicle_brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_type_ids` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'comma seperater types',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_brands`
--

INSERT INTO `vehicle_brands` (`id`, `name`, `vehicle_type_ids`, `image`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BMW', '1', 'public//08pUZEeHkZOZjOqvInTtcUh441pslsMHvwwwhzig.png', 1, '2021-04-02 00:07:01', '2021-04-03 06:54:54', NULL),
(2, 'Audi', '1', 'public//0H2ayIvSu3SLRrZ7Qp5SGc6pnPLpnUfyt0jOEcjx.png', 1, '2021-04-02 00:21:24', '2021-04-02 00:50:51', NULL),
(3, 'Mercedes-Benz', '1', 'public//CquBHW6AFzxOhMNk8BdaAIkb1LwVvH4LFuRMlVAI.png', 1, '2021-04-02 00:51:11', '2021-04-02 00:51:11', NULL),
(4, 'seat', '1', 'public//RG66PrYKYH77JYrLz8dqSPIM5eWyi4Xq9sSAKJhi.png', 1, '2021-04-02 00:51:26', '2021-04-02 00:51:26', NULL),
(5, 'Skoda', '1', 'public//vPR6ZpKam8f1q4SPzjAfNizNxkaOFqjHuEji7KVz.png', 1, '2021-04-02 00:51:38', '2021-04-02 00:51:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_categories`
--

CREATE TABLE `vehicle_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vehicle_type_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_categories`
--

INSERT INTO `vehicle_categories` (`id`, `vehicle_type_id`, `name`, `image`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Kombi', 'public//4k6noa2fbzhrjJSVc77Mr3dSJCwKaw4NiTN9chFN.png', 1, '2021-03-24 19:16:16', '2021-04-12 03:40:14', NULL),
(2, 1, 'Kleiwagen', 'public//XfnKG1Sbl3hnkELvhi1i0dI2dtW45H5IqsWZhEeF.png', 1, '2021-03-24 19:16:21', '2021-04-12 03:40:00', NULL),
(3, 1, 'Micro', 'public//0HzYX7kcfUNM1BQLBYShZ0bADQrY59yRqSzICVIe.png', 1, '2021-03-24 19:16:28', '2021-04-12 03:39:40', NULL),
(5, 1, 'Coupe', 'public//Z3vlAKqPYCBimIf0aXxXF7a2sFaey79YBQ3yr00E.png', 1, '2021-04-01 01:53:35', '2021-04-12 03:39:30', NULL),
(6, 1, 'Limousine', 'public//M0Cf3kQGLm8szoEYMlCKxcbL5vY6voA3OJ9GBzVW.png', 1, '2021-04-01 01:53:41', '2021-04-12 03:39:17', NULL),
(7, 1, 'LKW', 'public//j9v4H0uyPNFJCCswtBtINqAk30shfBIo7Cqbkhjd.png', 1, '2021-04-01 01:53:59', '2021-04-12 03:39:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_conditions`
--

CREATE TABLE `vehicle_conditions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_conditions`
--

INSERT INTO `vehicle_conditions` (`id`, `name`, `image`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Neuwagen', 'public//lUU2e9IIajVn5pfxoIfw1Gki0Wwusj3o6phbJRJw.png', 1, '2021-03-24 19:16:34', '2021-04-12 03:18:30', NULL),
(2, 'Gebrauchtwagen', 'public//vIxo2Dl5Gdewkjoad4ziiVh8BfBCsuRt6t3PYNnY.png', 1, '2021-03-24 19:16:39', '2021-04-12 03:19:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_services`
--

CREATE TABLE `vehicle_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_management_id` bigint(20) UNSIGNED DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_types`
--

CREATE TABLE `vehicle_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_types`
--

INSERT INTO `vehicle_types` (`id`, `name`, `image`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Auto', 'public//pTEuE6wX4e4woY2Wq3xP8qOiraevLsVvlfx84ULm.png', 1, '2021-03-24 19:15:12', '2021-04-12 01:49:06', NULL),
(2, 'Motorrad', 'public//v0zpZMpX2fe8JnHUZKDqR50w1fKxe2NqkpEhnbZy.png', 1, '2021-03-24 19:15:18', '2021-04-12 01:49:22', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addonservice`
--
ALTER TABLE `addonservice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_appointment`
--
ALTER TABLE `booking_appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drive_vehicles`
--
ALTER TABLE `drive_vehicles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drive_vehicles_vehicle_categorie_id_foreign` (`vehicle_categorie_id`),
  ADD KEY `drive_vehicles_vehicle_brand_id_foreign` (`vehicle_brand_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_categories`
--
ALTER TABLE `service_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_categories_vehicle_type_id_foreign` (`vehicle_type_id`);

--
-- Indexes for table `service_management`
--
ALTER TABLE `service_management`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_management_service_category_id_foreign` (`service_category_id`);

--
-- Indexes for table `service_prices`
--
ALTER TABLE `service_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_prices_service_management_id_foreign` (`service_management_id`),
  ADD KEY `service_prices_vehicle_service_id_foreign` (`vehicle_service_id`),
  ADD KEY `service_prices_vehicle_categorie_id_foreign` (`vehicle_categorie_id`),
  ADD KEY `service_prices_vehicle_condition_id_foreign` (`vehicle_condition_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vehicle_brands`
--
ALTER TABLE `vehicle_brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_conditions`
--
ALTER TABLE `vehicle_conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_services`
--
ALTER TABLE `vehicle_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicle_services_service_management_id_foreign` (`service_management_id`);

--
-- Indexes for table `vehicle_types`
--
ALTER TABLE `vehicle_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addonservice`
--
ALTER TABLE `addonservice`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `booking_appointment`
--
ALTER TABLE `booking_appointment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `drive_vehicles`
--
ALTER TABLE `drive_vehicles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `service_categories`
--
ALTER TABLE `service_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `service_management`
--
ALTER TABLE `service_management`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `service_prices`
--
ALTER TABLE `service_prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vehicle_brands`
--
ALTER TABLE `vehicle_brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `vehicle_conditions`
--
ALTER TABLE `vehicle_conditions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehicle_services`
--
ALTER TABLE `vehicle_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `vehicle_types`
--
ALTER TABLE `vehicle_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `drive_vehicles`
--
ALTER TABLE `drive_vehicles`
  ADD CONSTRAINT `drive_vehicles_vehicle_brand_id_foreign` FOREIGN KEY (`vehicle_brand_id`) REFERENCES `vehicle_brands` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `drive_vehicles_vehicle_categorie_id_foreign` FOREIGN KEY (`vehicle_categorie_id`) REFERENCES `vehicle_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_categories`
--
ALTER TABLE `service_categories`
  ADD CONSTRAINT `service_categories_vehicle_type_id_foreign` FOREIGN KEY (`vehicle_type_id`) REFERENCES `vehicle_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_management`
--
ALTER TABLE `service_management`
  ADD CONSTRAINT `service_management_service_category_id_foreign` FOREIGN KEY (`service_category_id`) REFERENCES `service_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_prices`
--
ALTER TABLE `service_prices`
  ADD CONSTRAINT `service_prices_service_management_id_foreign` FOREIGN KEY (`service_management_id`) REFERENCES `service_management` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `service_prices_vehicle_categorie_id_foreign` FOREIGN KEY (`vehicle_categorie_id`) REFERENCES `vehicle_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `service_prices_vehicle_condition_id_foreign` FOREIGN KEY (`vehicle_condition_id`) REFERENCES `vehicle_conditions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `service_prices_vehicle_service_id_foreign` FOREIGN KEY (`vehicle_service_id`) REFERENCES `vehicle_services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vehicle_services`
--
ALTER TABLE `vehicle_services`
  ADD CONSTRAINT `vehicle_services_service_management_id_foreign` FOREIGN KEY (`service_management_id`) REFERENCES `service_management` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
