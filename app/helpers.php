<?php 
use App\Http\Model\VehicleService;
use App\Http\Model\ServiceManagement;
use App\Http\Model\ServicePrice;
use App\Http\Model\ServiceCategory;
use App\Http\Model\DriveVehicle;

function getVehicleService($service_management_id){
	$vehicle_service = VehicleService::where('service_management_id',$service_management_id)->get();
	return $vehicle_service;
}

function getServices($service_category_id){
    $service_management = ServiceManagement::where('service_category_id',$service_category_id)->get();
    return $service_management;
}

function getSubCategory($category_id){
    $service_management = ServiceCategory::where('parent_id',$category_id)->get();
    return $service_management;
}

function getServicesWithPrice($service_management_id,$vehicle_condition_id,$vehicle_type_id){
    $servicePrice = ServicePrice::where('service_management_id',$service_management_id)
    ->where('vehicle_condition_id',$vehicle_condition_id)
    ->where('vehicle_categorie_id',$vehicle_type_id)
    ->first();
    return $servicePrice;
}

function getDriveVehicle($vehicle_brand_id,$vehicle_type){
    $drive_vehicle = DriveVehicle::where('vehicle_brand_id',$vehicle_brand_id)->where('vehicle_categorie_id',$vehicle_type)->get();
    return $drive_vehicle;
}

function getServicesName($id){
    $data = ServiceCategory::where('id',$id)->pluck('name')->first();
    return $data;
}

function getCategoryName($id){
    $data = ServiceCategory::where('id',$id)->pluck('name')->first();
    return $data;
}

function getSubCategoryById($sub_category_array, $parent_id){
    $serviceCategory = ServiceCategory::whereIn('id', $sub_category_array)->where('parent_id',$parent_id)->get();
    return $serviceCategory;
}

function getServicesById($service_management_array, $sub_cat_id){
    $serviceManagement = ServiceManagement::whereIn('id', $service_management_array)->where('service_category_id',$sub_cat_id)->get();
    return $serviceManagement;
}