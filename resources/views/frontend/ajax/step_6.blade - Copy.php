@php $temp_array = [];@endphp
@foreach ($service_management as $key => $value) 
    @if($value->type == "specification")
	    <div class="form-group">
	        <input type="checkbox" name="step_7" class="form-control checkbox" value="{{$value->id}}" id="step_7_{{$value->id}}" s-id="{{$value->service_category_id}}" p-id="{{$value->price}}">
	        <label role="button" class="align-items-center d-flex " for="step_7_{{$value->id}}"><span>{{ucfirst($value->name).' - '.$value->price.' CHF'}}</span>
		        <div class="infoicon-i ml-2"  role="button" data-toggle="modal" data-target="#modal_bookappointment_6" title="Read more">
		        	<img src="{{ asset('assets/frontend/images/info-icon.svg') }}" class="d-block">
		        </div>
		    </label>
	    </div>
	@endif    
@endforeach