<?php

namespace App\Http\Controllers;

use App\Http\Model\VehicleType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
class VehicleTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicle_type = VehicleType::latest()->get();
        return view('backend.vehicle_type.index', compact('vehicle_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.vehicle_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                $vehicle_type = VehicleType::where('id',$id)->first();
                if (empty($vehicle_type->is_active)) {
                    $vehicle_type->is_active = 1;
                } else {
                    $vehicle_type->is_active = 0;
                }
                    $vehicle_type->save();
                return response()->json(['success' => 1]);
            }else{
                $this->validate($request, [
                    'name' => 'required',
                ]);
                if(!empty($request->id)){
                    $id = Crypt::decrypt($request->id);
                    $vehicle_type = VehicleType::where('id',$id)->first();
                    $message = trans('message.update', ['entity' => trans('cruds.vehicletypes.title_singular')]);
                }else{
                    $vehicle_type = new VehicleType;
                    $message = trans('message.create', ['entity' => trans('cruds.vehicletypes.title_singular')]);
                }
                    if ($request->hasFile('image')) {                        
                        $storage_path = 'public/';
                        $vehicle_type->image = Storage::disk('local')->put($storage_path, $request->file('image'));
                    }
                    
                    $vehicle_type->name = $request->name;
                    $vehicle_type->save();
                    return redirect()->route('vehicletypes.index')->with('success',$message);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
            return redirect()->back()->with('danger', trans('global.something_wrong'));    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VehicleType  $vehicletype
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleType $vehicletype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VehicleType  $vehicletype
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $vehicletype = VehicleType::where('id',$id)->first();
        return view('backend.vehicle_type.edit', compact('vehicletype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VehicleType  $vehicletype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleType $vehicletype)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                VehicleType::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }
}
