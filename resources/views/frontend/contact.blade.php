@section('title', 'Contact')
@extends('layouts.front_end')
@section('content')

<style>
#loader {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: rgba(0, 0, 0, 0.9);
    z-index: 999999;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
}
#loader .loading {
    border: 10px solid #fff;
    border-radius: 50%;
    border-top: 10px solid transparent;
    width: 60px;
    height: 60px;
    animation: spin 1s linear infinite;
    background: transparent;
}
@keyframes spin {
  100% {
    transform: rotate(360deg);
  }
}
</style>
	<div class="main-wrapper">
		<!-- Section Breadcrumb 1 -->
		<div id="section-breadcrumb1" class="inner-banner-wrap">
			<img src="{{ asset('assets/frontend/images/contact-banner.jpg') }}" alt="" class="inner-page-banner">
			<div class="container">
				<div class="row">
					<div class="content col-12">
						<h1>Kontakt</h1>
						<ul>
							<li><a href="{{ route('index')}}">Zuhause</a></li>
							<li class="current text-light"><a href="#">Kontakt</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /.Section Breadcrumb 1 -->
		<!-- Section Contact -->
		<div id="section-contact" class="position-relative">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h3>Kontakt aufnehmen</h3>
					</div>
					<!-- Contact Form -->
					<div class="contact-form col-sm-12 col-md-6">
						<!-- Form -->
                        <div class="alert alert-success alertmsg" id="contactFormSuccess" style="display: none;"></div>
                        <div class="alert alert-danger alertmsg" id="contactFormDanger" style="display: none;"></div>
						<form id="contactform1" action="" method="post">
							<div class="row">
								<div class="form-group col-sm-12 col-md-12 col-lg-6">
									<input type="text" class="form-control" name="name" id="name" placeholder="Name" required="">
                                    <div class="name_form_error" style="color: red;display: none"></div>
								</div>
								<div class="form-group col-sm-12 col-md-12 col-lg-6">
									<input type="email" class="form-control" name="email" id="email" placeholder="Email*" required="">
                                    <div class="email_form_error" style="color: red;display: none"></div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-12 col-md-12 col-lg-6">
									<input type="text" class="form-control" name="phoneNumber" id="phoneNumber" placeholder="Telefonnummer*" required="">
                                    <div class="number_form_error" style="color: red;display: none"></div>
								</div>
								<div class="form-group col-sm-12 col-md-12 col-lg-6">
									<input type="text" class="form-control" name="subject" id="subject" placeholder="Gegenstand" required="">
                                    <div class="subject_form_error" style="color: red;display: none"></div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-12">
									<textarea class="form-control" name="message" id="message"  placeholder="Bericht" required=""></textarea>
                                    <div class="message_form_error" style="color: red;display: none"></div>
								</div>
							</div>
							<!-- <div class="row">
								<div class="form-group col-sm-12 col-md-12 col-lg-6">
									<div class="checkbox">
										<input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
										<label for="styled-checkbox-1">I am not a robot <i class="flaticon-user"></i></label>
									</div>
								</div>
							</div> -->
							<div class="row">
								<div class="form-group col-md-12">
									<button type="button" id="contactformbtn1">einreichen</button>
								</div>
							</div>
						</form>
						<!-- /.Form -->
					</div>
					<!-- /.Contact Form -->
					<!-- Contact Details -->
					<div class="contact-details col-sm-12 col-md-6">
						<!-- Item -->
						<div class="item ez-animate" data-animation="fadeInRight">
							<div class="thumb-icon">
								<i class="flaticon-house"></i>
							</div>
							<div class="description">
								<a href="https://www.google.ch/maps/place/Autokosmetik.li/@47.218252,9.511339,15z/data=!4m5!3m4!1s0x0:0x3025938a37e377f0!8m2!3d47.218252!4d9.511339" target="_blank">
									<!-- <p> Haldenstrasse 90, 9487 Gamprin,
									<br>Principality of Liechtenstein</p> -->
									<p> Messinastrasse 1
									<br> 9495 Triesen<br/>Liechtenstein</p>
								</a>
							</div>
						</div>
						<!-- /.Item -->
						<!-- Item -->
						<div class="item ez-animate" data-animation="fadeInRight">
							<div class="thumb-icon">
								<i class="flaticon-smartphone-1"></i>
							</div>
							<div class="description">
								<p>Office: <a href="callto:00423 373 03 54">+423 373 03 54</a><br>Mobile: <a href="callto:00423 792 57 75">+423 792 57 75</a></p>
							</div>
						</div>
						<!-- /.Item -->
						<!-- Item -->
						<div class="item ez-animate" data-animation="fadeInRight">
							<div class="thumb-icon">
								<i class="flaticon-paper-plane"></i>
							</div>
							<div class="description">
								<p><a href="mailto:info@autokosmetik.li">info@autokosmetik.li</a></p>
							</div>
						</div>
						<!-- /.Item -->
					</div>
					<!-- /.Contact Details -->
				</div>
			</div>
			<div id="loader" style="display:none"> 
				<span class="loading"></span>
			</div>
		</div>
		<!-- /.Section Contact -->
		<!-- Section Map -->
		<div id="section-map">
			<div class="row">
				<div class="col-6 p-0 d-flex">
					<div class="detail">
						<div class="marker-img">
							<img src="{{ asset('assets/frontend/images/marker.png') }}" alt="autokosmetik">
						</div>
						<div class="description">
							<p> Messinastrasse 1<br>
							9495 Triesen<br>
							Liechtenstein</p>
						</div>
					</div>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2714.9507204632764!2d9.517502515859576!3d47.11963722948226!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479b33e8d3f444ed%3A0x2c32a32d5a5bc7dd!2sMessinastrasse%201%2C%209495%20Triesen%2C%20Liechtenstein!5e0!3m2!1sen!2sin!4v1617771666494!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" class="d-flex h-100"></iframe>
				</div>
				<div class="col-6 p-0">
					<img src="{{ asset('assets/frontend/images/banner-new.png') }}" alt="" class="img-fluid">
				</div>
			</div>
		</div>
		<!-- /.Section Map -->
	</div>
@endsection

@section('script')    
<script>
$('body').on('click',"#contactform1",function(){
	$('#loader').show();
    var contact_name    = $("#name").val();
    var contact_email   = $("#email").val();
    var contact_number  = $("#phoneNumber").val();
    var contact_subject = $("#subject").val();
    var contact_message = $("#message").val();
    
    $('.name_form_error,.number_form_error,.email_form_error,.subject_form_error,.message_form_error').hide();
	var count = 0;
	if(contact_name == "" || contact_name == undefined){
		$('.name_form_error').text('Bitte Name eingeben');
		$('.name_form_error').show();
		count = count +1;
	}
	if(contact_number == "" || contact_number == undefined){
		$('.number_form_error').text('Bitte Kontaktnummer eingeben');
		$('.number_form_error').show();
		count = count +1;
	}
	if(contact_email == "" || contact_email == undefined){
		$('.email_form_error').text('Bitte E-Mail eingeben');
		$('.email_form_error').show();
		count = count +1;
	}else if(!validateEmail(contact_email)){
		$('.email_form_error').text('Bitte gültige E-Mail eingeben');
		$('.email_form_error').show();
		count = count +1;
	}
   	if(contact_subject == "" || contact_subject == undefined){
		$('.subject_form_error').text('Bitte Betreff eingeben');
		$('.subject_form_error').show();
		count = count +1;
	}
   	if(contact_message == "" || contact_message == undefined){
		$('.message_form_error').text('Bitte Nachricht eingeben');
		$('.message_form_error').show();
		count = count +1;
	}
	if(count != 0){
		$('#loader').hide();
		return false;
	}else{ 
       	$.ajax({
            method: 'POST',
            url: "{{ route('ajax.contactform1') }}",
            data: {
            	"_token": "{{ csrf_token() }}",
                "name": contact_name,
                "email": contact_email,
                "phoneNumber": contact_number,
                "subject": contact_subject,
                "message": contact_message
            }
        })
        .done(function(data) {
            if(data.result == 1){
                $('#contactFormSuccess').html('Vielen Dank für Ihre Kontaktaufnahme!');
                $('#contactFormSuccess').show();
            }else{
                $('#contactFormDanger').html('Fehler im Kontaktformular. Bitte versuchen Sie es nach einiger Zeit.');
                $('#contactFormDanger').show();
            }
            $('#contactform1').trigger("reset");
			$('#loader').hide();
            setTimeout( function() {
                hideAlert();
            }, 7000);
        })
	}
});

function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}

function hideAlert(){
    $('.alertmsg').hide();
}
</script>
@endsection