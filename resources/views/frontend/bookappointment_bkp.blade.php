@section('title', 'Book Appointment')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper bg-light">
		<div id="main-content">
			<!-- Section Breadcrumb 1 -->
			<div id="section-breadcrumb1" class="inner-banner-wrap">
				<img src="{{ asset('assets/frontend/images/bookappointment.jpg') }}" alt="" class="inner-page-banner">
				<div class="container">
					<div class="row">
						<div class="content col-12">
							<h1>Book Appointment</h1>
							<ul>
								<li><a href="{{ route('index')}}">HOME</a></li>
								<li class="current text-light">BOOK APPOINTMENT</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-6"></div>
					<div class="col-md-6">
						<!-- <div class="step-form-wrap py-5">
							<form action="">
								<div class="book-appo-wrap p-5 test-step active">
									<div class="slide-right-anim">
										<div class="test-progress">
											<div class="test-progress-step">
												<span class="step-number">1/6</span>
												<svg>
													<circle class="step-1" cx="30" cy="30" r="28" stroke-width="4" fill="none" role="slider" aria-orientation="vertical" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50"></circle>
												</svg>
											</div>
										</div>
										<h3>Car Model</h3>
										<div class="step-block">
											<div class="form-group">
												<input type="radio" name="car-model" class="form-control" id="yes">
												<label role="button" for="yes">New Model</label>
											</div>
											<div class="form-group">
												<input type="radio" name="car-model" class="form-control" id="no">
												<label role="button" for="no">Old Model</label>
											</div>
											<a href="#" class="comman-btn button">Next</a>
										</div>
									</div>
								</div>
								<div class="book-appo-wrap p-5 test-step">
									<div class="slide-right-anim">
										<div class="test-progress">
											<div class="test-progress-step">
												<span class="step-number">2/6</span>
												<svg>
												<circle class="step-2" cx="30" cy="30" r="28" stroke-width="4" fill="none" role="slider" aria-orientation="vertical" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50"></circle>
												</svg>
											</div>
										</div>
										<h3>Car Type</h3>
										<div class="step-block">
											<div class="form-group">
												<input type="radio" name="car-type" class="form-control" id="sports" value="sports">
												<label role="button" for="sports">Sports Car</label>
											</div>
											<div class="form-group">
												<input type="radio" name="car-type" class="form-control" id="sedan" value="sedan">
												<label role="button" for="sedan">Sedan</label>
											</div>
											<div class="form-group">
												<input type="radio" name="car-type" class="form-control" id="hatchback" value="hatchback">
												<label role="button" for="hatchback">Hatchback</label>
											</div>
											<div class="form-group">
												<input type="radio" name="car-type" class="form-control" id="luxury" value="luxury">
												<label role="button" for="luxury">Luxury Car</label>
											</div>
											<div class="prev-next-btn d-flex">
												<a href="#" class="comman-btn prev-btn"><img src="images/arrow-left-grey.png" alt="">Previous</a>
												<a href="#" class="comman-btn button ml-2">Next</a>
											</div>
										</div>
									</div>
								</div>
								<div class="book-appo-wrap p-5 test-step">
									<div class="slide-right-anim">
										<div class="test-progress">
											<div class="test-progress-step">
												<span class="step-number">3/6</span>
												<svg>
												<circle class="step-3" cx="30" cy="30" r="28" stroke-width="4" fill="none" role="slider" aria-orientation="vertical" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50"></circle>
												</svg>
											</div>
										</div>
										<h3>Car Company</h3>
										<div class="step-block">
											<div class="form-group">
												<input type="radio" name="car-company" class="form-control" id="mercedes" value="mercedes">
												<label role="button" for="mercedes">Mercedes</label>
											</div>
											<div class="form-group">
												<input type="radio" name="car-company" class="form-control" id="lamborghini" value="lamborghini">
												<label role="button" for="lamborghini">Lamborghini</label>
											</div>
											<div class="form-group">
												<input type="radio" name="car-company" class="form-control" id="bugatti" value="bugatti">
												<label role="button" for="bugatti">Bugatti</label>
											</div>
											<div class="prev-next-btn d-flex">
												<a href="#" class="comman-btn prev-btn"><img src="images/arrow-left-grey.png" alt="">Previous</a>
												<a href="#" class="comman-btn button ml-2">Next</a>
											</div>
										</div>
									</div>
								</div>
								<div class="book-appo-wrap p-5 test-step">
									<div class="slide-right-anim">
										<div class="test-progress">
											<div class="test-progress-step">
												<span class="step-number">4/6</span>
												<svg>
												<circle class="step-4" cx="30" cy="30" r="28" stroke-width="4" fill="none" role="slider" aria-orientation="vertical" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50"></circle>
												</svg>
											</div>
										</div>
										<h3>Car Model</h3>
										<div class="step-block">
											<div class="form-group">
												<input type="radio" name="car-model" class="form-control" id="c-class" value="c-class">
												<label role="button" for="c-class">C-Class</label>
											</div>
											<div class="form-group">
												<input type="radio" name="car-model" class="form-control" id="a-class" value="a-class">
												<label role="button" for="a-class">A-Class</label>
											</div>
											<div class="form-group">
												<input type="radio" name="car-model" class="form-control" id="g-class" value="g-class">
												<label role="button" for="g-class">G-Class</label>
											</div>
											<div class="form-group">
												<input type="radio" name="car-model" class="form-control" id="s-class" value="s-class">
												<label role="button" for="s-class">S-Class</label>
											</div>
											<div class="form-group">
												<input type="radio" name="car-model" class="form-control" id="e-class" value="e-class">
												<label role="button" for="e-class">E-Class</label>
											</div>
											<div class="form-group">
												<input type="radio" name="car-model" class="form-control" id="gla" value="gla">
												<label role="button" for="gla">GLA</label>
											</div>
											<div class="form-group">
												<input type="radio" name="car-model" class="form-control" id="glc" value="glc">
												<label role="button" for="glc">GLC</label>
											</div>
											<div class="prev-next-btn d-flex">
												<a href="#" class="comman-btn prev-btn"><img src="images/arrow-left-grey.png" alt="">Previous</a>
												<a href="#" class="comman-btn button ml-2">Next</a>
											</div>
										</div>
									</div>
								</div>
								<div class="book-appo-wrap p-5 test-step">
									<div class="slide-right-anim">
										<div class="test-progress">
											<div class="test-progress-step">
												<span class="step-number">5/6</span>
												<svg>
												<circle class="step-4" cx="30" cy="30" r="28" stroke-width="4" fill="none" role="slider" aria-orientation="vertical" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50"></circle>
												</svg>
											</div>
										</div>
										<h3>Select Service</h3>
										<div class="step-block">
											<div class="form-group">
												<input type="checkbox" name="car-service" class="form-control" id="interior" value="interior">
												<label role="button" for="interior">Interior</label>
											</div>
											<div class="form-group">
												<input type="checkbox" name="car-service" class="form-control" id="exterior" value="exterior">
												<label role="button" for="exterior">Exterior</label>
											</div>
											<div class="prev-next-btn d-flex">
												<a href="#" class="comman-btn prev-btn"><img src="images/arrow-left-grey.png" alt="">Previous</a>
												<a href="#" class="comman-btn button ml-2">Next</a>
											</div>
										</div>
									</div>
								</div>
								<div class="book-appo-wrap p-5 test-step">
									<div class="slide-right-anim">
										<div class="test-progress">
											<div class="test-progress-step">
												<span class="step-number">6/6</span>
												<svg>
												<circle class="step-4" cx="30" cy="30" r="28" stroke-width="4" fill="none" role="slider" aria-orientation="vertical" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50"></circle>
												</svg>
											</div>
										</div>
										<h3>Select Service</h3>
										<div class="step-block">
											<div class="form-group">
												<input type="checkbox" name="select-service" class="form-control" id="conventional" value="conventional">
												<label role="button" for="conventional">Conventional</label>
											</div>
											<div class="form-group">
												<input type="checkbox" name="select-service" class="form-control" id="swissvax" value="swissvax">
												<label role="button" for="swissvax">Swissvax</label>
											</div>
											<div class="form-group">
												<input type="checkbox" name="select-service" class="form-control" id="cases" value="cases">
												<label role="button" for="cases">Special Cases</label>
											</div>
											<div class="prev-next-btn d-flex">
												<a href="#" class="comman-btn prev-btn"><img src="images/arrow-left-grey.png" alt="">Previous</a>
												<a href="#" class="comman-btn ml-2">Submit</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div> -->
						<form action="" class="wizard-container" id="js-wizard-form">
							<div class="progress" id="js-progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
									<span class="progress-val">40%</span>
								</div>
							</div>
							<ul class="nav nav-tab">
								<li class="active">
									<a href="#tab1" data-toggle="tab">1</a>
								</li>
								<li>
									<a href="#tab2" data-toggle="tab">1</a>
								</li>
								<li>
									<a href="#tab3" data-toggle="tab">1</a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab1">
									<div class="input-group">
										<label class="label">Full name*:</label>
										<input class="input--style-1" type="text" name="fullname" placeholder="ex: Taylor Fuller">
									</div>
									<div class="input-group">
										<label class="label">Email*:</label>
										<input class="input--style-1" type="email" name="email" placeholder="ex: example@email.com">
									</div>
									<div class="input-group">
										<label class="label">Phone number*:</label>
										<input class="input--style-1" type="text" name="phone" placeholder="">
									</div>
									<div class="btn-next-con">
										<a class="btn-next" href="#">Next</a>
									</div>
								</div>
								<div class="tab-pane" id="tab2">
									<div class="input-group">
										<label class="label">Building Number:</label>
										<input class="input--style-1" type="text" name="buil-num" placeholder="02">
									</div>
									<div class="input-group">
										<label class="label">Street:</label>
										<input class="input--style-1" type="text" name="street" placeholder="ex: Philadelphia Avenue">
									</div>
									<div class="input-group">
										<label class="label">A Town:</label>
										<input class="input--style-1" type="text" name="town" placeholder="The Town">
									</div>
									<div class="input-group">
										<label class="label">Zip code:</label>
										<input class="input--style-1" type="text" name="code">
									</div>
									<div class="btn-next-con">
										<a class="btn-back" href="#">back</a>
										<a class="btn-next" href="#">Next</a>
									</div>
								</div>
								<div class="tab-pane" id="tab3">
									<div class="input-group">
										<label class="label">Card Holder Name:</label>
										<input class="input--style-1" type="text" name="cardName" value="Matthew Wood">
									</div>
									<div class="input-group">
										<label class="label">Card Number:</label>
										<div class="input-group-icon">
											<input class="input--style-1" type="text" name="cardNum" value="4224-3228-6160-5079">
											<i class="zmdi zmdi-card input-icon"></i>
										</div>
									</div>
									<div class="input-group">
										<label class="label">CVC:</label>
										<div class="input-group-icon">
											<input class="input--style-1" type="text" name="town" placeholder="The Town">
											<i class="zmdi zmdi-card input-icon"></i>
										</div>
									</div>
									<div class="input-group">
										<label class="label">Expiration(MM/YYYY):</label>
										<input class="input--style-1" type="text" name="exp" placeholder="MM/YYYY">
									</div>
									<div class="btn-next-con">
										<a class="btn-back" href="#">back</a>
										<a class="btn-last" href="#">Submit</a>
									</div>
								</div>
							</div>
						</form>
						<!-- <div class="step-form-wrap py-5">
							<div class="book-appo-wrap p-5 test-step">
								<div class="owl-carousel owl-theme">
									<div class="item">
										<div class="slide-right-anim">
											<div class="test-progress">
												<div class="test-progress-step">
													<span class="step-number">1/6</span>
													<svg>
														<circle class="step-1" cx="30" cy="30" r="28" stroke-width="4" fill="none" role="slider" aria-orientation="vertical" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50"></circle>
													</svg>
												</div>
											</div>
											<h3>Car Model</h3>
											<div class="step-block">
												<div class="form-group">
													<input type="radio" name="car-model" class="form-control" id="yes">
													<label role="button" for="yes">New Model</label>
												</div>
												<div class="form-group">
													<input type="radio" name="car-model" class="form-control" id="no">
													<label role="button" for="no">Old Model</label>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="slide-right-anim">
											<div class="test-progress">
												<div class="test-progress-step">
													<span class="step-number">2/6</span>
													<svg>
													<circle class="step-2" cx="30" cy="30" r="28" stroke-width="4" fill="none" role="slider" aria-orientation="vertical" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50"></circle>
													</svg>
												</div>
											</div>
											<h3>Car Type</h3>
											<div class="step-block">
												<div class="form-group">
													<input type="radio" name="car-type" class="form-control" id="sports" value="sports">
													<label role="button" for="sports">Sports Car</label>
												</div>
												<div class="form-group">
													<input type="radio" name="car-type" class="form-control" id="sedan" value="sedan">
													<label role="button" for="sedan">Sedan</label>
												</div>
												<div class="form-group">
													<input type="radio" name="car-type" class="form-control" id="hatchback" value="hatchback">
													<label role="button" for="hatchback">Hatchback</label>
												</div>
												<div class="form-group">
													<input type="radio" name="car-type" class="form-control" id="luxury" value="luxury">
													<label role="button" for="luxury">Luxury Car</label>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="slide-right-anim">
											<div class="test-progress">
												<div class="test-progress-step">
													<span class="step-number">3/6</span>
													<svg>
													<circle class="step-3" cx="30" cy="30" r="28" stroke-width="4" fill="none" role="slider" aria-orientation="vertical" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50"></circle>
													</svg>
												</div>
											</div>
											<h3>Car Company</h3>
											<div class="step-block">
												<div class="form-group">
													<input type="radio" name="car-company" class="form-control" id="mercedes" value="mercedes">
													<label role="button" for="mercedes">Mercedes</label>
												</div>
												<div class="form-group">
													<input type="radio" name="car-company" class="form-control" id="lamborghini" value="lamborghini">
													<label role="button" for="lamborghini">Lamborghini</label>
												</div>
												<div class="form-group">
													<input type="radio" name="car-company" class="form-control" id="bugatti" value="bugatti">
													<label role="button" for="bugatti">Bugatti</label>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="slide-right-anim">
											<div class="test-progress">
												<div class="test-progress-step">
													<span class="step-number">4/6</span>
													<svg>
													<circle class="step-4" cx="30" cy="30" r="28" stroke-width="4" fill="none" role="slider" aria-orientation="vertical" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50"></circle>
													</svg>
												</div>
											</div>
											<h3>Car Model</h3>
											<div class="step-block">
												<div class="form-group">
													<input type="radio" name="car-model" class="form-control" id="c-class" value="c-class">
													<label role="button" for="c-class">C-Class</label>
												</div>
												<div class="form-group">
													<input type="radio" name="car-model" class="form-control" id="a-class" value="a-class">
													<label role="button" for="a-class">A-Class</label>
												</div>
												<div class="form-group">
													<input type="radio" name="car-model" class="form-control" id="g-class" value="g-class">
													<label role="button" for="g-class">G-Class</label>
												</div>
												<div class="form-group">
													<input type="radio" name="car-model" class="form-control" id="s-class" value="s-class">
													<label role="button" for="s-class">S-Class</label>
												</div>
												<div class="form-group">
													<input type="radio" name="car-model" class="form-control" id="e-class" value="e-class">
													<label role="button" for="e-class">E-Class</label>
												</div>
												<div class="form-group">
													<input type="radio" name="car-model" class="form-control" id="gla" value="gla">
													<label role="button" for="gla">GLA</label>
												</div>
												<div class="form-group">
													<input type="radio" name="car-model" class="form-control" id="glc" value="glc">
													<label role="button" for="glc">GLC</label>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="slide-right-anim">
											<div class="test-progress">
												<div class="test-progress-step">
													<span class="step-number">5/6</span>
													<svg>
													<circle class="step-5" cx="30" cy="30" r="28" stroke-width="4" fill="none" role="slider" aria-orientation="vertical" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50"></circle>
													</svg>
												</div>
											</div>
											<h3>Select Service</h3>
											<div class="step-block">
												<div class="form-group">
													<input type="checkbox" name="car-service" class="form-control" id="interior" value="interior">
													<label role="button" for="interior">Interior</label>
												</div>
												<div class="form-group">
													<input type="checkbox" name="car-service" class="form-control" id="exterior" value="exterior">
													<label role="button" for="exterior">Exterior</label>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="slide-right-anim">
											<div class="test-progress">
												<div class="test-progress-step">
													<span class="step-number">6/6</span>
													<svg>
													<circle class="step-6" cx="30" cy="30" r="28" stroke-width="4" fill="none" role="slider" aria-orientation="vertical" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50"></circle>
													</svg>
												</div>
											</div>
											<h3>Select Service</h3>
											<div class="step-block">
												<div class="form-group">
													<input type="checkbox" name="select-service" class="form-control" id="conventional" value="conventional">
													<label role="button" for="conventional">Conventional</label>
												</div>
												<div class="form-group">
													<input type="checkbox" name="select-service" class="form-control" id="swissvax" value="swissvax">
													<label role="button" for="swissvax">Swissvax</label>
												</div>
												<div class="form-group">
													<input type="checkbox" name="select-service" class="form-control" id="cases" value="cases">
													<label role="button" for="cases">Special Cases</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection