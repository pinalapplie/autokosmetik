@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.update') }} {{ trans('cruds.servicemangement.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('servicemangement.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('servicemangement.store') }}" method="POST" enctype="multipart/form-data" id="add_form">
                    @csrf
                     <input type="hidden" name="id" value="{{encrypt($servicemanagement->id)}}">
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.servicemangement.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($servicemanagement->name) ? $servicemanagement->name : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">{{ trans('cruds.servicemangement.fields.description') }}</label>
                        <textarea name="description" class="form-control" id="description" rows="5">
                            {!! old('description', isset($servicemanagement->description) ? $servicemanagement->description : '') !!}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="category">{{ trans('cruds.servicemangement.fields.category') }}<span class="required_class">*</span></label>
                        <select class="form-control" name="service_category_id" id="category" disabled="">
                            <option value="">{{ trans('cruds.servicemangement.fields.select_category') }}</option>  
                            @foreach($service_category as $value)
                                <?php 
                                    $final_category_name='';
                                    if($value->parent_id == 0){
                                        $final_category_name = $value->name;
                                    }else {
                                        if($value->service_category_name->parent_id == 0){
                                            $category_name1=$value->service_category_name->name;
                                            $final_category_name = $value->name.'->'.$category_name1;    
                                        }else{
                                            $category_name1=$value->service_category_name->name;
                                            $category_name2=$value->service_category_name->service_category_name->name;
                                            $final_category_name = $value->name.'->'.$category_name1.'->'.$category_name2;
                                        }
                                    }
                                ?>
                                <option value="{{$value->id}}" @if($servicemanagement->service_category_id==$value->id) selected="" @endif>{{$final_category_name}}</option>  
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="category">{{ trans('cruds.servicemangement.fields.type') }}<span class="required_class">*</span></label>
                        <select class="form-control" name="type" disabled="">
                            <option value="">{{ trans('cruds.servicemangement.fields.select_type') }}</option>  
                            <option value="specification" @if($servicemanagement->type=="specification") selected="" @endif>{{ trans('cruds.servicemangement.fields.specification') }}</option>  
                            <option value="pricechart" @if($servicemanagement->type=="pricechart") selected="" @endif>{{ trans('cruds.servicemangement.fields.pricechart') }}</option>  
                        </select>
                    </div>
                    @if($servicemanagement->type=="pricechart")
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <input type="hidden" name="id" value="{{encrypt($servicemanagement->id)}}">
                                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                                        <thead>
                                        <tr>
                                            <th scope="col">{{ trans('cruds.servicemangement.fields.id') }}</th>
                                            <th scope="col">{{ trans('cruds.servicemangement.fields.service_category') }}</th>
                                            <th scope="col">{{ trans('cruds.servicemangement.fields.vehicle_condition') }}</th>
                                            @foreach($vehicle_category as $value)
                                                <th scope="col">{{$value->name}}</th>
                                            @endforeach    
                                        </tr>
                                        </thead>
                                        <tbody>
                                             
                                            @foreach($vehicle_service as $key => $value)
                                                <tr>
                                                    <input type="hidden" name="service_type[]" value="{{$value->id}}">
                                                    <th scope="row">{{$key+1}}</th>
                                                    <td>{{$value->name}}</td>
                                                    <td>
                                                    @foreach($vehicle_conditions as $key1 => $value1)
                                                            @php $uncheck = 0; @endphp
                                                            @if($servicemanagement->service_price && count($servicemanagement->service_price))
                                                               @foreach($servicemanagement->service_price as $service_price)
                                                                    @if($service_price->vehicle_service_id == $value->id && $service_price->vehicle_condition_id  == $value1->id)
                                                                         @php $uncheck = 1; @endphp
                                                                    @endif
                                                                @endforeach
                                                                <input type="radio" id="vehicle_condition_{{$key.$key1}}" name="vehicle_condition_{{$value->id}}" value="{{$value1->id}}"  @if($uncheck == 1) checked="" @elseif($uncheck==0 && $key1==0) checked="" @endif >

                                                            @else
                                                                 <input type="radio" id="vehicle_condition_{{$key.$key1}}" name="vehicle_condition_{{$value->id}}" value="{{$value1->id}}"  @if(empty($key1)) checked="" @endif >
                                                            @endif    
                                                            <label for="vehicle_condition_{{$key.$key1}}">{{$value1->name}}</label>
                                                    @endforeach
                                                    </td>
                                                    @php $temp = 0;@endphp
                                                    @foreach($vehicle_category as $value2)
                                                        <td>
                                                           @php $price = "";@endphp  
                                                           @if($servicemanagement->service_price && count($servicemanagement->service_price) && $servicemanagement->service_price[$temp]->vehicle_categorie_id  == $value2->id)
                                                                @foreach($servicemanagement->service_price as $service_price)
                                                                    @if($service_price->vehicle_service_id == $value->id && $service_price->vehicle_categorie_id  == $value2->id)
                                                                        @php $price = $service_price->price; @endphp
                                                                    @endif
                                                                @endforeach
                                                                <input type="number" name="vehicle_category_{{$value->id}}_{{$value2->id}}" placeholder="" class="form-control" required="" value="{{$price}}">
                                                            @else
                                                                <input type="number" name="vehicle_category_{{$value->id}}_{{$value2->id}}" placeholder="" class="form-control blanktemp" required="" value="">
                                                            @endif
                                                        </td>
                                                        @php $temp++;@endphp
                                                    @endforeach  
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="form-group">
                            <label for="">{{ trans('cruds.servicemangement.fields.price') }}</label>
                            <input type="number" class="form-control" id="name" name="price" value="{{ old('price', isset($servicemanagement->price) ? $servicemanagement->price : '') }}">
                        </div>
                    @endif
                    <button type="submit" class="btn btn-primary">{{ trans('global.submit') }}</button>
                </form>
            </div>
        </div>  
    </div>
</div>  
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        CKEDITOR.replace('description');
        //CKEDITOR.replace('short_description');
        $("#add_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
                // description: {
                //     required: function(textarea) {
                //         CKEDITOR.instances[textarea.id].updateElement();
                //         var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                //         return editorcontent.length === 0;
                //     }
                // },
                // service_category_id: {
                //     required: true
                // },
                // type: {
                //     required: true
                // }

            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                },
                // description: {
                //     required: "@lang('validation.required',['attribute'=>'Description'])",
                // },
                // service_category_id: {
                //     required: "@lang('validation.required',['attribute'=>'Category'])",
                // },
                // type: {
                //     required: "@lang('validation.required',['attribute'=>'Type'])",
                // }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection