x @section('title', 'Dienstleistungen')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<div id="main-content" class="active">
			<div id="section-breadcrumb1" class="inner-banner-wrap">
				<img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
				<div class="container">
					<div class="row">
						<div class="content col-12">
							<h1>SPEZIALFÄLLE</h1>
							<ul>
                                <li><a href="{{ route('index')}}">Zuhause</a></li>
								<li><a href="{{ route('services')}}">Dienstleistungen</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="section-services2" class="section-car-ceramic">
				<div class="container">
					<div class="row">
						<div class="left col-sm-12 col-md-8">
							<p>Bei diesen "speziellen Fällen" können wir keine Fixpreise machen, denn der jeweilige Reinigungsaufwand ist von Fall zu Fall sehr unterschiedlich. Entsprechend arbeiten wir nach dem Standardstundensatz von CHF 97.10 inkl. 10% für Material und inkl. Mwst. </p>
							<ul>
								<li class="circle">Hundehaare</li>
								<li class="circle">Geruchsbehandlungen</li>
								<li class="circle">Schimmel, usw.</li>
							</ul>
						</div>
						<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
                            <img class="img-fluid" src="{{ asset('assets/frontend/images/special-1.jpg') }}" alt="autokosmetik"> 
						</div>
                        <div class="col-md-12 pt-5">
                            <p>Ozon-Geruchsbehandlung: Voraussetzung für eine Ozon-Geruchsbehandlung ist die optimale Reinigung mit anschliessender Trocknung. Erst bei einem komplett trockenen Innenraum kann eine wirksame Ozonbehandlung durchgeführt werden. <br>Die Ozonbehandlung verrechnen wir mit CHF 30.-. Teilweise sind aber mehrere Ozonbehandlungen nötig.</p>
                        </div>
					</div>
				</div>
			</div>
			<div id="section-portfoliodetails1" class="ceramic-coting-images">
				<div class="container">
                    <div class="row">
                        <div class="related-projects col-12">
                            <h2 class="h5">Beispiele von extremen Verschmutzungen die unserer Spezialbehandlung bedürfen:</h2>
                            <div class="row">
                                <div class="item col-sm-12 col-md-4 my-3">
                                    <a href="#">
                                        <div class="img-container">
                                            <img class="img-fluid" src="{{ asset('assets/frontend/images/special-2.jpg') }}" alt="autokosmetik">
                                        </div>
                                    </a>
                                </div>
                                <div class="item col-sm-12 col-md-4 my-3">
                                    <a href="#">
                                        <div class="img-container">
                                            <img class="img-fluid" src="{{ asset('assets/frontend/images/special-3.jpg') }}" alt="autokosmetik">
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="related-projects col-12">
                            <h2 class="h5">Vorher-Nachher Gegenüberstellungen bei Extremfällen:</h2>
                            <div class="row">
                                <div class="item col-sm-12 col-md-4 my-3">
                                    <a href="#">
                                        <div class="img-container">
                                            <img class="img-fluid" src="{{ asset('assets/frontend/images/special-4.jpg') }}" alt="autokosmetik">
                                        </div>
                                    </a>
                                </div>
                                <div class="item col-sm-12 col-md-4 my-3">
                                    <a href="#">
                                        <div class="img-container">
                                            <img class="img-fluid" src="{{ asset('assets/frontend/images/special-5.png') }}" alt="autokosmetik">
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center pb-5">
                            <a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
@endsection    
	