<?php

namespace App\Http\Controllers;

use App\Http\Model\VehicleCategory;
use App\Http\Model\VehicleType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class VehicleCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicle_category = VehicleCategory::latest()->get();
        return view('backend.vehicle_category.index', compact('vehicle_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicle_type = VehicleType::get();
        return view('backend.vehicle_category.create',compact('vehicle_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                $vehicle_category = VehicleCategory::where('id',$id)->first();
                if (empty($vehicle_category->is_active)) {
                    $vehicle_category->is_active = 1;
                } else {
                    $vehicle_category->is_active = 0;
                }
                    $vehicle_category->save();
                return response()->json(['success' => 1]);
            }else{
                $this->validate($request, [
                    'name' => 'required',
                ]);
                if(!empty($request->id)){
                    $id = Crypt::decrypt($request->id);
                    $vehicle_category = VehicleCategory::where('id',$id)->first();
                    $message = trans('message.update', ['entity' => trans('cruds.vehiclecategories.title_singular')]);
                }else{
                    $vehicle_category = new VehicleCategory;
                    $message = trans('message.create', ['entity' => trans('cruds.vehiclecategories.title_singular')]);
                }

                    if ($request->hasFile('image')) {                        
                        $storage_path = 'public/';
                        $vehicle_category->image = Storage::disk('local')->put($storage_path, $request->file('image'));
                    }

                    $vehicle_category->name = $request->name;
                    $vehicle_category->vehicle_type_id = $request->vehicle_type_id;
                    $vehicle_category->save();
                    return redirect()->route('vehiclecategories.index')->with('success',$message);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
            return redirect()->back()->with('danger', trans('global.something_wrong'));    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VehicleCategory  $vehicleCategory
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleCategory $vehicleCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VehicleCategory  $vehicleCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $vehiclecategories = VehicleCategory::where('id',$id)->first();
        $vehicle_type = VehicleType::get();
        return view('backend.vehicle_category.edit', compact('vehiclecategories','vehicle_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VehicleCategory  $vehicleCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleCategory $vehicleCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VehicleCategory  $vehicleCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                VehicleCategory::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }
}
