@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.create') }} {{ trans('cruds.car_brand.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('carbrand.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('carbrand.store') }}" method="POST" enctype="multipart/form-data" id="add_car_type_form">
                    @csrf
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.car_brand.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($cartype->name) ? $cartype->name : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="car_type">{{ trans('cruds.car_brand.fields.car_type') }}<span class="required_class">*</span></label>
                        <select class="form-control" name="car_type" id="car_type">
                            <option value="">{{ trans('cruds.car_brand.fields.select_car_type') }}</option>  
                            @foreach($car_types as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>  
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="model">{{ trans('cruds.drive_car.fields.type') }}<span class="required_class">*</span></label><br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input radio_button" type="radio" name="vehicle_category" id="car_1" value="car" checked="">
                            <label class="form-check-label" for="car_1">{{ trans('cruds.drive_car.fields.car') }}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input radio_button" type="radio" name="vehicle_category" id="bike_1" value="bike">
                            <label class="form-check-label" for="bike_1">{{ trans('cruds.drive_car.fields.bike') }}</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        $("#add_car_type_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
                car_type: {
                    required: true
                }
            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                },
                car_type: {
                    required: "@lang('validation.required',['attribute'=>'Car Type'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection