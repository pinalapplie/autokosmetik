<?php

namespace App\Http\Controllers;

use App\CarType;
use Illuminate\Http\Request;

class CarTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $car_types = CarType::latest()->get();
        return view('car_type.index', compact('car_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('car_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $cartype = new CarType;
            $cartype->name = $request->name;
            $cartype->save();
            return redirect()->route('cartype.index')->with('success',trans('message.car_type.create'));
        }catch(\Exception $e){
            return redirect()->back()->with('danger', trans('global.something_wrong'));
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarType  $carType
     * @return \Illuminate\Http\Response
     */
    public function show(CarType $carType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarType  $cartype
     * @return \Illuminate\Http\Response
     */
    public function edit(CarType $cartype)
    {
        return view('car_type.edit', compact('cartype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarType  $cartype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CarType $cartype)
    {
        try{
            $cartype->name = $request->name;
            $cartype->save();
            return redirect()->route('cartype.index')->with('success',trans('message.car_type.update'));
        }catch(\Exception $e){
            return redirect()->back()->with('danger', trans('global.something_wrong'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarType  $carType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $cartype = CarType::find(request('ids'));
        $cartype->delete();
        return response()->noContent();
    }
    public function switchUpdate(Request $request)
    {
        $cartype = CarType::find($request->ids);
        if (empty($cartype->is_active)) {
            $cartype->is_active = 1;
        } else {
            $cartype->is_active = 0;
        }
        $cartype->save();
        return response()->noContent();
    }
}
