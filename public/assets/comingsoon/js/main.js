$(document).ready(function(){
    makeTimer();
    
    $(".btn-menu").on("click", function(){
        $("body").toggleClass("sidebar-open");
        if($("body").hasClass("sidebar-open")){
            $(this).html("<i class='fas fa-times text-white'></i>")
        }
        else{
            $(this).html("<i class='fas fa-bars text-white'></i>")
        }
        $('.overlay').toggleClass('expand');
    })
});

function makeTimer() {
    var endTime = new Date("31 March 2021 9:56:00 GMT+01:00");			
        endTime = (Date.parse(endTime) / 1000);

        var now = new Date();
        now = (Date.parse(now) / 1000);

        var timeLeft = endTime - now;

        var days = Math.floor(timeLeft / 86400); 
        var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
        var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
        var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

        if (hours < "10") { hours = "0" + hours; }
        if (minutes < "10") { minutes = "0" + minutes; }
        if (seconds < "10") { seconds = "0" + seconds; }

        $("#days").html(days + "<span>Days</span>");
        $("#hours").html(hours + "<span>Hours</span>");
        $("#minutes").html(minutes + "<span>Minutes</span>");
        $("#seconds").html(seconds + "<span>Seconds</span>");
    }
setInterval(function() { makeTimer(); }, 1000);