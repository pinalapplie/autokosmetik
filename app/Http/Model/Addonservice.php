<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Addonservice extends Model
{
	use SoftDeletes;  
   	protected $guarded = [];
    protected $table ="addonservice";
    protected $dates = ['deleted_at'];
}
