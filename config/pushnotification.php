<?php

return [
  'gcm' => [
    'priority' => 'normal',
    'dry_run' => false,
    'apiKey' => 'My_ApiKey',
  ],
  'fcm' => [
    'priority' => 'normal',
    'dry_run' => false,
    'apiKey' => 'AAAALkhou6Y:APA91bENDenRwWzaBMhbtGe2-fTEmNlwvOfRkgKzHl3xuVaV3TLbhrx2u0Kfjrty201zYNd-rzMuRXt4EoZ6Hr_fautRsTLhLHgSnuY4E2hsr9P0u9IEP3EibDyHAzjoHDPpXFqPJyxG',
  ],
  'apn' => [
    'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
    'passPhrase' => '1234', //Optional
    'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
    'dry_run' => true
  ]
];
