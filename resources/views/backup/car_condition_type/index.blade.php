@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>{{ trans('cruds.car_condition_type.title_singular') }} {{ trans('global.list') }}</h4>
            </div>

            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('carconditiontype.create') }}" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        {{ trans('global.add') }} {{ trans('cruds.car_condition_type.title_singular') }}
                    </a>
                </div>
            </div>
           
            <div class="col-md-12 mt-3">
               <div class="card">
                    <div class="card-body">
                        @include("partials.alert")
                        <div class="table-responsive">
                            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                    <tr>
                                        <th>{{ trans('cruds.car_brand.fields.id') }}</th>
                                        <th>{{ trans('cruds.car_brand.fields.name') }}</th>
                                        <th>{{ trans('global.status') }}</th>
                                        <th>{{ trans('global.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($car_condition_types as $key => $car_condition_type)
                                    <tr data-entry-id="{{ $car_condition_type->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $car_condition_type->name ?? ''}}</td>
                                        <td>
                                            @include('partials.switch', ['id'=>
                                            $car_condition_type->id,'is_active'=>$car_condition_type->is_active])
                                        </td>
                                        <td>@include('partials.actions', ['id' => $car_condition_type->id])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
