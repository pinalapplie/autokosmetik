<?php

namespace App\Http\Controllers;

use App\Http\Model\ServiceManagement;
use App\Http\Model\ServicePrice;
use App\Http\Model\VehicleCategory;
use App\Http\Model\ServiceCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use App\Http\Model\VehicleService;
use App\Http\Model\VehicleConditions;
use App\Http\Model\ServicePrices;

class ServiceManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service_management = ServiceManagement::latest()->get();
        return view('backend.service_management.index', compact('service_management'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicle_category = VehicleCategory::where('is_active', 1)->latest()->get();
        $vehicle_conditions = VehicleConditions::where('is_active', 1)->latest()->get();
        $service_category = ServiceCategory::where('parent_id', '!=',0)->get();
        return view('backend.service_management.create',compact('service_category', 'vehicle_category', 'vehicle_conditions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vehicle_category = VehicleCategory::where('is_active', 1)->latest()->get();
        $vehicle_conditions = VehicleConditions::where('is_active', 1)->latest()->get();
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                $service_management = ServiceManagement::where('id',$id)->first();
                if (empty($service_management->is_active)) {
                    $service_management->is_active = 1;
                } else {
                    $service_management->is_active = 0;
                }
                    $service_management->save();
                return response()->json(['success' => 1]);
            }else{
                $this->validate($request, [
                    'name' => 'required',
                ]);
                if(!empty($request->id)){
                    $id = Crypt::decrypt($request->id);
                    $service_management = ServiceManagement::where('id',$id)->first();
                    $service_management->name = $request->name;
                    $service_management->description = $request->description;
                    //$service_management->price = $request->price;
                    $service_management->save();

                    foreach($vehicle_category as $vehicleCategory){
                        foreach($vehicle_conditions as $vehicleConditions){
                            $dynamicPrice = strtolower($vehicleConditions->name).'_'.$vehicleCategory->id.'_'.$vehicleConditions->id;
                            $servicePriceId1 = strtolower($vehicleConditions->name).'1_'.$vehicleCategory->id.'_'.$vehicleConditions->id;
                            $servicePriceId = $request[$servicePriceId1];
                            
                            $servicePrice = ServicePrice::find($servicePriceId);
                            $servicePrice->service_management_id = $service_management->id;
                            $servicePrice->vehicle_categorie_id = $vehicleCategory->id;
                            $servicePrice->vehicle_condition_id = $vehicleConditions->id;
                            $servicePrice->price = $request[$dynamicPrice];
                            $servicePrice->save();
                        }
                    }
                    //$cartypes = CarType::where('is_active',1)->get();
                    /*$vehicle_category = VehicleCategory::get();
                    if($request->service_type && !empty($request->service_type)){
                        $input = $request->all();
                        foreach ($input['service_type'] as $key => $value) {
                            $radio_value = $input['vehicle_condition_'.$value];
                            foreach($vehicle_category as $value1){
                                $input_value = $input['vehicle_category_'.$value.'_'.$value1->id];
                                
                                $vehicle_service_id = $value;
                                $vehicle_condition_id = $input['vehicle_condition_'.$value];
                                $price=$input['vehicle_category_'.$value.'_'.$value1->id];
                                $vehicle_categorie_id = $value1->id;


                                $where_arr = ['service_management_id' => $id,'vehicle_service_id'=> $vehicle_service_id,'vehicle_categorie_id' => $vehicle_categorie_id];
                                $insert_arr = ['price' => $price,'vehicle_condition_id' => $vehicle_condition_id];
                                ServicePrice::updateOrCreate($where_arr, $insert_arr);

                            }
                        }
                    }*/
                    $message = trans('message.update', ['entity' => trans('cruds.servicemangement.title_singular')]);
                }else{
                    $service_management = new ServiceManagement;
                    $service_management->name = $request->name;
                    $service_management->service_category_id = $request->service_category_id;
                    //$service_management->type = $request->type;
                    $service_management->type = 'specification';
                    $service_management->description = $request->description;
                    $service_management->price = $request->price;
                    $service_management->save();
                    
                    foreach($vehicle_category as $vehicleCategory){
                        foreach($vehicle_conditions as $vehicleConditions){
                            $dynamicPrice = strtolower($vehicleConditions->name).'_'.$vehicleCategory->id.'_'.$vehicleConditions->id;
                            $servicePrice = new ServicePrice();
                            $servicePrice->service_management_id = $service_management->id;
                            $servicePrice->vehicle_categorie_id = $vehicleCategory->id;
                            $servicePrice->vehicle_condition_id = $vehicleConditions->id;
                            $servicePrice->price = $request[$dynamicPrice];
                            $servicePrice->save();
                        }
                    }
                    
                    $message = trans('message.create', ['entity' => trans('cruds.servicemangement.title_singular')]);
                }
                return redirect()->route('servicemangement.index')->with('success',$message);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
            return redirect()->back()->with('danger', $e->getMessage());    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceManagement  $serviceManagement
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceManagement $servicemanagement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceManagement  $serviceManagement
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
    {   
        $id = Crypt::decrypt($id);
        $servicemanagement = ServiceManagement::where('id',$id)->first();
        $service_category = ServiceCategory::get();
        $vehicle_service = VehicleService::where('service_management_id',$id)->get();
        $service_price = ServicePrice::where('service_management_id',$id)->get();
        $vehicle_category = VehicleCategory::where('is_active', 1)->latest()->get();
        $vehicle_conditions = VehicleConditions::where('is_active', 1)->latest()->get();
        
        //echo "<pre>"; print_r($service_price); exit();
        return view('backend.service_management.edit', compact('servicemanagement','service_category','vehicle_category','vehicle_service','vehicle_conditions','service_price'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceManagement  $serviceManagement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceManagement $serviceManagement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceManagement  $serviceManagement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                ServiceManagement::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }
}
