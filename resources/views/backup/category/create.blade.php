@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>Add Category</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('category.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('category.store') }}" method="POST" enctype="multipart/form-data" id="add_category_form">
                    @csrf
                    <div class="form-group">
                        <label for="title">Title<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="title" name="title">
                    </div>
                   <!--  <div class="form-group">
                        <label for="description">Image<span class="required_class">*</span></label>
                        <input type="file" name="image" class="form-control" id="image" accept="image/x-png,image/gif,image/jpeg">
                    </div> -->
                  <!--   <div class="form-group">
                        <label for="short_description">Short Description</label>
                        <textarea name="short_description" class="form-control" id="short_description" rows="2"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" class="form-control" id="description" rows="5"></textarea>
                    </div> -->
                    <div class="form-group">
                        <label for="category">Parent Category</label>
                        <select class="form-control" name="parent_id" id="category">
                            <option value="0">Select Category</option>  
                            @foreach($parent_categorys as $value)
                                <option value="{{$value->id}}">{{$value->title}}</option>  
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="model">{{ trans('cruds.drive_car.fields.type') }}<span class="required_class">*</span></label><br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input radio_button" type="radio" name="vehicle_category" id="car_1" value="car" checked="">
                            <label class="form-check-label" for="car_1">{{ trans('cruds.drive_car.fields.car') }}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input radio_button" type="radio" name="vehicle_category" id="bike_1" value="bike">
                            <label class="form-check-label" for="bike_1">{{ trans('cruds.drive_car.fields.bike') }}</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        CKEDITOR.replace('description');
        CKEDITOR.replace('short_description');
        $("#add_category_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                title: {
                    required: true
                },
                // image: {
                //     required: true
                // }
            },
            // Specify validation error messages
            messages: {
                title: {
                    required: "@lang('validation.required',['attribute'=>'Title'])",
                },
                image: {
                    required: "@lang('validation.required',['attribute'=>'Image'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection