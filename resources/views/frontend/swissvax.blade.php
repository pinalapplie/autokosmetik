@section('title', 'Dienstleistungen')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<div id="main-content" class="active">
			<div id="section-breadcrumb1" class="inner-banner-wrap">
				<img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
				<div class="container">
					<div class="row">
						<div class="content col-12">
							<h1>SWISSVAX</h1>
							<ul>
								<li><a href="{{ route('index')}}">Zuhause</a></li>
								<li><a href="{{ route('services')}}">Dienstleistungen</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="section-services2" class="section-car-ceramic">
				<div class="container">
					<div class="row">
						<div class="left col-sm-12 col-md-8">
							<p> In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. </p>
							<ul>
								<li class="circle">hochwertigste Reinigungsmittel</li>
								<li class="circle">optimale Langzeitpflege</li>
								<li class="circle">ausschliesslich Swissvax-Produkte</li>
							</ul>
							
						</div>
						<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
						</div>
						<div class="clearfix"></div>
						<p class="pt-5">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
					</div>
				</div>
			</div>
			<div class="section-services2 services2-car-bg-2">
				<div class="container">
					<div class="row align-items-center">
						<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
						</div>
						<div class="col-sm-12 col-md-7">
							<div class="ceramic-coating-pading">
								<h1>Swissvax-Saisonalitäten</h1>
								<ul>
									<li class="circle">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
									<li class="circle">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
									<li class="circle">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
								</ul>
								<p>
									Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
									Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
									Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="section-portfoliodetails1" class="ceramic-coting-images">
				<div class="container">
					<div class="row">
						<!-- Related Project -->
						<div class="related-projects col-12">
							<div class="row">
								<div class="related-projects col-12">
									<div class="row">
										<div class="item col-sm-12 col-md-4 my-3">
											<a href="#">
												<div class="img-container">
													<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
												</div>
											</a>
										</div>
										<div class="item col-sm-12 col-md-4 my-3">
											<a href="#">
												<div class="img-container">
													<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
												</div>
											</a>
										</div>
										<div class="item col-sm-12 col-md-4 my-3">
											<a href="#">
												<div class="img-container">
													<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="loadmore-btn col-12 text-center">
							<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection    
	