<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>Autokosmetik</title>
    <style>       
        * { box-sizing: border-box; -moz-box-sizing: border-box; }        
    </style>
</head>
<body style="background: #fff; margin: 0; padding: 0;font-family: 'Arial', cursive;font-size: 16px;color: #111111;">
    <table align="center" bgcolor="#fff" cellspacing="0" cellpadding="0" border="0" width="100%" style="max-width: 800px; margin: 30px auto;border: 6px solid #d0bb79;">
        <tr>
            <td style="padding: 15px;">
                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td valign="top">
                            <h1 style="font-size: 28px; font-weight: bold; margin-bottom: 0;text-transform: uppercase; margin-bottom: 0.5rem;">Rechnung:</h1>
                            <ul style="list-style: none;padding-left: 0;margin: 0;">
                                <li style="margin: 0 0 10px 0;">Autokosmetik </li>
                                <li style="margin-left: 0;">Messinastrasse 1,</li>
                                <li style="margin-left: 0;">9495 Triesen,</li>
                                <li style="margin: 0 0 10px 0;">Liechtenstein</li>
                                <li style="margin-left: 0;">
                                    <a style="color: #111111;text-decoration: none;" href="mailto:info@autokosmetik.li">info@autokosmetik.li</a>
                                </li>
                                <li style="margin-left: 0;">
                                    <a style="color: #111111;text-decoration: none;" href="www.autokosmetik.li">www.autokosmetik.li</a>
                                </li>
                            </ul>
                        </td>
                        <td align="right" valign="top">
                            <a href="#" target="_blank" style="color: #111111;text-decoration: none;"><img src="cid:logo_2u" alt="autokosmetik" style="max-height: 120px;"></a>
                        </td>
                        
                    </tr>
                    <tr>
                        <td height="10px"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><hr style="border: 2px dashed #d0bb79;"></td>
                        <td height="30px"></td>
                        
                    </tr>
                    <tr>
                        <td align="center" colspan="2" class="text-center"><h1 style="font-size: 20px; font-weight: bold; margin-bottom: 0;text-transform: uppercase; margin-bottom: 0.5rem;"> Welcome to Autokosmetik</h1></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <p>Hello name,</p>
                            <p>Thanks for using Autokosmetik. We're very exited to have you on board. We may need to communicate with you via email.</p>
                        </td>
                    </tr>
                   
                </table>
            </td>
        </tr>    
    </table>
    

</body>
</html> -->


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>Autokosmetik</title>
    <style>       
        * { box-sizing: border-box; -moz-box-sizing: border-box; }        
    </style>
</head>
<body style="background: url('../public/assets/frontend/images/inquiry-bg.png') no-repeat; margin: 0; padding: 0;font-family: 'Arial', cursive;font-size: 16px;color: #111111;">
    <table align="center" bgcolor="#fff" cellspacing="0" cellpadding="0" border="0" width="100%" style="max-width: 650px; margin: 30px auto; box-shadow: 1px 1px 20px 2px #00000014;">
        <tr>
            <td style="background: #7f141b;">
                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 25px 0; background: #fff;">
                    <tr>
                        <td align="center" valign="top" style="padding: 25px 0;">
                            <a href="#" target="_blank" style="color: #111111;text-decoration: none;"><img src="{{ asset('assets/frontend/images/logo-black.png') }}" alt="autokosmetik" style="max-height: 70px;"></a>
                        </td>
                        
                    </tr>
                    <!-- <tr>
                        <td height="10px"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><hr style="border: 2px dashed #d0bb79;"></td>
                        <td height="30px"></td>
                    </tr> -->
                    <tr>
                        <td align="center" colspan="2" class="text-center" style="padding: 70px; background: #F9F9F9;">
                            <h1 style="font-size: 20px; font-weight: 900;text-transform: capitalize; margin: 0; color: #7F141B;"> Herzlichen Glückwunsch </h1>
                            <h1 style="font-size: 18px; font-weight: normal; margin-bottom: 0; margin: 5px 0;">Sie haben eine neue Anfrage von name</h1>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding: 70px;">
                            <!-- <h1 style="font-size: 18px; font-weight: bold; margin-bottom: 0; margin-bottom: 0.5rem;">: <span style="font-size: 18px; font-weight: normal;">pinal@applieag.onmicrosoft.com</span></h1> -->
                            <h1 style="font-size: 18px; font-weight: bold;margin: 0;margin-bottom: 0.3rem;">E-Mail</h1>
                            <p style="margin:0;">pinal@applieag.onmicrosoft.com</p>
                            <hr style="border-color: #ffffff4d; margin: 20px 0;">
                            <h1 style="font-size: 18px; font-weight: bold;margin: 0;margin-bottom: 0.3rem;">Kontakt Nummer</h1>
                            <p style="margin:0;">123456789</p>
                            <hr style="border-color: #ffffff4d; margin: 20px 0;">
                            <h1 style="font-size: 18px; font-weight: bold;margin: 0;margin-bottom: 0.3rem;">Betreff</h1>
                            <p style="margin:0;">Test</p>
                            <hr style="border-color: #ffffff4d; margin: 20px 0;">
                            <h1 style="font-size: 18px; font-weight: bold;margin: 0;margin-bottom: 0.3rem;">Nachricht</h1>
                            <p style="margin:0;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam saepe sunt placeat, reiciendis beatae porro cupiditate adipisci alias, explicabo atque repellendus ipsam doloremque harum laborum. Provident ad similique maxime quidem?</p>
                            
                        </td>
                    </tr>
                </table>

                

            </td>
        </tr>    
    </table>


    <!-- <div class="page">
        <div class="subpage">Page 2/2</div>
    </div> -->

</body>
</html>