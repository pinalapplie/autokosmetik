<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarConditionType extends Model
{
    protected $table ="car_condition_types";
    protected $guarded = [];
}
