@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>Category Mgmt</h4>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('category.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add new category</a>
                </div>
            </div>
            <br>
            <div class="col-md-12  mt-3">
                @include("partials.alert")
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Title</th>
                                        <!-- <th>Image</th> -->
                                        <th>Parent Category</th>
                                        <th>{{ trans('cruds.drive_car.fields.type') }}</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($categorys as $key => $category)
                                    <tr data-entry-id="{{ $category->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $category->title ?? ''}}</td>
                                       <!--  <td>
                                            @if($category->image)
                                                <a href="{{ asset(Storage::url($category->image)) }}" target="_blank">
                                                    <img src="{{ asset(Storage::url($category->image)) }}" alt="" height=50 width=50>
                                                </a>
                                            @else
                                                '-'
                                            @endif
                                        </td> -->
                                        <td>{{ ($category->category_name) ? $category->category_name->title: '-'}}</td>
                                         <td>{{ $category->vehicle_category ? ucfirst($category->vehicle_category) : '-'}}</td>
                                        <td>
                                            @include('partials.switch', ['id'=>
                                            $category->id,'is_active'=>$category->is_active])
                                        </td>
                                        <td>@include('partials.actions', ['id' => $category->id])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
