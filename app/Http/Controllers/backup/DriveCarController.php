<?php

namespace App\Http\Controllers;

use App\DriveCar;
use App\CarBrand;
use Illuminate\Http\Request;

class DriveCarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drive_cars = DriveCar::with(['car_brand_name'])->latest()->get();
        //dd($drive_cars);
        return view('drive_car.index', compact('drive_cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $car_brands = CarBrand::where('is_active',1)->get();
        return view('drive_car.create',compact('car_brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $drivecar = new DriveCar;
            $drivecar->name = $request->name;
            $drivecar->car_brand = $request->car_brand;
           // $drivecar->vehicle_category = $request->vehicle_category;
            $drivecar->model = $request->model;
            //if($request->vehicle_category == "car"){
                $drivecar->number_of_window = $request->number_of_window;
            //}
            $drivecar->save();

            $this->updatewindow($drivecar->id);

            return redirect()->route('drivecar.index')->with('success',trans('message.drive_car.create'));
        }catch(\Exception $e){
            return redirect()->back()->with('danger', trans('global.something_wrong'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DriveCar  $driveCar
     * @return \Illuminate\Http\Response
     */
    public function show(DriveCar $driveCar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DriveCar  $driveCar
     * @return \Illuminate\Http\Response
     */
    public function edit(DriveCar $drivecar)
    {
        $car_brands = CarBrand::where('is_active',1)->get();
        $drivecar = DriveCar::with('car_brand_name')->where('id',$drivecar->id)->first();
        return view('drive_car.edit', compact('drivecar','car_brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DriveCar  $driveCar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DriveCar $drivecar)
    {
        try{
            $drivecarTemp = DriveCar::with('car_brand_name')->where('id',$drivecar->id)->first();

            $drivecar->name = $request->name;
            $drivecar->car_brand = $request->car_brand;
            //$drivecar->vehicle_category = $request->vehicle_category;
            $drivecar->model = $request->model;
            if($drivecarTemp->car_brand_name->vehicle_category == "car"){
                $drivecar->number_of_window = $request->number_of_window;
            }else{
                $drivecar->number_of_window=NULL;
            }
            $drivecar->save();

            $this->updatewindow($drivecar->id);
           

            return redirect()->route('drivecar.index')->with('success',trans('message.drive_car.update'));
        }catch(\Exception $e){
            return redirect()->back()->with('danger', trans('global.something_wrong'));
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DriveCar  $driveCar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $drivecar = DriveCar::find(request('ids'));
        $drivecar->delete();
        return response()->noContent();
    }
    public function switchUpdate(Request $request)
    {
        $drivecar = DriveCar::find($request->ids);
        if (empty($drivecar->is_active)) {
            $drivecar->is_active = 1;
        } else {
            $drivecar->is_active = 0;
        }
        $drivecar->save();
        return response()->noContent();
    }
    function updatewindow($drivecar_id){
        $drivecarTemp = DriveCar::with('car_brand_name')->where('id',$drivecar_id)->first();
        if($drivecarTemp->car_brand_name->vehicle_category == "bike"){
            $drivecarTemp->number_of_window=NULL;
            $drivecarTemp->save();
        }
    }
}
