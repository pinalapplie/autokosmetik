@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>{{ trans('cruds.drive_car.title_singular') }} {{ trans('global.list') }}</h4>
            </div>

            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('drivecar.create') }}" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        {{ trans('global.add') }} {{ trans('cruds.drive_car.title_singular') }}
                    </a>
                </div>
            </div>
           
            <div class="col-md-12 mt-3">
               <div class="card">
                    <div class="card-body">
                        @include("partials.alert")
                        <div class="table-responsive">
                            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                    <tr>
                                        <th>{{ trans('cruds.drive_car.fields.id') }}</th>
                                        <th>{{ trans('cruds.drive_car.fields.name') }}</th>
                                        <th>{{ trans('cruds.drive_car.fields.car_type') }}</th>
                                        <th>{{ trans('cruds.drive_car.fields.modal') }}</th>
                                        <th>{{ trans('cruds.drive_car.fields.type') }}</th>
                                        <th>{{ trans('cruds.drive_car.fields.window') }}</th>
                                        <th>{{ trans('global.status') }}</th>
                                        <th>{{ trans('global.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($drive_cars as $key => $drive_car)
                                    <tr data-entry-id="{{ $drive_car->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $drive_car->name ?? '-'}}</td>
                                        <td>{{ $drive_car->car_brand_name->name ?? '-'}}</td>
                                        <td>{{ $drive_car->model ?? '-'}}</td>
                                        <td>{{ $drive_car->car_brand_name ? ucfirst($drive_car->car_brand_name->vehicle_category) : '-'}}</td>
                                        <td>{{ $drive_car->number_of_window ?? '-'}}</td>
                                        <td>
                                            @include('partials.switch', ['id'=>
                                            $drive_car->id,'is_active'=>$drive_car->is_active])
                                        </td>
                                        <td>@include('partials.actions', ['id' => $drive_car->id])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
