@section('title', 'Dienstleistungen')
@extends('layouts.front_end')
@section('content')
	<div class="main-wrapper">
		<div id="main-content" class="active">
			<div id="section-breadcrumb1" class="inner-banner-wrap">				
				<img src="{{ asset('assets/frontend/images/services-banner.jpg') }}" alt="" class="inner-page-banner">
								
				<div class="container">
					<div class="row">
						<div class="content col-12">
							<h1>Unsere Leistungen</h1>
							<ul>
								<li><a href="{{ route('index')}}">ZUHAUSE</a></li>
								<li class="current text-light">UNSERE LEISTUNGEN</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="section-services1" class="service-accordian-main accordion">
				<div class="container-fluid">
					<div class="accordion-wrap" id="accordionExample">
						<div class="">
							<div class="d-flex align-items-center">
								<div class="item" data-order="1"></div>
								<div class="item" data-order="2"></div>
								<div class="item" data-order="3"></div>
							</div>
							<div class="d-flex align-items-center">
								<div class="item-img-clip item-img-clip1 mb-3">
									<div class="service-wrap img-clippath-anim" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
										<div class="service-title-wrap p-3 py-lg-4">
											<h4 class="text-light h2 mb-0">Pflege</h4>
										</div>
									</div>
								</div>
								<div class="item-img-clip item-img-clip2 mb-3">
									<div class="service-wrap img-clippath-anim collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
										<div class="service-title-wrap p-3 py-lg-4">
											<h4 class="text-light h2 mb-0">Schutz</h4>
										</div>
									</div>
								</div>
								<div class="item-img-clip item-img-clip3 mb-3">
									<div class="service-wrap img-clippath-anim collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
										<div class="service-title-wrap p-3 py-lg-4">
											<h4 class="text-light h2 mb-0">Design</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div id="collapse1" class="collapse mt-5 show" aria-labelledby="headingOne" data-parent="#accordionExample">
							<div class="container" id="service-category">
								<div class="row">
									<div class="col-md-12">
										<div class="service-items text-center mt-5">
											<img src="{{ asset('assets/frontend/images/paint-6.png') }}" alt="" class="img-fluid w-100 h-100 mb-3">
											<div class="service-item-text text-left">
												<p class="font-weight-bold h2 px-3 pt-3 pb-1 text-light">Aussenaufbereitung</p>
												<p class="px-3 h5 text-light">3 Items</p>
											</div>   
											<div class="service-items-button text-right w-100" data-toggle="collapse" data-target="#collapse1_a" aria-expanded="true" aria-controls="collapse1_a" role="button">
												<div class="h2 text-light">Weiterlesen</div>
											</div>
										</div>

										<div id="collapse1_a" class="collapse" aria-labelledby="heading4" data-parent="#service-category">
											<div class="row mt-5 py-5" id="service-category-2">
												<div class="col-md-4">
													<div class="category-collapse" data-toggle="collapse" data-target="#collapse_a1" aria-expanded="true" aria-controls="collapse_a1" role="button">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
														<h5 class="font-weight-bold h2 pt-3 m-0 text-uppercase">KERAMIKBESCHICHTUNG</h5>
														<p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...
															<span class="font-weight-bolder">Weiterlesen</span>
														</p>
													</div>
												</div>
												<div class="col-md-4 ">
													<div class="category-collapse" data-toggle="collapse" data-target="#collapse_a2" aria-expanded="true" aria-controls="collapse_a1" role="button">
														<img src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="" class="img-fluid">
														<h5 class="font-weight-bold h2 pt-3 m-0 text-uppercase">TEFLONVERSIEGELUNG</h5>
														<p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...
															<span class="font-weight-bolder">Weiterlesen</span>
														</p>
													</div>
												</div>
												<div class="col-md-4 ">
													<div class="category-collapse" data-toggle="collapse" data-target="#collapse_a3" aria-expanded="true" aria-controls="collapse_a1" role="button">
														<img src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="" class="img-fluid">
														<h5 class="font-weight-bold h2 pt-3 m-0 text-uppercase">SWISSVAX</h5>
														<p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...
															<span class="font-weight-bolder">Weiterlesen</span>
														</p>
													</div>
												</div>
											</div>
										</div>
										<div id="collapse_a1" class="collapse" aria-labelledby="heading4" data-parent="#service-category-2">
											<div id="section-services2" class="section-car-ceramic">
												<div class="container">
													<div class="row">
														<div class="left col-sm-12 col-md-8">
															<p>
															Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und ist absolut waschstrassenfest. Besonders geeignet ist unsere Keramikbeschichtung für dunkle, grosse Autos, welche im Alltagseinsatz stehen. Jede Keramikbeschichtung erhält ein Zertifikat, welches die Haltbarkeit von Minimum 3 Jahren garantiert.
															</p>
															<ul>
																<li class="circle">Der panzerglasartige Lackschutz</li>
																<li class="circle">Haltbarkeit 36+ Monate</li>
																<li class="circle">Waschstrassenfest</li>
															</ul>
														</div>
														<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
															<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="autokosmetik"> 
														</div>
													</div>
												</div>
											</div>
											<div class="services2-car-bg-2">
												<div class="container">
													<div class="row align-items-center">
														<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
															<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
														</div>
														<div class="col-sm-12 col-md-7">
															<div class="ceramic-coating-pading">
																<h1>Unser neues Keramik-Material ServFaces</h1>
																<p>In den ersten 3 Jahren unserer Arbeit mit sogenannten Keramikbeschichtungen, haben wir mit einem fernöstlichen Produkt gearbeitet. Bis wir feststellen mussten, dass sich das Material auf einmal anders verhält  und auch anders zu verarbeiten ist. Für unsere Firma ein absolutes "NO GO". <br><br> Wir konnten jedoch in kürzester Zeit eine Ersatz-Produktlinie aus deutscher Produktion finden und haben seit nunmehr 2 Jahren nur die besten Erfahrungen gemacht. Und so konnten wir unsere Garantie von 2 auf 3 Jahre erhöhen. Ab Anfang 2016 haben wir deshalb auch die Vertretung von ServFaces für die Schweiz und Deutschland übernommen.</p>
																<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen.</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div id="section-portfoliodetails1" class="ceramic-coting-images">
												<div class="container">
													<div class="row">
														<!-- Related Project -->
														<div class="related-projects col-12">
															<div class="row">
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-1.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-2.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-3.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-4.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-5.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-6.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-7.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-8.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/auto-9.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
															</div>
														</div>
														<div class="loadmore-btn col-12 text-center">
															<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="collapse_a2" class="collapse" aria-labelledby="heading4" data-parent="#service-category-2">
											<div id="section-services2" class="section-car-ceramic">
												<div class="container">
													<div class="row">
														<div class="left col-sm-12 col-md-8">
															<p>
															Eine Teflon-Versiegelung hält Minimum 6 Monate bis zu einem Jahr und genügt für die meisten Autos. Ausserdem ist sie sehr preiswert. Wir empfehlen jedoch bei der Teflon-Versiegelung Hand- oder Lanzenwäsche, da die Waschstrasse die Versiegelung zu schnell abreibt.
															</p>
															<ul>
																<li class="circle">Der Standard-Lackschutz</li>
																<li class="circle">Haltbarkeit 6+ Monate</li>
																<li class="circle">Preiswert</li>
															</ul>
														</div>
														<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
															<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-1.jpg') }}" alt="autokosmetik">
														</div>
													</div>
												</div>
											</div>
											<div class="section-services2 services2-car-bg-2">
												<div class="container">
													<div class="row align-items-center">
														<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
															<img class="img-fluid" src="{{ asset('assets/frontend/images/seal-2.jpg') }}" alt="autokosmetik">
														</div>
														<div class="col-sm-12 col-md-7">
															<div class="ceramic-coating-pading">
																<h1>Besonders geeignet für:</h1>
																<p>Anbei finden Sie die komplette Preisliste für die Aussen- und Innenaufbereitung inklusive detaillierter Beschreibung unserer Leistungen. <br> </p>
																<ul>
																	<li class="circle_2 m-0 h6">alle Autos, Motorräder, Wohnmobile, LKW's usw.</li>
																	<li class="circle_2 m-0 h6">Gebrauchtwagen</li>
																	<li class="circle_2 m-0 h6">Leasingrückgaben</li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div id="section-portfoliodetails1" class="ceramic-coting-images">
												<div class="container">
													<div class="row">
														<!-- Related Project -->
														<div class="related-projects col-12">
															<div class="row">
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-3.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-4.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-5.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-6.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-7.jpg') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-8.png') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-9.png') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-10.png') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
																<div class="item col-sm-12 col-md-4 my-3">
																	<a href="#">
																		<div class="img-container w-100">
																			<img class="img-fluid w-100" src="{{ asset('assets/frontend/images/seal-11.png') }}" alt="autokosmetik">
																		</div>
																	</a>
																</div>
															</div>
														</div>
														<div class="loadmore-btn col-12 text-center">
															<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="collapse_a3" class="collapse" aria-labelledby="heading4" data-parent="#service-category-2">
											<div id="section-services2" class="section-car-ceramic">
												<div class="container">
													<div class="row">
														<div class="left col-sm-12 col-md-8">
															<p> In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. </p>
															<ul>
																<li class="circle">hochwertigste Reinigungsmittel</li>
																<li class="circle">optimale Langzeitpflege</li>
																<li class="circle">ausschliesslich Swissvax-Produkte</li>
															</ul>
															
														</div>
														<div class="right ez-animate col-sm-12 col-md-4" data-animation="fadeInRight">
															<img class="img-fluid" src="{{ asset('assets/frontend/images/img-car-4.jpg') }}" alt="autokosmetik">
														</div>
														<div class="clearfix"></div>
														<p class="pt-5">In unserem Swissvax Car Care Center Liechtenstein/Rheintal können wir kaum mit Pauschalen arbeiten. Jeder Besitzer eines Autos, welches für die optimale Pflege mit der Swissvax-Methode vorgesehen ist, hat eigene Wünsche und Vorstellungen, was er genau an seinem „Liebling“ verbessert und perfektioniert haben möchte. <br><br>Entsprechend ist die vorgängige Begutachtung des Autos und das Kundengespräch die Basis für unser definitives Angebot. Die Begutachtung kann vor Ort beim Kunden oder aber auch bei uns in Gamprin erfolgen. Anschliessend erhält der Kunde ein schriftliches Angebot mit Definition der auszuführenden Arbeiten und dem verbindlichen Preis. Schlussendlich setzt sich der Preis aus dem Stundenaufwand, 15% Swissvax-Materialaufwand und 7.7% Mwst. zusammen.</p>
													</div>
												</div>
											</div>
											<div class="section-services2 services2-car-bg-2">
												<div class="container">
													<div class="row align-items-center">
														<div class="ez-animate col-sm-12 col-md-5 text-center" data-animation="fadeInLeft">
															<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
														</div>
														<div class="col-sm-12 col-md-7">
															<div class="ceramic-coating-pading">
																<h1>Swissvax-Saisonalitäten</h1>
																<ul>
																	<li class="circle">In der Hauptsaison April, Mai und Juni arbeiten wir mit einem Stundensatz inkl. Material und Mwst. von CHF 148.-</li>
																	<li class="circle">In der Zwischensaison März und Juli bis November gewähren wir einen Rabatt von 10% und arbeiten entsprechend mit einem Stundensatz von CHF 133.20.-</li>
																	<li class="circle">In der Nebensaison Dezember, Januar und Februar liegt der Rabatt bei 20%, entsprechend liegt der Stundensatz bei CHF 118.40</li>
																</ul>
																<p>
																	Bei einer Überschneidung der Saisonalitäten gilt der tiefere Stundenansatz. Selbstverständlich bieten wir auch einen Hol- und Bringservice an, welcher sich nach dem jeweiligen Aufwand berechnet. <br>
																	Hochglanz- und Schleifpolituren, Innenaufbereitungen, Lederreparaturen, Concoursaufbereitungen, usw. ausschliesslich gegen vorgängige Besichtigung und Offerte. <br>
																	Alle Preise für Arbeiten nach der Swissvax-Methode finden Sie hier: <a href="http://www.swissvax.li">www.swissvax.li</a>
																</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div id="section-portfoliodetails1" class="ceramic-coting-images">
												<div class="container">
													<div class="row">
														<!-- Related Project -->
														<div class="related-projects col-12">
															<div class="row">
																<div class="related-projects col-12">
																	<div class="row">
																		<div class="item col-sm-12 col-md-4 my-3">
																			<a href="#">
																				<div class="img-container">
																					<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-12.jpg') }}" alt="autokosmetik">
																				</div>
																			</a>
																		</div>
																		<div class="item col-sm-12 col-md-4 my-3">
																			<a href="#">
																				<div class="img-container">
																					<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-13.jpg') }}" alt="autokosmetik">
																				</div>
																			</a>
																		</div>
																		<div class="item col-sm-12 col-md-4 my-3">
																			<a href="#">
																				<div class="img-container">
																					<img class="img-fluid" src="{{ asset('assets/frontend/images/auto-14.jpg') }}" alt="autokosmetik">
																				</div>
																			</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="loadmore-btn col-12 text-center">
															<a href="{{ route('bookappointment')}}" class="btn-1">Einen Termin verabreden</a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="service-items text-center mt-5">
											<img src="{{ asset('assets/frontend/images/paint-6.png') }}" alt="" class="img-fluid w-100 h-100 mb-3">
											<div class="service-item-text text-left">
												<p class="font-weight-bold h2 px-3 pt-3 pb-1 text-light">Innenaufbereitung</p>
												<p class="px-3 h5 text-light">3 Items</p>
											</div>   
											<div class="service-items-button text-right w-100" data-toggle="collapse" data-target="#collapse1_b" aria-expanded="true" aria-controls="collapse1_a" role="button">
												<div class="h2 text-light">Weiterlesen</div>
											</div>
										</div>
										<div id="collapse1_b" class="collapse mt-5"  data-parent="#service-category">
											<div class="row">
												<div class="col-md-4 ">
													<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
													<h5 class="font-weight-bold h2 pt-3 m-0 text-uppercase">Herkömmlich</h5>
													<p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...<span class="font-weight-bolder">Weiterlesen</span></p>
												</div>
												<div class="col-md-4 ">
													<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
													<h5 class="font-weight-bold h2 pt-3 m-0 text-uppercase">Swissvax</h5>
													<p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...<span class="font-weight-bolder">Weiterlesen</span></p>
												</div>
												<div class="col-md-4 ">
													<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid">
													<h5 class="font-weight-bold h2 pt-3 m-0 text-uppercase">SPEZIALFÄLLE</h5>
													<p class="h6 py-2">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival...<span class="font-weight-bolder">Weiterlesen</span></p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="collapse2" class="collapse mt-5" aria-labelledby="headingTwo" data-parent="#accordionExample">
							<div class="container">
								<div class="row">
									<div class="col-md-4">
										<div class="list-group" id="list-tab" role="tablist">
										<a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#FOLIEN" role="tab" aria-controls="home">FOLIEN</a>
										<a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#BESCHICHTUNG" role="tab" aria-controls="profile">BESCHICHTUNG</a>
										</div>
									</div>
									<div class="col-md-8">
										<div class="tab-content" id="nav-tabContent">
											<div class="tab-pane fade show active" id="FOLIEN" role="tabpanel" aria-labelledby="list-home-list">
												<div class="row">
													<div class="col-md-3">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid img-thumbnail">
													</div>
													<div class="col-md-9 pl-md-0">
														<h5 class="h4 text-capitalize mb-1">LACK</h5>
														<p class="small mb-3">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival</p>
														<a href="{{route('bookappointment')}}" class="btn btn-sm btn-primary">jetzt buchen</a>   
														<a href="{{ route('ceramic.coating')}}" class="btn btn-sm btn-primary mr-2">Weiterlesen</a>
													</div>
												</div>
												<div class="row">
													<div class="col-md-3">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid img-thumbnail">
													</div>
													<div class="col-md-9 pl-md-0">
														<h5 class="h4 text-capitalize mb-1">SCHEIBEN</h5>
														<p class="small mb-3">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival</p>
														<a href="{{route('bookappointment')}}" class="btn btn-sm btn-primary">jetzt buchen</a>   
														<a href="{{ route('ceramic.coating')}}" class="btn btn-sm btn-primary mr-2">Weiterlesen</a>
													</div>
												</div>
											</div>
											<div class="tab-pane fade" id="BESCHICHTUNG" role="tabpanel" aria-labelledby="list-profile-list">
												<div class="row">
													<div class="col-md-3">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid img-thumbnail">
													</div>
													<div class="col-md-9 pl-md-0">
														<h5 class="h4 text-capitalize mb-1">KERAMIKBESCHICHTUNG</h5>
														<p class="small mb-3">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival</p>
														<a href="{{route('bookappointment')}}" class="btn btn-sm btn-primary">jetzt buchen</a>   
														<a href="{{ route('ceramic.coating')}}" class="btn btn-sm btn-primary mr-2">Weiterlesen</a>
													</div>
												</div>
												<div class="row">
													<div class="col-md-3">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid img-thumbnail">
													</div>
													<div class="col-md-9 pl-md-0">
														<h5 class="h4 text-capitalize mb-1">TEFLONBECHICHTUNG</h5>
														<p class="small mb-3">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival</p>
														<a href="{{route('bookappointment')}}" class="btn btn-sm btn-primary">jetzt buchen</a>   
														<a href="{{ route('ceramic.coating')}}" class="btn btn-sm btn-primary mr-2">Weiterlesen</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="collapse3" class="collapse mt-5" aria-labelledby="headingThree" data-parent="#accordionExample">
							<div class="container">
								<div class="row">
									<div class="col-md-4">
										<div class="list-group" id="list-tab" role="tablist">
											<a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#KOMPLETTFOLIERUNG" role="tab" aria-controls="home">KOMPLETTFOLIERUNG</a>
											<a class="list-group-item list-group-item-action" id="list-b-list" data-toggle="list" href="#TEILFOLIERUNG" role="tab" aria-controls="profile">TEILFOLIERUNG</a>
											<a class="list-group-item list-group-item-action" id="list-c-list" data-toggle="list" href="#FENSTERFOLIERUNG" role="tab" aria-controls="profile">FENSTERFOLIERUNG</a>
											<a class="list-group-item list-group-item-action" id="list-d-list" data-toggle="list" href="#INTERIEUR" role="tab" aria-controls="profile">INTERIEUR</a>
											<a class="list-group-item list-group-item-action" id="list-e-list" data-toggle="list" href="#BESCHRIFTUNG" role="tab" aria-controls="profile">BESCHRIFTUNG</a>
										</div>
									</div>
									<div class="col-md-8">
										<div class="tab-content" id="nav-tabContent">
											<div class="tab-pane fade show active" id="KOMPLETTFOLIERUNG" role="tabpanel" aria-labelledby="list-home-list">
												<div class="row">
													<div class="col-md-3">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid img-thumbnail">
													</div>
													<div class="col-md-9 pl-md-0">
														<h5 class="h4 text-capitalize mb-1">MONO </h5>
														<p class="small mb-3">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival</p>
														<a href="{{route('bookappointment')}}" class="btn btn-sm btn-primary">jetzt buchen</a>   
														<a href="{{ route('ceramic.coating')}}" class="btn btn-sm btn-primary mr-2">Weiterlesen</a>
													</div>
												</div>
												<div class="row">
													<div class="col-md-3">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid img-thumbnail">
													</div>
													<div class="col-md-9 pl-md-0">
														<h5 class="h4 text-capitalize mb-1">DESIGN</h5>
														<p class="small mb-3">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival</p>
														<a href="{{route('bookappointment')}}" class="btn btn-sm btn-primary">jetzt buchen</a>   
														<a href="{{ route('ceramic.coating')}}" class="btn btn-sm btn-primary mr-2">Weiterlesen</a>
													</div>
												</div>
											</div>
											<div class="tab-pane fade" id="TEILFOLIERUNG" role="tabpanel" aria-labelledby="list-b-list">
												<div class="row">
													<div class="col-md-3">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid img-thumbnail">
													</div>
													<div class="col-md-9 pl-md-0">
														<h5 class="h4 text-capitalize mb-1">MONO </h5>
														<p class="small mb-3">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival</p>
														<a href="{{route('bookappointment')}}" class="btn btn-sm btn-primary">jetzt buchen</a>   
														<a href="{{ route('ceramic.coating')}}" class="btn btn-sm btn-primary mr-2">Weiterlesen</a>
													</div>
												</div>
												<div class="row">
													<div class="col-md-3">
														<img src="{{ asset('assets/frontend/images/img-car-1.jpg') }}" alt="" class="img-fluid img-thumbnail">
													</div>
													<div class="col-md-9 pl-md-0">
														<h5 class="h4 text-capitalize mb-1">DESIGN</h5>
														<p class="small mb-3">Der gepanzerte glasartige Lackschutz, Haltbarkeit 36+ Monate, Autowaschfestival</p>
														<a href="{{route('bookappointment')}}" class="btn btn-sm btn-primary">jetzt buchen</a>   
														<a href="{{ route('ceramic.coating')}}" class="btn btn-sm btn-primary mr-2">Weiterlesen</a>
													</div>
												</div>
											</div>
											<div class="tab-pane fade show" id="FENSTERFOLIERUNG" role="tabpanel" aria-labelledby="list-c-list">...</div>
											<div class="tab-pane fade show" id="INTERIEUR" role="tabpanel" aria-labelledby="list-d-list">...</div>
											<div class="tab-pane fade show" id="BESCHRIFTUNG" role="tabpanel" aria-labelledby="list-e-list">...</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="section-services2" class="section-service-text-color">
				<div class="container">
					<div class="row align-items-center">
						<div class="left col-sm-12 col-md-6">
							<h1>AUSSENAUFBEREITUNG KERAMIKBESCHICHTUNG</h1>
							<p>Unser neuester Hit, eine Keramikbeschichtung hält Minimum 36 Monate und ist absolut waschstrassenfest. Besonders geeignet ist unsere Keramikbeschichtung für dunkle, grosse Autos, welche im Alltagseinsatz stehen. Jede Keramikbeschichtung erhält ein Zertifikat, welches die Haltbarkeit von Minimum 3 Jahren garantiert.</p>
						</div>
						<div class="right ez-animate col-sm-12 col-md-6" data-animation="fadeInRight">
							<img class="img-fluid w-75" src="{{ asset('assets/frontend/images/img-car-2.jpg') }}" alt="autokosmetik">
						</div>
					</div>
				</div>
			</div>
			<div class="section-services2 bg-2 bg-dark-autokosmetic">
				<div class="container">
					<div class="row">
						<div class="right ez-animate col-sm-12 col-md-6 text-center" data-animation="fadeInLeft">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/img-services2.png') }}" alt="autokosmetik">
						</div>
						<div class="left col-sm-12 col-md-6">
							<h1>INNENBEREITUNG</h1>
							<p class="text-light">Bei einer herkömmlichen Innenaufbereitung arbeiten wir mit einem neutralen, ökologischen Universalreiniger. Der ganze Innenraum inkl. Kofferraum wird gesaugt, Teppiche und Sitze werden shampooniert bzw. das Leder wird gereinigt und mit einer professionellen Ledermilch eingepflegt.</p>
							<p class="m-0 mb-3 text-light">INNENAUFBEREITUNG HERKÖMMLICH</p>
							<ul class="text-light">
								<li>inkl. shampoonieren</li>
								<li>Lederreinigung und -pflege</li>
								<li>Reinigung mit Tornador</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="section-services2 section-service-text-color">
				<div class="container">
					<div class="row">
						<div class="left col-sm-12 col-md-6">
							<h1 class="mw-100">FOLIENDESIGN</h1>
							<p>Besonders Individualisten wissen die vielfältigen Möglichkeiten von Autofolien zu schätzen. Dem Design und der individuellen Gestaltung sind fast keine Grenzen gesetzt. Gerne beraten wir diesbezüglich unsere anspruchsvollen Kunden. Sowohl betreffend die verwendeten Folien als auch in Bezug auf unsere Arbeit können Sie höchste Qualität erwarten. Bei allen Folien gilt eine Garantie von 7 Jahren.</p>
							<p class="mb-3">Besonders geeignet für:</p>
							<ul>
								<li>Individualisten</li>
								<li>Showfahrzeuge, die besonders auffallen wollen</li>
								<li>Fahrzeuge, welche für eine</li>
							</ul>
						</div>
						<div class="right ez-animate col-sm-12 col-md-6" data-animation="fadeInRight">
							<img class="img-fluid" src="{{ asset('assets/frontend/images/img-services3.png') }}" alt="autokosmetik">
						</div>
					</div>
				</div>
			</div>
			<div id="section-howwework">
				<div class="container">
					<div class="row">
						<div class="col-12 text-center">
							<i class="flaticon-multimedia"></i>
							<h1>How we <strong>Work</strong></h1>
						</div>
					</div>
				</div>
			</div>
			<div id="section-cta3">
				<div class="container">
					<div class="row ez-animate">
						<div class="title1 col-12" data-aos="fade-up" data-aos-delay="200">
							<h2><span>Immer auf der Suche nach mehr</span> Einzigartige Talente</h2>
							<i class="flaticon-download"></i>
						</div>
						<div class="cta-content col-12" data-aos="fade-up" data-aos-delay="400">
							<p>Alle Mitarbeiter der Autokosmetik.li Anstalt sind ausgebildete Autokosmetikerinnen, die seit Jahren in diesem Beruf tätig sind. Die Liebe zu perfekten, schönen Autos, einem sehr guten und geschulten Auge, einer guten körperlichen Verfassung und voll Konzentration bei der Arbeit vervollständigen das Bild der Mitarbeiter.</p>
							<a href="#" class="btn-4">Kaufe jetzt</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection    
	