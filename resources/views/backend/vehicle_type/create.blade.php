@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>  {{ trans('global.create') }} {{ trans('cruds.vehicletypes.title_singular') }}</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('vehicletypes.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                 @include("partials.alert")
                <form action="{{ route('vehicletypes.store') }}" method="POST" enctype="multipart/form-data" id="add_form">
                    @csrf
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.vehicletypes.fields.name') }}<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($vehicletype->name) ? $vehicletype->name : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="title">{{ trans('cruds.news.fields.image') }}<span class="required_class">*</span></label>
                        <input type="file" class="form-control" id="image" name="image" value="{{ old('image', isset($vehicletype->image) ? $vehicletype->image : '') }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        $("#add_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
            },
            image: {
                required: true
            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                },
                image: {
                    required: "@lang('validation.required',['attribute'=>'Image'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection