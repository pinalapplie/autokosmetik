@extends('layouts.admin')
@section('content')
<style type="text/css">
    .required_class{
        color: red;
    }
</style>
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>Add Service</h2>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('services.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                <form action="{{ route('services.store') }}" method="POST" enctype="multipart/form-data" id="add_service_form">
                    @csrf
                    <div class="form-group">
                        <label for="title">Title<span class="required_class">*</span></label>
                        <input type="text" class="form-control" id="title" name="title">
                    </div>
                    <div class="form-group">
                        <label for="description">Image<span class="required_class">*</span></label>
                        <input type="file" name="image" class="form-control" id="image" accept="image/x-png,image/gif,image/jpeg">
                    </div>
                    <div class="form-group">
                        <label for="short_description">Short Description<span class="required_class">*</span></label>
                        <textarea name="short_description" class="form-control" id="short_description" rows="2"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="description">Description<span class="required_class">*</span></label>
                        <textarea name="description" class="form-control" id="description" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="description">Portfolio Image</label>
                        <input type="file" name="portfolio" class="form-control" multiple accept="image/x-png,image/gif,image/jpeg">
                    </div>
                    <div class="form-group">
                        <label for="category">Type<span class="required_class">*</span></label>
                        <select class="form-control" name="type">
                            <option value="">Select Type</option>  
                            <option value="specification">Specification</option>  
                            <option value="pricechart" >Pricechart</option>  
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="category">Category<span class="required_class">*</span></label>
                        <select class="form-control" name="category" id="category">
                            <option value="">Select Category</option>  
                            @foreach($parent_categorys as $value)
                                <option value="{{$value->id}}">{{$value->title}}</option>  
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        //CKEDITOR.replace('description');
        // CKEDITOR.replace( 'description', {
        //     filebrowserUploadMethod: 'form'
        // });
        CKEDITOR.replace('description', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace('short_description', {
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });

        //CKEDITOR.replace('short_description');
        $("#add_service_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                title: {
                    required: true
                },
                short_description: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement();
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                        return editorcontent.length === 0;
                    }
                },
                description: {
                    required: function(textarea) {
                        CKEDITOR.instances[textarea.id].updateElement();
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                        return editorcontent.length === 0;
                    }
                },
                image: {
                    required: true
                },
                category: {
                    required: true
                },
                type: {
                    required: true
                }

            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Title'])",
                },
                description: {
                    required: "@lang('validation.required',['attribute'=>'Description'])",
                },
                short_description: {
                    required: "@lang('validation.required',['attribute'=>'Short Description'])",
                },
                image: {
                    required: "@lang('validation.required',['attribute'=>'Image'])",
                },
                category: {
                    required: "@lang('validation.required',['attribute'=>'Category'])",
                },
                type: {
                    required: "@lang('validation.required',['attribute'=>'Type'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection