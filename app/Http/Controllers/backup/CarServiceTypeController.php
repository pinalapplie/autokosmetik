<?php

namespace App\Http\Controllers;

use App\CarServiceType;
use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests\StoreCarServiceTypeRequest;
use App\Http\Requests\UpdateCarServiceTypeRequest;
use Illuminate\Support\Facades\Storage; 

class CarServiceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $car_service_types = CarServiceType::with(['car_service_type_name'])->latest()->get();
        return view('car_service_type.index', compact('car_service_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent_categorys = Category::get();
        return view('car_service_type.create',compact('parent_categorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $carservicetype = new CarServiceType;
        $carservicetype->title = $request->title;
        $carservicetype->category_id = $request->category_id;
        $carservicetype->save();
        return redirect()->route('carservicetype.index')->with('message', "Car Service Type Added Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarServiceType  $carServiceType
     * @return \Illuminate\Http\Response
     */
    public function show(CarServiceType $carServiceType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarServiceType  $carservicetype
     * @return \Illuminate\Http\Response
     */
    public function edit(CarServiceType $carservicetype)
    {
        $parent_categorys = Category::get();
        return view('car_service_type.edit', compact('carservicetype','parent_categorys'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarServiceType  $carservicetype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CarServiceType $carservicetype)
    {
        $carservicetype->title = $request->title;
        $carservicetype->category_id = $request->category_id;
        $carservicetype->save();
        return redirect()->route('carservicetype.index')->with('message', "Car Service Type updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarServiceType  $carServiceType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $categorys = CarServiceType::find(request('ids'));
        $categorys->delete();
        return response()->noContent();
    }
    public function switchUpdate(Request $request)
    {
        $categorys = CarServiceType::find($request->ids);
        if (empty($categorys->is_active)) {
            $categorys->is_active = 1;
        } else {
            $categorys->is_active = 0;
        }
        $categorys->save();
        return response()->noContent();
    }
}
